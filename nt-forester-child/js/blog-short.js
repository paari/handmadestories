/*global $, jQuery, alert*/
(function ($) {
    "use strict";

var postlayout = prefix.layoutmode;
if( !!postlayout ){postlayout = postlayout;}else{ postlayout = 'grid';}
//'grid', 'mosaic' or 'slider'

var postgrid = prefix.gridadjustment;
if( !!postgrid ){postgrid = postgrid;}else{ postgrid = 'responsive';}
//'default''alignCenter''responsive'


//for filter animation when click
var filteranimation = prefix.animationfilter;
if( !!filteranimation ){filteranimation = filteranimation;}else{ filteranimation = 'fadeOut';}
/* 'fadeOut''quicksand''bounceLeft''bounceTop''bounceBottom''moveLeft''slideLeft''fadeOutTop''sequentially''skew''slideDelay''3d' Flip'rotateSides''flipOutDelay''flipOut''unfold''foldLeft''scaleDown''scaleSides''frontRow''flipBottom''rotateRoom'  */

var horizontalgap = prefix.itemhorizontalgap;//default 10'
var horizontalgap = parseInt(horizontalgap);
if( !!horizontalgap ){horizontalgap = horizontalgap;}else{ horizontalgap = 30;}

var verticalgap = prefix.itemverticalgap;//default 10'
var verticalgap = parseInt(verticalgap);
if( !!verticalgap ){verticalgap = verticalgap;}else{ verticalgap = 30;}


    // Portfolio Boxed - 2 Columns
    jQuery('#blogtwo-boxed-col-2').cubeportfolio({
        filters: '.posts-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+postlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 2,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+postgrid+''
    });

    // Portfolio Boxed - 3 Columns
    jQuery('#blogtwo-boxed-col-3').cubeportfolio({
        filters: '.posts-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+postlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 3,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+postgrid+''
    });
	
    // Portfolio Boxed - 4 Columns
    jQuery('#blogtwo-boxed-col-4').cubeportfolio({
        filters: '.posts-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+postlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 4,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+postgrid+''
    });
	
    // Portfolio Boxed - 4 Columns
    jQuery('#blogtwo-boxed-col-5').cubeportfolio({
        filters: '.posts-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+postlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 5,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+postgrid+''
    });
	


}(jQuery));  