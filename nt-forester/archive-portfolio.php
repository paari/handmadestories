<?php

	get_header();
	get_template_part('index-header');

	$nt_forester_archive_hero_display 	= 	ot_get_option( 'nt_forester_archive_port_hero_display' );
	$nt_forester_archive_heading_display 	= 	ot_get_option( 'nt_forester_archive_port_heading_display' );
	$nt_forester_archive_heading 			= 	ot_get_option( 'nt_forester_archive_port_heading' );
	$nt_forester_archive_slogan_display 	= 	ot_get_option( 'nt_forester_archive_port_slogan_display' );
	$nt_forester_archive_slogan 			= 	ot_get_option( 'nt_forester_archive_port_slogan' );
	$nt_forester_bread_display  			= 	ot_get_option( 'nt_forester_bread_display' );
	$nt_forester_archive_layout 			= 	ot_get_option( 'nt_forester_archive_layout' );
	$nt_forester_archivepageheadbg 		= 	ot_get_option( 'nt_forester_archive_port_headbg' );
	$nt_forester_archivepageheadbg		= 	( $nt_forester_archivepageheadbg != '' ) ? $nt_forester_archivepageheadbg : get_theme_file_uri() . '/images/full_1.jpg';

	wp_enqueue_style( 'nt-forester-plugins' );
	wp_enqueue_style( 'nt-forester-custom-theme-style' );
	wp_enqueue_style( 'nt-forester-primary-color' );
	wp_enqueue_style( 'ion-icon');
	wp_enqueue_style( 'themify' );
	wp_enqueue_style( 'nt-forester-update-two' );

if ( $nt_forester_archive_hero_display != 'off' ) :
?>

	<!-- Start Hero Section -->
	<section id="hero" class="hero-fullwidth parallax archive-portfolio-section">
		<?php if( $nt_forester_archivepageheadbg != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_archivepageheadbg ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container white">
				<div class="m-auto">
					<div class="title-service mb15">

						<?php if( $nt_forester_archive_heading_display != 'off' ) : // PAGE TITLE
							if( $nt_forester_archive_heading != '' ) : ?>
								<h1 class="white uppercase lead-heading"><?php echo esc_html( $nt_forester_archive_heading ); ?></h1>
							<?php else : ?>
								<h1 class="white uppercase lead-heading"><?php the_archive_title(); ?></h1>
							<?php endif;
						endif;

						if( $nt_forester_archive_slogan_display != 'off' ) : // SUBTITLE
							if( $nt_forester_archive_slogan != '' ) : ?>
								<p class="heading-title cover-text-sublead"><?php echo esc_html( $nt_forester_archive_slogan ); ?></p>
							<?php else : ?>
								<p class="heading-title cover-text-sublead"><?php echo esc_html_e( 'Welcome to Our Archive','nt-forester' );?></p>
							<?php endif;
						endif;

						if( $nt_forester_bread_display != 'off' ) : //BREADCRUMB
							if( function_exists( 'bcn_display' ) ) : ?>
								<p class="breadcrubms"><?php bcn_display(); ?></p>
							<?php endif;
						endif; ?>

					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->
<?php endif;


	$layoutmode = 'grid' ;
	$gridadjustment = 'responsive';
	$animationcaption = 'fadeIn';
	$animationfilter = 'sequentially';
	$animationsingle = 'left';
	$itemhorizontalgap =  20;
	$itemverticalgap = 20;

	wp_enqueue_script('nt-forester-portfolio-short');
	wp_localize_script('nt-forester-portfolio-short', 'prefix',
	array(
		'layoutmode' 		=> $layoutmode,
		'gridadjustment' 	=> $gridadjustment,
		'animationcaption' 	=> $animationcaption,
		'animationfilter' 	=> $animationfilter,
		'animationpopup' 	=> $animationsingle,
		'itemhorizontalgap' => $itemhorizontalgap,
		'itemverticalgap' 	=> $itemverticalgap,
	));

	global $post;

	$nt_forester_ot_register_cpt1 = ot_get_option( 'nt_forester_cpt1' );

	$nt_forester_cpt_slug1 = ( $nt_forester_ot_register_cpt1 != '' ) ? strtolower( esc_html( $nt_forester_ot_register_cpt1 ) ) : 'portfolio';

	if ( post_type_exists( $nt_forester_cpt_slug1 ) ) {

		$nt_forester_port_query_arg = array(
			'post_type' 		=> ''.$nt_forester_cpt_slug1.'',
			'posts_per_page' 	=> 20,
			'order' 		 	=> 'DESC',
			'orderby' 		 	=> 'date',
			'post_status' 	 	=> 'publish'
		);


			$nt_forester_port_query 					= new WP_Query($nt_forester_port_query_arg);
			$nt_forester_loop_posts     				= $nt_forester_port_query->posts;
			$nt_forester_loop_posts_ids 				= wp_list_pluck( $nt_forester_loop_posts, 'ID' );
			$nt_forester_loop_term_objects			= wp_get_object_terms( $nt_forester_loop_posts_ids, ''.$nt_forester_cpt_slug1.'' );
			$nt_forester_loop_term_names_not_unique 	= wp_list_pluck( $nt_forester_loop_term_objects, 'name' );
			$nt_forester_loop_term_names_unique     	= array_unique( $nt_forester_loop_term_names_not_unique );
		?>
        <section id="portfolio" class="bg-white">
            <div class="container-fluid pt130 pb10 pl10 pr10">
                <div class="heading black text-center">
                    <div class="heading-title">
                        <?php echo esc_html_e( 'Our Portfolio', 'nt-forester' ); ?>
                    </div>
                    <h2><?php echo esc_html_e( 'Gallery', 'nt-forester' ); ?></h2>
                </div>
		<?php
			$terms = get_terms(''.$nt_forester_cpt_slug1.'');
			$count = count($terms);
				if ( $count > 0 ){
					?>
					<div class="portfolio-filter cbp-l-filters-button text-center">
						<div data-filter="*" class="cbp-filter-active cbp-filter-item"><?php echo esc_html_e( 'All', 'nt-forester' ); ?><div class="cbp-filter-counter"></div></div>
						<?php
							foreach ( $nt_forester_loop_term_names_unique as $term ) {
								$termfilter = strtolower( $term );
								$termfilter = str_replace(' ', '-', $termfilter);
								?>
								<div data-filter=".<?php echo ( $termfilter ); ?>" class="cbp-filter-item"><?php echo ( $term ); ?><div class="cbp-filter-counter"></div></div>
						<?php } ?>
					</div>
			<?php
				}
			?>

				<div id="portfolio-fullwidth-col-3" class="cbp">
		<?php

				if( $nt_forester_port_query->have_posts() ) :
				while ($nt_forester_port_query->have_posts()) : $nt_forester_port_query->the_post();

					$terms = get_the_terms( $post->ID, ''.$nt_forester_cpt_slug1.'' );
					if ( $terms && ! is_wp_error( $terms ) ) :
						$links = array();
						foreach ( $terms as $term ){
							$links[] = $term->name;
						}
						$links 	= str_replace(' ', '-', $links);
						$tax 	= join( " ", $links );
						$taxi 	= join( "  -  ", $links );
					else :
						$tax = '';
					endif;

					$linktype 	= get_post_meta( get_the_ID(), 'nt_forester_port_linktype', true );
					$vidurl 	= get_post_meta( get_the_ID(), 'nt_forester_port_vidurl', true );
					$gallery 	= get_post_meta( get_the_ID(), 'nt_forester_port_gallery_image', true );


				// portfolio image
				if ( has_post_thumbnail() ) {

						$nt_forester_portthumb 	 = get_post_thumbnail_id();
						$nt_forester_portthumbfull = wp_get_attachment_url( $nt_forester_portthumb,'full' );
					?>
						<div class="cbp-item <?php echo strtolower( $tax ); ?>">
					<?php if ( $linktype == 'single' ) { ?>
							<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>">
					<?php	}elseif( $linktype == 'single-popup' ){ ?>
							<a class="cbp-singlePage" href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>">
					<?php	}else{
							if ( $vidurl != '' ) { ?>
								<a href="<?php echo esc_url( $vidurl ); ?>" title="<?php echo get_the_title(); ?>" class="cbp-lightbox cbp-caption">
						<?php	}else{ ?>
								<a href="<?php echo esc_url( $nt_forester_portthumbfull ); ?>" title="<?php echo get_the_title(); ?>" class="cbp-lightbox cbp-caption">
						<?php	}
						}
						?>
								<div class="cbp-caption">

									<div class="cbp-caption-defaultWrap">
										<img src="<?php echo esc_url( $nt_forester_portthumbfull ); ?>" alt="<?php echo get_the_title(); ?>" />
									</div>
							<?php
									//post customstyle
									$activewrapbg_mb 		= get_post_meta( get_the_ID(), 'nt_forester_port_activewrapbg', true );
									$activewrapbgopacity_mb = get_post_meta( get_the_ID(), 'nt_forester_port_activewrapbgopacity', true );

									if( $activewrapbg_mb != '' ){
										$activewrapbgmb 	= nt_forester_hex2rgb($activewrapbg_mb);
										$activewrapbgrgb_mb = implode(", ", $activewrapbgmb);
										$activewrap_mbtotal =  ( $activewrapbgopacity_mb != '' ) ? ' style="background:rgba('.$activewrapbgrgb_mb.','.$activewrapbgopacity_mb.');"' : ' style="background:'.$activewrapbg_mb.';"';

										$activewrap_style = $activewrap_mbtotal;

									}else{
										$activewrap_style =  '';
									}

									//post category
									$port_iconcolor = get_post_meta( get_the_ID(), 'nt_forester_port_iconcolor', true );
									$porticoncolor = ( $port_iconcolor !='' ) ? ' style="color:'.$port_iconcolor.';"' : '';
								?>
									<div class="cbp-caption-activeWrap black"<?php echo( $activewrap_style ); ?>>
										<div class="wrap">
										<?php
											if ( $linktype == 'single' ) {  ?>

												<i class="ti-link x1"<?php echo( $porticoncolor ); ?>></i>
										<?php	}elseif ( $linktype == 'single-popup' ) {
												if ( !empty( $gallery ) ) { ?>
													<i class="ti-gallery x1"<?php echo( $porticoncolor ); ?>></i>
											<?php	}else{ ?>
													<i class="ti-fullscreen x1"<?php echo( $porticoncolor ); ?>></i>
											<?php	}
											}else{
												if ( $vidurl != '' ) {  ?>
													<i class="ti-video-camera x1"<?php echo( $porticoncolor ); ?>></i>
											<?php	}else{  ?>
													<i class="ti-fullscreen x1"<?php echo( $porticoncolor ); ?>></i>

											<?php	}
											}

											//post category
											$port_catcolor 	= get_post_meta( get_the_ID(), 'nt_forester_port_catcolor', true );
											$dtstyle = ( $port_catcolor !='' ) ? ' style="color:'.$port_catcolor.';"' : '';
											 ?>
											<h3 <?php echo( $dtstyle ); ?>><?php echo( $taxi ); ?></h3>
										<?php
											//post title
											$port_titlecolor = get_post_meta( get_the_ID(), 'nt_forester_port_titlecolor', true );
											$tstyle = ( $port_catcolor !='' ) ? ' style="color:'.$port_catcolor.';"' : '';
											 ?>
											<div class="heading-title"<?php echo( $tstyle ); ?>><?php echo get_the_title(); ?></div>

										</div>
									</div>
								</div>
							</a>
						</div>
		<?php	}

				endwhile;
				endif;
				wp_reset_postdata();
			?>
                </div>
            </div><!-- /.container -->
        </section>
        <!-- End Portfolio Section -->

<?php
	}
	get_footer();
?>
