<?php
/**
 * The default template for displaying Portfolio standart post format
 *
 * @package WordPress
 * @subpackage nt-kepler
 * @since nt-kepler 1.0
 */

/* SINGLE PORTFOLIO GENERAL SETTINGS */
$nt_forester_ot_register_cpt1 = ot_get_option( 'nt_forester_cpt1' );

$nt_forester_cpt_slug1 = ( $nt_forester_ot_register_cpt1 != '' ) ? strtolower( esc_html( $nt_forester_ot_register_cpt1 ) ) : 'portfolio';
 
/* for displaying post title */
$nt_forester_show_portfolio_all_detail 	= 	rwmb_meta( 'nt_forester_show_portfolio_all_meta' );
$nt_forester_portfolio_gallery_image 		= 	rwmb_meta( 'nt_forester_port_gallery_image' );
/* for displaying post title */
$nt_forester_show_post_title 				= 	rwmb_meta( 'nt_forester_show_post_title' );

/* for displaying before author text */
$nt_forester_show_author_name 			= 	rwmb_meta( 'nt_forester_show_author_name' );
$nt_forester_before_author_text 			= 	rwmb_meta( 'nt_forester_before_author_text' );
$nt_forester_before_author_text = ( $nt_forester_before_author_text != '' ) ? $nt_forester_before_author_text : '';

/* for displaying and control related post count */
$nt_forester_show_portfolio_related 		= 	rwmb_meta( 'nt_forester_show_portfolio_related');
$nt_forester_related_title 				= 	rwmb_meta( 'nt_forester_related_title');
$nt_forester_related_title = ( $nt_forester_related_title != '' ) ? esc_html( $nt_forester_related_title ) : 'RELATED POST';
$nt_forester_related_post_count 			= 	rwmb_meta( 'nt_forester_related_post_count');
$nt_forester_related_post_count = ( $nt_forester_related_post_count != '' ) ? esc_html( $nt_forester_related_post_count ) : 3;

/* for displaying buton and custom link */
$nt_forester_show_portfolio_custom_btn 	= 	rwmb_meta( 'nt_forester_show_portfolio_custom_btn' );
$nt_forester_portfolio_custom_btn_link 	= 	rwmb_meta( 'nt_forester_portfolio_custom_btn_link' );
$nt_forester_portfolio_custom_btn_target 	= 	rwmb_meta( 'nt_forester_portfolio_custom_btn_target' );
$nt_forester_portfolio_custom_btn_title 	= 	rwmb_meta( 'nt_forester_portfolio_custom_btn_title' );

$nt_forester_portfolio_custom_btn_title = ( $nt_forester_portfolio_custom_btn_title != '' ) ? $nt_forester_portfolio_custom_btn_title : 'OPEN PROJECT';
$nt_forester_portfolio_custom_btn_link = ( $nt_forester_portfolio_custom_btn_link != '' ) ? $nt_forester_portfolio_custom_btn_link : '0#';

/* END SINGLE PORTFOLIO GENERAL SETTINGS */


/* SINGLE PORTFOLIO META-DATA SETTINGS */

/* for displaying content */
$nt_forester_show_post_content 				= 	rwmb_meta( 'nt_forester_show_post_content' );

/* for displaying post client name */
$nt_forester_show_portfolio_client_meta 		= 	rwmb_meta( 'nt_forester_show_portfolio_client_meta' );
$nt_forester_portfolio_single_client_name 	= 	rwmb_meta( 'nt_forester_portfolio_single_client_name' );
$nt_forester_portfolio_client_custom_link 	= 	rwmb_meta( 'nt_forester_portfolio_client_link' );


$nt_forester_portfolio_client_custom_link 	= ( $nt_forester_portfolio_client_custom_link != '' ) ? $nt_forester_portfolio_client_custom_link : get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) ;

/* for displaying post categories */
$nt_forester_show_portfolio_date_meta 		= 	rwmb_meta( 'nt_forester_show_portfolio_date_meta' );

/* for displaying post date */
$nt_forester_show_portfolio_category_meta 	= 	rwmb_meta( 'nt_forester_show_portfolio_category_meta' );
$nt_forester_share 	= 	rwmb_meta( 'nt_forester_share' );

/* END SINGLE PORTFOLIO META-DATA SETTINGS */
	$nt_forester_thumb = get_post_thumbnail_id();
	$nt_forester_img_url = wp_get_attachment_url( $nt_forester_thumb,'full' );
	


	if ( $nt_forester_show_portfolio_all_detail != true ) : 
		// post title-->
		if ( $nt_forester_show_post_title != true ) : ?>
		<div class="cbp-l-project-title"><h1 class="white"><?php echo the_title();?></h1></div>
		<?php endif;

		// post author name-->
		if ( $nt_forester_show_author_name != true ) : ?>
		<div class="cbp-l-project-subtitle"><?php echo esc_html( $nt_forester_before_author_text ); ?> <?php the_author(); ?></div>
		<?php endif;
	endif; ?>

	<!--post slider media-->
	<?php if ( !empty( $nt_forester_portfolio_gallery_image ) ){ ?>
		<div class="cbp-slider">
			<ul class="cbp-slider-wrap">
				<?php
					foreach ( $nt_forester_portfolio_gallery_image as $image ) {
						if ( !empty( $image ) ){
							echo "<li class='cbp-slider-item'><img src='{$image['full_url']}' alt='{$image['alt']}' /></li>";
						}
					}
				
				?>
			</ul>
		</div>
	<?php }else{
	if ( $nt_forester_img_url !='' ) { ?>
		<!--post single media-->
		<div class="project-single-image">
			<img src="<?php echo esc_url($nt_forester_img_url); ?>" class="img-responsive" alt="<?php the_title_attribute(); ?>">
		</div>
	<?php }
	}


	if ( $nt_forester_show_post_content == true ) {
		$detail_class = 'disable-desc';
	}else{
		$detail_class = '';
	}
 
	if ( ( $nt_forester_show_portfolio_client_meta == true ) && ( $nt_forester_show_portfolio_date_meta == true ) && ( $nt_forester_show_portfolio_category_meta == true ) && ( $nt_forester_show_portfolio_custom_btn == true ) && ( $nt_forester_show_portfolio_custom_btn == true ) && ( $nt_forester_share == true ) ) {
		$desc_class = 'disable-meta';
	}else{
		$desc_class = '';
	}
	
	 if ( $nt_forester_show_portfolio_all_detail != true ) : ?>
		<!--post content-->
		<div class="cbp-l-project-container">
			<?php if ( $nt_forester_show_post_content != true ) : ?>
				<div class="cbp-l-project-desc <?php echo ( $desc_class ); ?>">
					<div class="cbp-l-project-desc-text">

						<?php
							/* translators: %s: Name of current post */
							the_content( sprintf(
								esc_html__( 'Continue reading %s', 'nt-forester' ),
								the_title( '<span class="screen-reader-text">', '</span>', false )
							) );
						?>

					</div>
				</div>
			<?php endif;

			if ( ( $nt_forester_show_portfolio_client_meta != true ) || ( $nt_forester_show_portfolio_date_meta != true ) || ( $nt_forester_show_portfolio_category_meta != true ) || ( $nt_forester_show_portfolio_custom_btn != true ) || ( $nt_forester_show_portfolio_custom_btn != true ) || ( $nt_forester_share != true ) ) : ?>
			<div class="cbp-l-project-details <?php echo( $detail_class ); ?>">
				<ul class="cbp-l-project-details-list">

					<!--post client-->
					<?php if ( $nt_forester_show_portfolio_client_meta != true ) : ?>
						<li><strong><?php echo esc_html_e( 'Client', 'nt-forester' ); ?></strong>
						<?php  if ( $nt_forester_portfolio_single_client_name !='' ) : ?>
						<a class="client-name" href="<?php echo esc_url( $nt_forester_portfolio_client_custom_link ); ?>"><?php echo esc_html( $nt_forester_portfolio_single_client_name ) ; ?></a>
						<?php endif; ?>
						</li>
					<?php endif;

					//<!--post date-->
					if ( $nt_forester_show_portfolio_date_meta != true ) :
						$archive_year  = get_the_time('Y');
						$archive_month = get_the_time('m');
						$archive_day   = get_the_time('d');
					?>
						<li><strong><?php echo esc_html_e( 'Date', 'nt-forester' ); ?></strong><a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day);?> "><?php echo get_the_date(); ?></a></li>
					<?php endif;

					//<!--post category-->
					if ( $nt_forester_show_portfolio_category_meta != true ) :
							// Get the ID of a given category
							$postType = get_post_type();
								$terms = get_the_terms( $post->ID, ''.$nt_forester_cpt_slug1.'' );
								if ( $terms && ! is_wp_error( $terms ) ) :
									$links = array();
									foreach ( $terms as $term ){
										$links[] = $term->name;
									}
									$links 	= str_replace(' ', '-', $links);
									$tax 	= join( " ", $links );
									$taxi 	= join( "  -  ", $links );
								else :
									$tax = '';
								endif;
						?>
						<li><strong><?php echo esc_html_e( 'Categories', 'nt-forester' ); ?></strong><?php foreach ( $terms as $term ){ ?><a href="<?php echo get_post_type_archive_link( ''.$nt_forester_cpt_slug1.'' ); ?>"><?php echo esc_html( $term->name );?></a> <?php } ?></li>
					<?php endif;

						$nt_forester_share_face		=   rwmb_meta( 'nt_forester_share_face' );
						$nt_forester_share_twitter	=   rwmb_meta( 'nt_forester_share_twitter' );
						$nt_forester_share_gplus		=   rwmb_meta( 'nt_forester_share_gplus' );
						$nt_forester_share_pinterest	=   rwmb_meta( 'nt_forester_share_pinterest' );

					if( $nt_forester_share != true ) : 
					?>
						<li class="share-list"><strong><?php esc_html_e( 'Share' , 'nt-forester' ); ?></strong><?php if( $nt_forester_share_face == true ) : ?><a class="share" href="http://www.facebook.com/sharer.php?u=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="ti-facebook"></i></a>
							<?php endif;
							if( $nt_forester_share_twitter == true ) : ?>
								<a class="share" href="http://twitter.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="ti-twitter"></i></a>
							<?php endif;
							if( $nt_forester_share_gplus == true ) : ?>
								<a class="share" href="https://plus.google.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="ti-google"></i></a>
							<?php endif;
							if( $nt_forester_share_pinterest == true ) : ?>
								<a class="share" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="ti-pinterest"></i></a>
							<?php endif; ?>
						</li>
					<?php endif; ?>
				</ul>

				<!--post custom button-->
				<?php  if ( $nt_forester_show_portfolio_custom_btn != true ) : ?>
					<a href="<?php echo esc_url( $nt_forester_portfolio_custom_btn_link ); ?>" target="<?php echo esc_attr( $nt_forester_portfolio_custom_btn_target ); ?>" class="btn btn-sm btn-primary text-center"><?php echo esc_html( $nt_forester_portfolio_custom_btn_title ); ?></a>
				<?php endif; ?>

			</div>
			<?php endif; ?>
			<!--/post details-->
		</div>
	<?php endif;

	// RELATED POST SECTION
	if ( $nt_forester_show_portfolio_related != true ) :

				// get the custom post type's taxonomy terms

				$nt_forester_custom_taxterms = wp_get_object_terms( $post->ID, ''.$nt_forester_cpt_slug1.'', array('fields' => 'ids') );
				$nt_forester_related_perpage = get_post_meta( get_the_ID(), 'nt_forester_related_post_count', true );
				// arguments
				$nt_forester_related_args = array(
				'post_type' 		=> ''.$nt_forester_cpt_slug1.'',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> $nt_forester_related_perpage, // you may edit this number
				'orderby' 			=> 'rand',
				'tax_query' 		=> array(
						array(
							'taxonomy' => ''.$nt_forester_cpt_slug1.'',
							'field' => 'id',
							'terms' => $nt_forester_custom_taxterms
						)
				),
				'post__not_in' => array ($post->ID),
				);
				$nt_forester_port_related = new WP_Query( $nt_forester_related_args );
				// loop over query
				if ($nt_forester_port_related->have_posts()) :
	echo'<div class="cbp-l-project-container">
			<div class="cbp-l-project-related">
				<div class="cbp-l-project-desc-title"><span>'.esc_html( $nt_forester_related_title ).'</span></div>
				<ul class="cbp-l-project-related-wrap">';
				while ( $nt_forester_port_related->have_posts() ) : $nt_forester_port_related->the_post();

				$nt_forester_related_thumb = get_post_thumbnail_id();
				$nt_forester_related_img_src = wp_get_attachment_url( $nt_forester_related_thumb,'full' );
				?>
					<li class="cbp-l-project-related-item">
						<a href="<?php echo get_the_permalink();?>" class="cbp-singlePage cbp-l-project-related-link" rel="nofollow">
							<img src="<?php echo esc_url($nt_forester_related_img_src); ?>" alt="<?php the_title_attribute(); ?>">
							<div class="cbp-l-project-related-title"><?php echo substr( get_the_title(), 0,50 ); ?></div>
						</a>
					</li>
				<?php
				endwhile;
			echo'</ul>
			</div>
		</div>';
		endif;
		// Reset Post Data
		wp_reset_postdata();
	 endif; ?>
<br><br><br>