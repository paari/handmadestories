
			<div class="col-md-7 col-sm-7">
				
				<?php 
				$nt_forester_images			=   rwmb_meta( 'nt_forester_images' );
				?>

				<?php 
				$nt_forester_image_id 	=	get_post_thumbnail_id();
				$nt_forester_image_url	=	wp_get_attachment_image_src($nt_forester_image_id, true);
				?>
				<img src="<?php echo esc_url( $nt_forester_image_url[0] ); ?>" class="img-responsive">

				<?php 
				$nt_forester_embed = rwmb_meta( 'nt_forester_video_embed' ); 
				if( $nt_forester_embed!='' ) : 
				?>

				<div class="media-element video-responsive"><?php echo ($nt_forester_embed); ?></div>

				<?php else : ?>

				<?php
				$nt_forester_m4v 			=	rwmb_meta( 'nt_forester_video_m4v' );
				$nt_forester_ogv 			=	rwmb_meta( 'nt_forester_video_ogv' );
				$nt_forester_webm 		=	rwmb_meta( 'nt_forester_video_webm' );
				$nt_forester_image_id 	=	get_post_thumbnail_id();
				$nt_forester_image_url	=	wp_get_attachment_image_src($nt_forester_image_id, true);
				$nt_forester_wp_video 	=	'[video poster="'.$nt_forester_image_url[0].'" mp4="'.$nt_forester_m4v.'"  webm="'.$nt_forester_webm.'"]';
				?>

				<div class="post-thumb"><?php echo do_shortcode ($nt_forester_wp_video); ?></div>
				<?php endif; ?>

				<?php
				$nt_forester_sc_color = rwmb_meta( 'nt_forester_audio_sc_color' );
				$nt_forester_sc_url 	= rwmb_meta( 'nt_forester_audio_sc' );
				$nt_forester_soundcloud_audio = '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='.urlencode( $nt_forester_sc_url ).'&amp;show_comments=true&amp;auto_play=false&amp;color='.$nt_forester_sc_color.'"></iframe>';
				?>

				<?php if($nt_forester_sc_url!='') : ?>
					<div class="post-thumb blog-bg"><?php echo ( $nt_forester_soundcloud_audio ); ?></div>
				<?php endif; ?>
					
			</div>
            <div class="col-md-4 col-md-push-1 col-sm-4 col-sm-push-1">

					<ul class="entry-meta portfolio">
						<li><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_time('F j, Y'); ?></a></li>
						<li><?php the_author(); ?></li>
					</ul>
					
					<?php the_content(); ?>

				<?php
					$nt_forester_share_face		=   rwmb_meta( 'nt_forester_share_face' );
					$nt_forester_share_twitter	=   rwmb_meta( 'nt_forester_share_twitter' );
					$nt_forester_share_gplus		=   rwmb_meta( 'nt_forester_share_gplus' );
					$nt_forester_share_pinterest	=   rwmb_meta( 'nt_forester_share_pinterest' );

				if( ($nt_forester_share_face == true ) || ( $nt_forester_share_twitter == true ) || ( $nt_forester_share_gplus == true ) || ( $nt_forester_share_pinterest == true ) ) : 
				?>
                <div class="fh5co-share">
                    <h3><?php esc_html_e( 'Share' , 'nt-forester' ); ?></h3>
                    <ul>
						<?php if( $nt_forester_share_face == true ) : ?>
							<li class="share-list"><a class="btn btn-sm btn-primary text-center" href="http://www.facebook.com/sharer.php?u=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><?php esc_html_e( 'Facebook' , 'nt-forester' ); ?></a></li>
						<?php endif; ?>
						<?php if( $nt_forester_share_twitter == true ) : ?>
							<li class="share-list"><a class="btn btn-sm btn-primary text-center" href="http://twitter.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><?php esc_html_e( 'Twitter' , 'nt-forester' ); ?></a></li>
						<?php endif; ?>
						<?php if( $nt_forester_share_gplus == true ) : ?>
							<li class="share-list"><a class="btn btn-sm btn-primary text-center" href="https://plus.google.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><?php esc_html_e( 'Google Plus' , 'nt-forester' ); ?></a></li>
						<?php endif; ?>
						<?php if( $nt_forester_share_pinterest == true ) : ?>
							<li class="share-list"><a class="btn btn-sm btn-primary text-center" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><?php esc_html_e( 'Pinterest' , 'nt-forester' ); ?></a></li>
						<?php endif; ?>
                    </ul>
                </div>
				<?php endif; ?>

            </div>
