

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="blog-post-list blog-post-single">
      <div class="blog-post-description">
        <div class="blog-post-category">
            <span><?php esc_html_e('in', 'nt-forester'); ?> <span class="subheading dark"><?php the_category(', '); ?></span></span>
        </div>
        <div class="blog-post-title">
            <?php
              if ( ! is_single() ) :
                 the_title( sprintf( '<h3 class="h3-lg"><a class="dark" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</h3></a>' );
              endif;
           ?>
        </div>
     </div>
		<div class="blog-bg">
				<?php
					wp_enqueue_style( 'nt-forester-custom-flexslider');
					wp_enqueue_script( 'nt-forester-custom-flexslider');
					wp_enqueue_script( 'fitvids');
					wp_enqueue_script( 'nt-forester-blog-settings');
					$nt_forester_images = rwmb_meta( 'nt_forester_gallery_image', 'type=image_advanced' );
					if( $nt_forester_images != '' ) :
				?>
					<div class="flexslider">
						<ul class="slides">
							<?php
								foreach ( $nt_forester_images as $image ) {
									echo "<li><img src='{$image['full_url']}' alt='{$image['alt']}' /></li>";
								}
							?>
						</ul>
					</div>
				<?php endif; ?>
		</div><!-- Ends Post Media -->



		<?php do_action('nt_forester_formats_content_action'); ?>

	</article><!-- #post-## -->
