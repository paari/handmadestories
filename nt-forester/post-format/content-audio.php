<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="blog-post-list blog-post-single">
      <div class="blog-post-description">
        <div class="blog-post-category">
            <span><?php esc_html_e('in', 'nt-forester'); ?> <span class="subheading dark"><?php the_category(', '); ?></span></span>
        </div>
        <div class="blog-post-title">
            <?php
              if ( ! is_single() ) :
                 the_title( sprintf( '<h3 class="h3-lg"><a class="dark" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</h3></a>' );
              endif;
           ?>
        </div>
     </div>

    <?php
		$nt_forester_mp3 		= rwmb_meta( 'nt_forester_audio_mp3' );
		$nt_forester_oga 		= rwmb_meta( 'nt_forester_audio_ogg' );
		$nt_forester_sc_url 	= rwmb_meta( 'nt_forester_audio_sc' );
		$nt_forester_sc_color = rwmb_meta( 'nt_forester_audio_sc_color' );
		$nt_forester_wp_audio = '[audio mp3="'.$nt_forester_mp3.'"  ogg="'.$nt_forester_oga.'"]';
		$nt_forester_soundcloud_audio = '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='.urlencode( $nt_forester_sc_url ).'&amp;show_comments=true&amp;auto_play=false&amp;color='.$nt_forester_sc_color.'"></iframe>';
	?>

	<?php if($nt_forester_sc_url!='') : ?>
		<div class="post-thumb blog-bg"><?php echo ( $nt_forester_soundcloud_audio ); ?></div>
	<?php else : ?>
		<div class="post-thumb blog-bg">
			<?php if(has_post_thumbnail()) : the_post_thumbnail(); endif; ?>
			<?php echo do_shortcode ( $nt_forester_wp_audio ); ?>
		</div>
	<?php endif; ?>

	<?php do_action('nt_forester_formats_content_action'); ?>

</article><!-- #post-## -->
