<?php
/**
 * The template for displaying posts in the Quote post format.
 *
 * @package WordPress
 * @subpackage nt_forester_
 * @since nt_forester_ 1.0
 */

	$terms = get_the_category();
	if ( $terms && ! is_wp_error( $terms ) ) :
		$links = array();
		foreach ( $terms as $term ){
			$links[] = $term->name;
		}
		$links 	= str_replace(' ', '-', $links);
		$tax 	= join( " ", $links );
		$taxi 	= join( " , ", $links );
	else :
		$tax = '';
	endif;

	$nt_forester_quote_text 		=	rwmb_meta( 'nt_forester_quote_text' );
	$nt_forester_quote_author 	=	rwmb_meta( 'nt_forester_quote_author' );
	$nt_forester_color 			=	rwmb_meta( 'nt_forester_quote_bg' );
	$nt_forester_opacity 			=	rwmb_meta( 'nt_forester_quote_bg_opacity' );
	$nt_forester_opacity 			=	$nt_forester_opacity / 100;

	$nt_forester_masonry_layoutmode 	= 	ot_get_option( 'nt_forester_masonry_layoutmode' );

	$nt_forester_thumb 	= get_post_thumbnail_id();
	$nt_forester_postthumb = wp_get_attachment_url( $nt_forester_thumb,'full' );

?>

	<div id="post-<?php the_ID(); ?>" class="cbp-item <?php echo strtolower($tax); ?>">
		<div class="blog-article">
			<div class="blog-article-head text-center">
				<div class="blog-article-category">
					<h6 class="inline"><?php echo esc_html_e( 'In', 'nt-forester' ); ?></h6> <h4 class="inline black"><?php the_category(', '); ?></h4>
				</div>
				<div class="blog-article-title black">
					<h3><?php echo esc_attr( $nt_forester_quote_text ); ?></h3>
				</div>
				<div class="blog-article-date">
					<h4><?php echo esc_attr( $nt_forester_quote_author ); ?></h4>
					<h6><?php echo esc_html_e( 'Posted on', 'nt-forester' ); ?> <?php echo get_the_date(); ?></h6>
				</div>
			</div>
			<a href="<?php echo esc_url( get_permalink() ); ?>">
				<?php if(has_post_thumbnail()) : ?>
					<div class="blog-article-image">
						<img src="<?php echo esc_url( $nt_forester_postthumb ); ?>" alt="<?php echo the_title(); ?>" class="pb10">
					</div>
				<?php endif; ?>
			</a>
			<h6><?php

				if ( has_excerpt() ) :
					the_excerpt();
				else :

					$nt_forester_masonry_content_limit= 	ot_get_option( 'nt_forester_masonry_content_limit' );
					$nt_forester_masonry_contentlimit = ( $nt_forester_masonry_content_limit != '' ) ? $nt_forester_masonry_content_limit : 300;

					$nt_forester_masonry_content = get_the_content();
					if(is_numeric($nt_forester_masonry_contentlimit) && $nt_forester_masonry_contentlimit > 0 && $nt_forester_masonry_contentlimit == round($nt_forester_masonry_contentlimit, 0)){
						echo ''.substr( $nt_forester_masonry_content, 0, $nt_forester_masonry_contentlimit ).'...';
					}else{
						echo ''.substr( $nt_forester_masonry_content, 0, 300 ).'...';
					}

				endif ;

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nt-forester' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'nt-forester' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?></h6>
			<div class="read-more">
				<a href="<?php echo esc_url( get_permalink() ); ?>"><h4 class="black"><?php echo esc_html_e( 'View Post', 'nt-forester' ); ?><i class="fa fa-angle-right ml10"><i class="fa fa-angle-right"></i></i></h4></a>
			</div>
		</div>
	</div>
