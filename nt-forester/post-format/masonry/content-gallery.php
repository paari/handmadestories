<?php // POST FORMAT GALLERY

	$terms = get_the_category();
	if ( $terms && ! is_wp_error( $terms ) ) :
		$links = array();
		foreach ( $terms as $term ){
			$links[] = $term->name;
		}
		$links 	= str_replace(' ', '-', $links);
		$tax 	= join( " ", $links );
		$taxi 	= join( " , ", $links );
	else :
		$tax = '';
	endif;

 ?>


	<div id="post-<?php the_ID(); ?>" class="cbp-item <?php echo strtolower($tax); ?>">
		<div class="blog-article">
			<div class="blog-article-head text-center">
				<div class="blog-article-category">
					<h6 class="inline"><?php echo esc_html_e( 'In', 'nt-forester' ); ?></h6> <h4 class="inline black"><?php the_category(', '); ?></h4>
				</div>
				<div class="blog-article-title black">
					<h2><?php the_title(); ?></h2>
				</div>
				<div class="blog-article-date">
					<h6><?php echo esc_html_e( 'Posted on', 'nt-forester' ); ?> <?php echo get_the_date(); ?></h6>
				</div>
			</div>
			<a href="<?php echo esc_url( get_permalink() ); ?>">
					<div class="blog-article-image">
					<?php
						wp_enqueue_style( 'nt-forester-custom-flexslider');
						wp_enqueue_script( 'nt-forester-custom-flexslider');
						wp_enqueue_script( 'fitvids');
						wp_enqueue_script( 'nt-forester-blog-settings');
						$nt_forester_images = rwmb_meta( 'nt_forester_gallery_image' );
						if( $nt_forester_images != '' ) :
					?>
						<div class="flexslider">
							<ul class="slides">
								<?php
									foreach ( $nt_forester_images as $image ) {
										echo "<li><img src='{$image['full_url']}' alt='{$image['alt']}' /></li>";
									}
								?>
							</ul>
						</div>
					<?php endif; ?>
				</div>
			</a>
			<h6><?php
				if ( has_excerpt() ) :
					the_excerpt();
					else :
					$content = get_the_content();
					echo substr($content, 0, 300);
				endif ;

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nt-forester' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'nt-forester' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?></h6>
			<div class="read-more">
				<a href="<?php echo esc_url( get_permalink() ); ?>"><h4 class="black"><?php echo esc_html_e( 'View Post', 'nt-forester' ); ?><i class="fa fa-angle-right ml10"><i class="fa fa-angle-right"></i></i></h4></a>
			</div>
		</div>
	</div>
