<?php
/**
 * The template for displaying posts in the Audio post format.
 *
 * @package WordPress
 * @subpackage nt_forester_
 * @since nt_forester_ 1.0
 */
	$terms = get_the_category();
	if ( $terms && ! is_wp_error( $terms ) ) :
		$links = array();
		foreach ( $terms as $term ){
			$links[] = $term->name;
		}
		$links 	= str_replace(' ', '-', $links);
		$tax 	= join( " ", $links );
		$taxi 	= join( " , ", $links );
	else :
		$tax = '';
	endif;
?>

	<div id="post-<?php the_ID(); ?>" class="cbp-item <?php echo strtolower($tax); ?>">
		<div class="blog-article audio-post">
			<div class="blog-article-head text-center">
				<div class="blog-article-category">
					<h6 class="inline"><?php echo esc_html_e( 'In', 'nt-forester' ); ?></h6> <h4 class="inline black"><?php the_category(', '); ?></h4>
				</div>
				<div class="blog-article-title black">
					<h2><?php the_title(); ?></h2>
				</div>
				<div class="blog-article-date">
					<h6><?php echo esc_html_e( 'Posted on', 'nt-forester' ); ?> <?php echo get_the_date(); ?></h6>
				</div>
			</div>
			<a href="<?php echo esc_url( get_permalink() ); ?>">
				<div class="blog-article-image video-responsive">
					<?php
						$nt_forester_mp3 		= rwmb_meta( 'nt_forester_audio_mp3' );
						$nt_forester_oga 		= rwmb_meta( 'nt_forester_audio_ogg' );
						$nt_forester_sc_url 	= rwmb_meta( 'nt_forester_audio_sc' );
						$nt_forester_sc_color = rwmb_meta( 'nt_forester_audio_sc_color' );
						$nt_forester_wp_audio = '[audio mp3="'.$nt_forester_mp3.'"  ogg="'.$nt_forester_oga.'"]';
						$nt_forester_soundcloud_audio = '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='.urlencode( $nt_forester_sc_url ).'&amp;show_comments=true&amp;auto_play=false&amp;color='.$nt_forester_sc_color.'"></iframe>';

						if($nt_forester_sc_url!='') :
							echo ( $nt_forester_soundcloud_audio );
						else :
							if(has_post_thumbnail()) : the_post_thumbnail();
								echo do_shortcode ( $nt_forester_wp_audio );
							endif;
						endif;
					?>
				</div>
			</a>
			<h6><?php

				if ( has_excerpt() ) :
					the_excerpt();
				else :

					$nt_forester_masonry_content_limit= 	ot_get_option( 'nt_forester_masonry_content_limit' );
					$nt_forester_masonry_contentlimit = ( $nt_forester_masonry_content_limit != '' ) ? $nt_forester_masonry_content_limit : 300;

					$nt_forester_masonry_content = get_the_content();
					if(is_numeric($nt_forester_masonry_contentlimit) && $nt_forester_masonry_contentlimit > 0 && $nt_forester_masonry_contentlimit == round($nt_forester_masonry_contentlimit, 0)){
						echo ''.substr( $nt_forester_masonry_content, 0, $nt_forester_masonry_contentlimit ).'...';
					}else{
						echo ''.substr( $nt_forester_masonry_content, 0, 300 ).'...';
					}

				endif ;

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nt-forester' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'nt-forester' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?></h6>
			<div class="read-more">
				<a href="<?php echo esc_url( get_permalink() ); ?>"><h4 class="black"><?php echo esc_html_e( 'View Post', 'nt-forester' ); ?><i class="fa fa-angle-right ml10"><i class="fa fa-angle-right"></i></i></h4></a>
			</div>
		</div>
	</div>
