<?php
/**
 * The template for displaying posts in the Quote post format.
 *
 * @package WordPress
 * @subpackage nt_forester_
 * @since nt_forester_ 1.0
 */
?>

<!-- Start .hentry -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="blog-post-list blog-post-single">
      <div class="blog-post-description">
        <div class="blog-post-category">
            <span><?php esc_html_e('in', 'nt-forester'); ?> <span class="subheading dark"><?php the_category(', '); ?></span></span>
        </div>
        <div class="blog-post-title">
            <?php
              if ( ! is_single() ) :
                 the_title( sprintf( '<h3 class="h3-lg"><a class="dark" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</h3></a>' );
              endif;
           ?>
        </div>
     </div>

	<div class="hentry-box">

		<?php

		$nt_forester_quote_text 		=	rwmb_meta( 'nt_forester_quote_text' );
		$nt_forester_quote_author 	=	rwmb_meta( 'nt_forester_quote_author' );
		$nt_forester_image_id 		=	get_post_thumbnail_id();
		$nt_forester_image_url 		=	wp_get_attachment_image_src($nt_forester_image_id, true);
		$nt_forester_color 			=	rwmb_meta( 'nt_forester_quote_bg' );
		$nt_forester_opacity 			=	rwmb_meta( 'nt_forester_quote_bg_opacity' );
		$nt_forester_opacity 			=	$nt_forester_opacity / 100;

		?>

		<div class="post-thumb">
			<div class="content-quote-format-wrapper">
				<?php if(has_post_thumbnail()) : ?>
				<div class="entry-media" style="background-image: url(<?php echo esc_url( $nt_forester_image_url[0] ); ?>); ">
				<?php else : ?>
				<div class="entry-media">
				<?php endif; ?>
					<div class="content-quote-format-overlay" style="background-color: <?php echo esc_attr( $nt_forester_color ); ?>; opacity: <?php echo esc_attr( $nt_forester_opacity ); ?>;"></div>
					<div class="content-quote-format-textwrap">
						<h3><a href="<?php esc_url( the_permalink() ); ?>"><?php echo esc_attr( $nt_forester_quote_text ); ?></a></h3>
						<p><a href="#0" target="_blank" style="color: #ffffff;"><?php echo esc_attr( $nt_forester_quote_author ); ?></a></p>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>



		<?php do_action('nt_forester_formats_content_action'); ?>

	</article><!-- #post-## -->
