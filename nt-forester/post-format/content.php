<?php
/**
* The default template for displaying content
*
* Used for both single and index/archive/search.
*
* @package WordPress
* @subpackage nt_forester
* @since nt_forester 1.0
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="blog-post-list blog-post-single">
      <div class="blog-post-description">
        <div class="blog-post-category">
            <span><?php esc_html_e('in', 'nt-forester'); ?> <span class="subheading dark"><?php the_category(', '); ?></span></span>
        </div>
        <div class="blog-post-title">
            <?php
              if ( ! is_single() ) :
                 the_title( sprintf( '<h3 class="h3-lg"><a class="dark" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</h3></a>' );
              endif;
           ?>
        </div>
     </div>

    <?php if ( has_post_thumbnail() ) : ?>
    <div class="blog-post-preview">
        	<?php the_post_thumbnail('full'); ?>
    </div>
    <?php endif; ?>

	<?php do_action('nt_forester_formats_content_action'); ?>


</article><!-- #post-## -->
