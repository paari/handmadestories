<?php
/**
 * The template for displaying posts in the Video post format.
 *
 * @package WordPress
 * @subpackage nt_forester_
 * @since nt_forester_ 1.0
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="blog-post-list blog-post-single">
      <div class="blog-post-description">
        <div class="blog-post-category">
            <span><?php esc_html_e('in', 'nt-forester'); ?> <span class="subheading dark"><?php the_category(', '); ?></span></span>
        </div>
        <div class="blog-post-title">
            <?php
              if ( ! is_single() ) :
                 the_title( sprintf( '<h3 class="h3-lg"><a class="dark" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</h3></a>' );
              endif;
           ?>
        </div>
     </div>


	<div class="hentry-box">
		<?php
			$nt_forester_embed = rwmb_meta( 'nt_forester_video_embed' );
			if( $nt_forester_embed!='' ) :
		?>
		<div class="post-thumb blog-bg">
			<div class="media-element video-responsive"><?php echo ($nt_forester_embed); ?></div>
		</div>

		<?php else : ?>

		<?php
			$nt_forester_m4v 			=	rwmb_meta( 'nt_forester_video_m4v' );
			$nt_forester_ogv 			=	rwmb_meta( 'nt_forester_video_ogv' );
			$nt_forester_webm 		=	rwmb_meta( 'nt_forester_video_webm' );
			$nt_forester_image_id 	=	get_post_thumbnail_id();
			$nt_forester_image_url	=	wp_get_attachment_image_src($nt_forester_image_id, true);
			$nt_forester_wp_video 	=	'[video poster="'.$nt_forester_image_url[0].'" mp4="'.$nt_forester_m4v.'"  webm="'.$nt_forester_webm.'"]';
		?>

	    <div class="post-thumb"><?php echo do_shortcode ($nt_forester_wp_video); ?></div>
		<?php endif; ?>

		<?php do_action('nt_forester_formats_content_action'); ?>

	</article><!-- #post-## -->
