<?php

	get_header();

	get_template_part('index-header');

	$nt_forester_pagelayout 			= 	rwmb_meta( 'nt_forester_pagelayout' );

	$nt_forester_page_hero_display 	= 	rwmb_meta( 'nt_forester_page_hero_display' );

	//page hero alternate title
	$nt_forester_title_display 		= 	rwmb_meta( 'nt_forester_title_display' );
	$nt_forester_page_title 			= 	rwmb_meta( 'nt_forester_alt_title' );

	//page hero subtitle
	$nt_forester_subtitle_display 	= 	rwmb_meta( 'nt_forester_subtitle_display' );
	$nt_forester_page_subtitle 		= 	rwmb_meta( 'nt_forester_subtitle' );

	//page breadcrumbs
	$nt_forester_bread_display 		= 	ot_get_option( 'nt_forester_bread_display' );

	//page footer
	$nt_forester_footer_display 		= 	rwmb_meta( 'nt_forester_page_footer_display' );

	//page custom button
	$nt_forester_herobtn_display 		= 	rwmb_meta( 'nt_forester_page_herobtn_display' );
	$nt_forester_herobtn 				= 	rwmb_meta( 'nt_forester_page_herobtn' );
	$nt_forester_herobtn_linktype 	= 	rwmb_meta( 'nt_forester_page_herobtn_linktype' );
	$nt_forester_herobtn_url 			= 	rwmb_meta( 'nt_forester_page_herobtn_url' );
	$nt_forester_hero_btn_linktype 	= 	( $nt_forester_herobtn_linktype != 'external' ) ? 'data-scroll' : '';
	$nt_forester_herobtn_title 		= 	( $nt_forester_herobtn != '' ) ? $nt_forester_herobtn : esc_html__( 'Back To Homepage', 'nt-forester' );
	$nt_forester_hero_btn_url 		= 	( $nt_forester_herobtn_url != '' ) ? $nt_forester_herobtn_url : home_url( '/' );

	//page hero bg image
	$nt_forester_page_bg_img 			=  wp_get_attachment_url( get_post_meta(get_the_ID(), 'nt_forester_page_bg_image', true ),'full' );
	$nt_forester_page_bg_img 			= ( $nt_forester_page_bg_img != '' ) ? $nt_forester_page_bg_img : get_theme_file_uri() . '/images/full_1.jpg';

// PAGE HEADING SECTION START
if ( $nt_forester_page_hero_display != true ) : ?>

	<!-- Start Hero Section -->
	<section id="hero" class="page-id-<?php echo get_the_ID(); ?> hero-fullwidth parallax">
		<?php if( $nt_forester_page_bg_img != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_page_bg_img ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container text-center white">
				<div class="m-auto">
					<div class="title-service mb15">

						<?php if( $nt_forester_title_display != true ) : ?>
							<?php if( $nt_forester_page_title ): ?>
								<h2><?php echo esc_html( $nt_forester_page_title ); ?></h2>
							<?php else : ?>
								<h2><?php echo the_title(); ?></h2>
							<?php endif; ?>
						<?php endif; ?>

						<?php if( $nt_forester_subtitle_display != true ) : ?>
							<?php if( $nt_forester_page_subtitle ): ?>
								<h5 class="subheading mt0 mb50"><?php echo ( $nt_forester_page_subtitle ); ?></h5>
							<?php endif; ?>
						<?php endif; ?>

						<?php if( $nt_forester_bread_display != 'off' ) : ?>
							<?php if( function_exists( 'bcn_display' ) ) : ?>
								<p class="breadcrubms headline-text"><?php bcn_display(); ?></p>
							<?php endif; ?>
						<?php endif; ?>

					</div>

					<?php if( $nt_forester_herobtn_display == 'on' ) : ?>
						<a <?php echo esc_html( $nt_forester_hero_btn_linktype ); ?> href="<?php echo esc_url( $nt_forester_hero_btn_url  ); ?>" class="btn btn-circle btn-sm btn-primary"><i class="ion-chevron-left pr5"></i><?php echo esc_html( $nt_forester_herobtn_title ); ?></a>
					<?php endif; ?>

				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->
<?php endif; ?>

	<section id="blog">
		<div class="container has-margin-bottom">
			<div class="row">
				<div class="col-md-12-off  has-margin-bottom-off">

					<?php if( ( $nt_forester_pagelayout ) =='right-sidebar' || ( $nt_forester_pagelayout ) =='' ){ ?>
					<div class="col-lg-8  col-md-8 col-sm-12 index float-right posts">
					<?php } elseif( ( $nt_forester_pagelayout ) == 'left-sidebar' ) { ?>
					<?php get_sidebar(); ?>
					<div class="col-lg-8  col-md-8 col-sm-12 index float-left posts">
					<?php } elseif( ( $nt_forester_pagelayout ) == 'full-width' ) { ?>
					<div class="col-xs-12 full-width-index v">
					<?php } ?>

						<?php
							// Start the loop.
							while ( have_posts() ) : the_post();

								// Include the page content template.
								get_template_part( 'content', 'page' );

								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;

							// End the loop.
							endwhile;
						?>
					</div>

					<?php if( ( $nt_forester_pagelayout ) =='right-sidebar' || ( $nt_forester_pagelayout ) =='' ){ ?>
						<?php get_sidebar(); ?>
					<?php } ?>

				</div>
			</div>
		</div>
	</section>

	<?php get_footer(); ?>
