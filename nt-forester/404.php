<?php

	get_header();
	get_template_part('index-header');
	$nt_forester_error_hero_display 		= 	ot_get_option( 'nt_forester_error_hero_display' );
	$nt_forester_h_h_bg 					= 	esc_attr( ot_get_option( 'nt_forester_error_headbg' ) );
	$nt_forester_h_h_bg_img 				= 	( $nt_forester_h_h_bg != '' ) ? $nt_forester_h_h_bg : get_theme_file_uri() . '/images/full_1.jpg';
	$nt_forester_error_heading_display	= 	ot_get_option( 'nt_forester_error_heading_display' );
	$nt_forester_error_heading 			= 	ot_get_option( 'nt_forester_error_heading' );
	$nt_forester_error_slogan_display 	= 	ot_get_option( 'nt_forester_error_slogan_display' );
	$nt_forester_error_slogan 			= 	ot_get_option( 'nt_forester_error_slogan' );
	$nt_forester_bread_display			= 	ot_get_option( 'nt_forester_bread' );
	$nt_forester_404_layout 				= 	ot_get_option( 'nt_forester_404_layout' );

	wp_enqueue_style( 'nt-forester-custom-theme-style' );
	wp_enqueue_style( 'nt-forester-primary-color' );
	wp_enqueue_style( 'themify' );
	wp_enqueue_style( 'nt-forester-update-two' );

if ( $nt_forester_error_hero_display != 'off' ) :
?>

	<!-- Start Hero Section -->
	<section id="hero" class="page-id-<?php echo get_the_ID(); ?> hero-fullwidth parallax nt-inner-pages-hero">
		<?php if( $nt_forester_h_h_bg_img != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_h_h_bg_img ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container white">
				<div class="m-auto">
					<div class="title-service mb15">

							<?php if( $nt_forester_error_heading_display != 'off' ) : // PAGE TITLE
								if ( ( $nt_forester_error_heading  ) != '') : ?>
									<h1 class="white"><?php echo esc_html( $nt_forester_error_heading ); ?></h1>
								<?php else : ?>
									<h1 class="white"><?php echo esc_html_e( '404 - Not Found','nt-forester' );?></h1>
								<?php endif;
							endif; ?>

							<?php if ( $nt_forester_error_slogan_display != 'off' ) : // SUBTITLE
								if ( $nt_forester_error_slogan != '' ) : ?>
									<p class="cover-text-sublead heading-title"><?php echo esc_html( $nt_forester_error_slogan ); ?></p>
								<?php endif;
							endif;

							if( $nt_forester_bread_display != 'off' ) : //BREADCRUMB
								if( function_exists( 'bcn_display' ) ) : ?>
									<p class="breadcrubms"><?php bcn_display(); ?></p>
								<?php endif;
							endif; ?>

					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->

<?php endif; ?>

	<section id="blog-post" class="bg-white blog-modern-sidebar-off">
		<div class="container pt150 pb100">
			<?php
				if ( $nt_forester_error_hero_display == 'off' ) :

					if ( $nt_forester_error_slogan_display != 'off' ) :
						if ( $nt_forester_error_slogan != '') : ?>
							<p class="cover-text-sublead text-center heading-title"><?php echo esc_html( $nt_forester_error_slogan ); ?></p>
						<?php else: ?>
							<p class="cover-text-sublead text-center heading-title"><?php echo esc_html_e( 'Search completed','nt-forester' );?></p>
						<?php endif;
					endif;

					if ( $nt_forester_error_heading_display != 'off') :
						if ( $nt_forester_error_heading != '' ) : ?>
							<h1 class="black text-center pb60"><?php echo esc_html( $nt_forester_error_heading ); ?></h1>
						<?php else: ?>
							<h4 class="black text-center pb60"><?php echo esc_html( $wp_query->found_posts ); ?> <?php esc_html_e( 'Search Results Found For', 'nt-forester' ); ?>: "<?php the_search_query(); ?>"</h4>
					<?php
						endif;
					endif;
				endif;
			?>

			<div class="row">

				<!-- right sidebar -->
				<?php if( ( $nt_forester_404_layout ) == 'right-sidebar' || ( $nt_forester_404_layout ) == '') { ?>
				<div class="col-lg-9 col-md-9 col-sm-12 text-center content-error">

				<!-- left sidebar -->
				<?php } elseif( ( $nt_forester_404_layout ) == 'left-sidebar') { ?>
				<?php get_sidebar(); ?>
				<div class="col-lg-9 col-md-9 col-sm-12 text-center content-error">

				<!-- no sidebar -->
				<?php } elseif( ( $nt_forester_404_layout ) == 'full-width') { ?>
				<div class="col-md-12 text-center content-error">
				<?php } ?>

					<h3 class="black text-center pb30"><?php esc_html_e( 'Hmm, we could not find what you are looking for.', 'nt-forester' ); ?></h3>
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'nt-forester' ); ?></p>

					<?php get_search_form(); ?>

					<div class="clearfix m-bottom-50"></div>
					<div class="recent">
						<div class="col-header">
							<h3 class="black"><?php esc_html_e( 'Recent Posts', 'nt-forester' ); ?></h3>
						</div>
						<ol>
							<?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 10, 'format' => 'custom', 'before' => '<li>', 'after' => '</li>' ) ); ?>
						</ol>
					</div>

				</div><!-- #end sidebar+ content -->

				<!-- right sidebar -->
				<?php if( ( $nt_forester_404_layout ) == 'right-sidebar' || ( $nt_forester_404_layout ) == '') {
					get_sidebar();
				} ?>

			</div>
		</div>
	</section>

<?php get_footer(); ?>
