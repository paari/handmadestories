<!DOCTYPE html>
<html <?php language_attributes(); ?>> 

<head>
	<!-- Meta UTF8 charset -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

	<!-- BODY START=========== -->
	<body <?php body_class(); ?> data-fade-in="true">

	<?php do_action('nt_forester_preloader_action'); ?>
