(function ($) {
    "use strict";

    function owlQuote() {
        jQuery('.owl-carousel-quote').owlCarousel({
            items: 1,
            autoplay: true,
            loop: true,
            rewind: true,
            autoplayTimeout: 4200,
            autoplaySpeed: 500,
        })
    }


    function owlTestimonials() {
        jQuery('.owl-carousel-testimonials').owlCarousel({
            items: 1,
            autoplay: true,
            loop: true,
            autoplayTimeout: 4200,
            autoplaySpeed: 500,
            dots: true
        })
    }


    function owlClients() {
        jQuery('.owl-clients').owlCarousel({
            autoplay : true,
            autoplayTimeout: 4000,
            rewind: true,
            dots: false,
            nav: false,
            navText: false,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 5,
                    nav: true,
                    loop: false
                }
            }
        });
    }


    function owlHeroSlider() {
        jQuery(".hero-slider").owlCarousel({
            autoplay: true,
            items: 1,
            dots: false,
            nav: false,
            autoHeight: true,
            loop: true,
            animateIn: "fadeIn",
            animateOut: "fadeOut",
        });
    }

    function owlContentSlider() {
        jQuery(".hero-content-slider").owlCarousel({
            autoplay: 8000,
            autoplayTimeout: 3500,
            autoplaySpeed: 1000,
            mouseDrag: false,
            touchDrag: false,
            loop: true,
            dots: false,
            dragEndSpeed: 800,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        })
    }

    function owlTeam() {
        $('.owlTeam').owlCarousel({
            items: 3,
            autoplay: false,
            autoplayTimeout: 7500,
            autoplaySpeed: 500,
            dots: true,
            rewind: true,
            responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
        })
    }

    // Counter Up Function
    function counterUp() {
        jQuery('.counter').counterUp({
            delay: 16,
            time: 3000
        });
    }



    // Navigation Close On Click Function
    jQuery(document).on('click','.navbar-collapse.in',function(e) {
        if( jQuery(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
            jQuery(this).collapse('hide');
        }
    });


    jQuery(".nav-menu-left li a").click(function() {
      jQuery(this).parent().addClass('active').siblings().removeClass('active');
     });

    if (jQuery(window).width() > 768 ) {

      jQuery('.nav-menu-left li.sub').hover(function() {
         jQuery(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn();
         jQuery(this).find('.dropdown-submenu').find('.dropdown-menu').stop(true, true);
      }, function() {
         jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
      });
      jQuery('.nav-menu-left li.sub .dropdown-submenu .dropdown-submenu').hover(function() {
         jQuery(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn();
      }, function() {
         jQuery(this).find('.dropdown-menu').stop(true, true).delay(10).fadeOut();
      });
    }

    if (jQuery(window).width() < 1350 ) {

          jQuery('.mobile-menu-inner li.sub .dropdown-submenu > a').toggle(
          function(){
             jQuery(this).parent().addClass('open');
          },
          function(){
             jQuery(this).parent().removeClass('open');
          });

          jQuery('.mobile-menu-inner li.sub .dropdown-submenu .dropdown-submenu > a').toggle(
          function(){
             jQuery(this).parent().addClass('open');
          },
          function(){
             jQuery(this).parent().removeClass('open');
          });
    }

   function navbarCollapse() {
       var collapsedDropdown = $(".collapsed-dropdown"),
           hamburgerButton = $(".hamburger-menu button");
       hamburgerButton.on("click", function() {
           $(this).toggleClass("clicked");
           collapsedDropdown.fadeToggle(200);
       })
       $(".collapsed-dropdown>ul>li.dropdown>a").on("click", function(forester) {
           $(this).next().slideToggle();
           forester.preventDefault();
       })

       $("nav.navbar ul.nav-menu>li>a").on("click", function(forester) {
           if ($(this).parent().hasClass("dropdown")) {
               forester.preventDefault();
           }
       })
       $(".collapsed-dropdown>ul>li:not(.item-has-children)>a").on("click", function() {
           collapsedDropdown.fadeOut(200);
           hamburgerButton.toggleClass("clicked");
       })
       $(".collapsed-dropdown>ul>li:not(.item-has-children)>ul>li:not(.dropdown-submenu)>a ").on("click", function() {
           collapsedDropdown.fadeOut(200);
           hamburgerButton.toggleClass("clicked");
       })
       if ($("nav.navbar").hasClass("navbar-static")) {
           $("#hero .hero-content").css("padding-top", "0px")
       }
   }




   function navSlide() {
        $(window).on("scroll", function () {
            // Navbar On Scroll Animation
            var navbar = $(".navbar.sticky"),
            HeroHeight = $("#home").height();
            if (jQuery(window).width() > 1350 ) {
               if ($(window).scrollTop() >= 650) {
                   navbar.addClass("nav-collapsed animated slideInDown");
               }
               else {
                   navbar.removeClass("nav-collapsed animated slideInDown");
               }
            }
            if (jQuery(window).width() < 600 ) {
               if ($(window).scrollTop() >= 46) {
                   navbar.addClass("mobile-help-class-adminbar");
               }
               else {
                   navbar.removeClass("mobile-help-class-adminbar");
               }
            }
        })
   };

   function progressCircles() {
   $('.progress-circle').waypoint(function () {
       var totalProgress, progress, circles;
       circles = document.querySelectorAll('.progress-svg');
           for(var i = 0; i < circles.length; i++) {
               totalProgress = circles[i].querySelector('circle').getAttribute('stroke-dasharray');
               progress = circles[i].parentElement.getAttribute('data-circle-percent');
               circles[i].querySelector('.bar').style['stroke-dashoffset'] = totalProgress * progress / 100;
           }
       }, { offset: '70%', triggerOnce: true });
   }


	// Back To Top Button Function
	function backtoTop() {
		jQuery(window).on("scroll", function (){
			var back_to_top = jQuery("#back-to-top");
			if (jQuery(window).scrollTop() > 70) {
				back_to_top.stop().animate({ opacity: '1' }, 150);
			} else {
				back_to_top.stop().animate({ opacity: '0' }, 150);
			};
		})
	}

    // Blog Boxed - 2 Columns
    $('#blog-boxed-col-2').cubeportfolio({
        filters: '.posts-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        mediaQueries: [{
            width: 1500,
            cols: 3,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: 'fadeOut',
        gapHorizontal: 30,
        gapVertical: 30,
        gridAdjustment: 'responsive'
    });

    // Blog Boxed - 2 Columns
    $('#blog-boxed-col-3').cubeportfolio({
        filters: '.posts-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        mediaQueries: [{
            width: 1500,
            cols: 4,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: 'fadeOut',
        gapHorizontal: 30,
        gapVertical: 30,
        gridAdjustment: 'responsive'
    });




   jQuery(document).ready(function () {

      var herosection = jQuery('#hero.hero-fullwidth').length;
      var herosection2 = jQuery('#hero.hero-fullscreen').length;
      if (herosection || herosection2 ) {
      	jQuery('body').addClass('blog-has-hero');
      }else{
      	jQuery('body').addClass('blog-hero-off');
      }


      parallaxen();
      owlQuote();
      counterUp();
      owlHeroSlider();
      owlContentSlider();
      owlTeam();
      owlTestimonials();
      owlClients();
      backtoTop();
      navSlide();
      navbarCollapse();
      progressCircles();

   });

}(jQuery));


jQuery(window).load(function(){
    var teamimgwidth=jQuery(".team-img img").css("width");
	jQuery('.team-img').css({'max-width' : teamimgwidth });

});


	/*!
	* Preloader
	*/
	!function(){
	    setTimeout(function(){
	        jQuery('.preloader').css({opacity: '0'}).one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function() {
	            jQuery(this).hide();
	            jQuery(this).remove();
	        });
	    }, 1000);
	}();




    /*!
    * Hero Youtube Background
    */
    if (jQuery('#forester-youtube').length) {
        var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        var ytVideoId = jQuery('#forester-youtube').attr('src');
        var player;
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('forester-youtube', {
                width: jQuery(window).width()+'0',
                height: jQuery(window).height()+'0',
                videoId: ytVideoId,
                playerVars: { playlist: ytVideoId, autoplay : 1, controls: 0, showinfo: 0, loop: 1, },
                events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange }
            });
            jQuery(window).on('load resize', function(){
                var w = jQuery(window).width()+0;
                var h = jQuery(window).height()+0;
                if (w/h > 16/9){
                    player.setSize(w, w/16*9);
                    jQuery('#forester-youtube').css({'left': '0px'});
                    } else {
                    player.setSize(h/9*16, h);
                    jQuery('#forester-youtube').css({'left': -(jQuery('#forester-youtube').outerWidth()-w)/2});
                }
            });

        }
        function onPlayerStateChange(event) {
        }
        function stopVideo() { player.stopVideo(); }
        function onPlayerReady() {
            player.playVideo();
            player.mute();
        }
    }
