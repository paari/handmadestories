

/*global $, jQuery, alert*/
(function ($) {
   "use strict";

   function foresterPortfolio() {
   var portlayout = prefix.layoutmode;
   if( !!portlayout ){portlayout = portlayout;}else{ portlayout = 'grid';}
   //'grid', 'mosaic' or 'slider'

   var portgrid = prefix.gridadjustment;
   if( !!portgrid ){portgrid = portgrid;}else{ portgrid = 'responsive';}
   //'default''alignCenter''responsive'

   var portcaption = prefix.animationcaption;
   if( !!portcaption ){portcaption = portcaption;}else{ portcaption = 'fadeIn';}
   /*'pushTop' 'pushDown' 'revealBottom' 'revealTop' 'revealLeft' 'moveRight' 'overlayBottom' 'overlayBottomPush' 'overlayBottomReveal' 'overlayBottomAlong' 'overlayRightAlong' 'minimal' 'fadeIn' 'zoom' 'opacity' */

   //for filter animation when click
   var filteranimation = prefix.animationfilter;
   if( !!filteranimation ){filteranimation = filteranimation;}else{ filteranimation = 'sequentially';}
   /* 'fadeOut''quicksand''bounceLeft''bounceTop''bounceBottom''moveLeft''slideLeft''fadeOutTop''sequentially''skew''slideDelay''3d' Flip'rotateSides''flipOutDelay''flipOut''unfold''foldLeft''scaleDown''scaleSides''frontRow''flipBottom''rotateRoom'  */

   var singlepageanimation = prefix.animationpopup;//'left' 'fade' 'right'
   if( !!singlepageanimation ){singlepageanimation = singlepageanimation;}else{ singlepageanimation = 'left';}


   var horizontalgap = prefix.itemhorizontalgap;//default 10'
   var horizontalgap = parseInt(horizontalgap);
   if( !!horizontalgap ){horizontalgap = horizontalgap;}else{ horizontalgap = 10;}

   var verticalgap = prefix.itemverticalgap;//default 10'
   var verticalgap = parseInt(verticalgap);
   if( !!verticalgap ){verticalgap = verticalgap;}else{ verticalgap = 10;}


   // Portfolio Fullwidth - 3 Columns
    jQuery('#portfolio-fullwidth-col-2').cubeportfolio({
        filters: '.portfolio-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+portlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 2,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+portgrid+'',
        caption: ''+portcaption+'',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
		  singlePageAnimation:''+singlepageanimation+'',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
		}
    });
jQuery('#portfolio-fullwidth-col-3').cubeportfolio('destroy');
   // Portfolio Fullwidth - 3 Columns
    jQuery('#portfolio-fullwidth-col-3').cubeportfolio({
        filters: '.portfolio-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+portlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 3,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+portgrid+'',
        caption: ''+portcaption+'',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
		singlePageAnimation:''+singlepageanimation+'',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
		}
    });


   // Portfolio Fullwidth - 4 Columns
    jQuery('#portfolio-fullwidth-col-4').cubeportfolio({
        filters: '.portfolio-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+portlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 4,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+portgrid+'',
        caption: ''+portcaption+'',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
		singlePageAnimation:''+singlepageanimation+'',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
		}
    });




    // Portfolio Boxed - 2 Columns
    jQuery('#portfolio-boxed-col-2').cubeportfolio({
        filters: '.portfolio-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+portlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 3,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+portgrid+'',
        caption: ''+portcaption+'',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
		singlePageAnimation:''+singlepageanimation+'',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
		}
    });


    // Portfolio Boxed - 3 Columns
    jQuery('#portfolio-boxed-col-3').cubeportfolio({
        filters: '.portfolio-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+portlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 3,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+portgrid+'',
        caption: ''+portcaption+'',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
		singlePageAnimation:''+singlepageanimation+'',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
		}
    });


    // Portfolio Boxed - 4 Columns
    jQuery('#portfolio-boxed-col-4').cubeportfolio({
        filters: '.portfolio-filter',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: ''+portlayout+'',
        mediaQueries: [{
            width: 1500,
            cols: 4,
        }, {
            width: 1100,
            cols: 4,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: ''+filteranimation+'',
        gapHorizontal: horizontalgap,
        gapVertical: verticalgap,
        gridAdjustment: ''+portgrid+'',
        caption: ''+portcaption+'',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
		singlePageAnimation:''+singlepageanimation+'',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
		}
    });

}
jQuery(document).ready( function () {

    foresterPortfolio();

});
}(jQuery));



jQuery(window).resize(function (){
    setTimeout(function(){
        jQuery('.portfolio-filters .active').trigger('click');
    }, 600);
});
