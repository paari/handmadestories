<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage nt_forester
 * @since nt_forester 1.0
 */
 $nt_forester_thumb 	= get_post_thumbnail_id();
 $nt_forester_postthumb = nt_forester_aq_resize( wp_get_attachment_url( $nt_forester_thumb,'full' ), 750, 450, true, true, true );
?>


<!-- ARTICLE -->

<div id="post-<?php the_ID(); ?>" <?php post_class('cbp-item'); ?>>
	<div class="blog-article blog-post-text">
		<div class="blog-article-head text-center">
			<div class="blog-article-title black">
				<h2><?php the_title(); ?></h2>
			</div>
			<div class="blog-article-date">
				<h6><?php echo esc_html_e( 'Posted on', 'nt-forester' ); ?> <?php echo get_the_date(); ?></h6>
			</div>
		</div>
		<?php if ( has_post_thumbnail() ) : ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>">
				<div class="blog-article-image">
					<img src="<?php echo esc_url( $nt_forester_postthumb ); ?>" alt="<?php echo the_title(); ?>" class="pb10">
				</div>
			</a>
		<?php endif; ?>
		<?php if ( has_excerpt() ) : ?>
			<h6><?php the_excerpt(); ?><h6>
		<?php endif; ?>
		<div class="read-more">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><h4 class="black"><?php echo esc_html_e( 'View Post', 'nt-forester' ); ?><i class="fa fa-angle-right ml10"><i class="fa fa-angle-right"></i></i></h4></a>
		</div>
	</div>
</div>
