<?php

	/*
	Template name: Custom-page Template
	*/

	get_header();

	$nt_forester_page_hero_display 	= 	rwmb_meta( 'nt_forester_page_hero_display' );

	//page hero alternate title
	$nt_forester_title_display 		= 	rwmb_meta( 'nt_forester_title_display' );
	$nt_forester_page_title 			= 	rwmb_meta( 'nt_forester_alt_title' );
	$nt_forester_use_bigtitle 		= 	rwmb_meta( 'nt_forester_use_bigtitle' );

	//page hero subtitle
	$nt_forester_subtitle_display 	= 	rwmb_meta( 'nt_forester_disable_subtitle' );
	$nt_forester_page_subtitle 		= 	rwmb_meta( 'nt_forester_subtitle' );

	//page breadcrumbs
	$nt_forester_bread_display 		= 	ot_get_option( 'nt_forester_bread_display' );
	$nt_forester_page_bread_display 	= 	rwmb_meta( 'nt_forester_page_bread_display' );

	//page custom button
	$nt_forester_herobtn_display 		= 	rwmb_meta( 'nt_forester_page_herobtn_display' );
	$nt_forester_page_herobtn_bg 		= 	rwmb_meta( 'nt_forester_page_herobtn_bg' );
	$nt_forester_page_herobtn_size 	= 	rwmb_meta( 'nt_forester_page_herobtn_size' );
	$nt_forester_page_herobtn_icon_display 	= 	rwmb_meta( 'nt_forester_page_herobtn_icon_display' );
	$nt_forester_page_herobtn_icon 	= 	rwmb_meta( 'nt_forester_page_herobtn_icon' );
	$nt_forester_page_herobtn_iconpos = 	rwmb_meta( 'nt_forester_page_herobtn_iconpos' );
	$nt_forester_page_herobtnicon 	= 	( $nt_forester_page_herobtn_icon != '' ) ? $nt_forester_page_herobtn_icon : 'ion-chevron-left';

	if ( $nt_forester_page_herobtn_iconpos == 'after' ) {
		$nt_forester_aftericon  = '<i class="'.esc_html($nt_forester_page_herobtnicon).' pl5"></i>';
	}else{
		$nt_forester_beforeicon = '<i class="'.esc_html($nt_forester_page_herobtnicon).' pr5"></i>';
	}
	if ( $nt_forester_page_herobtn_icon_display == true ) {
		$nt_forester_beforeicon  = '';
		$nt_forester_aftericon  = '';
	}
	$nt_forester_page_herobtnsize 	= 	( $nt_forester_page_herobtn_size != '' ) ? $nt_forester_page_herobtn_size : 'btn-sm';
	$nt_forester_herobtn 				= 	rwmb_meta( 'nt_forester_page_herobtn' );
	$nt_forester_herobtn_linktype 	= 	rwmb_meta( 'nt_forester_page_herobtn_linktype' );
	$nt_forester_herobtn_url 			= 	rwmb_meta( 'nt_forester_page_herobtn_url' );
	$nt_forester_hero_btn_linktype 	= 	( $nt_forester_herobtn_linktype != 'external' ) ? 'data-scroll' : '';
	$nt_forester_herobtn_title 		= 	( $nt_forester_herobtn != '' ) ? $nt_forester_herobtn : esc_html__( 'Back To Homepage', 'nt-forester' );
	$nt_forester_hero_btn_url 		= 	( $nt_forester_herobtn_url != '' ) ? $nt_forester_herobtn_url : home_url( '/' );

	//page hero bg image
	$nt_forester_page_bg_img 			=  wp_get_attachment_url( get_post_meta(get_the_ID(), 'nt_forester_page_bg_image', true ),'full' );
	$nt_forester_page_bg_img 			= ( $nt_forester_page_bg_img != '' ) ? $nt_forester_page_bg_img : get_theme_file_uri() . '/images/full_1.jpg';


	// DEFAULT MENU
	do_action('nt_forester_combinemenu_action');


// PAGE HEADING SECTION START
if ( $nt_forester_page_hero_display != true ) : ?>

        <!-- Start Hero Section -->
        <section id="hero" class="page-id-<?php echo get_the_ID(); ?> hero-fullwidth parallax">
			<?php if( $nt_forester_page_bg_img != '' ) : ?>
				<div class="background-image overlay" style="background-image:url('<?php echo esc_url( $nt_forester_page_bg_img ); ?>'); opacity:1;">
					<img src="<?php echo esc_url( $nt_forester_page_bg_img ); ?>" alt="<?php echo the_title(); ?>">
				</div>
			<?php endif; ?>

            <div class="hero-content">
                <div class="container text-center white">
                    <div class="m-auto">
                        <div class="title-service mb15">

							<?php if( $nt_forester_title_display != 'off' ) : ?>
								<?php if( $nt_forester_page_title ): ?>
									<?php if( $nt_forester_use_bigtitle != false ): ?>
										<h1><?php echo esc_html( $nt_forester_page_title ); ?></h1>
									<?php else : ?>
										<h2><?php echo esc_html( $nt_forester_page_title ); ?></h2>
									<?php endif; ?>
								<?php else : ?>
									<?php if( $nt_forester_use_bigtitle != false ): ?>
										<h1><?php echo esc_html( $nt_forester_page_title ); ?></h1>
									<?php else : ?>
										<h2><?php the_title(); ?></h2>
									<?php endif; ?>
								<?php endif; ?>
							<?php endif; ?>

							<?php if( $nt_forester_subtitle_display != 'off' ) : ?>
								<?php if( $nt_forester_page_subtitle ): ?>
									<h5 class="subheading mt0 mb50"><?php echo ( $nt_forester_page_subtitle ); ?></h5>
								<?php endif; ?>
							<?php endif; ?>

							<?php if( $nt_forester_bread_display != 'off' ) : ?>
								<?php if( $nt_forester_page_bread_display != true ) : ?>
									<?php if( function_exists( 'bcn_display' ) ) : ?>
										<p class="breadcrubms headline-text"><?php bcn_display(); ?></p>
									<?php endif; ?>
								<?php endif; ?>
							<?php endif; ?>

                        </div>
						<?php if( $nt_forester_herobtn_display != true ) : ?>
							<a <?php echo esc_html( $nt_forester_hero_btn_linktype ); ?>
                        href="<?php echo esc_url( $nt_forester_hero_btn_url  ); ?>"
                        class="btn btn-circle <?php echo esc_attr( $nt_forester_page_herobtnsize ); ?> <?php echo esc_attr( $nt_forester_page_herobtn_bg ); ?>">
                        <?php echo ( $nt_forester_beforeicon ); ?><?php echo esc_html( $nt_forester_herobtn_title ); ?><?php echo ( $nt_forester_aftericon ); ?>
                     </a>
						<?php endif; ?>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.hero-content -->
        </section>
        <!-- End Hero Section -->

<?php endif; ?>

	<main>

		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
			endif;
		?>

	</main>

<?php get_footer(); ?>
