	<?php
		/**
		* The template for displaying the footer
		*
		*
		* @package WordPress
		* @subpackage nt_forester
		* @since nt_forester 1.0
		*/


		do_action('nt_forester_widgetize_action');

		do_action('nt_forester_footer_action');

		do_action('nt_forester_backtop_action');

		wp_footer();
	?>

	</body>

</html>