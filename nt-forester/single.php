<?php

	get_header();

	get_template_part( 'index-header' );

	$nt_forester_post_content_style = ot_get_option( 'nt_forester_post_content_style' );
	$nt_forester_single_hero_display = ot_get_option( 'nt_forester_single_hero_display' );

	if( $nt_forester_post_content_style == 'style1' || $nt_forester_post_content_style == '' ){

      wp_enqueue_style( 'nt-forester-plugins' );
		wp_enqueue_style( 'nt-forester-custom-theme-style' );
		wp_enqueue_style( 'nt-forester-primary-color' );
		wp_enqueue_style( 'ion-icon');
		wp_enqueue_style( 'themify' );
		wp_enqueue_style( 'nt-forester-update-two' );

		if($nt_forester_single_hero_display != 'off'){

			do_action( 'nt_forester_single_header_action' );

		}

		do_action( 'nt_forester_before_post_single_action' );

			while ( have_posts() ) : the_post();
				get_template_part( 'post-format/content', get_post_format() );
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			endwhile;

			// post navigation
			nt_forester_post_nav();

		do_action( 'nt_forester_after_post_single_action' );

	}else{



      wp_enqueue_style( 'nt-forester-plugins' );
      wp_enqueue_style( 'nt-forester-custom-theme-style' );
      wp_enqueue_style( 'nt-forester-primary-color' );
      wp_enqueue_style( 'ion-icon');
      wp_enqueue_style( 'themify' );
      wp_enqueue_style( 'nt-forester-update-two' );

      if($nt_forester_single_hero_display != 'off'){

         do_action( 'nt_forester_single_header_action' );

      }
      do_action( 'nt_forester_before_post_single_style2_action' );

            while ( have_posts() ) : the_post();

               do_action( 'nt_forester_formats_content_single_style2_action' );

               if ( comments_open() || '0' != get_comments_number() ) :
                  comments_template();
               endif;
            endwhile;

            // post navigation
            nt_forester_post_nav();
      do_action( 'nt_forester_after_post_single_style2_action' );


	}

get_footer();

?>
