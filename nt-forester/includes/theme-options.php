<?php
	add_action( 'init', 'nt_forester_custom_theme_options' );
	function nt_forester_custom_theme_options() {
	if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
	return false;

	$nt_forester_saved_settings = get_option( ot_settings_id(), array() );

	$nt_forester_custom_settings = array(
    'contextual_help' => array(
      'sidebar'       => ''
    ),

	'sections'        => array(

		array(
			'id'		=> 'general',
			'title'		=> esc_html__( 'General', 'nt-forester' ),
		),
		array(
			'id'		=> 'logo',
			'title'		=> esc_html__( 'Logo', 'nt-forester' ),
		),
		array(
			'id'		=> 'navigation',
			'title'		=> esc_html__( 'Navigation', 'nt-forester' ),
		),
		array(
			'id'		=> 'pre',
			'title'		=> esc_html__( 'Preloader', 'nt-forester' ),
		),
		array(
			'id'		=> 'breadcrubms',
			'title'		=> esc_html__( 'Breadcrubms', 'nt-forester' ),
		),
		array(
			'id'		=> 'sidebars',
			'title'		=> esc_html__( 'Sidebars', 'nt-forester' ),
		),
		array(
			'id'        => 'sidebars_settings',
			'title'     => esc_html__( 'Sidebar Colors', 'nt-forester' ),
		),
      array(
			'id'		=> 'blogpage',
			'title'		=> esc_html__( 'Blog Page', 'nt-forester' ),
		),
		array(
			'id'		=> 'single_header',
			'title'		=> esc_html__( 'Single Page', 'nt-forester' ),
		),
		array(
			'id'		=> 'single_port',
			'title'		=> esc_html__( 'Single Portfolio', 'nt-forester' ),
		),
		array(
			'id'		=> 'archive_page',
			'title'		=> esc_html__( 'Archive Page', 'nt-forester' ),
		),
		array(
			'id'		=> 'archive_port',
			'title'		=> esc_html__( 'Archive Portfolio', 'nt-forester' ),
		),
		array(
			'id'		=> 'error_page',
			'title'		=> esc_html__( '404 Page', 'nt-forester' ),
		),
		array(
			'id'		=> 'search_page',
			'title'		=> esc_html__( 'Search Page', 'nt-forester' ),
		),
		array(
			'id'		=> 'footer_widgetize',
			'title'		=> esc_html__( 'Widgetize Footer', 'nt-forester' ),
		),
		array(
			'id'		=> 'main_footer',
			'title'		=> esc_html__( 'Main Footer', 'nt-forester' ),
		),
      array(
			'id'  		=> 'google_fonts',
			'title' 	=> esc_html__('Google Fonts', 'nt-forester' )
		),
		array(
			'id'  		=> 'typography',
			'title' 	=> esc_html__('Typography', 'nt-forester' )
		),
		array(
			'id'		=> 'cpt',
			'title'		=> esc_html__( 'CPT', 'nt-forester' ),
		),
	),// sidebar end

// options start
'settings'  => array(

		//GOOGLE FONTS  SETTINGS.
		array(
			'id'        => 'body_google_fonts',
			'label'     => esc_html__( 'Google Fonts', 'nt-forester'  ),
			'desc'      => esc_html__( 'Add Google Font and after the save settings follow these steps Dashbont-forester > Appearance > Theme Options > Typography', 'nt-forester' ),
			'type'      => 'google-fonts',
			'section'   => 'google_fonts',
			'operator'  => 'and'
		),
		//TYPOGRAPHY  SETTINGS.
		array(
			'id'        => 'nt_forester_typgrph',
			'label'     => esc_html__( 'Typography', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph1',
			'label'     => esc_html__( 'Typography h1', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph2',
			'label'     => esc_html__( 'Typography h2', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph3',
			'label'     => esc_html__( 'Typography h3', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph4',
			'label'     => esc_html__( 'Typography h4', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph5',
			'label'     => esc_html__( 'Typography h5', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph6',
			'label'     => esc_html__( 'Typography h6', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),
		array(
			'id'        => 'nt_forester_typgrph7',
			'label'     => esc_html__( 'Typography p', 'nt-forester' ),
			'desc'      => esc_html__( 'The Typography option type is for adding typography styles to your site.', 'nt-forester' ),
			'type'      => 'typography',
			'section'   => 'typography',
			'operator'  => 'and'
		),

		/** HEADER TYPE **/
		array(
			'id'          => 'nt_forester_theme_color',
			'label'       => esc_html__( 'Theme general color', 'nt-forester' ),
			'desc'        => esc_html__( 'Select a color' , 'nt-forester' ),
			'std'         => '#bed094',
			'type'        => 'select',
			'section'     => 'general',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'   => '#bed094',
					'label'   => esc_html__( 'Primary', 'nt-forester' )
				),
				array(
					'value'   => '#bed094',
					'label'   => esc_html__( 'Cyan', 'nt-forester' )
				),
				array(
					'value'   => '#ff981e',
					'label'   => esc_html__( 'Orange', 'nt-forester' )
				),
				array(
					'value'   => '#9c4cff',
					'label'   => esc_html__( 'Purple', 'nt-forester' )
				),
				array(
					'value'   => '#aed625',
					'label'   => esc_html__( 'Green', 'nt-forester' )
				),
				array(
					'value'   => 'custom',
					'label'   => esc_html__( 'Custom Color', 'nt-forester' )
				),
			)
		),
      array(
			'id'        => 'nt_forester_smoothscroll',
			'label'     => esc_html( 'Smoothscroll', 'nt-forester' ),
			'desc'      => sprintf( esc_html( 'Smoothscroll  %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'       => 'off',
			'type'      => 'on-off',
			'section'   => 'general',
			'operator'  => 'and'
		),
		array(
			'id'          => 'nt_forester_custom_color',
			'label'       => esc_html__( 'Custom color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select your custom color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_theme_color:is(custom)',
			'section'     => 'general'
		),

		//PRELOADER
		array(
			'id'        => 'nt_forester_pre',
			'label'     => esc_html( 'Preloader', 'nt-forester' ),
			'desc'      => sprintf( esc_html( 'Preloader display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'       => 'on',
			'type'      => 'on-off',
			'section'   => 'pre',
			'operator'  => 'and'
		),
		 array(
			'id'        => 'nt_forester_pre_type',
			'label'     => esc_html('Preloader types', 'nt-forester' ),
			'desc'      => esc_html('Please choose a preloader type', 'nt-forester' ),
			'std'       => 'default',
			'type'      => 'select',
			'section'   => 'pre',
			'condition' => 'nt_forester_pre:is(on)',
			'operator'  => 'and',
			'choices'   => array(
			array(
				'value' => 'default',
				'label' => esc_html('Default', 'nt-forester' ),
			),
			array(
				'value' => '01',
				'label' => esc_html('Type 1', 'nt-forester' ),
			),
			array(
				'value' => '02',
				'label' => esc_html('Type 2', 'nt-forester' ),
			),
			array(
				'value' => '03',
				'label' => esc_html('Type 3', 'nt-forester' ),
			),
			array(
				'value' => '04',
				'label' => esc_html('Type 4', 'nt-forester' ),
			),
			array(
				'value' => '05',
				'label' => esc_html('Type 5', 'nt-forester' ),
			),
			array(
				'value' => '06',
				'label' => esc_html('Type 6', 'nt-forester' ),
			),
			array(
				'value' => '07',
				'label' => esc_html('Type 7', 'nt-forester' ),
			),
			array(
				'value' => '08',
				'label' => esc_html('Type 8', 'nt-forester' ),
			),
			array(
				'value' => '09',
				'label' => esc_html('Type 9', 'nt-forester' ),
			),
			array(
				'value' => '10',
				'label' => esc_html('Type 10', 'nt-forester' ),
			),
			array(
				'value' => '11',
				'label' => esc_html('Type 11', 'nt-forester' ),
			),
			array(
				'value' => '12',
				'label' => esc_html('Type 12', 'nt-forester' ),
			),
		  )
		),
		array(
			'id'        => 'nt_forester_prebgcolor',
			'label'     => esc_html( 'Preloader BG color ', 'nt-forester' ),
			'desc'      => esc_html( 'Please select color', 'nt-forester' ),
			'type'      => 'colorpicker-opacity',
			'condition' => 'nt_forester_pre:is(on)',
			'section'   => 'pre'
		),
		array(
			'id'        => 'nt_forester_prespincolor',
			'label'     => esc_html( 'Preloader spin color ', 'nt-forester' ),
			'desc'      => esc_html( 'Please select color', 'nt-forester' ),
			'type'      => 'colorpicker-opacity',
			'condition' => 'nt_forester_pre:is(on)',
			'section'   => 'pre'
		),

		// LOGO OPTIONS
		array(
			'id'          => 'nt_forester_logo_type',
			'label'       => esc_html__('Logo Type', 'nt-forester' ),
			'desc'        => esc_html__('Choose logo type', 'nt-forester' ),
			'std'         => 'img',
			'type'        => 'select',
			'section'     => 'logo',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'img',
					'label'	=> esc_html__('Image Logo', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'text',
					'label'	=> esc_html__('Text Logo', 'nt-forester' ),
					'src'	=> ''
				)
			)
		),
		array(
			'id'          => 'nt_forester_textlogo',
			'label'       => esc_html__('Text logo', 'nt-forester' ),
			'desc'        => esc_html__('Text logo', 'nt-forester' ),
			'std'         => 'forester',
			'type'        => 'text',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'section'     => 'logo'
		),
		array(
			'id'          => 'nt_forester_img_staticlogo',
			'label'       => esc_html__( 'Upload static logo image', 'nt-forester' ),
			'desc'        => esc_html__( 'Upload logo image', 'nt-forester' ),
			'type'        => 'upload',
			'condition'   => 'nt_forester_logo_type:is(img)',
			'section'     => 'logo'
		),
		array(
			'id'          => 'nt_forester_img_stickylogo',
			'label'       => esc_html__( 'Upload sticky logo image', 'nt-forester' ),
			'desc'        => esc_html__( 'Upload logo image', 'nt-forester' ),
			'type'        => 'upload',
			'condition'   => 'nt_forester_logo_type:is(img)',
			'section'     => 'logo'
		),
		array(
			'id'          => 'nt_forester_logo_dimension',
			'label'       => esc_html__( 'Logo dimension', 'nt-forester' ),
			'desc'        => esc_html__( 'Logo dimension', 'nt-forester' ),
			'type'        => 'dimension',
			'condition'   => 'nt_forester_logo_type:is(img)',
			'section'     => 'logo',
		),
		array(
			'id'          => 'nt_forester_staticlogo_color',
			'label'       => esc_html__( 'Static text logo color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color for static menu text logo', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'section'     => 'logo'
		),
		array(
			'id'          => 'nt_forester_stickylogo_color',
			'label'       => esc_html__( 'Sticky text logo color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color for sticky menu text logo', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'section'     => 'logo'
		),
		array(
			'id'          => 'nt_forester_textlogo_fs',
			'label'       => esc_html__('Font size', 'nt-forester' ),
			'desc'        => esc_html__('Font size for text logo', 'nt-forester' ),
			'std'         => '19',
			'type'        => 'numeric-slider',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'min_max_step'=> '0,100',
			'section'     => 'logo',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_textlogo_fw',
			'label'       => esc_html__('Font weight', 'nt-forester' ),
			'desc'        => esc_html__('Font weight for text logo', 'nt-forester' ),
			'std'         => '700',
			'type'        => 'numeric-slider',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'min_max_step'=> '100,900,100',
			'section'     => 'logo',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_textlogo_lettersp',
			'label'       => esc_html__('Letter spacing', 'nt-forester' ),
			'desc'        => esc_html__('Letter spacing for text logo', 'nt-forester' ),
			'std'         => '0',
			'type'        => 'numeric-slider',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'min_max_step'=> '0,10',
			'section'     => 'logo',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_textlogo_fstyle',
			'label'       => esc_html__('Font style', 'nt-forester' ),
			'desc'        => esc_html__('Choose font style for text logo', 'nt-forester' ),
			'std'         => 'normal',
			'type'        => 'select',
			'section'     => 'logo',
			'condition'   => 'nt_forester_logo_type:is(text)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'normal',
					'label'	=> esc_html__('Normal', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'italic',
					'label'	=> esc_html__('Italic', 'nt-forester' ),
					'src'	=> ''
				)
			)
		),
		array(
			'id'          => 'nt_forester_padding_logo',
			'label'       => esc_html__( 'Logo padding', 'nt-forester' ),
			'desc'        => esc_html__( 'Logo padding', 'nt-forester' ),
			'type'        => 'spacing',
			'section'     => 'logo',
		),
		array(
			'id'          => 'nt_forester_margin_logo',
			'label'       => esc_html__( 'Logo margin', 'nt-forester' ),
			'desc'        => esc_html__( 'Logo margin', 'nt-forester' ),
			'type'        => 'spacing',
			'section'     => 'logo',
		),
		//CPT
		array(
			'id'          => 'nt_forester_cpt1',
			'label'       => esc_html__('CPT Portfolio slug name', 'nt-forester' ),
			'desc'        => esc_html__('Add your CPT slug name.Default slug name is portfolio', 'nt-forester' ),
			'std'         => '',
			'type'        => 'text',
			'section'     => 'cpt'
		),
		array(
			'id'          => 'nt_forester_cpt2',
			'label'       => esc_html__('CPT Team slug name', 'nt-forester' ),
			'desc'        => esc_html__('Add your CPT slug name.Default slug name is team', 'nt-forester' ),
			'std'         => '',
			'type'        => 'text',
			'section'     => 'cpt'
		),
		array(
			'id'          => 'nt_forester_cpt3',
			'label'       => esc_html__('CPT Price slug name', 'nt-forester' ),
			'desc'        => esc_html__('Add your CPT slug name.Default slug name is price', 'nt-forester' ),
			'std'         => '',
			'type'        => 'text',
			'section'     => 'cpt'
		),
		// NAVIGATION SETTINGS
		//static nav

		// nav smoothscroll offset
		array(
			'id'          => 'nt_forester_nav_gen_tab',
			'label'       => esc_html__( 'Navigation Settings', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'navigation'
		),

      array(
         'id'          => 'nt_forester_menu_layout_type',
         'label'       => esc_html__('Header top type', 'nt-forester' ),
         'desc'        => esc_html__('This option for menu container types', 'nt-forester' ),
         'std'         => '2',
         'type'        => 'select',
         'section'     => 'navigation',
         'operator'    => 'and',
         'choices'     => array(
            array(
               'value'	=> '1',
               'label'	=> esc_html__('Logo left + Navigation', 'nt-forester' ),
               'src'	=> ''
            ),
            array(
               'value'	=> '2',
               'label'	=> esc_html__('Logo center  + Navigation', 'nt-forester' ),
               'src'	=> ''
            ),
         )
      ),
		// nav smoothscroll offset
		array(
			'id'          => 'nt_forester_navoffset_tab',
			'label'       => esc_html__( 'Navigation Offset', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_custom_navoffset',
			'label'       => esc_html__('Smooth Scroll to Anchor', 'nt-forester' ),
			'desc'        => esc_html__('This option for smooth scroll to anchor.If you have given a link on the page, you can set the navigation distance with this option.Default offset 50', 'nt-forester' ),
			'std'         => '50',
			'type'        => 'numeric-slider',
			'min_max_step'=> '-200,500',
			'section'     => 'navigation',
			'operator'    => 'and'
		),
		//static nav
		array(
			'id'          => 'nt_forester_staticnav_tab',
			'label'       => esc_html__( 'Static Navigation', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_nav_bg',
			'label'       => esc_html__( 'Theme navigation background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_navitem',
			'label'       => esc_html__( 'Theme navigation menu item color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_navitemhover',
			'label'       => esc_html__( 'Theme navigation menu item hover color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_navborder',
			'label'       => esc_html__( 'Theme static menu border bottom color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_navspacing',
			'label'       => esc_html__( 'Theme static menu padding', 'nt-forester' ),
			'desc'        => esc_html__( 'The Spacing option type is used to set spacing values such as padding or margin in the form of top, right, bottom, and left.', 'nt-forester' ),
			'std'         => '',
			'type'        => 'spacing',
			'section'     => 'navigation',
		),
		//sticky nav
		array(
			'id'          => 'nt_forester_stickynav_tab',
			'label'       => esc_html__( 'Sticky Navigation', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_nav_fixed_display',
			'label'       => esc_html( 'Sticky menu on-off', 'nt-forester' ),
			'desc'        => sprintf( esc_html( 'Sticky menu display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'navigation',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_nav_fixed_bg',
			'label'       => esc_html__( 'Theme sticky navigation background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_nav_fixed_display:is(on)',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_fixed_navitem',
			'label'       => esc_html__( 'Theme sticky menu item color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_nav_fixed_display:is(on)',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_fixed_navitemhover',
			'label'       => esc_html__( 'Theme sticky menu item hover color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_nav_fixed_display:is(on)',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_fixed_navborder',
			'label'       => esc_html__( 'Theme sticky menu border bottom color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_nav_fixed_display:is(on)',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_stickyspacing',
			'label'       => esc_html__( 'Theme sticky menu padding', 'nt-forester' ),
			'desc'        => esc_html__( 'The Spacing option type is used to set spacing values such as padding or margin in the form of top, right, bottom, and left.', 'nt-forester' ),
			'std'         => '',
			'type'        => 'spacing',
			'condition'   => 'nt_forester_nav_fixed_display:is(on)',
			'section'     => 'navigation',
		),
		//dropdown sub menu
		array(
			'id'          => 'nt_forester_dropdownnav_tab',
			'label'       => esc_html__( 'Dropdown Style', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_dropdown_bg',
			'label'       => esc_html__( 'Dropdown menu background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_dropdown_item',
			'label'       => esc_html__( 'Dropdown menu item color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_dropdown_itemhover',
			'label'       => esc_html__( 'Dropdown menu item hover color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_dropdown_itemhoverbg',
			'label'       => esc_html__( 'Dropdown menu hover and active item background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		//mobile sub menu
		array(
			'id'          => 'nt_forester_mobilenav_tab',
			'label'       => esc_html__( 'Mobile Navigation', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_menubtnbghover',
			'label'       => esc_html__( 'Mobile menu open and close button color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_bg',
			'label'       => esc_html__( 'Mobile menu background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_item',
			'label'       => esc_html__( 'Mobile menu item color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_itemhover',
			'label'       => esc_html__( 'Mobile menu item hover color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_itemhoverbg',
			'label'       => esc_html__( 'Mobile menu hover and active item background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_dropitem',
			'label'       => esc_html__( 'Mobile menu dropdown item color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),
		array(
			'id'          => 'nt_forester_mobile_dropitem_hover',
			'label'       => esc_html__( 'Mobile menu dropdown item hover color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'navigation'
		),

		// SIDEBAR TYPE SETTINGS
		array(
			'id'          => 'nt_forester_blog_layout',
			'label'       => esc_html__( 'Blog layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose blog page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_layout',
			'label'       => esc_html__( 'Search page Layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose search page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_post_layout',
			'label'       => esc_html__( 'Blog single page layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose post page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_layout',
			'label'       => esc_html__( 'archive page layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose archive page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_404_layout',
			'label'       => esc_html__( '404 page layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose 404 page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		array(
			'id'          => 'woosingle_layout',
			'label'       => esc_html__( 'Woocommerce single page layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose Woocommerce single page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		   array(
			'id'          => 'woopage_layout',
			'label'       => esc_html__( 'Woocommerce page layout', 'nt-forester' ),
			'desc'        => esc_html__( 'Choose Woocommerce page layout type', 'nt-forester' ),
			'std'         => 'right-sidebar',
			'type'        => 'radio-image',
			'section'     => 'sidebars',
			'operator'    => 'and'
		),
		/**  SIDEBAR SETTINGS.  **/
		array(
			'id'          => 'nt_forester_sidebarwidgetareabgcolor',
			'label'       => esc_html__('Theme Sidebar widget area background color', 'nt-forester' ),
			'desc'        => esc_html__('Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'sidebars_settings',
			'operator'    => 'and'
		),
	    array(
			'id'          => 'nt_forester_sidebarwidgetgeneralcolor',
			'label'       => esc_html__( 'Theme Sidebar widget general color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker',
			'section'     => 'sidebars_settings'
		),
		array(
			'id'          => 'nt_forester_sidebarwidgettitlecolor',
			'label'       => esc_html__( 'Theme Sidebar widget title color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker',
			'section'     => 'sidebars_settings'
		),
 		array(
			'id'          => 'nt_forester_sidebarlinkcolor',
			'label'       => esc_html__( 'Theme Sidebar link title color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker',
			'section'     => 'sidebars_settings'
		),
 		array(
			'id'          => 'nt_forester_sidebarlinkhovercolor',
			'label'       => esc_html__( 'Theme Sidebar link title hover color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker',
			'section'     => 'sidebars_settings'
		),
  		array(
			'id'          => 'nt_forester_sidebarsearchsubmittextcolor',
			'label'       => esc_html__( 'Theme Sidebar search submit text color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker',
			'section'     => 'sidebars_settings'
		),
      	array(
			'id'          => 'nt_forester_sidebarsearchsubmitbgcolor',
			'label'       => esc_html__( 'Theme Sidebar search submit background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker',
			'section'     => 'sidebars_settings'
		),
		// BLOG/PAGE HEADER SETTINGS
		array(
			'id'          => 'nt_forester_blog_general_tab',
			'label'       =>  esc_html__( 'Blog General', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'blogpage',
		),
		array(
			'id'          => 'nt_forester_blog_style',
			'label'       => esc_html__('Blog style', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page post style', 'nt-forester' ),
			'std'         => 'normal',
			'type'        => 'select',
			'section'     => 'blogpage',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'masonry',
					'label'	=> esc_html__('Masonry ( no-sidebar )', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'masonry-sidebar',
					'label'	=> esc_html__('Modern with sidebar', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'normal',
					'label'	=> esc_html__('Classic', 'nt-forester' ),
					'src'	=> ''
				),
			)
		),
		array(
			'id'          => 'nt_forester_blog_masonry_custom_margin',
			'label'       => esc_html__( 'Blog post custom margin', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter blog post custom margin', 'nt-forester' ),
			'type'        => 'spacing',
			'condition'   => 'nt_forester_blog_style:is(masonry-sidebar)',
			'section'     => 'blogpage',
		),
		array(
			'id'          => 'nt_forester_blog_masonry_container_style',
			'label'       => esc_html__('Container style', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page masonry container style', 'nt-forester' ),
			'std'         => 'grid',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'boxed',
					'label'	=> esc_html__('Boxed', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'fullwidth',
					'label'	=> esc_html__('Fullwidth', 'nt-forester' ),
					'src'	=> ''
				),
			)
		),
		array(
			'id'          => 'nt_forester_blog_masonrycolumn',
			'label'       => esc_html__('Masonry column', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( '%s is only compatible with the current %s style. If you want to use %s, please select the %s from the %s selectbox.', 'nt-forester' ), '<code>4 column and 5 column</code>', '<code>Fullwidth style</code>', '<code>4 column or 5 column</code>', '<code>Fullwidth mode</code>', '<code>Container style</code>' ),
			'std'         => '2',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> '2',
					'label'	=> esc_html__('2 column', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '3',
					'label'	=> esc_html__('3 column', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '4',
					'label'	=> esc_html__('4 column', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '5',
					'label'	=> esc_html__('5 column', 'nt-forester' ),
					'src'	=> ''
				)
			)
		),
		array(
			'id'          => 'nt_forester_masonry_layoutmode',
			'label'       => esc_html__('Layout mode', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page masonry layout mode', 'nt-forester' ),
			'std'         => 'grid',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'grid',
					'label'	=> esc_html__('Masonry-grid', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'mosaic',
					'label'	=> esc_html__('Mosaic', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'slider',
					'label'	=> esc_html__('Slider', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'custom',
					'label'	=> esc_html__('Grid Custom', 'nt-forester' ),
					'src'	=> ''
				)
			)
		),
		array(
			'id'          => 'nt_forester_masonry_img_w',
			'label'       => esc_html__( 'Post image custom width', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'To trim, enter the blog page post image custom width.Please use only numbers.This option is only compatible with the blog post %s. If you want to use other post format, please select the options outside %s from the %s selectbox.', 'nt-forester' ), '<code>standart format</code>', '<code>Grid custom</code>', '<code>Layout mode</code>' ),
			'std'         => '750',
			'type'        => 'text',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:is(custom)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_masonry_img_h',
			'label'       => esc_html__( 'Post image custom height', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'To trim, enter the blog page post image custom heightPlease use only numbers.This option is only compatible with the blog post %s. If you want to use other post format, please select the options outside %s from the %s selectbox.', 'nt-forester' ), '<code>standart format</code>', '<code>custom</code>', '<code>Layout mode</code>' ),
			'std'         => '750',
			'type'        => 'text',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:is(custom)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_masonry_content_limit',
			'label'       => esc_html__( 'Post content text limit', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter number for post content text character limit or use post excerpt content area.Default limit is 300 character.', 'nt-forester' ),
			'std'         => '300',
			'type'        => 'text',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:is(custom)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_blog_masonry_slider_navdisplay',
			'label'       => esc_html__( 'Slider navigation display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select blog post page masonry style slider navigation display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_masonry_layoutmode:is(slider)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_blog_masonry_filter_display',
			'label'       => esc_html__( 'Filter display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select blog post page masonry style filter display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:not(slider)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_blog_masonry_showcounter',
			'label'       => esc_html__( 'Filter counter display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select blog post page masonry style filter counter display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:not(slider),nt_forester_blog_masonry_filter_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_masonry_animationfilter',
			'label'       => esc_html__('Animation Filter', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page masonry column', 'nt-forester' ),
			'std'         => 'fadeOut',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:not(slider),nt_forester_blog_masonry_filter_display:is(on)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'fadeOut',
					'label'	=> esc_html__('fadeOut', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'quicksand',
					'label'	=> esc_html__('quicksand', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'bounceLeft',
					'label'	=> esc_html__('bounceLeft', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'bounceTop',
					'label'	=> esc_html__('bounceTop', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'bounceBottom',
					'label'	=> esc_html__('bounceBottom', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'moveLeft',
					'label'	=> esc_html__('moveLeft', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'slideLeft',
					'label'	=> esc_html__('slideLeft', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'fadeOutTop',
					'label'	=> esc_html__('fadeOutTop', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'sequentially',
					'label'	=> esc_html__('sequentially', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'skew',
					'label'	=> esc_html__('skew', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'slideDelay',
					'label'	=> esc_html__('slideDelay', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'rotateSides',
					'label'	=> esc_html__('rotateSides', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'flipOutDelay',
					'label'	=> esc_html__('flipOutDelay', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'flipOut',
					'label'	=> esc_html__('flipOut', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'unfold',
					'label'	=> esc_html__('unfold', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'foldLeft',
					'label'	=> esc_html__('foldLeft', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'scaleDown',
					'label'	=> esc_html__('scaleDown', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'scaleSides',
					'label'	=> esc_html__('scaleSides', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'frontRow',
					'label'	=> esc_html__('frontRow', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'flipBottom',
					'label'	=> esc_html__('flipBottom', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'rotateRoom',
					'label'	=> esc_html__('rotateRoom', 'nt-forester' ),
					'src'	=> ''
				),
			)
		),
		array(
			'id'          => 'nt_forester_masonry_gridadjustment',
			'label'       => esc_html__('Grid adjustment', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page masonry grid adjustment', 'nt-forester' ),
			'std'         => 'default',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry),nt_forester_masonry_layoutmode:not(slider)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'default',
					'label'	=> esc_html__('default', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'alignCenter',
					'label'	=> esc_html__('alignCenter', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'responsive',
					'label'	=> esc_html__('responsive', 'nt-forester' ),
					'src'	=> ''
				)
			)
		),
		array(
			'id'          => 'nt_forester_masonry_itemhorizontalgap',
			'label'       => esc_html__('Post horizontal gap', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page masonry post horizontal gap', 'nt-forester' ),
			'std'         => '30',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> '1',
					'label'	=> esc_html__('1 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '5',
					'label'	=> esc_html__('5 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '10',
					'label'	=> esc_html__('10 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '15',
					'label'	=> esc_html__('15 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '20',
					'label'	=> esc_html__('20 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '25',
					'label'	=> esc_html__('25 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '30',
					'label'	=> esc_html__('30 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '35',
					'label'	=> esc_html__('35 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '40',
					'label'	=> esc_html__('40 px', 'nt-forester' ),
					'src'	=> ''
				),
			)
		),
		array(
			'id'          => 'nt_forester_masonry_itemverticalgap',
			'label'       => esc_html__('Post vertical gap', 'nt-forester' ),
			'desc'        => esc_html__('Choose blog page masonry post vertical gap', 'nt-forester' ),
			'std'         => '30',
			'type'        => 'select',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_style:is(masonry)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> '1',
					'label'	=> esc_html__('1 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '5',
					'label'	=> esc_html__('5 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '10',
					'label'	=> esc_html__('10 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '15',
					'label'	=> esc_html__('15 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '20',
					'label'	=> esc_html__('20 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '25',
					'label'	=> esc_html__('25 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '30',
					'label'	=> esc_html__('30 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '35',
					'label'	=> esc_html__('35 px', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> '40',
					'label'	=> esc_html__('40 px', 'nt-forester' ),
					'src'	=> ''
				),
			)
		),

		// blog hero
		array(
			'id'          => 'nt_forester_blog_hero_tab',
			'label'       =>  esc_html__( 'Blog Hero', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'blogpage',
		),
		array(
			'id'          => 'nt_forester_blog_hero_display',
			'label'       => esc_html__( 'Blog post pages hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select blog post page hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'blogpage',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_blog_hero_info',
			'label'       => esc_html__( 'Blog post pages hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_blog_headerbg',
			'label'       =>  esc_html__( 'Blog pages hero section background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for header', 'nt-forester' ),
			'type'        => 'upload',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_blog_header_bgcolor',
			'label'       => esc_html__( 'Blog page hero section background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_blog_hero_display:is(on)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_blog_header_bgheight',
			'label'       => esc_html__('Blog pages hero section height', 'nt-forester' ),
			'desc'        => esc_html__('Blog pages hero section height', 'nt-forester' ),
			'std'         => '60',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_blog_h_p',
			'label'       => esc_html__( 'Blog hero section padding', 'nt-forester' ),
			'desc'        => esc_html__( 'Blog hero section padding', 'nt-forester' ),
			'type'        => 'spacing',
			'condition'   => 'nt_forester_blog_hero_display:is(on)',
			'section'     => 'blogpage',
		),
		//blog heading
		array(
			'id'          => 'nt_forester_blog_heading_tab',
			'label'       =>  esc_html__( 'Blog Heading', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'blogpage',
		),
		array(
			'id'          => 'nt_forester_blog_heading_display',
			'label'       => esc_html__( 'Blog post pages heading display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select blog post page heading display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'blogpage',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_heading_info',
			'label'       => esc_html__( 'Blog page heading is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_heading_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_blog_heading',
			'label'       => esc_html__( 'Blog page heading', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter blog page heading', 'nt-forester' ),
			'std'         => 'BLOG',
			'type'        => 'text',
			'condition'   => 'nt_forester_blog_heading_display:is(on)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_blog_heading_color',
			'label'       => esc_html__( 'Blog pages heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_blog_heading_display:is(on)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_blog_heading_fontsize',
			'label'       => esc_html__('Blog heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter Blog heading font size', 'nt-forester' ),
			'std'         => '72',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,200',
			'condition'   => 'nt_forester_blog_heading_display:is(on)',
			'section'     => 'blogpage',
			'operator'    => 'and'
		),
		//blog slogan
		array(
			'id'          => 'nt_forester_blog_slogan_tab',
			'label'       =>  esc_html__( 'Blog Slogan', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'blogpage',
		),
		array(
			'id'          => 'nt_forester_blog_slogan_display',
			'label'       => esc_html__( 'Blog post page slogan display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select blog post page slogan display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'blogpage',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_slogan_info',
			'label'       => esc_html__( 'Blog Slogan is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'blogpage',
			'condition'   => 'nt_forester_blog_slogan_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_blog_slogan',
			'label'       => esc_html__( 'Blog page slogan', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter blog page slogan', 'nt-forester' ),
			'std'         => 'Just another WordPress site',
			'type'        => 'text',
			'condition'   => 'nt_forester_blog_slogan_display:is(on)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_blog_slogan_color',
			'label'       => esc_html__( 'Blog pages slogan color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_blog_slogan_display:is(on)',
			'section'     => 'blogpage'
		),
		array(
			'id'          => 'nt_forester_blog_slogan_fontsize',
			'label'       => esc_html__('Blog slogan font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter blog slogan font size', 'nt-forester' ),
			'std'         => '16',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'condition'   => 'nt_forester_blog_slogan_display:is(on)',
			'section'     => 'blogpage',
			'operator'    => 'and'
		),

		//SINGLE HEADER SETTINGS
		array(
			'id'          => 'nt_forester_post_content_style',
			'label'       => esc_html__('Single style', 'nt-forester' ),
			'desc'        => esc_html__('Choose single page content style', 'nt-forester' ),
			'std'         => 'style1',
			'type'        => 'select',
			'section'     => 'single_header',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'	=> 'style1',
					'label'	=> esc_html__('Style 1', 'nt-forester' ),
					'src'	=> ''
				),
				array(
					'value'	=> 'style2',
					'label'	=> esc_html__('Style 2', 'nt-forester' ),
					'src'	=> ''
				)
			)
		),
		array(
			'id'          => 'nt_forester_single_hero_tab',
			'label'       => esc_html__( 'Single Hero', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'single_header',
		),
		array(
			'id'          => 'nt_forester_single_hero_display',
			'label'       => esc_html__( 'Single hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select single hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'single_header',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_blog_hero_info',
			'label'       => esc_html__( 'Single page hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'single_header',
			'condition'   => 'nt_forester_single_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_single_headbg',
			'label'       =>  esc_html__( 'Single hero section background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for parallax hero', 'nt-forester' ),
			'type'        => 'upload',
			'section'     => 'single_header',
			'condition'   => 'nt_forester_single_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_single_header_bgcolor',
			'label'       => esc_html__( 'Single page hero section background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_single_hero_display:is(on)',
			'section'     => 'single_header'
		),
		array(
			'id'          => 'nt_forester_single_header_bgheight',
			'label'       => esc_html__('Single pages hero height', 'nt-forester' ),
			'desc'        => esc_html__('Single pages hero height', 'nt-forester' ),
			'std'         => '60',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'section'     => 'single_header',
			'condition'   => 'nt_forester_single_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_single_header_paddingtop',
			'label'       => esc_html__('Hero section padding top', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'single_header',
			'condition'   => 'nt_forester_single_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_single_header_paddingbottom',
			'label'       => esc_html__('Hero section padding bottom', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'single_header',
			'condition'   => 'nt_forester_single_hero_display:is(on)',
			'operator'    => 'and'
		),
		//Single heading
		array(
			'id'          => 'nt_forester_single_heading_tab',
			'label'       => esc_html__( 'Single Heading', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'single_header',
		),
		array(
			'id'          => 'nt_forester_single_heading_display',
			'label'       => esc_html__( 'Single page heading post title display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select single page heading post title display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'single_header',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_single_heading_info',
			'label'       => esc_html__( 'Single page hero section post title is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'single_header',
			'condition'   => 'nt_forester_single_heading_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_single_headingcolor',
			'label'       => esc_html__( 'Single page heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_single_heading_display:is(on)',
			'section'     => 'single_header'
		),
		array(
			'id'          => 'nt_forester_single_heading_fontsize',
			'label'       => esc_html__('Single heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter Single heading font size', 'nt-forester' ),
			'std'         => '72',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,200',
			'condition'   => 'nt_forester_single_heading_display:is(on)',
			'section'     => 'single_header',
			'operator'    => 'and'
		),

		/**  SINGLE GALLERY HEADER SETTINGS.  **/
		//Single Gallery header Image
		array(
			'id'          => 'nt_forester_single_port_hero_display',
			'label'       => esc_html__( 'Single portfolio hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select single hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'single_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_single_port_hero_info',
			'label'       => esc_html__( 'Single portfolio page hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'single_port',
			'condition'   => 'nt_forester_single_port_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_singleport_headbg',
			'label'       =>  esc_html__( 'Single Gallery hero background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for hero background', 'nt-forester' ),
			'type'        => 'upload',
			'section'     => 'single_port',
			'condition'   => 'nt_forester_single_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_singleport_header_bgcolor',
			'label'       => esc_html__( 'Single page hero section background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_single_port_hero_display:is(on)',
			'section'     => 'single_port'
		),
		array(
			'id'          => 'nt_forester_singleport_header_bgheight',
			'label'       => esc_html__('Single page hero height', 'nt-forester' ),
			'desc'        => esc_html__('Single page hero height', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'std'         => '60',
			'section'     => 'single_port',
			'condition'   => 'nt_forester_single_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_singleport_header_paddingtop',
			'label'       => esc_html__('Hero section padding top', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'std'         => '70',
			'min_max_step'=> '0,500',
			'section'     => 'single_port',
			'condition'   => 'nt_forester_single_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_singleport_header_paddingbottom',
			'label'       => esc_html__('Hero section padding bottom', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'std'         => '70',
			'min_max_step'=> '0,500',
			'section'     => 'single_port',
			'condition'   => 'nt_forester_single_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		//Single Gallery heading
		array(
			'id'          => 'nt_forester_singleport_heading_display',
			'label'       => esc_html__( 'Single page heading post title display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select single page heading post title display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'single_port',
			'condition'   => 'nt_forester_single_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_singleport_heading_color',
			'label'       => esc_html__( 'Single page heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_single_port_hero_display:is(on),nt_forester_singleport_heading_display:is(on)',
			'section'     => 'single_port'
		),
		array(
			'id'          => 'nt_forester_singleport_heading_fontsize',
			'label'       => esc_html__('Single heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter Single heading font size', 'nt-forester' ),
			'std'         => '72',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,200',
			'condition'   => 'nt_forester_single_port_hero_display:is(on),nt_forester_singleport_heading_display:is(on)',
			'section'     => 'single_port',
			'operator'    => 'and'
		),
		// ARCHIVE HEADER SETTINGS
		array(
			'id'          => 'nt_forester_archive_hero_tab',
			'label'       => esc_html__( 'Archive Hero', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'archive_page',
		),
		array(
			'id'          => 'nt_forester_archive_hero_display',
			'label'       => esc_html__( 'Archive hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select archive hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'archive_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_hero_info',
			'label'       => esc_html__( '404 page hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'archive_page',
			'condition'   => 'nt_forester_archive_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_archive_headbg',
			'label'       =>  esc_html__( 'Archive hero section background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for parallax hero', 'nt-forester' ),
			'type'        => 'upload',
			'condition'   => 'nt_forester_archive_hero_display:is(on)',
			'section'     => 'archive_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_header_bgcolor',
			'label'       => esc_html__( 'Archive page hero section background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_archive_hero_display:is(on)',
			'section'     => 'archive_page'
		),
		array(
			'id'          => 'nt_forester_archive_header_bgheight',
			'label'       => esc_html__('Archive page hero height', 'nt-forester' ),
			'desc'        => esc_html__('Archive page hero height', 'nt-forester' ),
			'std'         => '60',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'section'     => 'archive_page',
			'condition'   => 'nt_forester_archive_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_header_paddingtop',
			'label'       => esc_html__('Hero section padding top', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'archive_page',
			'condition'   => 'nt_forester_archive_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_header_paddingbottom',
			'label'       => esc_html__('Hero section padding bottom', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'archive_page',
			'condition'   => 'nt_forester_archive_hero_display:is(on)',
			'operator'    => 'and'
		),
		//Archive heading
		array(
			'id'          => 'nt_forester_archive_heading_tab',
			'label'       => esc_html__( 'Archive Heading', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'archive_page',
		),
		array(
			'id'          => 'nt_forester_archive_heading_display',
			'label'       => esc_html__( 'Archive heading display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Archive heading display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'archive_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_heading_info',
			'label'       => esc_html__( 'Archive heading is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'archive_page',
			'condition'   => 'nt_forester_archive_heading_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_archive_heading',
			'label'       => esc_html__( 'Archive heading', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter Archive heading', 'nt-forester' ),
			'std'         => 'Our Archive',
			'type'        => 'text',
			'condition'   => 'nt_forester_archive_heading_display:is(on)',
			'section'     => 'archive_page'
		),
		array(
			'id'          => 'nt_forester_archive_heading_fontsize',
			'label'       => esc_html__('Archive heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter Archive heading font size', 'nt-forester' ),
			'std'         => '72',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,200',
			'condition'   => 'nt_forester_archive_heading_display:is(on)',
			'section'     => 'archive_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_heading_color',
			'label'       => esc_html__( 'Archive heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_archive_heading_display:is(on)',
			'section'     => 'archive_page'
		),
		//Archive Slogan
		array(
			'id'          => 'nt_forester_archive_slogan_tab',
			'label'       => esc_html__( 'Archive Slogan', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'archive_page',
		),
		array(
			'id'          => 'nt_forester_archive_slogan_display',
			'label'       => esc_html__( 'Archive slogan display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Archive slogan display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'archive_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_slogan_info',
			'label'       => esc_html__( 'Archive slogan is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'archive_page',
			'condition'   => 'nt_forester_archive_slogan_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_archive_slogan',
			'label'       => esc_html__( 'Archive slogan', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter Archive slogan', 'nt-forester' ),
			'std'         => 'Welcom to our archive',
			'type'        => 'text',
			'condition'   => 'nt_forester_archive_slogan_display:is(on)',
			'section'     => 'archive_page'
		),
		array(
			'id'          => 'nt_forester_archive_slogan_fontsize',
			'label'       => esc_html__('Archive page slogan font-size', 'nt-forester' ),
			'desc'        => esc_html__('Enter archive slogan font-size', 'nt-forester' ),
			'std'         => '16',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'condition'   => 'nt_forester_archive_slogan_display:is(on)',
			'section'     => 'archive_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_slogan_color',
			'label'       => esc_html__( 'Archive slogan color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_archive_slogan_display:is(on)',
			'section'     => 'archive_page'
		),
		// ARCHIVE PORTFOLIO HEADER SETTINGS
		array(
			'id'          => 'nt_forester_archive_port_hero_tab',
			'label'       => esc_html__( 'Archive Hero', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'archive_port',
		),
		array(
			'id'          => 'nt_forester_archive_port_hero_display',
			'label'       => esc_html__( 'Archive hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select archive hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'archive_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_hero_info',
			'label'       => esc_html__( '404 page hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'archive_port',
			'condition'   => 'nt_forester_archive_port_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_archive_port_headbg',
			'label'       =>  esc_html__( 'Archive hero section background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for parallax hero', 'nt-forester' ),
			'type'        => 'upload',
			'condition'   => 'nt_forester_archive_port_hero_display:is(on)',
			'section'     => 'archive_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_header_bgcolor',
			'label'       => esc_html__( 'Archive page hero section background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_archive_port_hero_display:is(on)',
			'section'     => 'archive_port'
		),
		array(
			'id'          => 'nt_forester_archive_port_header_bgheight',
			'label'       => esc_html__('Archive page hero height', 'nt-forester' ),
			'desc'        => esc_html__('Archive page hero height', 'nt-forester' ),
			'std'         => '60',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'section'     => 'archive_port',
			'condition'   => 'nt_forester_archive_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_header_paddingtop',
			'label'       => esc_html__('Hero section padding top', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'archive_port',
			'condition'   => 'nt_forester_archive_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_header_paddingbottom',
			'label'       => esc_html__('Hero section padding bottom', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'archive_port',
			'condition'   => 'nt_forester_archive_port_hero_display:is(on)',
			'operator'    => 'and'
		),
		//Archive heading
		array(
			'id'          => 'nt_forester_archive_port_heading_tab',
			'label'       => esc_html__( 'Archive Heading', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'archive_port',
		),
		array(
			'id'          => 'nt_forester_archive_port_heading_display',
			'label'       => esc_html__( 'Archive heading display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Archive heading display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'archive_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_heading_info',
			'label'       => esc_html__( 'Archive heading is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'archive_port',
			'condition'   => 'nt_forester_archive_port_heading_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_archive_port_heading',
			'label'       => esc_html__( 'Archive heading', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter Archive heading', 'nt-forester' ),
			'std'         => 'Our Archive',
			'type'        => 'text',
			'condition'   => 'nt_forester_archive_port_heading_display:is(on)',
			'section'     => 'archive_port'
		),
		array(
			'id'          => 'nt_forester_archive_port_heading_fontsize',
			'label'       => esc_html__('Archive heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter Archive heading font size', 'nt-forester' ),
			'std'         => '72',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,200',
			'condition'   => 'nt_forester_archive_port_heading_display:is(on)',
			'section'     => 'archive_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_heading_color',
			'label'       => esc_html__( 'Archive heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_archive_port_heading_display:is(on)',
			'section'     => 'archive_port'
		),
		//Archive Slogan
		array(
			'id'          => 'nt_forester_archive_port_slogan_tab',
			'label'       => esc_html__( 'Archive Slogan', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'archive_port',
		),
		array(
			'id'          => 'nt_forester_archive_port_slogan_display',
			'label'       => esc_html__( 'Archive slogan display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Archive slogan display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'archive_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_slogan_info',
			'label'       => esc_html__( 'Archive slogan is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'archive_port',
			'condition'   => 'nt_forester_archive_port_slogan_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_archive_port_slogan',
			'label'       => esc_html__( 'Archive slogan', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter Archive slogan', 'nt-forester' ),
			'std'         => 'Welcom to our archive',
			'type'        => 'text',
			'condition'   => 'nt_forester_archive_port_slogan_display:is(on)',
			'section'     => 'archive_port'
		),
		array(
			'id'          => 'nt_forester_archive_port_slogan_fontsize',
			'label'       => esc_html__('Archive page slogan font-size', 'nt-forester' ),
			'desc'        => esc_html__('Enter archive slogan font-size', 'nt-forester' ),
			'std'         => '16',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'condition'   => 'nt_forester_archive_port_slogan_display:is(on)',
			'section'     => 'archive_port',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_archive_port_slogan_color',
			'label'       => esc_html__( 'Archive slogan color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_archive_port_slogan_display:is(on)',
			'section'     => 'archive_port'
		),
		// 404 PAGE HERO SETTINGS
		array(
			'id'          => 'nt_forester_error_hero_tab',
			'label'       => esc_html__( '404 Hero', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'error_page',
		),
		array(
			'id'          => 'nt_forester_error_hero_display',
			'label'       => esc_html__( '404 page hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select 404 page hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'error_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_hero_info',
			'label'       => esc_html__( '404 page hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_error_headbg',
			'label'       =>  esc_html__( '404 page hero section background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for parallax hero', 'nt-forester' ),
			'type'        => 'upload',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_header_bgcolor',
			'label'       => esc_html__( '404 page hero section background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_error_hero_display:is(on)',
			'section'     => 'error_page'
		),
		array(
			'id'          => 'nt_forester_error_header_bgheight',
			'label'       => esc_html__('404 page hero height', 'nt-forester' ),
			'desc'        => esc_html__('404 page hero height', 'nt-forester' ),
			'std'         => '60',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_header_paddingtop',
			'label'       => esc_html__('Hero section padding top', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_header_paddingbottom',
			'label'       => esc_html__('Hero section padding bottom', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_hero_display:is(on)',
			'operator'    => 'and'
		),
		//404 heading
		array(
			'id'          => 'nt_forester_error_heading_tab',
			'label'       => esc_html__( '404 Heading', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'error_page',
		),
		array(
			'id'          => 'nt_forester_error_heading_display',
			'label'       => esc_html__( '404 page heading display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Error 404 page heading display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'error_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_heading_info',
			'label'       => esc_html__( '404 page heading is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_heading_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_error_heading',
			'label'       => esc_html__( '404 page heading', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter Error heading', 'nt-forester' ),
			'std'         => '404 page',
			'type'        => 'text',
			'condition'   => 'nt_forester_error_heading_display:is(on)',
			'section'     => 'error_page'
		),
		array(
			'id'          => 'nt_forester_error_heading_fontsize',
			'label'       => esc_html__('404 page heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter 404 page heading font size', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'std'		  => '72',
			'min_max_step'=> '0,200',
			'condition'   => 'nt_forester_error_heading_display:is(on)',
			'section'     => 'error_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_heading_color',
			'label'       => esc_html__( '404 page heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_error_heading_display:is(on)',
			'section'     => 'error_page'
		),
		//404 slogan
		array(
			'id'          => 'nt_forester_error_slogan_tab',
			'label'       => esc_html__( '404 Slogan', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'error_page',
		),
		array(
			'id'          => 'nt_forester_error_slogan_display',
			'label'       => esc_html__( '404 page slogan display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( '404 page slogan display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'error_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_slogan_info',
			'label'       => esc_html__( '404 page slogan is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'error_page',
			'condition'   => 'nt_forester_error_slogan_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_error_slogan',
			'label'       => esc_html__( '404 page Slogan', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter 404 page slogan', 'nt-forester' ),
			'std'         => 'Oops! That page can not be found.',
			'type'        => 'text',
			'condition'   => 'nt_forester_error_slogan_display:is(on)',
			'section'     => 'error_page'
		),
		array(
			'id'          => 'nt_forester_error_slogan_fontsize',
			'label'       => esc_html__('404 page slogan font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter 404 page slogan font size', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'std'		  => '16',
			'condition'   => 'nt_forester_error_slogan_display:is(on)',
			'section'     => 'error_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_error_slogan_color',
			'label'       => esc_html__( '404 page slogan color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_error_slogan_display:is(on)',
			'section'     => 'error_page'
		),
		// SEARCH PAGE HEADER SETTINGS
		array(
			'id'          => 'nt_forester_search_hero_tab',
			'label'       => esc_html__( 'Search Hero', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'search_page',
		),
		array(
			'id'          => 'nt_forester_search_hero_display',
			'label'       => esc_html__( 'Search page hero section display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Please select 404 page hero section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'search_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_hero_info',
			'label'       => esc_html__( 'Search page hero section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_hero_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_search_headbg',
			'label'       =>  esc_html__( 'Search hero section background image', 'nt-forester' ),
			'desc'        =>  esc_html__( 'You can upload your image for parallax hero', 'nt-forester' ),
			'type'        => 'upload',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_header_bgcolor',
			'label'       => esc_html__( 'Search page hero section background color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'search_page'
		),
		array(
			'id'          => 'nt_forester_search_header_bgheight',
			'label'       => esc_html__('Search page hero height', 'nt-forester' ),
			'desc'        => esc_html__('Search page hero height', 'nt-forester' ),
			'std'         => '60',
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_header_paddingtop',
			'label'       => esc_html__('Hero section padding top', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_hero_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_header_paddingbottom',
			'label'       => esc_html__('Hero section padding bottom', 'nt-forester' ),
			'desc'        => esc_html__('You can use this option for heading text vertical align', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,500',
			'std'		  => '70',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_hero_display:is(on)',
			'operator'    => 'and'
		),
		//Search heading
		array(
			'id'          => 'nt_forester_search_heading_tab',
			'label'       => esc_html__( 'Search Heading', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'search_page',
		),
		array(
			'id'          => 'nt_forester_search_heading_display',
			'label'       => esc_html__( 'Search page heading display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Search page heading display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'search_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_heading_info',
			'label'       => esc_html__( 'Search page heading is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_heading_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_search_heading',
			'label'       => esc_html__( 'Search page heading', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter search heading', 'nt-forester' ),
			'std'         => 'Search page',
			'type'        => 'text',
			'condition'   => 'nt_forester_search_heading_display:is(on)',
			'section'     => 'search_page'
		),
		array(
			'id'          => 'nt_forester_search_heading_fontsize',
			'label'       => esc_html__('Search page heading font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter search page heading font size', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,200',
			'std'		  => '72',
			'condition'   => 'nt_forester_search_heading_display:is(on)',
			'section'     => 'search_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_heading_color',
			'label'       => esc_html__( 'Search page heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_search_heading_display:is(on)',
			'section'     => 'search_page'
		),
		//Search Slogan
		array(
			'id'          => 'nt_forester_search_slogan_tab',
			'label'       => esc_html__( 'Search Slogan', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'search_page',
		),
		array(
			'id'          => 'nt_forester_search_slogan_display',
			'label'       => esc_html__( 'Search page slogan display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Search page slogan display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'search_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_slogan_info',
			'label'       => esc_html__( 'Search page slogan is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'search_page',
			'condition'   => 'nt_forester_search_slogan_display:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_search_slogan',
			'label'       => esc_html__( 'Search page slogan', 'nt-forester' ),
			'desc'        => esc_html__( 'Enter search page Slogan', 'nt-forester' ),
			'std'         => 'Search completed',
			'type'        => 'text',
			'condition'   => 'nt_forester_search_slogan_display:is(on)',
			'section'     => 'search_page'
		),
		array(
			'id'          => 'nt_forester_search_slogan_fontsize',
			'label'       => esc_html__('Search page slogan font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter search page slogan font size', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'min_max_step'=> '0,100',
			'std'		  => '16',
			'condition'   => 'nt_forester_search_slogan_display:is(on)',
			'section'     => 'search_page',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_search_slogan_color',
			'label'       => esc_html__( 'Search page slogan color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_search_slogan_display:is(on)',
			'section'     => 'search_page'
		),
		// BREADCRUBMS SETTINGS.
		array(
			'id'          => 'nt_forester_bread',
			'label'       => esc_html__( 'Breadcrubms display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Breadcrubms display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'breadcrubms',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_bread_info',
			'label'       => esc_html__( 'Breadcrubms is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'breadcrubms',
			'condition'   => 'nt_forester_bread:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_breadcrubms_color',
			'label'       => esc_html__( 'Blog pages Breadcrubms color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_bread:is(on)',
			'section'     => 'breadcrubms'
		),
		array(
			'id'          => 'nt_forester_breadcrubms_hovercolor',
			'label'       => esc_html__( 'Breadcrubms hover color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_bread:is(on)',
			'section'     => 'breadcrubms'
		),
		array(
			'id'          => 'nt_forester_breadcrubms_currentcolor',
			'label'       => esc_html__( 'Breadcrubms current page text color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_bread:is(on)',
			'section'     => 'breadcrubms'
		),
		array(
			'id'          => 'nt_forester_breadcrubms_font_size',
			'label'       => esc_html__('Breadcrubms font size', 'nt-forester' ),
			'desc'        => esc_html__('Enter breadcrubms font size', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'std'		  => '11',
			'min_max_step'=> '0,100',
			'condition'   => 'nt_forester_bread:is(on)',
			'section'     => 'breadcrubms',
			'operator'    => 'and'
		),

		// WIDGETIZE FOOTER SETTINGS
		array(
			'id'          => 'nt_forester_widgetize',
			'label'       => esc_html__( 'Footer widgetize area section', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Choose footer widgetize section %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'off',
			'type'        => 'on-off',
			'section'     => 'footer_widgetize',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_widgetize_info',
			'label'       => esc_html__( 'Footer widgetize area section is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'footer_widgetize',
			'condition'   => 'nt_forester_widgetize:is(off)',
			'operator'    => 'or'
		),
		array(
			'id'          => 'nt_forester_fw_bg_img',
			'label'       => esc_html__( 'Footer widgetize background image', 'nt-forester' ),
			'desc'        => esc_html__( 'Upload background image', 'nt-forester' ),
			'type'        => 'upload',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_bg_img_parallax',
			'label'       => esc_html__( 'Footer widgetize background image parallax effect display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Choose footer widgetize background image parallax effect display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'off',
			'type'        => 'on-off',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_fw_bg_overlay',
			'label'       => esc_html__( 'Footer widgetize background image overlay color display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Choose footer widgetize background image overlay color display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'off',
			'type'        => 'on-off',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_fw_bg_overlaycolor',
			'label'       => esc_html__( 'Footer widgetize background image overlay color', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color and use opacity for bg image', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_widgetize:is(on),nt_forester_fw_bg_overlay:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_bg_c',
			'label'       => esc_html__( 'Footer widgetize background color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_h_c',
			'label'       => esc_html__( 'Footer widget heading color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_t_c',
			'label'       => esc_html__( 'Footer general text color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_a_c',
			'label'       => esc_html__( 'Footer general a(link/URL) color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_a_hc',
			'label'       => esc_html__( 'Footer general a(link/URL) hover color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize'
		),
		array(
			'id'          => 'nt_forester_fw_pad_top',
			'label'       => esc_html__('Footer widgetize padding top', 'nt-forester' ),
			'desc'        => esc_html__('Enter padding top for widgetize footer', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'std'		  => '70',
			'min_max_step'=> '0,250',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_fw_pad_bot',
			'label'       => esc_html__('Footer widgetize padding botom', 'nt-forester' ),
			'desc'        => esc_html__('Enter padding botom for widgetize footer', 'nt-forester' ),
			'type'        => 'numeric-slider',
			'std'		  => '40',
			'min_max_step'=> '0,250',
			'condition'   => 'nt_forester_widgetize:is(on)',
			'section'     => 'footer_widgetize',
			'operator'    => 'and'
		),
      // nt_forester_footer_copyright
      array(
         'id'          => 'nt_forester_footer_general',
         'label'       => esc_html__( 'Footer General', 'nt-forester' ),
         'type'        => 'tab',
         'section'     => 'main_footer',
      ),
		// FOOTER COPYRIGHT
		// footer_contact_display
		array(
			'id'          => 'nt_forester_footer_display',
			'label'       => esc_html__( 'Footer display', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Main footer display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'main_footer',
			'operator'    => 'and'
		),
      array(
			'id'          => 'nt_forester_footer_type',
			'label'       => esc_html__( 'Footer type', 'nt-forester' ),
			'desc'        => esc_html__( 'Select social media target type. Default : _blank' , 'nt-forester' ),
			'std'         => '_blank',
			'type'        => 'select',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(on)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'       => '1',
					'label'       => esc_html__( '1 White', 'nt-forester' )
				),
				array(
					'value'       => '2',
					'label'       => esc_html__( '2 White', 'nt-forester' )
				),
				array(
					'value'       => '3',
					'label'       => esc_html__( '3 Black', 'nt-forester' )
				),
				array(
					'value'       => '4',
					'label'       => esc_html__( '4 Black', 'nt-forester' )
				),
			)
		),
		array(
			'id'          => 'nt_forester_footer_info',
			'label'       => esc_html__( 'Footer is off', 'nt-forester' ),
			'type'        => 'textblock-titled',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(off)',
			'operator'    => 'or'
		),
      // nt_forester_social_tab
		array(
			'id'          => 'nt_forester_social_tab',
			'label'       => esc_html__( 'Social Icons', 'nt-forester' ),
			'type'        => 'tab',
			'section'     => 'main_footer'
		),
		// footer_social_display
		array(
			'id'          => 'nt_forester_footer_social_display',
			'label'       => esc_html__( 'Footer social display ', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Choose social section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'condition'   => 'nt_forester_footer_display:is(on)',
			'section'     => 'main_footer',
			'operator'    => 'and'
		),

		array(
			'id'          => 'nt_forester_footer_social',
			'label'       => esc_html__( 'Footer Social Icons', 'nt-forester' ),
			'desc'        => esc_html__( 'Add social media icons', 'nt-forester' ),
			'type'        => 'list-item',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_social_display:is(on)',
			'settings'    => array(
				array(
					'id'          => 'nt_forester_footer_social_text',
					'label'       => esc_html__( 'Social icon name', 'nt-forester' ),
					'desc'        => esc_html__( 'Enter icon name. example : fa fa-facebook', 'nt-forester' ),
					'type'        => 'text'
				),
				array(
					'id'          => 'nt_forester_footer_social_title',
					'label'       => esc_html__( 'Social icon title', 'nt-forester' ),
					'desc'        => esc_html__( 'Enter social title. example : Facebook', 'nt-forester' ),
					'type'        => 'text'
				),
				array(
					'id'          => 'nt_forester_footer_social_link',
					'label'       => esc_html__( 'URL', 'nt-forester' ),
					'desc'        => esc_html__( 'Enter a url for social media', 'nt-forester' ),
					'type'        => 'text'
				),
			)
		),
		array(
			'id'          => 'nt_forester_footer_social_target',
			'label'       => esc_html__( 'Target social media', 'nt-forester' ),
			'desc'        => esc_html__( 'Select social media target type. Default : _blank' , 'nt-forester' ),
			'std'         => '_blank',
			'type'        => 'select',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_social_display:is(on)',
			'operator'    => 'and',
			'choices'     => array(
				array(
					'value'       => '_blank',
					'label'       => esc_html__( '_blank', 'nt-forester' )
				),
				array(
					'value'       => '_self',
					'label'       => esc_html__( '_self', 'nt-forester' )
				),
				array(
					'value'       => '_parent',
					'label'       => esc_html__( '_parent', 'nt-forester' )
				),
				array(
					'value'       => '_top',
					'label'       => esc_html__( '_top', 'nt-forester' )
				),
			)
		),
      array(
         'id'          => 'nt_forester_footer_s_c',
         'label'       => esc_html__( 'Footer social color ', 'nt-forester' ),
         'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
         'type'        => 'colorpicker-opacity',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_social_display:is(on)',
      ),
      array(
         'id'          => 'nt_forester_footer_s_hc',
         'label'       => esc_html__( 'Footer social hover color ', 'nt-forester' ),
         'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
         'type'        => 'colorpicker-opacity',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_social_display:is(on)',
      ),

      // nt_forester_footer_menu_tab
      array(
         'id'          => 'nt_forester_footer_menu_tab',
         'label'       => esc_html__( 'Footer menu', 'nt-forester' ),
         'type'        => 'tab',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_type:is(2)',
      ),
      // nt_forester_footer_menu_display
      array(
         'id'          => 'nt_forester_footer_menu_display',
         'label'       => esc_html__( 'Footer menu display ', 'nt-forester' ),
         'desc'        => sprintf( esc_html__( 'Choose footer menu section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
         'std'         => 'on',
         'type'        => 'on-off',
         'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_type:not(1)',
         'section'     => 'main_footer',
         'operator'    => 'and'
      ),

      array(
         'id'          => 'nt_forester_footer_menu',
         'label'       => esc_html__( 'Footer menu', 'nt-forester' ),
         'desc'        => esc_html__( 'Add menu items', 'nt-forester' ),
         'type'        => 'list-item',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_menu_display:is(on),nt_forester_footer_type:not(1)',
         'settings'    => array(
            array(
               'id'          => 'nt_forester_footer_menu_title',
               'label'       => esc_html__( 'Footer menu title', 'nt-forester' ),
               'desc'        => esc_html__( 'Enter social title. example : Facebook', 'nt-forester' ),
               'type'        => 'text'
            ),
            array(
               'id'          => 'nt_forester_footer_menu_link',
               'label'       => esc_html__( 'Footer menu item URL', 'nt-forester' ),
               'desc'        => esc_html__( 'Enter a url for footer menu', 'nt-forester' ),
               'type'        => 'text'
            ),
         )
      ),
      array(
         'id'          => 'nt_forester_footer_menu_target',
         'label'       => esc_html__( 'Target type for footer menu links ', 'nt-forester' ),
         'desc'        => esc_html__( 'Select menu target type. Default : _blank' , 'nt-forester' ),
         'std'         => '_blank',
         'type'        => 'select',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_menu_display:is(on),nt_forester_footer_type:not(1)',
         'operator'    => 'and',
         'choices'     => array(
            array(
               'value'       => '_blank',
               'label'       => esc_html__( '_blank', 'nt-forester' )
            ),
            array(
               'value'       => '_self',
               'label'       => esc_html__( '_self', 'nt-forester' )
            ),
            array(
               'value'       => '_parent',
               'label'       => esc_html__( '_parent', 'nt-forester' )
            ),
            array(
               'value'       => '_top',
               'label'       => esc_html__( '_top', 'nt-forester' )
            ),
         )
      ),
      // nt_forester_footer_info_tab
      array(
         'id'          => 'nt_forester_footer_info_tab',
         'label'       => esc_html__( 'Footer info', 'nt-forester' ),
         'type'        => 'tab',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on)',
      ),
      // header contact
      array(
         'id'          => 'nt_forester_footer_contact_info_display',
         'label'       => esc_html__( 'Footer top contact display', 'nt-forester' ),
         'desc'        => sprintf( esc_html__( 'Choose contact section display %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
         'std'         => 'on',
         'type'        => 'on-off',
         'condition'   => 'nt_forester_footer_display:is(on)',
         'section'     => 'main_footer',
         'operator'    => 'and'
      ),
		array(
			'id'          => 'nt_forester_footer_phone',
			'label'       => esc_html__('Phone', 'nt-forester' ),
			'desc'        => esc_html__('Add your mail', 'nt-forester' ),
			'std'         => '053558797444',
			'type'        => 'text',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
			'section'     => 'main_footer'
		),
		array(
			'id'          => 'nt_forester_footer_phone_i',
			'label'       => esc_html__('Phone icon', 'nt-forester' ),
			'desc'        => esc_html__('You can use any font icon, default: fa fa-phone ( or leave blank )', 'nt-forester' ),
			'type'        => 'text',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
			'section'     => 'main_footer'
		),
		array(
			'id'          => 'nt_forester_footer_mail',
			'label'       => esc_html__('Mail', 'nt-forester' ),
			'desc'        => esc_html__('Add your mail', 'nt-forester' ),
			'std'         => 'info@saulsforester.com',
			'type'        => 'text',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
			'section'     => 'main_footer'
		),
		array(
			'id'          => 'nt_forester_footer_mail_i',
			'label'       => esc_html__('Mail icon', 'nt-forester' ),
			'desc'        => esc_html__('You can use any font icon, default: fa fa-envelope-o ( or leave blank )', 'nt-forester' ),
			'type'        => 'text',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
			'section'     => 'main_footer'
		),
		array(
			'id'          => 'nt_forester_footer_address',
			'label'       => esc_html__('Contact address', 'nt-forester' ),
			'desc'        => esc_html__('Add your address', 'nt-forester' ),
			'std'         => '3 Byron Ave, Yonkers NY',
			'type'        => 'text',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
			'section'     => 'main_footer'
		),
		array(
			'id'          => 'nt_forester_footer_address_i',
			'label'       => esc_html__('Contact address icon', 'nt-forester' ),
			'desc'        => esc_html__('You can use any font icon, default: fa fa-home ( or leave blank )', 'nt-forester' ),
			'type'        => 'text',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
			'section'     => 'main_footer'
		),
		array(
			'id'          => 'nt_forester_f_add_c',
			'label'       => esc_html__( 'Footer address mail info color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_footer_contact_info_display:is(on)',
		),
      // nt_forester_footer_copyright
      array(
         'id'          => 'nt_forester_footer_copyright',
         'label'       => esc_html__( 'Footer powered', 'nt-forester' ),
         'type'        => 'tab',
         'section'     => 'main_footer',
         'condition'   => 'nt_forester_footer_display:is(on)',
      ),
		//copyright_display
		array(
			'id'          => 'nt_forester_copyright_display',
			'label'       => esc_html__( 'footer copyright section', 'nt-forester' ),
			'desc'        => sprintf( esc_html__( 'Choose footer copyright section %s or %s.', 'nt-forester' ), '<code>on</code>', '<code>off</code>' ),
			'std'         => 'on',
			'type'        => 'on-off',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(on)',
			'operator'    => 'and'
		),
		array(
			'id'          => 'nt_forester_copyright',
			'label'       => 'Footer copyright',
			'std'         => '2017 forester. All Rights Reserved.',
			'type'        => 'textarea',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_copyright_display:is(on)',
			'section'     => 'main_footer'
		),

		// FOOTER COPYRIGHT COLOR SETTINGS
		array(
			'id'          => 'nt_forester_footer_p_c',
			'label'       => esc_html__( 'Footer copyright text color ', 'nt-forester' ),
			'desc'        => esc_html__( 'Please select color', 'nt-forester' ),
			'type'        => 'colorpicker-opacity',
			'section'     => 'main_footer',
			'condition'   => 'nt_forester_footer_display:is(on),nt_forester_copyright_display:is(on)',
		),

	) // end array
);

// end function
	/* allow settings to be filtered before saving */
	$nt_forester_custom_settings = apply_filters( ot_settings_id() . '_args', $nt_forester_custom_settings );
	/* settings are not the same update the DB */
	if ( $nt_forester_saved_settings !== $nt_forester_custom_settings ) {
		update_option( ot_settings_id(), $nt_forester_custom_settings );
	}
	/* Lets OptionTree know the UI Builder is being overridden */
	global $ot_has_custom_theme_options;
	$ot_has_custom_theme_options = true;
	}
