<?php

/*-----------------------------------------------------------------------------------*/
/*	Shortcode Filter
/*-----------------------------------------------------------------------------------*/

vc_set_as_theme( $disable_updater = false );
vc_is_updater_disabled();


function nt_forester_vc_remove_woocommerce() {
    if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        vc_remove_element( 'woocommerce_cart' );
        vc_remove_element( 'woocommerce_checkout' );
        vc_remove_element( 'woocommerce_order_tracking' );
        vc_remove_element( 'woocommerce_my_account' );
        vc_remove_element( 'recent_products' );
        vc_remove_element( 'featured_products' );
        vc_remove_element( 'product' );
        vc_remove_element( 'products' );
        vc_remove_element( 'add_to_cart' );
        vc_remove_element( 'add_to_cart_url' );
        vc_remove_element( 'product_page' );
        vc_remove_element( 'product_category' );
        vc_remove_element( 'product_categories' );
        vc_remove_element( 'sale_products' );
        vc_remove_element( 'best_selling_products' );
        vc_remove_element( 'top_rated_products' );
        vc_remove_element( 'product_attribute' );
        vc_remove_element( 'related_products' );
    }
}
// Hook for admin editor.
add_action( 'vc_build_admin_page', 'nt_forester_vc_remove_woocommerce', 11 );
// Hook for frontend editor.
add_action( 'vc_load_shortcode', 'nt_forester_vc_remove_woocommerce', 11 );


vc_remove_element(  'vc_wp_search');
vc_remove_element(  'vc_wp_meta' );
vc_remove_element(  'vc_wp_recentcomments' );
vc_remove_element(  'vc_wp_calendar' );
vc_remove_element(  'vc_wp_pages' );
vc_remove_element(  'vc_wp_tagcloud' );
vc_remove_element(  'vc_wp_custommenu' );
vc_remove_element(  'vc_wp_text' );
vc_remove_element(  'vc_wp_posts' );
vc_remove_element(  'vc_wp_categories' );
vc_remove_element(  'vc_wp_archives' );
vc_remove_element(  'vc_wp_rss' );
vc_remove_element(  'vc_flickr' );
vc_remove_element(  'vc_facebook' );
vc_remove_element(  'vc_tweetmeme' );
vc_remove_element(  'vc_googleplus' );
vc_remove_element(  'vc_pinterest' );



//FOR ROW EXTRA ATTR
$nt_forester_background_one_attributes = array(

	array(
		'type'        	=> 'dropdown',
		'heading'     	=> esc_html__('Background position Y-X axis', 'nt-forester' ),
		'param_name'  	=> 'nt_forester_bg_position',
		'description' 	=> esc_html__('Change position Y-X axis for bg image.', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'value'       	=> array(
			esc_html__('Select Y-X position', 	'nt-forester' )	=> '',
			esc_html__('Left-Top', 		'nt-forester' )	=> 'left top',
			esc_html__('Left-Center', 	'nt-forester' )	=> 'left center',
			esc_html__('Left-Bottom', 	'nt-forester' )	=> 'left bottom',
			esc_html__('Right-Top', 	'nt-forester' )	=> 'right top',
			esc_html__('Right-Center', 	'nt-forester' )	=> 'right center',
			esc_html__('Right-Bottom', 	'nt-forester' )	=> 'right bottom',
			esc_html__('Center-Top', 	'nt-forester' )	=> 'center top',
			esc_html__('Center-Center', 'nt-forester' )	=> 'center center',
			esc_html__('Center-Bottom', 'nt-forester' )	=> 'center bottom',
			esc_html__('Custom', 		'nt-forester' )	=> 'custom',
		),
	),

	array(
		'type' 			=> 'textfield',
		'heading' 		=> esc_html__('Background position Y axis', 'nt-forester'),
		'param_name' 	=> 'nt_forester_bg_positiony',
		'description' 	=> esc_html__('Change position X axis offset for bg image.example: center or 40px or 25% ...etc', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'dependency' 	=> array(
						'element' 	=> 'nt_forester_bg_position',
						'value'   	=> 'custom'
		)
	),
	array(
		'type' 			=> 'textfield',
		'heading' 		=> esc_html__('Background position X axis', 'nt-forester'),
		'param_name' 	=> 'nt_forester_bg_positionx',
		'description' 	=> esc_html__('Change position X axis offset for bg image.example: center or 40px or 25% ...etc', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'dependency' 	=> array(
						'element' 	=> 'nt_forester_bg_position',
						'value'   	=> 'custom'
		)
	),
	array(
		'type' 			=> 'textfield',
		'heading' 		=> esc_html__('Background position X axis offset', 'nt-forester'),
		'param_name' 	=> 'nt_forester_bg_xoffset',
		'description' 	=> esc_html__('Change position X axis offset for bg image.example: 40px or 25% ...etc', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'dependency' 	=> array(
						'element' 	=> 'nt_forester_bg_position',
						'value'   	=> 'custom'
		)
	),
	array(
		'type' 			=> 'textfield',
		'heading' 		=> esc_html__('Background size', 'nt-forester'),
		'param_name' 	=> 'nt_forester_bg_size',
		'description' 	=> esc_html__('Change size for bg image.example: 100px or 100% or 100vh', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
	),
	array(
		'type'        	=> 'dropdown',
		'heading'     	=> esc_html__('Background image display ( under 992px )', 'nt-forester' ),
		'param_name'  	=> 'nt_forester_bg_ontablet',
		'description' 	=> esc_html__('You can select show or hide background image under 992px', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'value'       	=> array(
			esc_html__('Select options', 'nt-forester' )	=> '',
			esc_html__('Show', 	'nt-forester' )	=> 'show',
			esc_html__('Hide', 	'nt-forester' )	=> 'hide',
		),
	),
	array(
		'type'        	=> 'dropdown',
		'heading'     	=> esc_html__('Background image display ( under 768px )', 'nt-forester' ),
		'param_name'  	=> 'nt_forester_bg_onmobile',
		'description' 	=> esc_html__('You can select show or hide background image under 768px', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'value'       	=> array(
			esc_html__('Select options', 'nt-forester' )	=> '',
			esc_html__('Show', 	'nt-forester' )	=> 'show',
			esc_html__('Hide', 	'nt-forester' )	=> 'hide',
		),
	),
	array(
		'type'        	=> 'dropdown',
		'heading'     	=> esc_html__('Background image display ( under 480px )', 'nt-forester' ),
		'param_name'  	=> 'nt_forester_bg_onphone',
		'description' 	=> esc_html__('You can select show or hide background image under 480px', 'nt-forester'),
		'group' 		=> esc_html__('Design Options','nt-forester'),
		'value'       	=> array(
			esc_html__('Select options', 'nt-forester' )	=> '',
			esc_html__('Show', 	'nt-forester' )	=> 'show',
			esc_html__('Hide', 	'nt-forester' )	=> 'hide',
		),
	),
);
vc_add_params( 'vc_row', $nt_forester_background_one_attributes );


//FOR ROW 480 RESOLUTION
$nt_forester_vc_row_responsive_attributes = array(

	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 992px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_row_992_responsive',
		'description' 	=> esc_html__( 'These options for 992px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),
	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 768px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_row_768_responsive',
		'description' 	=> esc_html__( 'These options for 768px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),
	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 480px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_row_480_responsive',
		'description' 	=> esc_html__( 'These options for 480px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),

);
vc_add_params( 'vc_row', $nt_forester_vc_row_responsive_attributes );


//FOR ROW EXTRA 3 OVERLAY ATTR
$nt_forester_row_overlay_attributes = array(

		//OVERLAY 1
		array(
			'type'			=> 'colorpicker',
			'heading'		=> esc_html__('Overlay Color', 'nt-forester'),
			'param_name'	=> 'overlaybg',
			'description'	=> esc_html__('Add color.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Width', 'nt-forester'),
			'param_name'	=> 'overlaywidth',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Height', 'nt-forester'),
			'param_name'	=> 'overlayheight',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Top offset', 'nt-forester'),
			'param_name'	=> 'overlaytop',
			'description'	=> esc_html__('Add Top offset for top position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Left offset', 'nt-forester'),
			'param_name'	=> 'overlayleft',
			'description'	=> esc_html__('Add left offset for left position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Right offset', 'nt-forester'),
			'param_name'	=> 'overlayright',
			'description'	=> esc_html__('Add right offset for right position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Bottom offset', 'nt-forester'),
			'param_name'	=> 'overlaybottom',
			'description'	=> esc_html__('Add bottom offset for bottom position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 1','nt-forester'),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 992px', 'nt-forester' ),
			'param_name'	=> 'overlay992',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 992px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Overlay 1', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 768px', 'nt-forester' ),
			'param_name'	=> 'overlay768',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 768px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Overlay 1', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 480px', 'nt-forester' ),
			'param_name'	=> 'overlay480',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 480px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Overlay 1', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		//OVERLAY 2
		array(
			'type'			=> 'colorpicker',
			'heading'		=> esc_html__('Overlay Color', 'nt-forester'),
			'param_name'	=> 'overlaybg2',
			'description'	=> esc_html__('Add color.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Width', 'nt-forester'),
			'param_name'	=> 'overlaywidth2',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Height', 'nt-forester'),
			'param_name'	=> 'overlayheight2',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Top offset', 'nt-forester'),
			'param_name'	=> 'overlaytop2',
			'description'	=> esc_html__('Add Top offset for top position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Left offset', 'nt-forester'),
			'param_name'	=> 'overlayleft2',
			'description'	=> esc_html__('Add left offset for left position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Right offset', 'nt-forester'),
			'param_name'	=> 'overlayright2',
			'description'	=> esc_html__('Add right offset for right position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Bottom offset', 'nt-forester'),
			'param_name'	=> 'overlaybottom2',
			'description'	=> esc_html__('Add bottom offset for bottom position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Overlay 2','nt-forester'),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 992px', 'nt-forester' ),
			'param_name'	=> 'overlay2_992',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 992px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Overlay 2', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 768px', 'nt-forester' ),
			'param_name'	=> 'overlay2_768',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 768px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Overlay 2', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 480px', 'nt-forester' ),
			'param_name'	=> 'overlay2_480',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 480px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Overlay 2', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),


);
vc_add_params( 'vc_row', $nt_forester_row_overlay_attributes );

//FOR COLUMN
$nt_forester_vc_column_responsive_attributes = array(

	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 992px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_column_992',
		'description' 	=> esc_html__( 'These options for 992px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),
	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 768px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_column_768',
		'description' 	=> esc_html__( 'These options for 768px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),
	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 480px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_column_480',
		'description' 	=> esc_html__( 'These options for 480px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),

);
vc_add_params( 'vc_column', $nt_forester_vc_column_responsive_attributes );

//FOR COLUMN INNER
$nt_forester_vc_column_inner_responsive_attributes = array(

	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 992px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_colinner_992',
		'description' 	=> esc_html__( 'These options for 992px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),
	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 768px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_colinner_768',
		'description' 	=> esc_html__( 'These options for 768px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),
	array(
		'type' 			=> 'css_editor',
		'heading' 		=> esc_html__( 'Max width 480px resolution', 'nt-forester' ),
		'param_name' 	=> 'nt_forester_vc_colinner_480',
		'description' 	=> esc_html__( 'These options for 480px resolution - responsive medya', 'nt-forester' ),
		'group' 		=> esc_html__('Responsive Extra','nt-forester'),
	),

);
vc_add_params( 'vc_column_inner', $nt_forester_vc_column_inner_responsive_attributes );

//FOR COLUMN INNER
$nt_forester_vc_single_img_attributes = array(

		//overlay image
		array(
			'type'			=> 'colorpicker',
			'heading'		=> esc_html__('Overlay Color', 'nt-forester'),
			'param_name'	=> 'ntbgcolor',
			'description'	=> esc_html__('Add color.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('z-index', 'nt-forester'),
			'param_name'	=> 'zindex',
			'description'	=> esc_html__('Add z-index. example: 1', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Width', 'nt-forester'),
			'param_name'	=> 'ntwidth',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Height', 'nt-forester'),
			'param_name'	=> 'ntheight',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Top offset', 'nt-forester'),
			'param_name'	=> 'nttop',
			'description'	=> esc_html__('Add Top offset for top position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Left offset', 'nt-forester'),
			'param_name'	=> 'ntleft',
			'description'	=> esc_html__('Add left offset for left position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Right offset', 'nt-forester'),
			'param_name'	=> 'ntright',
			'description'	=> esc_html__('Add right offset for right position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Bottom offset', 'nt-forester'),
			'param_name'	=> 'ntbottom',
			'description'	=> esc_html__('Add bottom offset for bottom position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 1','nt-forester'),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 992px', 'nt-forester' ),
			'param_name'	=> 'ntres992',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 992px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Extras 1', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 768px', 'nt-forester' ),
			'param_name'	=> 'ntres768',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 768px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Extras 1', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 480px', 'nt-forester' ),
			'param_name'	=> 'ntres480',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 480px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Extras 1', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		//underlay image
		array(
			'type'			=> 'colorpicker',
			'heading'		=> esc_html__('Underlay Color', 'nt-forester'),
			'param_name'	=> 'ntbgcolor2',
			'description'	=> esc_html__('Add color.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('z-index', 'nt-forester'),
			'param_name'	=> 'ntzindex2',
			'description'	=> esc_html__('Add z-index. example: 0, -1, 1', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Width', 'nt-forester'),
			'param_name'	=> 'ntwidth2',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Height', 'nt-forester'),
			'param_name'	=> 'ntheight2',
			'description'	=> esc_html__('Add width.example:100% or 75%....etc.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Top offset', 'nt-forester'),
			'param_name'	=> 'nttop2',
			'description'	=> esc_html__('Add Top offset for top position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Left offset', 'nt-forester'),
			'param_name'	=> 'ntleft2',
			'description'	=> esc_html__('Add left offset for left position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Right offset', 'nt-forester'),
			'param_name'	=> 'ntright2',
			'description'	=> esc_html__('Add right offset for right position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__('Bottom offset', 'nt-forester'),
			'param_name'	=> 'ntbottom2',
			'description'	=> esc_html__('Add bottom offset for bottom position.example:10px or 10%.', 'nt-forester'),
			'group' 		=> esc_html__('Extras 2','nt-forester'),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 992px', 'nt-forester' ),
			'param_name'	=> 'ntres992_u',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 992px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Extras 2', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 768px', 'nt-forester' ),
			'param_name'	=> 'ntres768_u',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 768px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Extras 2', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__('Display under 480px', 'nt-forester' ),
			'param_name'	=> 'ntres480_u',
			'description'	=> esc_html__('You can select show or hide overlay and underlay color maximum device width 480px', 'nt-forester' ),
			'group' 	  	=> esc_html__('Extras 2', 'nt-forester' ),
			'value'			=> array(
				esc_html__('Select position', 'nt-forester' )	=> '',
				esc_html__('Show', 	'nt-forester' )	=> 'show',
				esc_html__('Hide', 	'nt-forester' )	=> 'hide',
			),
		),
);
vc_add_params( 'vc_single_image', $nt_forester_vc_single_img_attributes );

/*-----------------------------------------------------------------------------------*/
/*	HERO PARALLAX forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_hero1_integrateWithVC' );
function nt_forester_hero1_integrateWithVC() {
   vc_map( array(
		'name' 	   => esc_html__( 'Hero Parallax', 'nt-forester' ),
		'base' 	   => 'nt_forester_section_hero1',
		'category' => esc_html__( 'NT forester', 'nt-forester'),
		'params'   => array(
			array(
				'type' 		  	=> 'attach_image',
				'heading' 	  	=> esc_html__('BG image', 'nt-forester'),
				'param_name'  	=> 'bgimg',
				'description' 	=> esc_html__('Add your background image icon', 'nt-forester'),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Top title', 'nt-forester'),
				'param_name' 	  => 'toptitle',
				'description' 	  => esc_html__('Add slider top title for item.', 'nt-forester'),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add slider title for item.', 'nt-forester'),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Detail', 'nt-forester'),
				'param_name' 	  => 'desc',
				'description' 	  => esc_html__('Add description or text block for slider item.', 'nt-forester'),
			),
			//button
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__('Button (Link)', 'nt-forester' ),
				'param_name'    => 'btnlink',
				'description'   => esc_html__('Add button title and link.', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button link type', 'nt-forester' ),
				'param_name'	=> 'linktype',
				'description'	=> esc_html__('You can select button link type', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select type', 	'nt-forester' )	=> '',
					esc_html__('Internal link', 'nt-forester' )	=> 'internal',
					esc_html__('External link', 'nt-forester' )	=> 'external',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button style', 'nt-forester' ),
				'param_name'	=> 'btnstyle',
				'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 	'nt-forester' )	=> '',
					esc_html__('Primary', 	 'nt-forester' )	=> 'btn-primary',
					esc_html__('Ghost white','nt-forester' )	=> 'btn-ghost-white',
					esc_html__('Ghost black','nt-forester' )	=> 'btn-ghost-black',
					esc_html__('Black button','nt-forester' )	=> 'btn-black',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button size', 'nt-forester' ),
				'param_name'	=> 'btnsize',
				'description'	=> esc_html__('You can select button size', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 	'nt-forester' )	=> '',
					esc_html__('Mini', 	'nt-forester' )	=> 'btn-xs',
					esc_html__('Small', 'nt-forester' )	=> 'btn-sm',
					esc_html__('Medium','nt-forester' )	=> 'btn-md',
					esc_html__('Large', 'nt-forester' )	=> 'btn-lg',
				),
			),
			//button
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button BG color', 'nt-forester'),
				'param_name'	=> 'btnbg',
				'description'	=> esc_html__('Change button background color.', 'nt-forester'),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button title color', 'nt-forester'),
				'param_name'	=> 'btncolor',
				'description'	=> esc_html__('Change button title color.', 'nt-forester'),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button border color', 'nt-forester'),
				'param_name'	=> 'btnborder',
				'description'	=> esc_html__('Change button border color.', 'nt-forester'),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
         //toptitle
         array(
            'type'			=> 'textfield',
            'heading'		=> esc_html__('Title top font-size', 'nt-forester'),
            'param_name'	=> 'ttsize',
            'description'	=> esc_html__('Change title top fontsize.use number in ( px or unit )', 'nt-forester'),
            'group'			=> esc_html__('Custom Style', 'nt-forester' ),
         ),
         array(
            'type'			=> 'textfield',
            'heading'		=> esc_html__('Title top line-height', 'nt-forester'),
            'param_name'	=> 'ttlineh',
            'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
            'group'			=> esc_html__('Custom Style', 'nt-forester' ),
         ),
         array(
            'type'			=> 'colorpicker',
            'heading'		=> esc_html__('Title color', 'nt-forester'),
            'param_name'	=> 'ttcolor',
            'description'	=> esc_html__('Change title color.', 'nt-forester'),
            'group'			=> esc_html__('Custom Style', 'nt-forester' ),
         ),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//detail
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change detail fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change detail line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Detail color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change detail color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_hero1 extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	HERO VIDEO forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_herovideo_integrateWithVC' );
function nt_forester_herovideo_integrateWithVC() {
   vc_map( array(
		'name' 	   => esc_html__( 'Hero Video', 'nt-forester' ),
		'base' 	   => 'nt_forester_section_herovideo',
		'category' => esc_html__( 'NT forester', 'nt-forester'),
		'params'   => array(
			array(
				'type' 		  	=> 'attach_image',
				'heading' 	  	=> esc_html__('BG image', 'nt-forester'),
				'param_name'  	=> 'bgimg',
				'description' 	=> esc_html__('Add your background image icon', 'nt-forester'),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Youtube video ID', 'nt-forester'),
				'param_name' 	  => 'vidurl',
				'description' 	  => esc_html__('Add your youtube video ID.  example:MsC8rRD3Gac', 'nt-forester'),
			),
         array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Top title', 'nt-forester'),
				'param_name' 	  => 'toptitle',
				'description' 	  => esc_html__('Add slider top title for item.', 'nt-forester'),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Slider Title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add slider title for item.', 'nt-forester'),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Detail', 'nt-forester'),
				'param_name' 	  => 'desc',
				'description' 	  => esc_html__('Add description or text block for slider item.', 'nt-forester'),
			),
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Buttons', 'nt-forester' ),
				'param_name'  => 'btnloop',
				'group' 	  => esc_html__('Buttons', 'nt-forester' ),
				'params' 	  => array(
					//button
					array(
						'type'          => 'vc_link',
						'heading'       => esc_html__('Button (Link)', 'nt-forester' ),
						'param_name'    => 'link',
						'description'   => esc_html__('Add button title and link.', 'nt-forester' ),
					),
					array(
						'type'			=> 'dropdown',
						'heading'		=> esc_html__('Button link type', 'nt-forester' ),
						'param_name'	=> 'linktype',
						'description'	=> esc_html__('You can select button link type', 'nt-forester' ),
						'group'			=> esc_html__('Button', 'nt-forester' ),
						'value'			=> array(
							esc_html__('Select type', 	'nt-forester' )	=> '',
							esc_html__('Internal link', 'nt-forester' )	=> 'internal',
							esc_html__('External link', 'nt-forester' )	=> 'external',
						),
					),
					array(
						'type'			=> 'dropdown',
						'heading'		=> esc_html__('Button style', 'nt-forester' ),
						'param_name'	=> 'btnstyle',
						'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
						'value'			=> array(
							esc_html__('Select option', 	'nt-forester' )	=> '',
							esc_html__('Primary', 	 'nt-forester' )	=> 'btn-primary',
							esc_html__('Ghost white','nt-forester' )	=> 'btn-ghost-white',
							esc_html__('Ghost black','nt-forester' )	=> 'btn-ghost-black',
							esc_html__('Black button','nt-forester' )	=> 'btn-black',
						),
					),
					array(
						'type'			=> 'dropdown',
						'heading'		=> esc_html__('Button size', 'nt-forester' ),
						'param_name'	=> 'btnsize',
						'description'	=> esc_html__('You can select button size', 'nt-forester' ),
						'value'			=> array(
							esc_html__('Select option', 	'nt-forester' )	=> '',
							esc_html__('Mini', 	'nt-forester' )	=> 'btn-xs',
							esc_html__('Small', 'nt-forester' )	=> 'btn-sm',
							esc_html__('Medium','nt-forester' )	=> 'btn-md',
							esc_html__('Large', 'nt-forester' )	=> 'btn-lg',
						),
					),
					//button
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__('Button BG color', 'nt-forester'),
						'param_name'	=> 'btnbg',
						'description'	=> esc_html__('Change button background color.', 'nt-forester'),
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__('Button title color', 'nt-forester'),
						'param_name'	=> 'btncolor',
						'description'	=> esc_html__('Change button title color.', 'nt-forester'),
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__('Button border color', 'nt-forester'),
						'param_name'	=> 'btnborder',
						'description'	=> esc_html__('Change button border color.', 'nt-forester'),
					),
				)
			),
         //toptitle
         array(
            'type'			=> 'textfield',
            'heading'		=> esc_html__('Title top font-size', 'nt-forester'),
            'param_name'	=> 'ttsize',
            'description'	=> esc_html__('Change title top fontsize.use number in ( px or unit )', 'nt-forester'),
            'group'			=> esc_html__('Custom Style', 'nt-forester' ),
         ),
         array(
            'type'			=> 'textfield',
            'heading'		=> esc_html__('Title top line-height', 'nt-forester'),
            'param_name'	=> 'ttlineh',
            'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
            'group'			=> esc_html__('Custom Style', 'nt-forester' ),
         ),
         array(
            'type'			=> 'colorpicker',
            'heading'		=> esc_html__('Title color', 'nt-forester'),
            'param_name'	=> 'ttcolor',
            'description'	=> esc_html__('Change title color.', 'nt-forester'),
            'group'			=> esc_html__('Custom Style', 'nt-forester' ),
         ),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//detail
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change detail fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change detail line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Detail color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change detail color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_herovideo extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	HERO SLIDER forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_heroslider_integrateWithVC' );
function nt_forester_heroslider_integrateWithVC() {
   vc_map( array(
		'name' 	   => esc_html__( 'Hero Slider', 'nt-forester' ),
		'base' 	   => 'nt_forester_section_heroslider',
		'category'   => esc_html__( 'NT forester', 'nt-forester'),
		'params'     => array(
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Slider items', 'nt-forester' ),
				'param_name'  => 'sloop',
				'group' 	  => esc_html__('Slider', 'nt-forester' ),
				'params' 	  => array(
					array(
						'type' 		  	=> 'attach_image',
						'heading' 	  	=> esc_html__('BG image', 'nt-forester'),
						'param_name'  	=> 'bgimg',
						'description' 	=> esc_html__('Add your background image icon', 'nt-forester'),
					),
					array(
						'type'        => 'param_group',
						'heading'     => esc_html__('Slider items', 'nt-forester' ),
						'param_name'  => 'layerloop',
						'group' 	  => esc_html__('Slider', 'nt-forester' ),
						'params' 	  => array(
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Layer type', 'nt-forester' ),
								'param_name'	=> 'layertype',
								'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
								'group'			=> esc_html__('Button', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select type','nt-forester' )	=> '',
									esc_html__('Title', 	 'nt-forester' )	=> 'title',
									esc_html__('Description','nt-forester' )	=> 'desc',
									esc_html__('Button',	 'nt-forester' )	=> 'btn',
								),
							),
							//title
							array(
								'type' 			=> 'textarea',
								'heading' 		=> esc_html__('Slider Title', 'nt-forester'),
								'param_name' 	=> 'title',
								'description' 	=> esc_html__('Add slider title for item.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							//title
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Title font-size', 'nt-forester'),
								'param_name'	=> 'tsize',
								'description'	=> esc_html__('Change title fontsize.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Title line-height', 'nt-forester'),
								'param_name'	=> 'tlineh',
								'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Title color', 'nt-forester'),
								'param_name'	=> 'tcolor',
								'description'	=> esc_html__('Change title color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							//desc
							array(
								'type' 			 => 'textarea',
								'heading' 		 => esc_html__('Detail', 'nt-forester'),
								'param_name' 	 => 'desc',
								'description' 	 => esc_html__('Add description or text block for slider item.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							//detail
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Detail font-size', 'nt-forester'),
								'param_name'	=> 'dtsize',
								'description'	=> esc_html__('Change detail fontsize.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Detail line-height', 'nt-forester'),
								'param_name'	=> 'dtlineh',
								'description'	=> esc_html__('Change detail line-height.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Detail color', 'nt-forester'),
								'param_name'	=> 'dtcolor',
								'description'	=> esc_html__('Change detail color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							//button
							array(
								'type'          => 'vc_link',
								'heading'       => esc_html__('Button (Link)', 'nt-forester' ),
								'param_name'    => 'link',
								'description'   => esc_html__('Add button title and link.', 'nt-forester' ),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Button link type', 'nt-forester' ),
								'param_name'	=> 'linktype',
								'description'	=> esc_html__('You can select button link type', 'nt-forester' ),
								'group'			=> esc_html__('Button', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select type', 	'nt-forester' )	=> '',
									esc_html__('Internal link', 'nt-forester' )	=> 'internal',
									esc_html__('External link', 'nt-forester' )	=> 'external',
								),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Button style', 'nt-forester' ),
								'param_name'	=> 'btnstyle',
								'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select option', 	'nt-forester' )	=> '',
									esc_html__('Primary', 	 'nt-forester' )	=> 'btn-primary',
									esc_html__('Ghost white','nt-forester' )	=> 'btn-ghost-white',
									esc_html__('Ghost black','nt-forester' )	=> 'btn-ghost-black',
									esc_html__('Black button','nt-forester' )	=> 'btn-black',
								),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Button size', 'nt-forester' ),
								'param_name'	=> 'btnsize',
								'description'	=> esc_html__('You can select button size', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select option', 	'nt-forester' )	=> '',
									esc_html__('Mini', 	'nt-forester' )	=> 'btn-xs',
									esc_html__('Small', 'nt-forester' )	=> 'btn-sm',
									esc_html__('Medium','nt-forester' )	=> 'btn-md',
									esc_html__('Large', 'nt-forester' )	=> 'btn-lg',
								),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							//button
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Button BG color', 'nt-forester'),
								'param_name'	=> 'btnbg',
								'description'	=> esc_html__('Change button background color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Button title color', 'nt-forester'),
								'param_name'	=> 'btncolor',
								'description'	=> esc_html__('Change button title color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Button border color', 'nt-forester'),
								'param_name'	=> 'btnborder',
								'description'	=> esc_html__('Change button border color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
						)
					)
				)
			),
		)
	));
} class nt_forester_section_heroslider extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	HERO TEXT SLIDER forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_herotxtslider_integrateWithVC' );
function nt_forester_herotxtslider_integrateWithVC() {
   vc_map( array(
		'name' 	   => esc_html__( 'Hero Text Slider', 'nt-forester' ),
		'base' 	   => 'nt_forester_section_herotxtslider',
		'category'   => esc_html__( 'NT forester', 'nt-forester'),
		'params'     => array(
			array(
				'type' 		  	=> 'attach_image',
				'heading' 	  	=> esc_html__('BG image', 'nt-forester'),
				'param_name'  	=> 'bgimg',
				'description' 	=> esc_html__('Add your background image icon', 'nt-forester'),
			),
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Slider items', 'nt-forester' ),
				'param_name'  => 'sloop',
				'group' 	  => esc_html__('Slider', 'nt-forester' ),
				'params' 	  => array(
					array(
						'type'        => 'param_group',
						'heading'     => esc_html__('Slider items', 'nt-forester' ),
						'param_name'  => 'layerloop',
						'group' 	  => esc_html__('Slider', 'nt-forester' ),
						'params' 	  => array(
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Layer type', 'nt-forester' ),
								'param_name'	=> 'layertype',
								'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
								'group'			=> esc_html__('Button', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select type','nt-forester' )	=> '',
									esc_html__('Title', 	 'nt-forester' )	=> 'title',
									esc_html__('Description','nt-forester' )	=> 'desc',
									esc_html__('Button',	 'nt-forester' )	=> 'btn',
								),
							),
							//title
							array(
								'type' 			=> 'textarea',
								'heading' 		=> esc_html__('Slider Title', 'nt-forester'),
								'param_name' 	=> 'title',
								'description' 	=> esc_html__('Add slider title for item.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							//title
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Title font-size', 'nt-forester'),
								'param_name'	=> 'tsize',
								'description'	=> esc_html__('Change title fontsize.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Title line-height', 'nt-forester'),
								'param_name'	=> 'tlineh',
								'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Title color', 'nt-forester'),
								'param_name'	=> 'tcolor',
								'description'	=> esc_html__('Change title color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'title'
								)
							),
							//desc
							array(
								'type' 			 => 'textarea',
								'heading' 		 => esc_html__('Detail', 'nt-forester'),
								'param_name' 	 => 'desc',
								'description' 	 => esc_html__('Add description or text block for slider item.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							//detail
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Detail font-size', 'nt-forester'),
								'param_name'	=> 'dtsize',
								'description'	=> esc_html__('Change detail fontsize.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> esc_html__('Detail line-height', 'nt-forester'),
								'param_name'	=> 'dtlineh',
								'description'	=> esc_html__('Change detail line-height.use number in ( px or unit )', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Detail color', 'nt-forester'),
								'param_name'	=> 'dtcolor',
								'description'	=> esc_html__('Change detail color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'desc'
								)
							),
							//button
							array(
								'type'          => 'vc_link',
								'heading'       => esc_html__('Button (Link)', 'nt-forester' ),
								'param_name'    => 'link',
								'description'   => esc_html__('Add button title and link.', 'nt-forester' ),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Button link type', 'nt-forester' ),
								'param_name'	=> 'linktype',
								'description'	=> esc_html__('You can select button link type', 'nt-forester' ),
								'group'			=> esc_html__('Button', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select type', 	'nt-forester' )	=> '',
									esc_html__('Internal link', 'nt-forester' )	=> 'internal',
									esc_html__('External link', 'nt-forester' )	=> 'external',
								),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Button style', 'nt-forester' ),
								'param_name'	=> 'btnstyle',
								'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select option', 	'nt-forester' )	=> '',
									esc_html__('Primary', 	 'nt-forester' )	=> 'btn-primary',
									esc_html__('Ghost white','nt-forester' )	=> 'btn-ghost-white',
									esc_html__('Ghost black','nt-forester' )	=> 'btn-ghost-black',
									esc_html__('Black button','nt-forester' )	=> 'btn-black',
								),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> esc_html__('Button size', 'nt-forester' ),
								'param_name'	=> 'btnsize',
								'description'	=> esc_html__('You can select button size', 'nt-forester' ),
								'value'			=> array(
									esc_html__('Select option', 	'nt-forester' )	=> '',
									esc_html__('Mini', 	'nt-forester' )	=> 'btn-xs',
									esc_html__('Small', 'nt-forester' )	=> 'btn-sm',
									esc_html__('Medium','nt-forester' )	=> 'btn-md',
									esc_html__('Large', 'nt-forester' )	=> 'btn-lg',
								),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							//button
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Button BG color', 'nt-forester'),
								'param_name'	=> 'btnbg',
								'description'	=> esc_html__('Change button background color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Button title color', 'nt-forester'),
								'param_name'	=> 'btncolor',
								'description'	=> esc_html__('Change button title color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> esc_html__('Button border color', 'nt-forester'),
								'param_name'	=> 'btnborder',
								'description'	=> esc_html__('Change button border color.', 'nt-forester'),
								'dependency' 	=> array(
									'element' 	=> 'layertype',
									'value'   	=> 'btn'
								)
							),
						)
					)
				)
			),
		)
	));
} class nt_forester_section_herotxtslider extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	SERVICES forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_secheading_integrateWithVC' );
function nt_forester_secheading_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Section Heading', 'nt-forester' ),
		'base'		=> 'nt_forester_section_secheading',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Subheading', 'nt-forester'),
				'param_name' 	  => 'subtitle',
				'description' 	  => esc_html__('Add subheading for section.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Subtitle tag', 'nt-forester' ),
				'param_name'  => 'subtag',
				'description' => esc_html__('You can select tag for subheading font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
					esc_html__('div', 	'nt-forester' )	=> 'div',
				),
			),
			//title
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Heading', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add heading for section.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Heading tag', 'nt-forester' ),
				'param_name'  => 'titletag',
				'description' => esc_html__('You can select tag for title font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
					esc_html__('div', 	'nt-forester' )	=> 'div',
				),
			),
			//desc
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Description', 'nt-forester'),
				'param_name' 	  => 'desc',
				'description' 	  => esc_html__('Add description for section.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Description tag', 'nt-forester' ),
				'param_name'  => 'desctag',
				'description' => esc_html__('You can select tag for description font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
					esc_html__('div', 	'nt-forester' )	=> 'div',
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Alignment', 'nt-forester' ),
				'param_name'  => 'alignment',
				'description' => esc_html__('You can select text position with alignment', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select position', 	'nt-forester' )	=> '',
					esc_html__('Left', 	'nt-forester' )	=> 'text-left',
					esc_html__('Right', 'nt-forester' )	=> 'text-right',
					esc_html__('Center', 'nt-forester' )	=> 'text-center',
				),
			),
			//custom style
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Subheading font-size', 'nt-forester'),
				'param_name'	=> 'stsize',
				'description'	=> esc_html__('Change subheading fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Subheading line-height', 'nt-forester'),
				'param_name'	=> 'stlineh',
				'description'	=> esc_html__('Change subheading line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Subheading color', 'nt-forester'),
				'param_name'	=> 'stcolor',
				'description'	=> esc_html__('Change subheading color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Heading font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change heading fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Heading line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change heading line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Heading color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change heading color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//detail
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Description font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change description fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Description line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change description line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Description color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change description color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//bg style
			array(
				'type'			=> 'css_editor',
				'heading'		=> esc_html__('Background CSS', 'nt-forester' ),
				'param_name'	=> 'bgcss',
				'group'			=> esc_html__('Background options', 'nt-forester' ),
			),

		)
	));
} class nt_forester_section_secheading extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	SERVICES forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_services_integrateWithVC' );
function nt_forester_services_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Services Item', 'nt-forester' ),
		'base'		=> 'nt_forester_section_services',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Fonticon name', 'nt-forester'),
				'param_name' 	  => 'icon',
				'description' 	  => esc_html__('Add icon name(fonticon class name). example : fa fa-facebook', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Icon size', 'nt-forester' ),
				'param_name'  => 'itype',
				'description' => esc_html__('You can select icon size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select size', 	'nt-forester' )	=> '',
					esc_html__('x1', 	'nt-forester' )	=> 'x1',
					esc_html__('x2', 	'nt-forester' )	=> 'x2',
					esc_html__('x3', 	'nt-forester' )	=> 'x3',
					esc_html__('x4', 	'nt-forester' )	=> 'x4',
					esc_html__('x5', 	'nt-forester' )	=> 'x5',
				),
			),
			//title
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Title tag', 'nt-forester' ),
				'param_name'  => 'titletag',
				'description' => esc_html__('You can select tag for title font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
				),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add title for item.', 'nt-forester'),
			),
			//desc
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Description tag', 'nt-forester' ),
				'param_name'  => 'desctag',
				'description' => esc_html__('You can select tag for description font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
				),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Detail', 'nt-forester'),
				'param_name' 	  => 'desc',
				'description' 	  => esc_html__('Add detail for item.', 'nt-forester'),
			),

			//button
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__('Button (Link)', 'nt-forester' ),
				'param_name'    => 'btnlink',
				'description'   => esc_html__('Add button title and link.', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button style', 'nt-forester' ),
				'param_name'	=> 'btnstyle',
				'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 	'nt-forester' )	=> '',
					esc_html__('Primary', 	 'nt-forester' )	=> 'btn-primary',
					esc_html__('Ghost white','nt-forester' )	=> 'btn-ghost-white',
					esc_html__('Ghost black','nt-forester' )	=> 'btn-ghost-black',
					esc_html__('Black button','nt-forester' )	=> 'btn-black',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button size', 'nt-forester' ),
				'param_name'	=> 'btnsize',
				'description'	=> esc_html__('You can select button size', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 	'nt-forester' )	=> '',
					esc_html__('Mini', 	'nt-forester' )	=> 'btn-xs',
					esc_html__('Small', 'nt-forester' )	=> 'btn-sm',
					esc_html__('Medium','nt-forester' )	=> 'btn-md',
					esc_html__('Large', 'nt-forester' )	=> 'btn-lg',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button alignment', 'nt-forester' ),
				'param_name'	=> 'btnpos',
				'description'	=> esc_html__('You can select button position', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 'nt-forester' )	=> '',
					esc_html__('Left', 	'nt-forester' )	=> 'text-left',
					esc_html__('Center','nt-forester' )	=> 'text-center',
					esc_html__('Right', 'nt-forester' )	=> 'text-right',
				),
			),
			//button
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button BG color', 'nt-forester'),
				'param_name'	=> 'btnbg',
				'description'	=> esc_html__('Change button background color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button title color', 'nt-forester'),
				'param_name'	=> 'btncolor',
				'description'	=> esc_html__('Change button title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button border color', 'nt-forester'),
				'param_name'	=> 'btnborder',
				'description'	=> esc_html__('Change button border color.', 'nt-forester'),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),

			//custom style
			//icon
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Icon font-size', 'nt-forester'),
				'param_name'	=> 'isize',
				'description'	=> esc_html__('Change icon fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Icon color', 'nt-forester'),
				'param_name'	=> 'icolor',
				'description'	=> esc_html__('Add icon color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change item title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change item title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change item title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//detail
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change item detail fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change item detail line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Detail color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change item detail color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//bg style
			array(
				'type'			=> 'css_editor',
				'heading'		=> esc_html__('Background CSS', 'nt-forester' ),
				'param_name'	=> 'bgcss',
				'group'			=> esc_html__('Background Image', 'nt-forester' ),
			),

		)
	));
} class nt_forester_section_services extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	features forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_features_integrateWithVC' );
function nt_forester_features_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Features Item', 'nt-forester' ),
		'base'		=> 'nt_forester_section_features',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Icon size', 'nt-forester' ),
				'param_name'  => 'itype',
				'description' => esc_html__('You can select icon size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select size', 	'nt-forester' )	=> '',
					esc_html__('x1', 	'nt-forester' )	=> 'x1',
					esc_html__('x2', 	'nt-forester' )	=> 'x2',
					esc_html__('x3', 	'nt-forester' )	=> 'x3',
					esc_html__('x4', 	'nt-forester' )	=> 'x4',
					esc_html__('x5', 	'nt-forester' )	=> 'x5',
					esc_html__('x6', 	'nt-forester' )	=> 'x6',
				),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Icon name', 'nt-forester'),
				'param_name' 	  => 'icon',
				'description' 	  => esc_html__('Add icon name(fonticon class name). example : ti-eye', 'nt-forester'),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add title for item.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Title tag', 'nt-forester' ),
				'param_name'  => 'titletag',
				'description' => esc_html__('You can select tag for title font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
				),
			),
			array(
				'type' 			  => 'textarea',
				'heading' 		  => esc_html__('Detail', 'nt-forester'),
				'param_name' 	  => 'desc',
				'description' 	  => esc_html__('Add detail for item.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Detail tag', 'nt-forester' ),
				'param_name'  => 'desctag',
				'description' => esc_html__('You can select tag for description font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
				),
			),
			//custom style
			//icon
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Icon font-size', 'nt-forester'),
				'param_name'	=> 'isize',
				'description'	=> esc_html__('Change item icon fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Icon color', 'nt-forester'),
				'param_name'	=> 'icolor',
				'description'	=> esc_html__('Add icon color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change item title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change item title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change item title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//detail
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change item detail fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Detail line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change item detail line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Detail color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change item detail color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//bg style
			array(
				'type'			=> 'css_editor',
				'heading'		=> esc_html__('Background CSS', 'nt-forester' ),
				'param_name'	=> 'bgcss',
				'group'			=> esc_html__('Background Image', 'nt-forester' ),
			),
         array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Text transform', 'nt-forester' ),
				'param_name'  => 'transform',
				'description' => esc_html__('You can select tag for transform', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> 'none',
					esc_html__('uppercase', 	'nt-forester' )	=> 'uppercase',
					esc_html__('capitalize', 	'nt-forester' )	=> 'capitalize',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
				),
			),

		)
	));
} class nt_forester_section_features extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	COUNTER forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_counter_integrateWithVC' );
function nt_forester_counter_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Counter Section', 'nt-forester' ),
		'base'		=> 'nt_forester_section_counter',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Number', 'nt-forester'),
				'param_name' 	  => 'number',
				'description' 	  => esc_html__('Add number for item.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Number tag', 'nt-forester' ),
				'param_name'  => 'numbertag',
				'description' => esc_html__('You can select tag for number font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
					esc_html__('div', 	'nt-forester' )	=> 'div',
				),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add title for item.', 'nt-forester'),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Title tag', 'nt-forester' ),
				'param_name'  => 'titletag',
				'description' => esc_html__('You can select tag for title font-size', 'nt-forester' ),
				'value'       => array(
					esc_html__('Select tag', 	'nt-forester' )	=> '',
					esc_html__('h1', 	'nt-forester' )	=> 'h1',
					esc_html__('h2', 	'nt-forester' )	=> 'h2',
					esc_html__('h3', 	'nt-forester' )	=> 'h3',
					esc_html__('h4', 	'nt-forester' )	=> 'h4',
					esc_html__('h5', 	'nt-forester' )	=> 'h5',
					esc_html__('h6', 	'nt-forester' )	=> 'h6',
					esc_html__('p', 	'nt-forester' )	=> 'p',
					esc_html__('div', 	'nt-forester' )	=> 'div',
				),
			),
			//Number
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number font-size', 'nt-forester'),
				'param_name'	=> 'nsize',
				'description'	=> esc_html__('Change item number fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number line-height', 'nt-forester'),
				'param_name'	=> 'nlineh',
				'description'	=> esc_html__('Change item number line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Number color', 'nt-forester'),
				'param_name'	=> 'ncolor',
				'description'	=> esc_html__('Change item number color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'css_editor',
				'heading'		=> esc_html__('Background CSS', 'nt-forester' ),
				'param_name'	=> 'bgcss',
				'group'			=> esc_html__('Background options', 'nt-forester' ),
			),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change item title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change item title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change item title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_counter extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	COUNTER forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_circle_integrateWithVC' );
function nt_forester_circle_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Circle', 'nt-forester' ),
		'base'		=> 'nt_forester_section_circle',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Percent', 'nt-forester'),
				'param_name' 	  => 'percent',
				'description' 	  => esc_html__('Add percent for item.', 'nt-forester'),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add title for item.', 'nt-forester'),
			),

			/*
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number font-size', 'nt-forester'),
				'param_name'	=> 'nsize',
				'description'	=> esc_html__('Change item number fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number line-height', 'nt-forester'),
				'param_name'	=> 'nlineh',
				'description'	=> esc_html__('Change item number line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Number color', 'nt-forester'),
				'param_name'	=> 'ncolor',
				'description'	=> esc_html__('Change item number color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'css_editor',
				'heading'		=> esc_html__('Background CSS', 'nt-forester' ),
				'param_name'	=> 'bgcss',
				'group'			=> esc_html__('Background options', 'nt-forester' ),
			),
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change item title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change item title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change item title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
         */
		)
	));
} class nt_forester_section_circle extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	PARTNER forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_partner_integrateWithVC' );
function nt_forester_partner_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Partner Section', 'nt-forester' ),
		'base'		=> 'nt_forester_section_partner',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			//Partner loop
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Partner items', 'nt-forester' ),
				'param_name'  => 'ploop',
				'group' 	  => esc_html__('Partner', 'nt-forester' ),
				'params' 	  => array(
					array(
						'type' 		  	=> 'attach_image',
						'heading' 	  	=> esc_html__('Partner image', 'nt-forester'),
						'param_name'  	=> 'img',
						'description' 	=> esc_html__('Add your partner image', 'nt-forester'),
					),
				)
			),

		)
	));
} class nt_forester_section_partner extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	TESTIMONIAL forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_testimonial_integrateWithVC' );
function nt_forester_testimonial_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Testimonial Section', 'nt-forester' ),
		'base'		=> 'nt_forester_section_testimonial',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(

         array(
            'type' 			  => 'textfield',
            'heading' 		  => esc_html__('Shortcode title', 'nt-forester'),
            'param_name' 	  => 'nameoff',
            'description' 	  => esc_html__('Add title.', 'nt-forester'),
         ),

			//testimonial loop
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Testimonial items', 'nt-forester' ),
				'param_name'  => 'testiloop',
				'group' 	  => esc_html__('Testimonial', 'nt-forester' ),
				'params' 	  => array(
               array(
      				'type' 		  	=> 'attach_image',
      				'heading' 	  	=> esc_html__('Item image', 'nt-forester'),
      				'param_name'  	=> 'image',
      				'description' 	=> esc_html__('Add your image', 'nt-forester'),
      			),
					array(
						'type' 			  => 'textarea',
						'heading' 		  => esc_html__('Quote', 'nt-forester'),
						'param_name' 	  => 'quote',
						'description' 	  => esc_html__('Add testimonial quote.', 'nt-forester'),
					),
					array(
						'type' 			  => 'textfield',
						'heading' 		  => esc_html__('Name', 'nt-forester'),
						'param_name' 	  => 'name',
						'description' 	  => esc_html__('Add testimonial name.', 'nt-forester'),
					),
					array(
						'type' 			  => 'textfield',
						'heading' 		  => esc_html__('Job', 'nt-forester'),
						'param_name' 	  => 'job',
						'description' 	  => esc_html__('Add testimonial job.', 'nt-forester'),
					),
				)
			),

			//custom style
			//quote
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Quote font-size', 'nt-forester'),
				'param_name'	=> 'qsize',
				'description'	=> esc_html__('Change quote fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Quote line-height', 'nt-forester'),
				'param_name'	=> 'qlineh',
				'description'	=> esc_html__('Change quote line-height.use number in  ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Quote color', 'nt-forester'),
				'param_name'	=> 'qcolor',
				'description'	=> esc_html__('Change quote color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//name
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Name font-size', 'nt-forester'),
				'param_name'	=> 'nsize',
				'description'	=> esc_html__('Change testimonial name fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Name line-height', 'nt-forester'),
				'param_name'	=> 'nlineh',
				'description'	=> esc_html__('Change testimonial name line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Name color', 'nt-forester'),
				'param_name'	=> 'ncolor',
				'description'	=> esc_html__('Change testimonial name color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//Job
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Job font-size', 'nt-forester'),
				'param_name'	=> 'jsize',
				'description'	=> esc_html__('Change testimonial job fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Job line-height', 'nt-forester'),
				'param_name'	=> 'jlineh',
				'description'	=> esc_html__('Change testimonial job line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Job color', 'nt-forester'),
				'param_name'	=> 'jcolor',
				'description'	=> esc_html__('Change testimonial job color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_testimonial extends WPBakeryShortCode {}


/*-----------------------------------------------------------------------------------*/
/*	QUOTE forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_quote_integrateWithVC' );
function nt_forester_quote_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Quote Slider', 'nt-forester' ),
		'base'		=> 'nt_forester_section_quote',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			//testimonial loop
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Quote items', 'nt-forester' ),
				'param_name'  => 'testiloop',
				'group' 	  => esc_html__('Quote', 'nt-forester' ),
				'params' 	  => array(
					array(
						'type' 			  => 'textarea',
						'heading' 		  => esc_html__('Quote', 'nt-forester'),
						'param_name' 	  => 'quote',
						'description' 	  => esc_html__('Add testimonial quote.', 'nt-forester'),
					),
					array(
						'type' 			  => 'textfield',
						'heading' 		  => esc_html__('Name', 'nt-forester'),
						'param_name' 	  => 'name',
						'description' 	  => esc_html__('Add testimonial name.', 'nt-forester'),
					),
				)
			),

			//custom style
			//quote
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Quote font-size', 'nt-forester'),
				'param_name'	=> 'qsize',
				'description'	=> esc_html__('Change quote fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Quote line-height', 'nt-forester'),
				'param_name'	=> 'qlineh',
				'description'	=> esc_html__('Change quote line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Quote color', 'nt-forester'),
				'param_name'	=> 'qcolor',
				'description'	=> esc_html__('Change quote color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//name
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Name font-size', 'nt-forester'),
				'param_name'	=> 'nsize',
				'description'	=> esc_html__('Change testimonial name fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Name line-height', 'nt-forester'),
				'param_name'	=> 'nlineh',
				'description'	=> esc_html__('Change testimonial name line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Name color', 'nt-forester'),
				'param_name'	=> 'ncolor',
				'description'	=> esc_html__('Change testimonial name color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_quote extends WPBakeryShortCode {}


/*-----------------------------------------------------------------------------------*/
/*	BUTTON forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_button_integrateWithVC' );
function nt_forester_button_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Button', 'nt-forester' ),
		'base'		=> 'nt_forester_section_button',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
			//button
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__('Button (Link)', 'nt-forester' ),
				'param_name'    => 'btnlink',
				'description'   => esc_html__('Add button title and link.', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button link type', 'nt-forester' ),
				'param_name'	=> 'linktype',
				'description'	=> esc_html__('You can select button link type', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select type', 	'nt-forester' )	=> '',
					esc_html__('Internal link', 'nt-forester' )	=> 'internal',
					esc_html__('External link', 'nt-forester' )	=> 'external',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button style', 'nt-forester' ),
				'param_name'	=> 'btnstyle',
				'description'	=> esc_html__('You can select button color style', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 	'nt-forester' )	=> '',
					esc_html__('Primary', 	 'nt-forester' )	=> 'btn-primary',
					esc_html__('Ghost white','nt-forester' )	=> 'btn-ghost-white',
					esc_html__('Ghost black','nt-forester' )	=> 'btn-ghost-black',
					esc_html__('Black button','nt-forester' )	=> 'btn-black',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button size', 'nt-forester' ),
				'param_name'	=> 'btnsize',
				'description'	=> esc_html__('You can select button size', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 	'nt-forester' )	=> '',
					esc_html__('Mini', 	'nt-forester' )	=> 'btn-xs',
					esc_html__('Small', 'nt-forester' )	=> 'btn-sm',
					esc_html__('Medium','nt-forester' )	=> 'btn-md',
					esc_html__('Large', 'nt-forester' )	=> 'btn-lg',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Button alignment', 'nt-forester' ),
				'param_name'	=> 'btnpos',
				'description'	=> esc_html__('You can select button position', 'nt-forester' ),
				'group'			=> esc_html__('Button', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 'nt-forester' )	=> '',
					esc_html__('Left', 	'nt-forester' )	=> 'text-left',
					esc_html__('Center','nt-forester' )	=> 'text-center',
					esc_html__('Right', 'nt-forester' )	=> 'text-right',
				),
			),
			//button
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button BG color', 'nt-forester'),
				'param_name'	=> 'btnbg',
				'description'	=> esc_html__('Change right button background color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button title color', 'nt-forester'),
				'param_name'	=> 'btncolor',
				'description'	=> esc_html__('Change right button title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button border color', 'nt-forester'),
				'param_name'	=> 'btnborder',
				'description'	=> esc_html__('Change right button border color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Button border width', 'nt-forester'),
				'param_name'	=> 'border_width',
				'description'	=> esc_html__('Change button border width.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Button padding', 'nt-forester'),
				'param_name'	=> 'padding',
				'description'	=> esc_html__('Change button margin.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Button margin', 'nt-forester'),
				'param_name'	=> 'margin',
				'description'	=> esc_html__('Change button margin.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Button border radius', 'nt-forester'),
				'param_name'	=> 'radius',
				'description'	=> esc_html__('Change button border radius. 50px 60px', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_button extends WPBakeryShortCode {}


/*-----------------------------------------------------------------------------------*/
/*	CONTACT INFO forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_contactinfo_integrateWithVC' );
function nt_forester_contactinfo_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__( 'Contact Info', 'nt-forester' ),
		'base'		=> 'nt_forester_section_contactinfo',
		'category'	=> esc_html__( 'NT forester', 'nt-forester'),
		'params'	=> array(
         array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Form info types', 'nt-forester' ),
				'param_name'	=> 'type',
				'description'	=> esc_html__('You can select button position', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 'nt-forester' )	=> '',
					esc_html__('Type 1 - Top', 	'nt-forester' )	=> '1',
					esc_html__('Type 2 - left', 	'nt-forester' )	=> '2',
					esc_html__('Type 3 - Top', 	'nt-forester' )	=> '3',
				),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Section mini title', 'nt-forester'),
				'param_name' 	  => 'title',
				'description' 	  => esc_html__('Add title for this section.', 'nt-forester'),
            'dependency' 	=> array(
                  'element' 	=> 'type',
                  'value'   	=> '1'
            ),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Phone number', 'nt-forester'),
				'param_name' 	  => 'number',
				'description' 	  => esc_html__('Add phone number.', 'nt-forester'),
            'dependency' 	=> array(
                  'element' 	=> 'type',
                  'value'   	=> '1'
            ),
			),
			array(
				'type' 			  => 'textfield',
				'heading' 		  => esc_html__('Icon number', 'nt-forester'),
				'param_name' 	  => 'icon',
				'description' 	  => esc_html__('Add icon class name for phone number.', 'nt-forester'),
            'dependency' 	=> array(
                  'element' 	=> 'type',
                  'value'   	=> '1'
            ),
			),
			//testimonial loop
			array(
				'type'        => 'param_group',
				'heading'     => esc_html__('Info items', 'nt-forester' ),
				'param_name'  => 'infoloop',
				'params' 	  => array(
					array(
						'type' 			  => 'textfield',
						'heading' 		  => esc_html__('Info line 1', 'nt-forester'),
						'param_name' 	  => 'infotitle',
						'description' 	  => esc_html__('Add any text for type 2', 'nt-forester'),
                  'dependency' 	=> array(
                        'element' 	=> 'type',
                        'value'   	=> '2'
                  ),
					),
					array(
						'type' 			  => 'textfield',
						'heading' 		  => esc_html__('Info line 2', 'nt-forester'),
						'param_name' 	  => 'info',
						'description' 	  => esc_html__('Add any text', 'nt-forester'),
					),
					array(
						'type' 			  => 'textfield',
						'heading' 		  => esc_html__('Info icon name', 'nt-forester'),
						'param_name' 	  => 'infoicon',
						'description' 	  => esc_html__('Add icon classes', 'nt-forester'),
					),
				)
			),

			//custom style
			//title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//name
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number font-size', 'nt-forester'),
				'param_name'	=> 'nsize',
				'description'	=> esc_html__('Change number fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number line-height', 'nt-forester'),
				'param_name'	=> 'nlineh',
				'description'	=> esc_html__('Change number line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Number color', 'nt-forester'),
				'param_name'	=> 'ncolor',
				'description'	=> esc_html__('Change number color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//icon number
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Number icon font-size', 'nt-forester'),
				'param_name'	=> 'nsize',
				'description'	=> esc_html__('Change number icon fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Number color', 'nt-forester'),
				'param_name'	=> 'ncolor',
				'description'	=> esc_html__('Change number icon color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//info
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Info font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change info fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Info line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change info line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Info color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change info job color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//info icon
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Info icon font-size', 'nt-forester'),
				'param_name'	=> 'disize',
				'description'	=> esc_html__('Change info icon fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Info icon color', 'nt-forester'),
				'param_name'	=> 'dicolor',
				'description'	=> esc_html__('Change info icon job color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// BG style
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__('Background CSS', 'nt-forester' ),
				'param_name'    => 'bgcss',
				'group' 	    => esc_html__('Background options', 'nt-forester' ),
			),
		)
	));
} class nt_forester_section_contactinfo extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	PORTFOLIO forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_port_integrateWithVC' );
function nt_forester_section_port_integrateWithVC() {
   vc_map( array(
      'name'       => esc_html__('Portfolio ( CPT )', 'nt-forester'),
      'base'       => 'nt_forester_section_port',
	  'icon'       => 'icon-wpb-row',
	  'category'   => esc_html__('NT forester', 'nt-forester'),
      'params'     => array(
			array(
				'type'			=> 'posttypes',
				'heading'		=> esc_html__('Select your post type', 'nt-forester'),
				'param_name'	=> 'custompost',
				'description'	=> esc_html__('You can select your created CPT.Default post type name is portfolio.', 'nt-forester'),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Portfolio style ?', 'nt-forester' ),
				'param_name'	=> 'portstyle',
				'description'	=> esc_html__('You can select portfolio style', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select style', 	'nt-forester' )		=> '',
					esc_html__('Fullwidth','nt-forester' )	=> 'full',
					esc_html__('Boxed','nt-forester' )		=> 'box',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Portfolio column ?', 'nt-forester' ),
				'param_name'	=> 'portcolumn',
				'description'	=> esc_html__('You can select column', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select style', 	'nt-forester' )		=> '',
					esc_html__('2 Column','nt-forester' )	=> '2',
					esc_html__('3 Column','nt-forester' )	=> '3',
					esc_html__('4 Column','nt-forester' )	=> '4',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Layout mode?', 'nt-forester' ),
				'param_name'	=> 'layoutmode',
				'description'	=> esc_html__('You can select layout mode', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('grid masonry',		'nt-forester' )	=> 'grid',
					esc_html__('mosaic',			'nt-forester' )	=> 'mosaic',
					esc_html__('slider',			'nt-forester' )	=> 'slider',
					esc_html__('grid custom size',	'nt-forester' )	=> 'custom',
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Portfolio image width', 'nt-forester'),
				'param_name'	=> 'img_w',
				'description'	=> esc_html__('Add image width for auto cropping.use simple number without px or unit', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
				'dependency' 	=> array(
						'element' 	=> 'layoutmode',
						'value'   	=> 'custom'
				)
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Portfolio image height', 'nt-forester'),
				'param_name'	=> 'img_h',
				'description'	=> esc_html__('Add image height for auto cropping.use simple number without px or unit', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
				'dependency' 	=> array(
						'element' 	=> 'layoutmode',
						'value'   	=> 'custom'
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Grid Adjustment?', 'nt-forester' ),
				'param_name'	=> 'gridadjustment',
				'description'	=> esc_html__('You can select grid type', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Align Center',		'nt-forester' )	=> 'alignCenter',
					esc_html__('Responsive',		'nt-forester' )	=> 'responsive',
				),
				'dependency' 	=> array(
						'element' 	=> 'layoutmode',
						'value'   	=> array('grid','mosaic','custom')
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Navigation display?', 'nt-forester' ),
				'param_name'	=> 'navdisplay',
				'description'	=> esc_html__('You can select show or hide slider type navigation', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('hide',				'nt-forester' )	=> 'hide',
				),
				'dependency' 	=> array(
						'element' 	=> 'layoutmode',
						'value'   	=> 'slider'
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Filter display ?', 'nt-forester' ),
				'param_name'	=> 'showfilter',
				'description'	=> esc_html__('You can select show or hide filter', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('Hide',				'nt-forester' )	=> 'hide',
				),
				'dependency' 	=> array(
						'element' 	=> 'layoutmode',
						'value'   	=> array('grid','mosaic','custom')
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Animation filter ?', 'nt-forester' ),
				'param_name'	=> 'animationfilter',
				'description'	=> esc_html__('This option is for animating the item when the filters are clicked.', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option','nt-forester' )	=> '',
					esc_html__('fadeOut',		'nt-forester' )	=> 'fadeOut',
					esc_html__('quicksand',		'nt-forester' )	=> 'quicksand',
					esc_html__('bounceLeft',	'nt-forester' )	=> 'bounceLeft',
					esc_html__('bounceTop',		'nt-forester' )	=> 'bounceTop',
					esc_html__('bounceBottom',	'nt-forester' )	=> 'bounceBottom',
					esc_html__('moveLeft',		'nt-forester' )	=> 'moveLeft',
					esc_html__('slideLeft',		'nt-forester' )	=> 'slideLeft',
					esc_html__('fadeOutTop',	'nt-forester' )	=> 'fadeOutTop',
					esc_html__('sequentially',	'nt-forester' )	=> 'sequentially',
					esc_html__('skew',			'nt-forester' )	=> 'skew',
					esc_html__('slideDelay',	'nt-forester' )	=> 'slideDelay',
					esc_html__('rotateSides',	'nt-forester' )	=> 'rotateSides',
					esc_html__('flipOutDelay',	'nt-forester' )	=> 'flipOutDelay',
					esc_html__('flipOut',		'nt-forester' )	=> 'flipOut',
					esc_html__('unfold',		'nt-forester' )	=> 'unfold',
					esc_html__('foldLeft',		'nt-forester' )	=> 'foldLeft',
					esc_html__('scaleDown',		'nt-forester' )	=> 'scaleDown',
					esc_html__('scaleSides',	'nt-forester' )	=> 'scaleSides',
					esc_html__('frontRow',		'nt-forester' )	=> 'frontRow',
					esc_html__('flipBottom',	'nt-forester' )	=> 'flipBottom',
					esc_html__('rotateRoom',	'nt-forester' )	=> 'rotateRoom',
				),
				'dependency' 	=> array(
						'element' 	=> 'showfilter',
						'value'   	=> 'show'
				)
			),
			array(
				'type'        	=> 'checkbox',
				'heading'     	=> esc_html__('Filter counter?', 'nt-forester' ),
				'param_name'  	=> 'showcounter',
				'value'       	=> array('Show'=>'show',), //value
				'std'         	=> 'not',
				'description' 	=> esc_html__('This option is for displaying filter counter', 'nt-forester'),
				'dependency' 	=> array(
						'element' 	=> 'showfilter',
						'value'   	=> 'show'
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Animation caption?', 'nt-forester' ),
				'param_name'	=> 'animationcaption',
				'description'	=> esc_html__('You can select animation for post caption', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('pushTop',			'nt-forester' )	=> 'pushTop',
					esc_html__('pushDown',			'nt-forester' )	=> 'pushDown',
					esc_html__('revealBottom',		'nt-forester' )	=> 'revealBottom',
					esc_html__('revealTop',			'nt-forester' )	=> 'revealTop',
					esc_html__('revealLeft',		'nt-forester' )	=> 'revealLeft',
					esc_html__('moveRight',			'nt-forester' )	=> 'moveRight',
					esc_html__('overlayBottom',		'nt-forester' )	=> 'overlayBottom',
					esc_html__('overlayBottomPush',	'nt-forester' )	=> 'overlayBottomPush',
					esc_html__('overlayBottomReveal','nt-forester' )	=> 'overlayBottomReveal',
					esc_html__('overlayBottomAlong','nt-forester' )	=> 'overlayBottomAlong',
					esc_html__('overlayRightAlong',	'nt-forester' )	=> 'overlayRightAlong',
					esc_html__('minimal',			'nt-forester' )	=> 'minimal',
					esc_html__('fadeIn',			'nt-forester' )	=> 'fadeIn',
					esc_html__('zoom',				'nt-forester' )	=> 'zoom',
					esc_html__('opacity',			'nt-forester' )	=> 'opacity',
				),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Caption active wrap background color', 'nt-forester'),
				'param_name'	=> 'activewrapbg',
				'description'	=> esc_html__('Change active wrap background color.', 'nt-forester'),
			),
			array(
				'type'        	=> 'checkbox',
				'heading'     	=> esc_html__('Active wrap color customize?', 'nt-forester' ),
				'param_name'  	=> 'use_mbcolor',
				'value'       	=> array('Use metabox active wrap color'=>'true',), //value
				'std'         	=> 'not',
				'description' 	=> esc_html__('This option allows Metabox customization options for each item.', 'nt-forester'),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Animation Popup single page?', 'nt-forester' ),
				'param_name'	=> 'animationsingle',
				'description'	=> esc_html__('This option is for animating the popup single page when the item are clicked.', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('left',			'nt-forester' )	=> 'left',
					esc_html__('fade',			'nt-forester' )	=> 'fade',
					esc_html__('right',			'nt-forester' )	=> 'right',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Horizontal gap?', 'nt-forester' ),
				'param_name'	=> 'itemhorizontalgap',
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('0 px',			'nt-forester' )	=> '-1',
					esc_html__('1 px',			'nt-forester' )	=> '1',
					esc_html__('5 px',			'nt-forester' )	=> '5',
					esc_html__('10 px',			'nt-forester' )	=> '10',
					esc_html__('15 px',			'nt-forester' )	=> '15',
					esc_html__('20 px',			'nt-forester' )	=> '20',
					esc_html__('25 px',			'nt-forester' )	=> '25',
					esc_html__('30 px',			'nt-forester' )	=> '30',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Vertical gap?', 'nt-forester' ),
				'param_name'	=> 'itemverticalgap',
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
               esc_html__('0 px',			'nt-forester' )	=> '-1',
					esc_html__('1 px',			'nt-forester' )	=> '1',
					esc_html__('5 px',			'nt-forester' )	=> '5',
					esc_html__('10 px',			'nt-forester' )	=> '10',
					esc_html__('15 px',			'nt-forester' )	=> '15',
					esc_html__('20 px',			'nt-forester' )	=> '20',
					esc_html__('25 px',			'nt-forester' )	=> '25',
					esc_html__('30 px',			'nt-forester' )	=> '30',
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Post per page', 'nt-forester' ),
				'param_name'  => 'perpage',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('You can control with number your post.Please enter a number', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Category', 'nt-forester' ),
				'param_name'  => 'postcat',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter post category or write all', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Order', 'nt-forester' ),
				'param_name'  => 'order',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter post order. DESC or ASC', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Orderby', 'nt-forester' ),
				'param_name'  => 'orderby',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter post orderby. Default is : date', 'nt-forester'),
			),

			// custom style
			// Post title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post category font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change category font-size.use number in( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post category line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change category line-height.use number in( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post category color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change category color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change title font-size.use number in( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change title line-height.use number in( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// Post button
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post icon font-size', 'nt-forester'),
				'param_name'	=> 'isize',
				'description'	=> esc_html__('Change icon font-size.use number in( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post icon color', 'nt-forester'),
				'param_name'	=> 'icolor',
				'description'	=> esc_html__('Change icon color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// BG style
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__('Background CSS', 'nt-forester' ),
				'param_name'    => 'bgcss',
				'group' 	    => esc_html__('Background options', 'nt-forester' ),
			),
		),
	));
}
class nt_forester_section_port extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	TEAM forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_team_integrateWithVC' );
function nt_forester_section_team_integrateWithVC() {
   vc_map( array(
      'name'       => esc_html__('Team item ( CPT )', 'nt-forester'),
      'base'       => 'nt_forester_section_team',
	  'icon'       => 'icon-wpb-row',
	  'category'   => esc_html__('NT forester', 'nt-forester'),
      'params'     => array(
			array(
				'type'			=> 'posttypes',
				'heading'		=> esc_html__('Select your post type', 'nt-forester'),
				'param_name'	=> 'custompost',
				'description'	=> esc_html__('You can select your created CPT.Default post type name is team.', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Enter post name( title )', 'nt-forester'),
				'param_name'	=> 'postname',
				'description'	=> esc_html__('Please enter the name or slug name of the team you wish to show.', 'nt-forester'),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Team post style', 'nt-forester' ),
				'param_name'	=> 'teamstyle',
				'description'	=> esc_html__('You can select team post style', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 'nt-forester' )	=> '',
					esc_html__('Style 1', 	'nt-forester' )	=> 'style1',
					esc_html__('Style 2', 	'nt-forester' )	=> 'style2',
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team image width', 'nt-forester'),
				'param_name'	=> 'img_w',
				'description'	=> esc_html__('Add img width for auto cropping.use simple number without px or unit', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team image height', 'nt-forester'),
				'param_name'	=> 'img_h',
				'description'	=> esc_html__('Add img height for auto cropping.use simple number without px or unit', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
			),
			// custom style
			// team title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team name font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change team team font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Team name color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change team name color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// team job
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team job font-size', 'nt-forester'),
				'param_name'	=> 'jsize',
				'description'	=> esc_html__('Change team job font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Team job color', 'nt-forester'),
				'param_name'	=> 'jcolor',
				'description'	=> esc_html__('Change team job color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// Team social
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Team social icon color', 'nt-forester'),
				'param_name'	=> 'icolor',
				'description'	=> esc_html__('Change social icon color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			//bg css
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__('Background CSS', 'nt-forester' ),
				'param_name'    => 'bgcss',
				'group' 	    => esc_html__('Background options', 'nt-forester' ),
			),
		),
	));
}
class nt_forester_section_team extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	TEAM LOOP forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_teamloop_integrateWithVC' );
function nt_forester_section_teamloop_integrateWithVC() {
   vc_map( array(
      'name'       => esc_html__('Team Loop( CPT )', 'nt-forester'),
      'base'       => 'nt_forester_section_teamloop',
	  'icon'       => 'icon-wpb-row',
	  'category'   => esc_html__('NT forester', 'nt-forester'),
      'params'     => array(
			array(
				'type'			=> 'posttypes',
				'heading'		=> esc_html__('Select your post type', 'nt-forester'),
				'param_name'	=> 'custompost',
				'description'	=> esc_html__('You can select your created CPT.Default post type name is team.', 'nt-forester'),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Team post style', 'nt-forester' ),
				'param_name'	=> 'teamstyle',
				'description'	=> esc_html__('You can select team post style', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select option', 'nt-forester' )	=> '',
					esc_html__('Style 1', 	'nt-forester' )	=> 'style1',
					esc_html__('Style 2', 	'nt-forester' )	=> 'style2',
               esc_html__('Style 3', 	'nt-forester' )	=> 'style3',
				),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team image width', 'nt-forester'),
				'param_name'	=> 'img_w',
				'description'	=> esc_html__('Add img width for auto cropping.use simple number without px or unit', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team image height', 'nt-forester'),
				'param_name'	=> 'img_h',
				'description'	=> esc_html__('Add img height for auto cropping.use simple number without px or unit', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Post per page', 'nt-forester' ),
				'param_name'  => 'perpage',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('You can control with number your team.Please enter a number', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Category', 'nt-forester' ),
				'param_name'  => 'postcat',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter team category or write all', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-6',
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Order', 'nt-forester' ),
				'param_name'  => 'order',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter team order. DESC or ASC', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-3',
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Orderby', 'nt-forester' ),
				'param_name'  => 'orderby',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter team orderby. Default is : date', 'nt-forester'),
				'edit_field_class' => 'vc_col-sm-3',
			),
			// custom style
			// team title
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Team name color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change team name color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
				'edit_field_class' => 'vc_col-sm-6',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team name font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change team team font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
				'edit_field_class' => 'vc_col-sm-6',
			),
			// team job
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Team job color', 'nt-forester'),
				'param_name'	=> 'jcolor',
				'description'	=> esc_html__('Change team job color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
				'edit_field_class' => 'vc_col-sm-6',
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Team job font-size', 'nt-forester'),
				'param_name'	=> 'jsize',
				'description'	=> esc_html__('Change team job font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
				'edit_field_class' => 'vc_col-sm-6',
			),
			// Team social
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Team social icon color', 'nt-forester'),
				'param_name'	=> 'icolor',
				'description'	=> esc_html__('Change social icon color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		),
	));
}
class nt_forester_section_teamloop extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	PRICING forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_pricing_integrateWithVC' );
function nt_forester_section_pricing_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__('Pricing Item ( CPT )', 'nt-forester'),
		'base'		=> 'nt_forester_section_pricing',
		'category'	=> esc_html__('NT forester', 'nt-forester'),
		'icon'		=> 'icon-wpb-row',
		'params'	=> array(

			array(
				'type'			=> 'posttypes',
				'heading'		=> esc_html__('Select your post type', 'nt-forester'),
				'param_name'	=> 'custompost',
				'description'	=> esc_html__('You can select your created CPT.Default post type name is price.', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Enter post name( title )', 'nt-forester'),
				'param_name'	=> 'postname',
				'description'	=> esc_html__('Please enter the name or slug name of the pricing you wish to show.', 'nt-forester'),
			),
			// custom style
			// pack title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Pack title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change pack title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Pack title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change pack title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Pack title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change pack title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// pack price
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Price font-size', 'nt-forester'),
				'param_name'	=> 'prcsize',
				'description'	=> esc_html__('Change price font-size.use number in ( px or unit )example: 24', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Pack price line-height', 'nt-forester'),
				'param_name'	=> 'prclineh',
				'description'	=> esc_html__('Change pack price line-height.use number in ( px or unit )example: 44', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Price color', 'nt-forester'),
				'param_name'	=> 'prccolor',
				'description'	=> esc_html__('Change price color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// period
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Period font-size', 'nt-forester'),
				'param_name'	=> 'prysize',
				'description'	=> esc_html__('Change period font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Period line-height', 'nt-forester'),
				'param_name'	=> 'prylineh',
				'description'	=> esc_html__('Change pack period line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Period color', 'nt-forester'),
				'param_name'	=> 'prycolor',
				'description'	=> esc_html__('Change period color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// list item
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Features font-size', 'nt-forester'),
				'param_name'	=> 'listsize',
				'description'	=> esc_html__('Change features font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Features line-height', 'nt-forester'),
				'param_name'	=> 'listlineh',
				'description'	=> esc_html__('Change pack features line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Features color', 'nt-forester'),
				'param_name'	=> 'listcolor',
				'description'	=> esc_html__('Change features color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button background color', 'nt-forester'),
				'param_name'	=> 'btnbg',
				'description'	=> esc_html__('Change button background color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button title color', 'nt-forester'),
				'param_name'	=> 'btncolor',
				'description'	=> esc_html__('Change button title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
}
class nt_forester_section_pricing extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	PRICING LOOP forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_pricingloop_integrateWithVC' );
function nt_forester_section_pricingloop_integrateWithVC() {
	vc_map( array(
		'name'		=> esc_html__('Pricing Loop ( CPT )', 'nt-forester'),
		'base'		=> 'nt_forester_section_pricingloop',
		'category'	=> esc_html__('NT forester', 'nt-forester'),
		'icon'		=> 'icon-wpb-row',
		'params'	=> array(
			array(
				'type'		  => 'posttypes',
				'heading'	  => esc_html__('Select your post type', 'nt-forester'),
				'param_name'  => 'custompost',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('You can select your created CPT.Default post type name is price.', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Post per page', 'nt-forester' ),
				'param_name'  => 'perpage',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('You can control with number your post.Please enter a number', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Category', 'nt-forester' ),
				'param_name'  => 'postcat',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter post category or write all', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Order', 'nt-forester' ),
				'param_name'  => 'order',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter post order. DESC or ASC', 'nt-forester'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Orderby', 'nt-forester' ),
				'param_name'  => 'orderby',
				'group'       => esc_html__('Post Options', 'nt-forester'),
				'description' => esc_html__('Enter post orderby. Default is : date', 'nt-forester'),
			),

			// custom style
			// pack title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Pack title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change pack title fontsize.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Pack title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change pack title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Pack title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change pack title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// pack price
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Price font-size', 'nt-forester'),
				'param_name'	=> 'prcsize',
				'description'	=> esc_html__('Change price font-size.use number in ( px or unit )example: 24', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Pack price line-height', 'nt-forester'),
				'param_name'	=> 'prclineh',
				'description'	=> esc_html__('Change pack price line-height.use number in ( px or unit )example: 44', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Price color', 'nt-forester'),
				'param_name'	=> 'prccolor',
				'description'	=> esc_html__('Change price color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// period
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Period font-size', 'nt-forester'),
				'param_name'	=> 'prysize',
				'description'	=> esc_html__('Change period font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Period line-height', 'nt-forester'),
				'param_name'	=> 'prylineh',
				'description'	=> esc_html__('Change pack period line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Period color', 'nt-forester'),
				'param_name'	=> 'prycolor',
				'description'	=> esc_html__('Change period color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// list item
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Features font-size', 'nt-forester'),
				'param_name'	=> 'listsize',
				'description'	=> esc_html__('Change features font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Features line-height', 'nt-forester'),
				'param_name'	=> 'listlineh',
				'description'	=> esc_html__('Change pack features line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Features color', 'nt-forester'),
				'param_name'	=> 'listcolor',
				'description'	=> esc_html__('Change features color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button background color', 'nt-forester'),
				'param_name'	=> 'btnbg',
				'description'	=> esc_html__('Change button background color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Button title color', 'nt-forester'),
				'param_name'	=> 'btncolor',
				'description'	=> esc_html__('Change button title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
		)
	));
}
class nt_forester_section_pricingloop extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	BLOG forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_blog_integrateWithVC' );
function nt_forester_section_blog_integrateWithVC() {
   vc_map( array(
      'name'       => esc_html__('Blog ( Default Post )', 'nt-forester'),
      'base'       => 'nt_forester_section_blog',
	  'icon'       => 'icon-wpb-row',
	  'category'   => esc_html__('NT forester', 'nt-forester'),
      'params'     => array(
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Section subtitle', 'nt-forester'),
				'param_name'	=> 'subtitle',
				'description'	=> esc_html__('Add section subtitle', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Section heading', 'nt-forester'),
				'param_name'	=> 'heading',
				'description'	=> esc_html__('Add section heading', 'nt-forester'),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Boxed or Fullwidth ?', 'nt-forester' ),
				'param_name'	=> 'containerstyle',
				'description'	=> esc_html__('You can select show or hide filter', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Boxed',				'nt-forester' )	=> 'boxed',
					esc_html__('Fullwidth',			'nt-forester' )	=> 'fullwidth',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Blog column ?', 'nt-forester' ),
				'param_name'	=> 'postcolumn',
				'description'	=> esc_html__('You can select column', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select style', 	'nt-forester' ) => '',
					esc_html__('2 Column','nt-forester' )	=> '2',
					esc_html__('3 Column','nt-forester' )	=> '3',
					esc_html__('4 Column','nt-forester' )	=> '4',
					esc_html__('5 Column','nt-forester' )	=> '5',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Filter display ?', 'nt-forester' ),
				'param_name'	=> 'showfilter',
				'description'	=> esc_html__('You can select show or hide filter', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('Hide',				'nt-forester' )	=> 'hide',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Animation filter ?', 'nt-forester' ),
				'param_name'	=> 'animationfilter',
				'description'	=> esc_html__('This option is for animating the item when the filters are clicked.', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option','nt-forester' )	=> '',
					esc_html__('fadeOut',		'nt-forester' )	=> 'fadeOut',
					esc_html__('quicksand',		'nt-forester' )	=> 'quicksand',
					esc_html__('bounceLeft',	'nt-forester' )	=> 'bounceLeft',
					esc_html__('bounceTop',		'nt-forester' )	=> 'bounceTop',
					esc_html__('bounceBottom',	'nt-forester' )	=> 'bounceBottom',
					esc_html__('moveLeft',		'nt-forester' )	=> 'moveLeft',
					esc_html__('slideLeft',		'nt-forester' )	=> 'slideLeft',
					esc_html__('fadeOutTop',	'nt-forester' )	=> 'fadeOutTop',
					esc_html__('sequentially',	'nt-forester' )	=> 'sequentially',
					esc_html__('skew',			'nt-forester' )	=> 'skew',
					esc_html__('slideDelay',	'nt-forester' )	=> 'slideDelay',
					esc_html__('rotateSides',	'nt-forester' )	=> 'rotateSides',
					esc_html__('flipOutDelay',	'nt-forester' )	=> 'flipOutDelay',
					esc_html__('flipOut',		'nt-forester' )	=> 'flipOut',
					esc_html__('unfold',		'nt-forester' )	=> 'unfold',
					esc_html__('foldLeft',		'nt-forester' )	=> 'foldLeft',
					esc_html__('scaleDown',		'nt-forester' )	=> 'scaleDown',
					esc_html__('scaleSides',	'nt-forester' )	=> 'scaleSides',
					esc_html__('frontRow',		'nt-forester' )	=> 'frontRow',
					esc_html__('flipBottom',	'nt-forester' )	=> 'flipBottom',
					esc_html__('rotateRoom',	'nt-forester' )	=> 'rotateRoom',
				),
				'dependency' 	=> array(
						'element' 	=> 'showfilter',
						'value'   	=> 'show'
				)
			),
			array(
				'type'        	=> 'checkbox',
				'heading'     	=> esc_html__('Filter counter?', 'nt-forester' ),
				'param_name'  	=> 'showcounter',
				'value'       	=> array('Show'=>'show',), //value
				'std'         	=> 'not',
				'description' 	=> esc_html__('This option is for displaying filter counter', 'nt-forester'),
				'dependency' 	=> array(
						'element' 	=> 'showfilter',
						'value'   	=> 'show'
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Layout mode?', 'nt-forester' ),
				'param_name'	=> 'layoutmode',
				'description'	=> esc_html__('You can select layout mode', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('grid masonry',		'nt-forester' )	=> 'grid',
					esc_html__('mosaic',			'nt-forester' )	=> 'mosaic',
					esc_html__('slider',			'nt-forester' )	=> 'slider',
				),
			),

			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Navigation display?', 'nt-forester' ),
				'param_name'	=> 'navdisplay',
				'description'	=> esc_html__('You can select show or hide slider type navigation', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('hide',				'nt-forester' )	=> 'hide',
				),
				'dependency' 	=> array(
						'element' 	=> 'layoutmode',
						'value'   	=> 'slider'
				)
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Grid Adjustment?', 'nt-forester' ),
				'param_name'	=> 'gridadjustment',
				'description'	=> esc_html__('You can select grid type', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Align Center',		'nt-forester' )	=> 'alignCenter',
					esc_html__('Responsive',		'nt-forester' )	=> 'responsive',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Horizontal gap?', 'nt-forester' ),
				'param_name'	=> 'itemhorizontalgap',
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('10 px',			'nt-forester' )	=> '10',
					esc_html__('15 px',			'nt-forester' )	=> '15',
					esc_html__('20 px',			'nt-forester' )	=> '20',
					esc_html__('25 px',			'nt-forester' )	=> '25',
					esc_html__('30 px',			'nt-forester' )	=> '30',
					esc_html__('35 px',			'nt-forester' )	=> '35',
					esc_html__('40 px',			'nt-forester' )	=> '40',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Vertical gap?', 'nt-forester' ),
				'param_name'	=> 'itemverticalgap',
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('10 px',			'nt-forester' )	=> '10',
					esc_html__('15 px',			'nt-forester' )	=> '15',
					esc_html__('20 px',			'nt-forester' )	=> '20',
					esc_html__('25 px',			'nt-forester' )	=> '25',
					esc_html__('30 px',			'nt-forester' )	=> '30',
					esc_html__('35 px',			'nt-forester' )	=> '35',
					esc_html__('40 px',			'nt-forester' )	=> '40',
				),
			),

			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Button title', 'nt-forester'),
				'param_name'	=> 'pbtntitle',
				'description'	=> esc_html__('Add post button title for permalink', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Content limit', 'nt-forester'),
				'param_name'	=> 'limit',
				'description'	=> esc_html__('You can control with limit content text.', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post per page', 'nt-forester' ),
				'param_name'	=> 'perpage',
				'description'	=> esc_html__('You can control with number your post.Please enter a number or leave a blank', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post Category', 'nt-forester' ),
				'param_name'	=> 'postcat',
				'description'	=> esc_html__('Enter post category or write all', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Before category text', 'nt-forester' ),
				'param_name'	=> 'beforecat',
				'description'	=> esc_html__('Enter before post category text.example: In', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Before date text', 'nt-forester' ),
				'param_name'	=> 'beforedate',
				'description'	=> esc_html__('Enter before post date text.example: Posted on', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post Order', 'nt-forester' ),
				'param_name'	=> 'order',
				'description'	=> esc_html__('Enter post order. DESC or ASC', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post Orderby', 'nt-forester' ),
				'param_name'	=> 'orderby',
				'description'	=> esc_html__('Enter post orderby. Default is : date', 'nt-forester'),
				'group'			=> esc_html__('Post Options', 'nt-forester'),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Title display ?', 'nt-forester' ),
				'param_name'	=> 'showtitle',
				'description'	=> esc_html__('You can select show or hide for post title', 'nt-forester' ),
				'group'			=> esc_html__('Post Options', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('Hide',				'nt-forester' )	=> 'hide',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Content text display ?', 'nt-forester' ),
				'param_name'	=> 'showcontent',
				'description'	=> esc_html__('You can select show or hide for post content text', 'nt-forester' ),
				'group'			=> esc_html__('Post Options', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('Hide',				'nt-forester' )	=> 'hide',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Date display ?', 'nt-forester' ),
				'param_name'	=> 'showdate',
				'description'	=> esc_html__('You can select show or hide for post date', 'nt-forester' ),
				'group'			=> esc_html__('Post Options', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('Hide',				'nt-forester' )	=> 'hide',
				),
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> esc_html__('Category display ?', 'nt-forester' ),
				'param_name'	=> 'showcat',
				'description'	=> esc_html__('You can select show or hide for post category', 'nt-forester' ),
				'group'			=> esc_html__('Post Options', 'nt-forester' ),
				'value'			=> array(
					esc_html__('Select a option', 	'nt-forester' )	=> '',
					esc_html__('Show',				'nt-forester' )	=> 'show',
					esc_html__('Hide',				'nt-forester' )	=> 'hide',
				),
			),
			// custom style
			// Section subtitle
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Section subtitle font-size', 'nt-forester'),
				'param_name'	=> 'stsize',
				'description'	=> esc_html__('Change subtitle font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Section subtitle line-height', 'nt-forester'),
				'param_name'	=> 'stlineh',
				'description'	=> esc_html__('Change subtitle line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Section subtitle color', 'nt-forester'),
				'param_name'	=> 'stcolor',
				'description'	=> esc_html__('Change title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// Section heading
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Section heading font-size', 'nt-forester'),
				'param_name'	=> 'hsize',
				'description'	=> esc_html__('Change heading font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Section heading line-height', 'nt-forester'),
				'param_name'	=> 'hlineh',
				'description'	=> esc_html__('Change heading line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Section heading color', 'nt-forester'),
				'param_name'	=> 'hcolor',
				'description'	=> esc_html__('Change heading color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// Post title
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post title font-size', 'nt-forester'),
				'param_name'	=> 'tsize',
				'description'	=> esc_html__('Change title font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post title line-height', 'nt-forester'),
				'param_name'	=> 'tlineh',
				'description'	=> esc_html__('Change title line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post title color', 'nt-forester'),
				'param_name'	=> 'tcolor',
				'description'	=> esc_html__('Change title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// Post content
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post content text font-size', 'nt-forester'),
				'param_name'	=> 'dtsize',
				'description'	=> esc_html__('Change content text font-size.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> esc_html__('Post content text line-height', 'nt-forester'),
				'param_name'	=> 'dtlineh',
				'description'	=> esc_html__('Change content text line-height.use number in ( px or unit )', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post content text color', 'nt-forester'),
				'param_name'	=> 'dtcolor',
				'description'	=> esc_html__('Change content text color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),

			// Post meta
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post category color', 'nt-forester'),
				'param_name'	=> 'catcolor',
				'description'	=> esc_html__('Change post category color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post date color', 'nt-forester'),
				'param_name'	=> 'datecolor',
				'description'	=> esc_html__('Change post date color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),
			// Post button
			array(
				'type'			=> 'colorpicker',
				'heading'		=> esc_html__('Post button title color', 'nt-forester'),
				'param_name'	=> 'btncolor',
				'description'	=> esc_html__('Change button title color.', 'nt-forester'),
				'group'			=> esc_html__('Custom Style', 'nt-forester' ),
			),

			// BG style
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__('Background CSS', 'nt-forester' ),
				'param_name'    => 'bgcss',
				'group' 	    => esc_html__('Background options', 'nt-forester' ),
			),
		)
	));
}
class nt_forester_section_blog extends WPBakeryShortCode {}

/*-----------------------------------------------------------------------------------*/
/*	POST NAVIGATION forester
/*-----------------------------------------------------------------------------------*/
add_action( 'vc_before_init', 'nt_forester_section_postnav_integrateWithVC' );
function nt_forester_section_postnav_integrateWithVC() {
   vc_map( array(
      'name'       => esc_html__('Post Navigation', 'nt-forester'),
      'base'       => 'nt_forester_section_postnav',
	  'icon'       => 'icon-wpb-row',
	  'category'   => esc_html__('NT forester', 'nt-forester'),
      'params'     => array(

		)
	));
}
class nt_forester_section_postnav extends WPBakeryShortCode {}
