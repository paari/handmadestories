<?php function nt_forester_css_options() {

   /* CSS to output */
   $theCSS = '';


   // navbar
   $nt_forester_menu_layout_type 		= ( ot_get_option('nt_forester_menu_layout_type') );
   $nt_forester_custom_navoffset       = ot_get_option( 'nt_forester_custom_navoffset' );
   wp_enqueue_script('nt-forester-custom-scripts');
   wp_localize_script('nt-forester-custom-scripts', 'prefix',
   array(
   'navcustomoffset' => $nt_forester_custom_navoffset,
   'header_layout_type' => $nt_forester_menu_layout_type,
   ));


	// admin bar
	if( is_admin_bar_showing() && ! is_customize_preview() ) {
		$theCSS .= '
		 .navbar-fixed-top { top: 32px!important; }
		 .cbp-popup-singlePage .cbp-popup-navigation,.cbp-popup-singlePage { top: 32px!important; }
		 @media (max-width: 992px){
			.navbar-fixed-top { top: 32px!important; }
		}
		@media (max-width: 1350px){ nav.navbar.navbar-collaps { top: 32px!important; } }
		@media (max-width: 782px){ nav.navbar.navbar-collaps { top: 46px!important; } }
		@media (max-width: 782px){ .mobile-help-class-adminbar nav.navbar.navbar-collaps { top: 0px!important; } }
		@media (max-width: 768px){
			.navbar-fixed-top{ top: 46px!important; }
		}
		@media (max-width: 600px){
			html.desktop, body #header.mobile-header-after-scroll,.navbar.transparent-white.nav-collapsed { top: 0px!important; margin-top: 0px !important; }
		}
		@media (max-width: 480px){
			.navbar-fixed-top { top: 46px!important; }
		}';

	}

   // preloader
	function nt_forester_hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
	$r = hexdec(substr($hex,0,1).substr($hex,0,1));
	$g = hexdec(substr($hex,1,1).substr($hex,1,1));
	$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
	$r = hexdec(substr($hex,0,2));
	$g = hexdec(substr($hex,2,2));
	$b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);

	return $rgb; // returns an array with the rgb values
	}


	function nt_forester_colourBrightness($hex, $percent) {
		// Work out if hash given
		$hash = '';
		if (stristr($hex,'#')) {
			$hex = str_replace('#','',$hex);
			$hash = '#';
		}
		/// HEX TO RGB
		$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
		//// CALCULATE
		for ($i=0; $i<3; $i++) {
			// See if brighter or darker
			if ($percent > 0) {
				// Lighter
				$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
			} else {
				// Darker
				$positivePercent = $percent - ($percent*2);
				$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
			}
			// In case rounding up causes us to go to 256
			if ($rgb[$i] > 255) {
				$rgb[$i] = 255;
			}
		}
		//// RBG to Hex
		$hex = '';
		for($i=0; $i < 3; $i++) {
			// Convert the decimal digit to hex
			$hexDigit = dechex($rgb[$i]);
			// Add a leading zero if necessary
			if(strlen($hexDigit) == 1) {
			$hexDigit = "0" . $hexDigit;
			}
			// Append to the hex string
			$hex .= $hexDigit;
		}
		return $hash.$hex;
	}


	// preloader
	if ( ot_get_option( 'nt_forester_pre' ) !='off' ) :
		if ( ot_get_option( 'nt_forester_pre_type' ) !='default' ) :
			$theCSS .= 'div#preloader {';
			if ( ot_get_option( 'nt_forester_prebgcolor' ) !='' ) :
				$theCSS .= 'background-color: '.esc_attr( ot_get_option( 'nt_forester_prebgcolor' ) ).';';
			else :
				$theCSS .= 'background-color: #000;';
			endif;

			   $theCSS .= ' overflow: hidden;
				background-repeat: no-repeat;
				background-position: center center;
				height: 100%;
				left: 0;
				position: fixed;
				top: 0;
				width: 100%;
				z-index: 10000;
			}';

			if ( ot_get_option( 'nt_forester_prespincolor' ) !='' ) :

				$prespincolor 	= ot_get_option( 'nt_forester_prespincolor' );
				$pre_spincolor 	= nt_forester_hex2rgb($prespincolor);
				$pre_spin_color = implode(", ", $pre_spincolor);

				if ( ot_get_option( 'nt_forester_pre_type' ) =='01' ) { $theCSS .= '.loader01:after { background: '.$prespincolor.'!important;}';
				$theCSS .= '.loader01 { border:8px solid '.$prespincolor.'!important; border-right-color: transparent!important;}';}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='02' ) { $theCSS .= '.loader02 { border: 8px solid rgba('.$pre_spin_color.', 0.25)!important;}';
				$theCSS .= '.loader02 { border-top-color: '.$prespincolor.'!important;}'; }
				if ( ot_get_option( 'nt_forester_pre_type' ) =='03' ) { $theCSS .= '.loader03{ border-top-color: '.$prespincolor.'!important;}';
				$theCSS .= '.loader03 { border-bottom-color: '.$prespincolor.'!important;}';}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='04' ) { $theCSS .= '.loader04 { border: 2px solid rgba('.$pre_spin_color.', 0.5)!important;}';
				$theCSS .= '.loader04:after { background: '.$prespincolor.'!important;}';}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='05' ) { $theCSS .= '.loader05 { border-color: '.$prespincolor.'!important;}'; }
				if ( ot_get_option( 'nt_forester_pre_type' ) =='06' ) { $theCSS .= '.loader06:before { border: 4px solid rgba('.$pre_spin_color.', 0.5)!important;}';
				$theCSS .= '.loader06:after { border: 4px solid '.$prespincolor.';}';}

				if ( ot_get_option( 'nt_forester_pre_type' ) =='07' ) {
				$theCSS .= '@keyframes loader-circles {
				  0% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.05), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.1), 27px 0 0 0 rgba('.$pre_spin_color.', 0.2), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.3), 0 27px 0 0 rgba('.$pre_spin_color.', 0.4), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.6), -27px 0 0 0 rgba('.$pre_spin_color.', 0.8), -19px -19px 0 0 '.$prespincolor.'; }
				  12.5% {
					box-shadow: 0 -27px 0 0 '.$prespincolor.', 19px -19px 0 0 rgba('.$pre_spin_color.', 0.05), 27px 0 0 0 rgba('.$pre_spin_color.', 0.1), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.2), 0 27px 0 0 rgba('.$pre_spin_color.', 0.3), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.4), -27px 0 0 0 rgba('.$pre_spin_color.', 0.6), -19px -19px 0 0 rgba('.$pre_spin_color.', 0.8); }
				  25% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.8), 19px -19px 0 0 '.$prespincolor.', 27px 0 0 0 rgba('.$pre_spin_color.', 0.05), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.1), 0 27px 0 0 rgba('.$pre_spin_color.', 0.2), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.3), -27px 0 0 0 rgba('.$pre_spin_color.', 0.4), -19px -19px 0 0 rgba('.$pre_spin_color.', 0.6); }
				  37.5% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.6), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.8), 27px 0 0 0 '.$prespincolor.', 19px 19px 0 0 rgba('.$pre_spin_color.', 0.05), 0 27px 0 0 rgba('.$pre_spin_color.', 0.1), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.2), -27px 0 0 0 rgba('.$pre_spin_color.', 0.3), -19px -19px 0 0 rgba('.$pre_spin_color.', 0.4); }
				  50% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.4), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.6), 27px 0 0 0 rgba('.$pre_spin_color.', 0.8), 19px 19px 0 0 '.$prespincolor.', 0 27px 0 0 rgba('.$pre_spin_color.', 0.05), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.1), -27px 0 0 0 rgba('.$pre_spin_color.', 0.2), -19px -19px 0 0 rgba('.$pre_spin_color.', 0.3); }
				  62.5% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.3), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.4), 27px 0 0 0 rgba('.$pre_spin_color.', 0.6), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.8), 0 27px 0 0 '.$prespincolor.', -19px 19px 0 0 rgba('.$pre_spin_color.', 0.05), -27px 0 0 0 rgba('.$pre_spin_color.', 0.1), -19px -19px 0 0 rgba('.$pre_spin_color.', 0.2); }
				  75% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.2), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.3), 27px 0 0 0 rgba('.$pre_spin_color.', 0.4), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.6), 0 27px 0 0 rgba('.$pre_spin_color.', 0.8), -19px 19px 0 0 '.$prespincolor.', -27px 0 0 0 rgba('.$pre_spin_color.', 0.05), -19px -19px 0 0 rgba('.$pre_spin_color.', 0.1); }
				  87.5% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.1), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.2), 27px 0 0 0 rgba('.$pre_spin_color.', 0.3), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.4), 0 27px 0 0 rgba('.$pre_spin_color.', 0.6), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.8), -27px 0 0 0 '.$prespincolor.', -19px -19px 0 0 rgba('.$pre_spin_color.', 0.05); }
				  100% {
					box-shadow: 0 -27px 0 0 rgba('.$pre_spin_color.', 0.05), 19px -19px 0 0 rgba('.$pre_spin_color.', 0.1), 27px 0 0 0 rgba('.$pre_spin_color.', 0.2), 19px 19px 0 0 rgba('.$pre_spin_color.', 0.3), 0 27px 0 0 rgba('.$pre_spin_color.', 0.4), -19px 19px 0 0 rgba('.$pre_spin_color.', 0.6), -27px 0 0 0 rgba('.$pre_spin_color.', 0.8), -19px -19px 0 0 '.$prespincolor.'; } }';
				}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='08' ) {
				$theCSS .= '@keyframes loader08 {
				  0%, 100% {
					box-shadow: -13px 20px 0 '.$prespincolor.', 13px 20px 0 rgba('.$pre_spin_color.', 0.2), 13px 46px 0 rgba('.$pre_spin_color.', 0.2), -13px 46px 0 rgba('.$pre_spin_color.', 0.2); }
				  25% {
					box-shadow: -13px 20px 0 rgba('.$pre_spin_color.', 0.2), 13px 20px 0 '.$prespincolor.', 13px 46px 0 rgba('.$pre_spin_color.', 0.2), -13px 46px 0 rgba('.$pre_spin_color.', 0.2); }
				  50% {
					box-shadow: -13px 20px 0 rgba('.$pre_spin_color.', 0.2), 13px 20px 0 rgba('.$pre_spin_color.', 0.2), 13px 46px 0 '.$prespincolor.', -13px 46px 0 rgba('.$pre_spin_color.', 0.2); }
				  75% {
					box-shadow: -13px 20px 0 rgba('.$pre_spin_color.', 0.2), 13px 20px 0 rgba('.$pre_spin_color.', 0.2), 13px 46px 0 rgba('.$pre_spin_color.', 0.2), -13px 46px 0 '.$prespincolor.'; } }';
				}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='09' ) {
					$theCSS .= '.loader09, .loader09:after, .loader09:before { background: '.$prespincolor.'!important;}';

					$theCSS .= '@keyframes loader09 {
					  0%, 100% {
						box-shadow: 0 0 0 '.$prespincolor.', 0 0 0 '.$prespincolor.'; }
					  50% {
						box-shadow: 0 -8px 0 '.$prespincolor.', 0 8px 0 '.$prespincolor.'; } }
					}';
				}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='10' ) {
					$theCSS .= '@keyframes loader10 {
					  0% {
						box-shadow: 0 28px 0 -28px '.$prespincolor.'; }
					  100% {
						box-shadow: 0 28px 0 '.$prespincolor.'; } }';
				}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='11' ) {

					$theCSS .= ' .loader11::after, .loader11::before {box-shadow: 0 40px 0 '.$prespincolor.'; }';
					$theCSS .= '@keyframes loader11 {
				  0% {
					box-shadow: 0 40px 0 '.$prespincolor.'; }
				  100% {
					box-shadow: 0 20px 0 '.$prespincolor.'; } }';
				}
				if ( ot_get_option( 'nt_forester_pre_type' ) =='12' ) {
					$theCSS .= '@keyframes loader12 {
					  0% {
						box-shadow: -60px 40px 0 2px '.$prespincolor.', -30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 0 40px 0 0 rgba('.$pre_spin_color.', 0.2), 30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 60px 40px 0 0 rgba('.$pre_spin_color.', 0.2); }
					  25% {
						box-shadow: -60px 40px 0 0 rgba('.$pre_spin_color.', 0.2), -30px 40px 0 2px '.$prespincolor.', 0 40px 0 0 rgba('.$pre_spin_color.', 0.2), 30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 60px 40px 0 0 rgba('.$pre_spin_color.', 0.2); }
					  50% {
						box-shadow: -60px 40px 0 0 rgba('.$pre_spin_color.', 0.2), -30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 0 40px 0 2px '.$prespincolor.', 30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 60px 40px 0 0 rgba('.$pre_spin_color.', 0.2); }
					  75% {
						box-shadow: -60px 40px 0 0 rgba('.$pre_spin_color.', 0.2), -30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 0 40px 0 0 rgba('.$pre_spin_color.', 0.2), 30px 40px 0 2px '.$prespincolor.', 60px 40px 0 0 rgba('.$pre_spin_color.', 0.2); }
					  100% {
						box-shadow: -60px 40px 0 0 rgba('.$pre_spin_color.', 0.2), -30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 0 40px 0 0 rgba('.$pre_spin_color.', 0.2), 30px 40px 0 0 rgba('.$pre_spin_color.', 0.2), 60px 40px 0 2px '.$prespincolor.'; } }';
				}
			endif;
		else:
				$preloaderbg = ( ot_get_option( 'nt_forester_prebgcolor' ) != '' ) ?  ot_get_option( 'nt_forester_prebgcolor' )  : '#090909';
				$prespincolor 	= ot_get_option( 'nt_forester_prespincolor' );
				$pre_spincolor 	= nt_forester_hex2rgb($prespincolor);
				$pre_spin_color = implode(", ", $pre_spincolor);
				$prespincolor = ( $prespincolor != '' ) ? $pre_spin_color : '255,255,255';

			$theCSS .= '.preloader {
				background-color: '.$preloaderbg.';
				width: 100%;
				height: 100%;
				position: fixed;
				top: 0;
				left: 0;
				z-index: 1000000000;
				z-index: 99999999;
				opacity: 1;
				-webkit-transition: opacity .3s;
				-moz-transition: opacity .3s;
				transition: opacity .3s;
			}
			.preloader div {
				position: absolute;
				top: 50%;
				margin: 0 auto;
				position: relative;
				text-indent: -9999em;

				top: 50%;
				height: 50px;
				width: 50px;
				position: relative;
				margin: -25px auto 0 auto;
				display: block;

				border-top: 2px solid rgba('.$prespincolor.', 0.2);
				border-right: 2px solid rgba('.$prespincolor.', 0.2);
				border-bottom: 2px solid rgba('.$prespincolor.', 0.2);
				border-left: 2px solid rgb('.$prespincolor.');

				-webkit-transform: translateZ(0);
				-ms-transform: translateZ(0);
				transform:  translateY(50%);
				-webkit-animation: load9 1.1s infinite linear;
				animation: load9 1.1s infinite linear;
			}
			.preloader div,
			.preloader div:after {
				border-radius: 50%;
				width: 40px;
				height: 40px;
			}
			@-webkit-keyframes load9 {
			  0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			  }
			  100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
			  }
			}
			@keyframes load9 {
			  0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			  }
			  100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
			  }
			}';
		endif;
	endif;

	// frontpage custom color
	$nt_forester_theme_color = ot_get_option( 'nt_forester_theme_color' ) ;
	$custom_color = ot_get_option( 'nt_forester_custom_color' ) ;

	if( $nt_forester_theme_color == 'custom' ){

	$one_color = $custom_color;

	}else{

		$one_color = $nt_forester_theme_color;

	}

	if ( $one_color !='' ) {

		$theCSS .= '
		.counter, .highlight,#widget-area .widget a, .blog-post-social ul li a:hover i, .single-style-1 .pager .previous a, .single-style-1 .pager .next a, .single-style-2 .pager .previous a, .single-style-2 .pager .next a {
			color: '.esc_attr( $one_color ).';
		}
		#back-to-top, .bg-primary {
			background-color: '.esc_attr( $one_color ).'!important;
		}
		.btn-primary,
		.btn-primary:hover,
		.btn-primary:focus,
		.btn-primary:target,
		.btn-primary:active:focus {
			background-color: '.esc_attr( $one_color ).'!important;
		}
		.btn-primary:hover, .btn-primary:active:focus,.single-style-1 .pager .previous a:hover, .single-style-1 .pager .next a:hover, .single-style-2 .pager .previous a:hover, .single-style-2 .pager .next a:hover {
			background-color: '.esc_attr( $one_color ).'!important;
		}
		.btn-primary-sec,
		.btn-primary-sec:hover,
		.btn-primary-sec:focus,
		.btn-primary-sec:target,
		.btn-primary-sec:active:focus {
			background-color: '.esc_attr( $one_color ).'!important;
		}
		.btn-black:hover, .btn-black:focus {
			background-color: '.esc_attr( $one_color ).'!important;
		}
		.cbp-popup-navigation, .cbp-nav-pagination-active, .cbp-l-project-details-list .share-list .share:hover i{
			background-color: '.esc_attr( $one_color ).'!important;
		}
		.cbp-l-project-details-list > li, .cbp-l-project-details-list > div, .cbp-l-project-subtitle, .cbp-l-project-desc-title, .cbp-l-project-details-list .share-list .share i{
			color: '.esc_attr( $one_color ).'!important;
		}
		.cbp-l-project-details-list .share-list .share:hover i{
			color: #fff!important;
		}
		.cbp-l-project-desc-title,.single-style-1 .pager .previous a, .single-style-1 .pager .next a, .single-style-2 .pager .previous a, .single-style-2 .pager .next a{
			border-color: '.esc_attr( $one_color ).'!important;
		}
		.cbp-nav-next, .cbp-nav-prev {
			background: '.esc_attr( $one_color ).'!important;
		}
		.cbp-l-filters-button .cbp-filter-counter {
			background-color: '.esc_attr( $one_color ).';
		}
		.cbp-l-filters-button .cbp-filter-counter::after {
			border-top: 4px solid '.esc_attr( $one_color ).';
		}
		body.error404 .index .searchform input[type="submit"], .pager li > a:hover,body.search article .searchform input[type="submit"], #widget-area #searchform input#searchsubmit, #respond input:hover, .pager li > span, .nav-links span.current, body.error404 .content-error .searchform input[type="submit"]{background-color:'.esc_attr( $one_color ).';}


		.widget-title:after, a { color:'.esc_attr( $one_color ).'; }
		a:hover, a:focus{ color: '.nt_forester_colourBrightness($one_color,-0.8).'; }
		#widget-area .widget ul li a:hover, .entry-title a:hover, .entry-meta a, #share-buttons i:hover{ color:'.esc_attr( $one_color ).'; }

		input[type="color"]:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="email"]:focus, input[type="month"]:focus, input[type="number"]:focus, input[type="password"]:focus, .ie input[type="range"]:focus, .ie9 input[type="range"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="time"]:focus, input[type="url"]:focus, input[type="week"]:focus, select:focus, textarea:focus { border-color:'.esc_attr( $one_color ).'; }

		body.error404 .index .searchform input[type="submit"]:hover, body.search article .searchform input[type="submit"]:hover, input[type="button"]:hover, input[type="submit"]:hover, input[type="reset"]:hover { background-color: '.nt_forester_colourBrightness($one_color,-0.9).';}

		.breadcrubms, .breadcrubms span a span { color: '.nt_forester_colourBrightness($one_color,0.7).';}
		.breadcrubms span { color: '.nt_forester_colourBrightness($one_color,-0.6).';}
		.breadcrubms span a span:hover, .text-logo:hover { color: '.nt_forester_colourBrightness($one_color,-0.8).';}
		';

	}

	//static nav

   $nav_padding 			= ( ot_get_option( 'nt_forester_navspacing', array() ) );
   $static_nav_bg 			= esc_attr( ot_get_option( 'nt_forester_nav_bg' ) );
   $static_navitem 		= esc_attr( ot_get_option( 'nt_forester_navitem' ) );
   $static_navitemhover 	= esc_attr( ot_get_option( 'nt_forester_navitemhover' ) );
   $static_navborder 		= esc_attr( ot_get_option( 'nt_forester_navborder' ) );

	// logo img width and height
	//logo padding
	if ( !empty($nav_padding) ) {
		if(!empty($nav_padding['unit']))   { $logopadunit = $nav_padding['unit'];}else{ $logopadunit = 'px'; }
		if(!empty($nav_padding['top']))    { $theCSS .= '.navbar.transparent-white:not(.nav-collapsed){ padding-top:'.$nav_padding['top'].''.$logopadunit.' !important; }'; }
		if(!empty($nav_padding['bottom'])) { $theCSS .= '.navbar.transparent-white:not(.nav-collapsed){ padding-bottom:'.$nav_padding['bottom'].''.$logopadunit.' !important; }'; }
		if(!empty($nav_padding['right']))  { $theCSS .= '.navbar.transparent-white:not(.nav-collapsed){ padding-right:'.$nav_padding['right'].''.$logopadunit.' !important; }'; }
		if(!empty($nav_padding['left']))   { $theCSS .= '.navbar.transparent-white:not(.nav-collapsed){ padding-left:'.$nav_padding['left'].''.$logopadunit.' !important; }'; }
	}
	if ( $static_nav_bg !='' ) 		{ $theCSS .= '.navbar.transparent-white:not(.nav-collapsed) {background-color:' . $static_nav_bg . '!important;}'; }
	if ( $static_navitem !='' ) 	{ $theCSS .= '.navbar.transparent-white:not(.nav-collapsed) .navbar-nav > li > a {color:' . $static_navitem . '!important;}'; }
	if ( $static_navitemhover !='' ){ $theCSS .= '.navbar.transparent-white:not(.nav-collapsed) .navbar-nav > li > a:hover {color:' . $static_navitemhover . '!important;}'; }
	if ( $static_navborder !='' )   { $theCSS .= '.navbar.transparent-white:not(.nav-collapsed) {border-color:' . $static_navborder . '!important;}'; }

	//sticky nav
	$snav_pad 				= ( ot_get_option( 'nt_forester_stickyspacing', array() ) );
	$nav_fixed_display 		= esc_attr( ot_get_option( 'nt_forester_nav_fixed_display' ) );
	$sticky_navbg 			= esc_attr( ot_get_option( 'nt_forester_nav_fixed_bg' ) );
    $sticky_navitem 		= esc_attr( ot_get_option( 'nt_forester_fixed_navitem' ) );
    $sticky_navitemhover 	= esc_attr( ot_get_option( 'nt_forester_fixed_navitemhover' ) );
    $sticky_navborder 		= esc_attr( ot_get_option( 'nt_forester_fixed_navborder' ) );

	if ( $nav_fixed_display == 'off' ) {$theCSS .= ' .navbar-fixed-top {position: absolute!important;}';}
	if ( $nav_fixed_display != 'off' ) {
		if ( !empty($snav_pad) ) {
			if(!empty($snav_pad['unit']))   { $logopadunit = $snav_pad['unit'];}else{ $logopadunit = 'px'; }
			if(!empty($snav_pad['top']))    { $theCSS .= '.navbar.transparent-white.nav-collapsed{ padding-top:'.$snav_pad['top'].''.$logopadunit.' !important; }'; }
			if(!empty($snav_pad['bottom'])) { $theCSS .= '.navbar.transparent-white.nav-collapsed{ padding-bottom:'.$snav_pad['bottom'].''.$logopadunit.' !important; }'; }
			if(!empty($snav_pad['right']))  { $theCSS .= '.navbar.transparent-white.nav-collapsed{ padding-right:'.$snav_pad['right'].''.$logopadunit.' !important; }'; }
			if(!empty($snav_pad['left']))   { $theCSS .= '.navbar.transparent-white.nav-collapsed{ padding-left:'.$snav_pad['left'].''.$logopadunit.' !important; }'; }
		}
		if ( $sticky_navbg !='' ) 	   {$theCSS .= '.navbar.transparent-white.nav-collapsed {background-color:'.$sticky_navbg.'!important;}'; }
		if ( $sticky_navitem !='' )    {$theCSS .= '.navbar.transparent-white.nav-collapsed .navbar-nav > li > a {color:'.$sticky_navitem.'!important;}'; }
		if ( $sticky_navitemhover !=''){$theCSS .= '.navbar.transparent-white.nav-collapsed .navbar-nav > li > a:hover{color:'.$sticky_navitemhover.'!important;}';}
		if ( $sticky_navborder !='' )  {$theCSS .= '.navbar.transparent-white.nav-collapsed{border-color:'.$sticky_navborder.'!important;}'; }
	}
	//dropdown submenu
	$dropdownbg 			= esc_attr( ot_get_option( 'nt_forester_dropdown_bg' ) );
    $dropdownitem 			= esc_attr( ot_get_option( 'nt_forester_dropdown_item' ) );
    $dropdownitemhover 		= esc_attr( ot_get_option( 'nt_forester_dropdown_itemhover' ) );
    $dropdownitemhoverbg 	= esc_attr( ot_get_option( 'nt_forester_dropdown_itemhoverbg' ) );

	if ( $dropdownbg !='' )  { $theCSS .= '.dropdown-menu {background-color:'.$dropdownbg.'!important;}'; }
	if ( $dropdownitem !='' ){ $theCSS .= '.dropdown-menu > li > a{color:'.$dropdownitem.'!important;}'; }
	if ( $dropdownitemhover !='' ) 	{ $theCSS .= '.dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover, .dropdown-menu > li.active > a{color:'.$dropdownitemhover . '!important;}'; }
	if ( $dropdownitemhoverbg !='' ){ $theCSS .= '.dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover, .dropdown-menu > li.active > a{background-color:'.$dropdownitemhoverbg.'!important;}'; }


	// mobile nav
	$mobile_bg 		 = esc_attr( ot_get_option( 'nt_forester_mobile_bg' ) );
    $mobile_item 	 = esc_attr( ot_get_option( 'nt_forester_mobile_item' ) );
    $mobile_i_h 	 = esc_attr( ot_get_option( 'nt_forester_mobile_itemhover' ) );
    $mobile_i_hbg 	 = esc_attr( ot_get_option( 'nt_forester_mobile_itemhoverbg' ) );
    $mobile_d_i		 = esc_attr( ot_get_option( 'nt_forester_mobile_dropitem' ) );
    $mobile_d_hi	 = esc_attr( ot_get_option( 'nt_forester_mobile_dropitem_hover' ) );
    $mobile_d_hi	 = esc_attr( ot_get_option( 'nt_forester_mobile_dropitem_hover' ) );
    $mobile_menubtnbg= esc_attr( ot_get_option( 'nt_forester_mobile_menubtnbghover' ) );

	$theCSS .= '@media (max-width: 768px){';
		if ( $mobile_bg !='' ) { $theCSS .= '.navbar.transparent-white {background-color:'.$mobile_bg.'!important;}.navbar.transparent-white .dropdown-menu{background-color: transparent!important}';}
		if ( $mobile_item !='' ) { $theCSS .= '.navbar.transparent-white .collapse.in .navbar-nav > li > a{color:'.$mobile_item.'!important;}';}
		if ( $mobile_i_h !='' )  { $theCSS .= '.navbar.transparent-white .collapse.in .navbar-nav > li > a:hover{color:'.$mobile_i_h.'!important;}';}
		if ( $mobile_i_hbg !='' ){ $theCSS .= '.navbar .dropdown-menu > li.active > a, .navbar .dropdown-menu > li > a:hover, .navbar .dropdown-menu > li > a:focus, .navbar li.active > a, .navbar li > a:hover, .navbar li > a:focus{background-color:' . $mobile_i_hbg . ' !important;}';}
		if ( $mobile_d_i !='' )	 { $theCSS .= '.navbar .dropdown-menu > li > a{color:'.$mobile_d_i.'!important;}';}
		if ( $mobile_d_hi !='' ) { $theCSS .= '.navbar .dropdown-menu > li > a:hover{color:'.$mobile_d_hi.'!important;}';}
		if ( $mobile_d_hi !='' ) 	{ $theCSS .= '.dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover, .dropdown-menu > li.active > a{color:'.$mobile_d_hi . '!important;}'; }
		if ( $mobile_menubtnbg !='' ) { $theCSS .= '.navbar.transparent-white .navbar-toggle .icon-bar{background-color:'.$mobile_menubtnbg.'!important;}';}
	$theCSS .= '}';
	// logo style
	$theme_logo_type = esc_attr( ot_get_option( 'nt_forester_logo_type' ) );
	if ( $theme_logo_type == 'text' ){

		//variable for text logo custom style
		$textlogo_fs 			= esc_attr( ot_get_option( 'nt_forester_textlogo_fs' ) );
		$textlogo_fw 			= esc_attr( ot_get_option( 'nt_forester_textlogo_fw' ) );
		$textlogo_fstyle		= esc_attr( ot_get_option( 'nt_forester_textlogo_fstyle' ) );
		$textlogo_ltsp			= esc_attr( ot_get_option( 'nt_forester_textlogo_lettersp' ) );
		$staticlogo_color		= esc_attr( ot_get_option( 'nt_forester_staticlogo_color' ) );
		$stickylogo_color		= esc_attr( ot_get_option( 'nt_forester_stickylogo_color' ) );

		//static menu text logo
		$theCSS .= '.navbar.transparent-white .nt-text-logo {';
		if ( $textlogo_fw 		!= '' ){ $theCSS .= 'font-weight:'.$textlogo_fw.';'; }
		if ( $textlogo_ltsp 	!= '' ){ $theCSS .= 'letter-spacing:'.$textlogo_ltsp.'px;';}
		if ( $textlogo_fs 		!= '' ){ $theCSS .= 'font-size:'.$textlogo_fs.'px;';}
		if ( $textlogo_fstyle 	!= '' ){ $theCSS .= 'font-style:'.$textlogo_fstyle.';';}
		if ( $staticlogo_color 	!= '' ){ $theCSS .= 'color:'.$staticlogo_color.'!important;';}
		$theCSS .= '}';

		//sticky menu text logo
		$theCSS .= '.navbar.transparent-white.nav-collapsed .nt-text-logo {';
		if ( $textlogo_fw 		!= '' ){ $theCSS .= 'font-weight:'.$textlogo_fw.';'; }
		if ( $textlogo_ltsp 	!= '' ){ $theCSS .= 'letter-spacing:'.$textlogo_ltsp.'px;';}
		if ( $textlogo_fs 		!= '' ){ $theCSS .= 'font-size:'.$textlogo_fs.'px;';}
		if ( $textlogo_fstyle 	!= '' ){ $theCSS .= 'font-style:'.$textlogo_fstyle.';';}
		if ( $stickylogo_color 	!= '' ){ $theCSS .= 'color:'.$stickylogo_color.'!important;';}
		$theCSS .= '}';

	}

	// logo dimension var
	$logo 		= ( ot_get_option( 'nt_forester_logo_dimension', array() ) );
	$logo_m 	= ( ot_get_option( 'nt_forester_margin_logo', array() ) );
	$logo_p 	= ( ot_get_option( 'nt_forester_padding_logo', array() ) );

	// logo img width and height
	if ( !empty($logo) ) {
		if(!empty($logo['unit']))   { $logounit = $logo['unit'];}else{ $logounit = 'px'; }
		if(!empty($logo['width']))   { $theCSS .= '.navbar-header img{ width:' . $logo['width'] .''. $logounit . ' !important; }'; }
		if(!empty($logo['height']))  { $theCSS .= '.navbar-header img{ height:' . $logo['height'] .''. $logounit . ' !important; }'; }
	}
	//logo  margin
	if ( !empty($logo_m) ) {
		if(!empty($logo_m['unit']))   { $logomarunit = $logo_m['unit'];}else{ $logomarunit = 'px'; }
		if(!empty($logo_m['top']))    { $theCSS .= '.navbar-header img{ margin-top:' . $logo_m['top'] .''. $logomarunit . ' !important; }'; }
		if(!empty($logo_m['bottom'])) { $theCSS .= '.navbar-header img{ margin-bottom:' . $logo_m['bottom'] .''. $logomarunit . ' !important; }'; }
		if(!empty($logo_m['right']))  { $theCSS .= '.navbar-header img{ margin-right:' . $logo_m['right'] .''. $logomarunit . ' !important; }'; }
		if(!empty($logo_m['left']))   { $theCSS .= '.navbar-header img{ margin-left:' . $logo_m['left'] .''. $logomarunit . ' !important; }'; }
	}
	//logo padding
	if ( !empty($logo_p) ) {
		if(!empty($logo_p['unit']))   { $logopadunit = $logo_p['unit'];}else{ $logopadunit = 'px'; }
		if(!empty($logo_p['top']))    { $theCSS .= '.navbar-header img{ padding-top:' . $logo_p['top'] .''. $logopadunit . ' !important; }'; }
		if(!empty($logo_p['bottom'])) { $theCSS .= '.navbar-header img{ padding-bottom:' . $logo_p['bottom'] .''. $logopadunit . ' !important; }'; }
		if(!empty($logo_p['right']))  { $theCSS .= '.navbar-header img{ padding-right:' . $logo_p['right'] .''. $logopadunit . ' !important; }'; }
		if(!empty($logo_p['left']))   { $theCSS .= '.navbar-header img{ padding-left:' . $logo_p['left'] .''. $logopadunit . ' !important; }'; }
	}
	// sidebar
    $sb_bg		= esc_attr( ot_get_option( 'nt_forester_sidebarwidgetareabgcolor' ) );
	$sb_c		= esc_attr( ot_get_option( 'nt_forester_sidebarwidgetgeneralcolor' ) );
    $sb_t_c		= esc_attr( ot_get_option( 'nt_forester_sidebarwidgettitlecolor' ) );
    $sb_l_c		= esc_attr( ot_get_option( 'nt_forester_sidebarlinkcolor' ) );
    $sb_l_c_h	= esc_attr( ot_get_option( 'nt_forester_sidebarlinkhovercolor' ) );
    $sb_s_t		= esc_attr( ot_get_option( 'nt_forester_sidebarsearchsubmittextcolor' ) );
    $sb_s_bg	= esc_attr( ot_get_option( 'nt_forester_sidebarsearchsubmitbgcolor' ) );

	if ( $sb_bg !='' ) 		{ $theCSS .= '#widget-area { background-color:' . $sb_bg . '!important; padding: 20px;}'; }
	if ( $sb_t_c !='' ) 	{ $theCSS .= '#widget-area .widget-title, .widget-title:after{ color:' . $sb_t_c . '!important;}'; }
	if ( $sb_c !='' ) 		{ $theCSS .= '#widget-area .widget ul{ color:' . $sb_c . '!important;}'; }
	if ( $sb_l_c !='' ) 	{ $theCSS .= '#widget-area .widget ul li a{ color:' . $sb_l_c . '!important;}'; }
	if ( $sb_l_c_h !='' ) 	{ $theCSS .= '#widget-area .widget ul li a:hover{ color:' . $sb_l_c_h . '!important;}'; }
	if ( $sb_s_t !='' ) 	{ $theCSS .= '#widget-area #searchform input#searchsubmit{ color:' . $sb_s_t . '!important;}'; }
	if ( $sb_s_bg !='' ) 	{ $theCSS .= '#widget-area #searchform input#searchsubmit{ background-color:' . $sb_s_bg . '!important;}'; }


	// BLOG HEADER
	$blog_bg_c		= esc_attr( ot_get_option( 'nt_forester_blog_header_bgcolor' ) );
	$blog_h_h		= esc_attr( ot_get_option( 'nt_forester_blog_header_bgheight' ) );
	$blog_h_c		= esc_attr( ot_get_option( 'nt_forester_blog_heading_color' ) );
	$blog_h_fs		= esc_attr( ot_get_option( 'nt_forester_blog_heading_fontsize' ) );
	$blog_sub_h_c	= esc_attr( ot_get_option( 'nt_forester_blog_slogan_color' ) );
	$blog_sub_h_fs	= esc_attr( ot_get_option( 'nt_forester_blog_slogan_fontsize' ) );
	$blog_h_p		= ( ot_get_option( 'nt_forester_blog_h_p', array() ) );

	if ( $blog_bg_c !='' ) 		{ $theCSS .= '.blog .hero-fullwidth { background-color: '.$blog_bg_c .'!important; }'; }
	if ( $blog_h_h !='' ) 		{ $theCSS .= '.blog .hero-fullwidth { height: '.$blog_h_h.'vh !important; max-height: 100%; }';
								$theCSS .= '@media (max-width: 767px){.blog .hero-fullwidth { max-height : 60vh !important; }}';
	}
	if ( $blog_h_c !='' ) 		{ $theCSS .= '.blog .hero-fullwidth .hero-content h1.white, .masonry-post h1.black, .blog-modern-sidebar h1{color: '.$blog_h_c.' !important; }'; }
	if ( $blog_h_fs !='' ) 		{ $theCSS .= '.blog .hero-fullwidth .hero-content h1.white, .masonry-post h1.black, .blog-modern-sidebar h1{font-size: '.$blog_h_fs.'px; }'; }
	if ( $blog_h_fs !='' ) 		{ $theCSS .= '@media (max-width: 767px){.blog .hero-fullwidth .hero-content h1.white{font-size: 50px; }}'; }
	if ( $blog_sub_h_c !='' ) 	{ $theCSS .= '.blog .hero-fullwidth .hero-content .cover-text-sublead, .blog-modern-sidebar .heading-title{color: '.$blog_sub_h_c.' !important; }'; }
	if ( $blog_sub_h_fs !='' ) 	{ $theCSS .= '.blog .hero-fullwidth .hero-content .cover-text-sublead, .blog-modern-sidebar .heading-title{font-size: '.$blog_sub_h_fs.'px !important; }'; }

	if ( !empty($blog_h_p) ) {
		$headerunit = (!empty($blog_h_p['unit'])) ? $blog_h_p['unit'] : 'px';
		if(!empty($blog_h_p['top']))    { $theCSS .= '.blog .hero-fullwidth { padding-top:'.$blog_h_p['top'].''.$headerunit.' !important; }'; }
		if(!empty($blog_h_p['bottom'])) { $theCSS .= '.blog .hero-fullwidth { padding-bottom:'.$blog_h_p['bottom'].''.$headerunit.' !important; }'; }
		if(!empty($blog_h_p['right']))  { $theCSS .= '.blog .hero-fullwidth { padding-right:'.$blog_h_p['right'].''.$headerunit.' !important; }'; }
		if(!empty($blog_h_p['left']))   { $theCSS .= '.blog .hero-fullwidth { padding-left:'.$blog_h_p['left'].''.$headerunit.' !important; }'; }
	}

	$blog_post_m	= ( ot_get_option( 'nt_forester_blog_masonry_custom_margin', array() ) );

	if ( !empty($blog_post_m) ) {
		$headerunit = (!empty($blog_post_m['unit'])) ? $blog_post_m['unit'] : 'px';
		if(!empty($blog_post_m['top']))   { $theCSS .= '.blog-modern-sidebar .cbp-item { margin-top:'.$blog_post_m['top'].''.$headerunit.' !important; }'; }
		if(!empty($blog_post_m['bottom'])){ $theCSS .= '.blog-modern-sidebar .cbp-item { margin-bottom:'.$blog_post_m['bottom'].''.$headerunit.' !important; }'; }
		if(!empty($blog_post_m['right'])) { $theCSS .= '.blog-modern-sidebar .cbp-item { margin-right:'.$blog_post_m['right'].''.$headerunit.' !important; }'; }
		if(!empty($blog_post_m['left']))  { $theCSS .= '.blog-modern-sidebar .cbp-item { margin-left:'.$blog_post_m['left'].''.$headerunit.' !important; }'; }
	}

	// PAGE.PHP AND FULLWIDTH-PAGE.PHP HEADER - CUSTOM PAGE METABOX OPTIONS

	$nt_forester_page_hero_display  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_hero_display', true ) );
	$nt_forester_page_bg_color 	  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_bg_color', true ) );
	$nt_forester_use_bigtitle   	  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_use_bigtitle', true ) );
	$nt_forester_page_title_color   =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_title_color', true ) );
	$nt_forester_page_title_mb 	  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_title_mb', true ) );
	$nt_forester_page_subtitle_color=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_subtitle_color', true ) );
	$nt_forester_page_subtitle_mb	  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_subtitle_mb', true ) );
	$nt_forester_page_subtitle_maxw =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_subtitle_maxw', true ) );
	$nt_forester_page_header_height =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_header_height', true ) );
	$nt_forester_header_p_top 	  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_header_p_top', true ) );
	$nt_forester_header_p_bottom 	  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_header_p_bottom', true ) );
	$nt_forester_page_herobtn_custombg =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_herobtn_custombg', true ) );
	$nt_forester_page_herobtn_hoverbg  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_herobtn_hoverbg', true ) );
	$nt_forester_page_herobtn_titlecolor  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_herobtn_titlecolor', true ) );
	$nt_forester_page_herobtn_titlehover  =  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_herobtn_titlehover', true ) );

	$nt_forester_page_nav_bgcolor	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_nav_bgcolor', true ) );
	$nt_forester_page_sticky_bg	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_sticky_bg', true ) );
	$nt_forester_page_nav_border	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_nav_border_color', true ) );
	$nt_forester_page_nav_color 	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_nav_color', true ) );
	$nt_forester_page_nav_hover	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_page_nav_hovercolor', true ) );

	if ( $nt_forester_page_nav_bgcolor !='' ){$theCSS .= '.navbar.transparent-white:not(.nav-collapsed) {background-color:'.$nt_forester_page_nav_bgcolor.' !important;}'; }
	if ( $nt_forester_page_sticky_bg !='' ){$theCSS .= '.navbar.transparent-white.nav-collapsed {background-color:'.$nt_forester_page_sticky_bg.' !important;}'; }
	if ( $nt_forester_page_nav_border !='' )  {$theCSS .= '.navbar.transparent-white:not(.nav-collapsed)  {border-color:'.$nt_forester_page_nav_border.' !important;}'; }
	if ( $nt_forester_page_nav_color !='' )  {$theCSS .= '.navbar.transparent-white:not(.nav-collapsed)  .navbar-nav > li > a {color:'.$nt_forester_page_nav_color.' !important;}'; }
	if ( $nt_forester_page_nav_hover !='' )  {$theCSS .= '.navbar.transparent-white:not(.nav-collapsed)  .navbar-nav > li > a:hover {color:'.$nt_forester_page_nav_hover.' !important;}'; }

   $f_bgcolor	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_f_bgcolor', true ) );
   $f_border_t_c	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_f_border_t_c', true ) );
   $f_text_c	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_f_text_c', true ) );
   $f_social_bg 	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_f_social_bg', true ) );
   $f_social_i_c	=  esc_attr( get_post_meta(get_the_ID(), 'nt_forester_f_social_i_c', true ) );

   if ( $f_bgcolor !='' ){ $theCSS .= '.nt_theme_footer {background-color:'.$f_bgcolor.' !important;}'; }
   if ( $f_border_t_c !='' ){ $theCSS .= '.nt_theme_footer p, nt_theme_footer  {border-color:'.$f_border_t_c.' !important;}'; }
   if ( $f_text_c !='' )  { $theCSS .= '.nt_theme_footer  {color:'.$f_text_c.' !important;}'; }
   if ( $f_social_bg !='' )  { $theCSS .= '.nt_theme_footer .social-icons li {color:'.$f_social_bg.' !important;}'; }
   if ( $f_social_i_c !='' )  { $theCSS .= '.nt_theme_footer .social-icons li a,.nt_theme_footer .social-icons li a i{color:'.$f_social_i_c.' !important;}'; }


	if ( $nt_forester_page_bg_color !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth{ background-color: '.$nt_forester_page_bg_color.' !important; }';
	}
	if ( $nt_forester_page_title_color !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth h2, .page-id-'.get_the_ID().'.hero-fullwidth h1{ color: '.$nt_forester_page_title_color.' !important; }';
	}
	if ( $nt_forester_use_bigtitle == true ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth .hero-content .container{ padding: 0 ; }';
	}
	if ( $nt_forester_page_title_mb !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth h2{ margin-bottom: '.$nt_forester_page_title_mb.'px !important; }';
	}
	if ( $nt_forester_page_subtitle_color !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth h5.subheading { color: ' .$nt_forester_page_subtitle_color.' !important; }';
	}
	if ( $nt_forester_page_subtitle_mb !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth h5.subheading { margin-bottom: ' .$nt_forester_page_subtitle_mb.'px !important; }';
	}
	if ( $nt_forester_page_subtitle_maxw !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth h5.subheading { max-width: ' .$nt_forester_page_subtitle_maxw.'px !important; margin-left:auto;margin-right:auto;padding-left:15px;padding-right:15px; }';
	}
	if ( $nt_forester_page_header_height !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth { height : '.$nt_forester_page_header_height.'vh !important; max-height:100%; }';
		$theCSS .= '@media (max-width: 767px){.page-id-'.get_the_ID().'.hero-fullwidth { max-height : 60vh !important; }}';
	}
	if ( $nt_forester_header_p_top !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth { padding-bottom : '.$nt_forester_header_p_top.'px !important; }';
	}
	if ( $nt_forester_header_p_bottom !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth { padding-top : '.$nt_forester_header_p_bottom .'px !important; }';
	}
	if ( $nt_forester_page_herobtn_custombg !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth .btn.custom{ background-color : '.$nt_forester_page_herobtn_custombg .' !important; border-color : '.$nt_forester_page_herobtn_custombg .' !important;  }';
	}
	if ( $nt_forester_page_herobtn_hoverbg !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth .btn.custom:hover{ background-color : '.$nt_forester_page_herobtn_hoverbg .' !important; border-color : '.$nt_forester_page_herobtn_hoverbg .' !important; }';
	}
	if ( $nt_forester_page_herobtn_titlecolor !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth .btn.custom{ color : '.$nt_forester_page_herobtn_titlecolor .' !important; }';
	}
	if ( $nt_forester_page_herobtn_titlehover !='' ) {
		$theCSS .= '.page-id-'.get_the_ID().'.hero-fullwidth .btn.custom:hover{ color : '.$nt_forester_page_herobtn_titlehover .' !important; }';
	}

	// SINGLE POST
	if ( ot_get_option( 'nt_forester_single_header_bgcolor' ) !='' ){
    $theCSS .= '.single .hero-fullwidth {background-color: '.esc_attr( ot_get_option( 'nt_forester_single_header_bgcolor' ) ).'; }';
    }
	if ( ot_get_option( 'nt_forester_single_headingcolor' ) !='' ){
    $theCSS .= '.single .hero-fullwidth .title-service h1{ color: '.esc_attr( ot_get_option( 'nt_forester_single_headingcolor' ) ).'; }';
    }
	if ( ot_get_option( 'nt_forester_single_heading_fontsize' ) !='' ){
    $theCSS .= '.single .hero-fullwidth .title-service h1{ font-size: '.esc_attr( ot_get_option( 'nt_forester_single_heading_fontsize' ) ).'px!important; }';
	$theCSS .= '@media (max-width: 767px){.single .hero-fullwidth .title-service h1{font-size: 50px!important; }}';
	}
	if ( ot_get_option( 'nt_forester_single_header_bgheight' ) !='' ){
    $theCSS .= '.single .hero-fullwidth { height: '.esc_attr( ot_get_option( 'nt_forester_single_header_bgheight' ) ).'vh !important; }';
	$theCSS .= '@media (max-width: 767px){.single .hero-fullwidth { max-height : 60vh !important; }}';
    }
	if ( ot_get_option( 'nt_forester_single_header_paddingtop' ) !='' ){
		$theCSS .= '.single .hero-fullwidth { padding-top: '.esc_attr( ot_get_option( 'nt_forester_single_header_paddingtop' ) ).'px !important; }';
	}
	if  ( ot_get_option( 'nt_forester_single_header_paddingbottom' ) !='' ){
		$theCSS .= '.single .hero-fullwidth { padding-bottom: '.esc_attr( ot_get_option( 'nt_forester_single_header_paddingbottom' ) ).'px !important; }';
    }

	// SINGLE PORTFOLIO POST
	if ( ot_get_option( 'nt_forester_singleport_header_bgcolor' ) !='' ){
    $theCSS .= '.single .hero-fullwidth {background-color: '.esc_attr( ot_get_option( 'nt_forester_singleport_header_bgcolor' ) ).'!important; }';
    }
	if ( ot_get_option( 'nt_forester_singleport_headingcolor' ) !='' ){
    $theCSS .= '.single .hero-fullwidth .title-service h1{ color: '.esc_attr( ot_get_option( 'nt_forester_singleport_headingcolor' ) ).'!important; }';
    }
	if ( ot_get_option( 'nt_forester_singleport_heading_fontsize' ) !='' ){
    $theCSS .= '.single .hero-fullwidth .title-service h1{ font-size: '.esc_attr( ot_get_option( 'nt_forester_single_heading_fontsize' ) ).'px!important; }';
	$theCSS .= '@media (max-width: 767px){.single .hero-fullwidth .title-service h1{font-size: 50px!important; }}';
	}
	if ( ot_get_option( 'nt_forester_singleport_header_bgheight' ) !='' ){
    $theCSS .= '.single .hero-fullwidth { height: '.esc_attr( ot_get_option( 'nt_forester_singleport_header_bgheight' ) ).'vh !important; }';
	$theCSS .= '@media (max-width: 767px){.single .hero-fullwidth { max-height : 60vh !important; }}';
    }
	if ( ot_get_option( 'nt_forester_singleport_header_paddingtop' ) !='' ){
		$theCSS .= '.single .hero-fullwidth { padding-top: '.esc_attr( ot_get_option( 'nt_forester_singleport_header_paddingtop' ) ).'px !important; }';
	}
	if  ( ot_get_option( 'nt_forester_singleport_header_paddingbottom' ) !='' ){
		$theCSS .= '.single .hero-fullwidth { padding-bottom: '.esc_attr( ot_get_option( 'nt_forester_singleport_header_paddingbottom' ) ).'px !important; }';
    }

	// ARCHIVE PAGES
	if  ( ot_get_option( 'nt_forester_archive_header_bgcolor' ) !='' ){
    $theCSS .= '.archive .hero-fullwidth {background-color: '.  esc_attr( ot_get_option( 'nt_forester_archive_header_bgcolor' ) ).'!important; }';
    }
	if  ( ot_get_option( 'nt_forester_archive_header_bgheight' ) !='' ){
    $theCSS .= '.archive .hero-fullwidth { height: '.esc_attr( ot_get_option( 'nt_forester_archive_header_bgheight' ) ).'vh !important; }';
	$theCSS .= '@media (max-width: 767px){.archive .hero-fullwidth { max-height : 60vh !important; }}';
    }
	if ( ot_get_option( 'nt_forester_archive_header_paddingtop' ) !='' ){
	$theCSS .= '.archive .hero-fullwidth { padding-top: '.esc_attr( ot_get_option( 'nt_forester_archive_header_paddingtop' ) ).'px !important; }';
	}
	if  ( ot_get_option( 'nt_forester_archive_header_paddingbottom' ) !='' ){
	$theCSS .= '.archive .hero-fullwidth { padding-bottom: '.esc_attr( ot_get_option( 'nt_forester_archive_header_paddingbottom' ) ).'px !important; }';
    }
	if  ( ot_get_option( 'nt_forester_archive_heading_fontsize' ) !='' ){
    $theCSS .= '.archive .hero-fullwidth .title-service h1{ font-size: '.esc_attr( ot_get_option( 'nt_forester_archive_heading_fontsize' ) ).'px!important; }';
	$theCSS .= '@media (max-width: 767px){.archive .hero-fullwidth .title-service h1{font-size: 50px!important; }}';
	}
	if  ( ot_get_option( 'nt_forester_archive_heading_color' ) !='' ){
    $theCSS .= '.archive .hero-fullwidth .title-service h1{ color: '.esc_attr( ot_get_option( 'nt_forester_archive_heading_color' ) ).'!important; }';
	}
	if  ( ot_get_option( 'nt_forester_archive_slogan_fontsize' ) !='' ){
    $theCSS .= '.archive .hero-fullwidth .heading-title{ font-size: '.esc_attr( ot_get_option( 'nt_forester_archive_slogan_fontsize' ) ).'px!important; }';
    }
	if  ( ot_get_option( 'nt_forester_archive_slogan_color' ) !='' ){
    $theCSS .= '.archive .hero-fullwidth .heading-title{ color: '.esc_attr( ot_get_option( 'nt_forester_archive_slogan_color' ) ).'!important; }';
    }

	// ERROR 404 PAGE
	if  ( ot_get_option( 'nt_forester_error_header_bgcolor' ) !='' ){
    $theCSS .= '.error404 .hero-fullwidth {background-color: '.  esc_attr( ot_get_option( 'nt_forester_error_header_bgcolor' ) ).'!important; }';
    }
	if  ( ot_get_option( 'nt_forester_error_header_bgheight' ) !='' ){
    $theCSS .= '.error404 .hero-fullwidth { height: '.esc_attr( ot_get_option( 'nt_forester_error_header_bgheight' ) ).'vh !important; }';
	$theCSS .= '@media (max-width: 767px){.error404 .hero-fullwidth { max-height : 60vh !important; }}';
    }
	if ( ot_get_option( 'nt_forester_error_header_paddingtop' ) !='' ){
	$theCSS .= '.error404 .hero-fullwidth { padding-top: '.esc_attr( ot_get_option( 'nt_forester_error_header_paddingtop' ) ).'px !important; }';
	}
	if  ( ot_get_option( 'nt_forester_error_header_paddingbottom' ) !='' ){
	$theCSS .= '.error404 .hero-fullwidth { padding-bottom: '.esc_attr( ot_get_option( 'nt_forester_error_header_paddingbottom' ) ).'px !important; }';
    }
	if  ( ot_get_option( 'nt_forester_error_heading_fontsize' ) !='' ){
    $theCSS .= '.error404 .hero-fullwidth .title-service h1{ font-size: '.esc_attr( ot_get_option( 'nt_forester_error_heading_fontsize' ) ).'px!important; }';
	$theCSS .= '@media (max-width: 767px){.error404 .hero-fullwidth .title-service h1{font-size: 50px!important; }}';
	}
	if  ( ot_get_option( 'nt_forester_error_heading_color' ) !='' ){
    $theCSS .= '.error404 .hero-fullwidth .title-service h1{ color: '.esc_attr( ot_get_option( 'nt_forester_error_heading_color' ) ).'!important; }';
	}
	if  ( ot_get_option( 'nt_forester_error_slogan_fontsize' ) !='' ){
    $theCSS .= '.error404 .hero-fullwidth .heading-title{ font-size: '.esc_attr( ot_get_option( 'nt_forester_error_slogan_fontsize' ) ).'px!important; }';
    }
	if  ( ot_get_option( 'nt_forester_error_slogan_color' ) !='' ){
    $theCSS .= '.error404 .hero-fullwidth .heading-title{ color: '.esc_attr( ot_get_option( 'nt_forester_error_slogan_color' ) ).'!important; }';
    }

	// SEARCH PAGE
	if  ( ot_get_option( 'nt_forester_search_header_bgcolor' ) !='' ){
    $theCSS .= '.search .hero-fullwidth {background-color: '.  esc_attr( ot_get_option( 'nt_forester_search_header_bgcolor' ) ).'!important; }';
    }
	if  ( ot_get_option( 'nt_forester_search_header_bgheight' ) !='' ){
    $theCSS .= '.search .hero-fullwidth { height: '.esc_attr( ot_get_option( 'nt_forester_search_header_bgheight' ) ).'vh !important; }';
	$theCSS .= '@media (max-width: 767px){.search .hero-fullwidth { max-height : 60vh !important; }}';
    }
	if ( ot_get_option( 'nt_forester_search_header_paddingtop' ) !='' ){
	$theCSS .= '.search .hero-fullwidth { padding-top: '.esc_attr( ot_get_option( 'nt_forester_search_header_paddingtop' ) ).'px !important; }';
	}
	if  ( ot_get_option( 'nt_forester_search_header_paddingbottom' ) !='' ){
	$theCSS .= '.search .hero-fullwidth { padding-bottom: '.esc_attr( ot_get_option( 'nt_forester_search_header_paddingbottom' ) ).'px !important; }';
    }
	if  ( ot_get_option( 'nt_forester_search_heading_fontsize' ) !='' ){
    $theCSS .= '.search .hero-fullwidth .title-service h1{ font-size: '.esc_attr( ot_get_option( 'nt_forester_search_heading_fontsize' ) ).'px!important; }';
	$theCSS .= '@media (max-width: 767px){.search .hero-fullwidth .title-service h1{font-size: 50px!important; }}';
	}
	if  ( ot_get_option( 'nt_forester_search_heading_color' ) !='' ){
    $theCSS .= '.search .hero-fullwidth .title-service h1{ color: '.esc_attr( ot_get_option( 'nt_forester_search_heading_color' ) ).'!important; }';
	}
	if  ( ot_get_option( 'nt_forester_search_slogan_fontsize' ) !='' ){
    $theCSS .= '.search .hero-fullwidth .heading-title{ font-size: '.esc_attr( ot_get_option( 'nt_forester_search_slogan_fontsize' ) ).'px!important; }';
    }
	if  ( ot_get_option( 'nt_forester_search_slogan_color' ) !='' ){
    $theCSS .= '.search .hero-fullwidth .heading-title{ color: '.esc_attr( ot_get_option( 'nt_forester_search_slogan_color' ) ).'!important; }';
    }

	// BREADCRUBMS
	if  ( ot_get_option( 'nt_forester_breadcrubms_color' ) !='' ){
     $theCSS .= '.breadcrubms, .breadcrubms span a span{color: '.  esc_attr( ot_get_option( 'nt_forester_breadcrubms_color' ) ) .'; }';
	}
	if  ( ot_get_option( 'nt_forester_breadcrubms_hovercolor' ) !='' ){
     $theCSS .= '.breadcrubms span a span:hover{color: '.  esc_attr( ot_get_option( 'nt_forester_breadcrubms_hovercolor' ) ) .'; }';
	}
	if  ( ot_get_option( 'nt_forester_breadcrubms_currentcolor' ) !='' ){
     $theCSS .= '.breadcrubms span {color: '.  esc_attr( ot_get_option( 'nt_forester_breadcrubms_currentcolor' ) ) .'; }';
	}
	if  ( ot_get_option( 'nt_forester_breadcrubms_font_size' ) !='' ){
     $theCSS .= '.breadcrubms{font-size: '.  esc_attr( ot_get_option( 'nt_forester_breadcrubms_font_size' ) ) .'px; }';
	}

	// FOOTER WIDGETIZE OPTIONS
	$nt_forester_fw_bg_img	= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_img' ) );
	$nt_forester_fw_bg_img_parallax= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_img_parallax' ) );
	$nt_forester_fw_bg_overlay= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_overlay' ) );
	$nt_forester_fw_bg_overlaycolor= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_overlaycolor' ) );
	$nt_forester_fw_bg_c		= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_c' ) );
	$nt_forester_fw_pad_t		= 	esc_attr( ot_get_option( 'nt_forester_fw_pad_top' ) );
	$nt_forester_fw_pad_b		= 	esc_attr( ot_get_option( 'nt_forester_fw_pad_bot' ) );
	$nt_forester_fw_h_c		= 	esc_attr( ot_get_option( 'nt_forester_fw_h_c' ) );
	$nt_forester_fw_a_c		= 	esc_attr( ot_get_option( 'nt_forester_fw_a_c' ) );
	$nt_forester_fw_a_hc		= 	esc_attr( ot_get_option( 'nt_forester_fw_a_hc' ) );
	$nt_forester_fw_t_c		= 	esc_attr( ot_get_option( 'nt_forester_fw_t_c' ) );

	// WIDGETIZE FOOTER
	if  ( $nt_forester_fw_bg_img !='' ){
		$theCSS .= '#footer-widget-area{position:relative;}';
		if ( $nt_forester_fw_bg_img_parallax =='off' ){
		$theCSS .= '#footer-widget-area{background-image: url('.esc_url($nt_forester_fw_bg_img).')!important;position:relative;}';
		$theCSS .= '.background-image-overlay{z-index:0!important;}';
		}
	}
	if  ( $nt_forester_fw_bg_overlay =='off' ){
	$theCSS .= '#footer-widget-area .background-image-overlay{display:none!important;}';
	}
	if  ( $nt_forester_fw_bg_overlay !='off' ){
		$theCSS .= '#footer-widget-area .background-image-overlay{position:absolute;width:100%;height:100%;top:0;left:0;background-color:rgba(0,0,0,0.5);z-index:1;}';
		if  ( $nt_forester_fw_bg_overlaycolor !='' ){
		$theCSS .= '#footer-widget-area .background-image-overlay{background-color:'.$nt_forester_fw_bg_overlaycolor.';}';
		}
	}

	if  ( $nt_forester_fw_bg_c !='' ){
    $theCSS .= '#footer-widget-area{ background-color: '.$nt_forester_fw_bg_c.'; }';
    }
	if  ( $nt_forester_fw_pad_t !='' ){
    $theCSS .= '#footer-widget-area{ padding-top: '.$nt_forester_fw_pad_t.'px!important; }';
    }
	if  ( $nt_forester_fw_pad_b !='' ){
    $theCSS .= '#footer-widget-area{ padding-bottom: '.$nt_forester_fw_pad_b.'px!important; }';
    }
	if  ( $nt_forester_fw_h_c !='' ){
     $theCSS .= '#footer-widget-area .widget .widget-head{color: '.$nt_forester_fw_h_c.'!important; }';
	}
	if  ( $nt_forester_fw_a_c ){
     $theCSS .= '#footer-widget-area .widget ul li a, #footer-widget-area .widget a{ color: '.$nt_forester_fw_a_c.'!important; }';
	}
	if  ( $nt_forester_fw_a_hc ){
     $theCSS .= '#footer-widget-area .widget ul li a:hover, #footer-widget-area .widget a:hover{ color: '.$nt_forester_fw_a_hc.'!important; }';
	}
	if  ( $nt_forester_fw_t_c !='' ){
     $theCSS .= '#footer-widget-area .widget .textwidget{color: '. $nt_forester_fw_t_c .'!important; }';
	}

	// FOOTER OPTIONS
	$nt_forester_f_p_c		= 	esc_attr( ot_get_option( 'nt_forester_footer_p_c' ) );
	$nt_forester_f_s_c		= 	esc_attr( ot_get_option( 'nt_forester_footer_s_c' ) );
	$nt_forester_f_s_hc		= 	esc_attr( ot_get_option( 'nt_forester_footer_s_hc' ) );
	$nt_forester_f_add_c		= 	esc_attr( ot_get_option( 'nt_forester_f_add_c' ) );

	if ( $nt_forester_f_p_c !='' ) 	{
		$theCSS .= '#footer .footer-copyright{ color: '.  $nt_forester_f_p_c .'; }';
	}
	if ( $nt_forester_f_s_c !='' ) 	{
		$theCSS .= '#footer .social-icons li a{ color: '.  $nt_forester_f_s_c .'; }';
	}
	if ( $nt_forester_f_s_hc !='' ) 	{
		$theCSS .= '#footer .social-icons li a:hover{ color: '.  $nt_forester_f_s_hc .'; }';
	}
	if ( $nt_forester_f_add_c !='' ) 	{
		$theCSS .= '#footer .footer-info h6{ color: '.  $nt_forester_f_add_c .'; }';
	}

// tipigrof
	$nt_forester_typgrph = ot_get_option( 'nt_forester_typgrph', array() );
	if ( !empty($nt_forester_typgrph) ) :
	$theCSS .= 'body{';
	if ( !empty($nt_forester_typgrph['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['font-family']) ) {$theCSS .= 'font-family:"'.esc_attr( $nt_forester_typgrph['font-family'] ).'"!important;'; }
	if ( !empty($nt_forester_typgrph['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	//
	$nt_forester_typgrph1 = ot_get_option( 'nt_forester_typgrph1', array() );
	if ( !empty($nt_forester_typgrph1) ) :
	$theCSS .= 'body h1{';
	if ( !empty($nt_forester_typgrph1['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph1['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['font-family']) ) {$theCSS .= 'font-family:"'.esc_attr( $nt_forester_typgrph1['font-family'] ).'"!important;'; }
	if ( !empty($nt_forester_typgrph1['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph1['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph1['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph1['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph1['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph1['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph1['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph1['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph1['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph1['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph1['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	//
	$nt_forester_typgrph2 = ot_get_option( 'nt_forester_typgrph2', array() );
	if ( !empty($nt_forester_typgrph2) ) :
	$theCSS .= 'body h2{';
	if ( !empty($nt_forester_typgrph2['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph2['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['font-family']) ) {$theCSS .= 'font-family:'.esc_attr( $nt_forester_typgrph2['font-family'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph2['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph2['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph2['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph2['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph2['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph2['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph2['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph2['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph2['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph2['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	//
	$nt_forester_typgrph3 = ot_get_option( 'nt_forester_typgrph3', array() );
	if ( !empty($nt_forester_typgrph3) ) :
	$theCSS .= 'body h3{';
	if ( !empty($nt_forester_typgrph3['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph3['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['font-family']) ) {$theCSS .= 'font-family:'.esc_attr( $nt_forester_typgrph3['font-family'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph3['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph3['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph3['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph3['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph3['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph3['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph3['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph3['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph3['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph3['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	//
	$nt_forester_typgrph4 = ot_get_option( 'nt_forester_typgrph4', array() );
	if ( !empty($nt_forester_typgrph4) ) :
	$theCSS .= 'body h4{';
	if ( !empty($nt_forester_typgrph4['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph4['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['font-family']) ) {$theCSS .= 'font-family:'.esc_attr( $nt_forester_typgrph4['font-family'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph4['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph4['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph4['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph4['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph4['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph4['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph4['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph4['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph4['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph4['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	//
	$nt_forester_typgrph5 = ot_get_option( 'nt_forester_typgrph5', array() );
	if ( !empty($nt_forester_typgrph5) ) :
	$theCSS .= 'body h5{';
	if ( !empty($nt_forester_typgrph5['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph5['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['font-family']) ) {$theCSS .= 'font-family:'.esc_attr( $nt_forester_typgrph5['font-family'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph5['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph5['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph5['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph5['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph5['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph5['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph5['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph5['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph5['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph5['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	//
	$nt_forester_typgrph6 = ot_get_option( 'nt_forester_typgrph6', array() );
	if ( !empty($nt_forester_typgrph6) ) :
	$theCSS .= 'body h6{';
	if ( !empty($nt_forester_typgrph6['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph6['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['font-family']) ) {$theCSS .= 'font-family:'.esc_attr( $nt_forester_typgrph6['font-family'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph6['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph6['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph6['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph6['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph6['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph6['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph6['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph6['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph6['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph6['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;
	$nt_forester_typgrph7 = ot_get_option( 'nt_forester_typgrph7', array() );
	if ( !empty($nt_forester_typgrph7) ) :
	$theCSS .= 'body p{';
	if ( !empty($nt_forester_typgrph7['font-color']) ) {$theCSS .= 'color:'.esc_attr( $nt_forester_typgrph7['font-color'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['font-family']) ) {$theCSS .= 'font-family:'.esc_attr( $nt_forester_typgrph7['font-family'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['font-size']) ) {$theCSS .= 'font-size:'.esc_attr( $nt_forester_typgrph7['font-size'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['font-style']) ) {$theCSS .= 'font-style:'.esc_attr( $nt_forester_typgrph7['font-style'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['font-variant']) ) {$theCSS .= 'font-variant:'.esc_attr( $nt_forester_typgrph7['font-variant'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['font-weight']) ) {$theCSS .= 'font-weight:'.esc_attr( $nt_forester_typgrph7['font-weight'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['letter-spacing']) ) {$theCSS .= 'letter-spacing:'.esc_attr( $nt_forester_typgrph7['letter-spacing'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['line-height'])) {$theCSS .= 'line-height:'.esc_attr( $nt_forester_typgrph7['line-height'] ).'!important;'; }
	if ( !empty($nt_forester_typgrph7['text-decoration'])){$theCSS .= 'text-decoration:'.esc_attr($nt_forester_typgrph7['text-decoration']).'!important;'; }
	if ( !empty($nt_forester_typgrph7['text-transform'])){$theCSS .= 'text-transform:'.esc_attr($nt_forester_typgrph7['text-transform']).'!important;'; }
	$theCSS .= '}';
	endif;

    /* Add CSS to style.css */
    wp_add_inline_style( 'nt-forester-custom-style', $theCSS );
	}

add_action( 'wp_enqueue_scripts', 'nt_forester_css_options' );
