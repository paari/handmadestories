<?php
/**
 * Custom template parts for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package nt_forester
*/

// Start null function

// START PRELOADER
if ( ! function_exists( 'nt_forester_preloader' ) ) :
	function nt_forester_preloader() {

		if ( ot_get_option('nt_forester_pre') != 'off' ) { ?>

		<?php if ( ot_get_option('nt_forester_pre_type') == 'default' OR  ot_get_option('nt_forester_pre_type') == '' ) : ?>
			<div class="preloader default"><div></div></div>
		<?php else : ?>
         <div id="preloader" class="preloader"><div class="loader<?php echo esc_attr( ot_get_option('nt_forester_pre_type') ); ?>"></div></div>
		<?php
         endif;
		}
	}
endif;
add_action( 'nt_forester_preloader_action',  'nt_forester_preloader', 10 );


// START BACKTOP
if ( ! function_exists( 'nt_forester_backtop' ) ) :

	function nt_forester_backtop() {

		 ?>
        <!-- Start Back To Top Section -->
        <a data-scroll id="back-to-top" href="#hero"><i class="icon ion-chevron-up"></i></a>
        <!-- End Back To Top Section -->
		<?php
	}
endif;

add_action( 'nt_forester_backtop_action',  'nt_forester_backtop', 10 );


// Start logo
if ( ! function_exists( 'nt_forester_logo' ) ) :

	function nt_forester_logo() {

		$nt_forester_logo_type 		= ( ot_get_option('nt_forester_logo_type') );
		$nt_forester_text_logo 		= ( ot_get_option('nt_forester_textlogo') );
		$nt_forester_img_staticlogo 	= ( ot_get_option('nt_forester_img_staticlogo') );
		$nt_forester_img_stickylogo	= ( ot_get_option('nt_forester_img_stickylogo') );

		// TEXT LOGO
		if ( ( $nt_forester_logo_type ) == 'text' ) :
			if ( $nt_forester_text_logo ) : ?>
				<a id="header-logo" class="site-logo nt-text-logo nt-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html( $nt_forester_text_logo ); ?></a>
			<?php else : ?>
				<a id="header-logo" class="site-logo nt-text-logo nt-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html_e( 'forester', 'nt-forester' ); ?></a>
			<?php endif;
		endif;

		// IMAGE LOGO
		if ( ( $nt_forester_logo_type ) == 'img' || ( $nt_forester_logo_type ) == '') :
			if ( $nt_forester_img_staticlogo || $nt_forester_img_stickylogo  ) : ?>
				<a  class="nt-img-logo nt-logo a-logo-white" href="<?php echo esc_url( home_url( '/' ) ); ?>" ><img src="<?php echo esc_url( $nt_forester_img_staticlogo ); ?>" alt="Logo" class="logo-white hide-mobile"></a>
				<a  class="logo-dark site-logo nt-img-logo nt-logo a-logo-dark" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $nt_forester_img_stickylogo ); ?>" alt="Logo" class="logo-black"></a>
			<?php else : ?>
            <a  class="nt-img-logo nt-logo a-logo-white" href="<?php echo esc_url( home_url( '/' ) ); ?>" ><img src="<?php  echo get_theme_file_uri(); ?>/images/logo.png" alt="Logo" class="logo-white hide-mobile"></a>
            <a  class="logo-dark site-logo nt-img-logo nt-logo a-logo-dark" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php  echo get_theme_file_uri(); ?>/images/logo-dark.png ?>" alt="Logo" class="logo-black"></a>

			<?php endif;
		endif;
	}
endif;

add_action( 'nt_forester_logo_action',  'nt_forester_logo', 10 );


// START PRIMARY MENU
if ( ! function_exists( 'nt_forester_menu' ) ) :
	function nt_forester_menu() {
	?>

		<nav id="navigation">
			<?php

				wp_nav_menu( array(
					'menu'              => 'primary',
					'theme_location'    => 'primary',
					'depth'             => 4,
					'container'         => '',
					'container_class'   => '',
					'menu_class'        => 'nav navbar-nav navbar-right',
					'menu_id'		    => '',
					'echo' 				=> true,
					'fallback_cb'       => 'nt_forester_Wp_Bootstrap_Navwalker::fallback',
					'walker'            => new nt_forester_Wp_Bootstrap_Navwalker()
				));

			?>
		</nav>

	<?php
	}
endif;

add_action( 'nt_forester_menu_action',  'nt_forester_menu', 10 );


// START METABOXMENU
if ( ! function_exists( 'nt_forester_combinemenu' ) ) :
	function nt_forester_combinemenu() {

      $nt_forester_menu_layout_type 		= ( ot_get_option('nt_forester_menu_layout_type') );
		$nt_forester_menu_item_name 	      = 	rwmb_meta( 'nt_forester_section_name' );
		$nt_forester_menu_item_url 	      = 	rwmb_meta( 'nt_forester_section_url' );
		$nt_forester_menutype 		         = 	rwmb_meta( 'nt_forester_menutype' );
?>
<?php  if( $nt_forester_menu_layout_type == '1' OR $nt_forester_menu_layout_type == '' ) : ?>
   <!-- TRANSPARENT WHITE NAV BAR -->
 <nav class="navbar sticky transparent-white  nt-theme-menu nav-logo-center">
      <div class="container fluid-container hidden-md hidden-1350">
          <div class="row">

             <div class="col-md-3">
                 <div class="hero-logo">
                    <div class="logo-img">
                        <?php  do_action( 'nt_forester_logo_action'); ?>
                    </div>
                 </div>
             </div>

              <div class="col-md-9">
                 <?php

                     wp_nav_menu( array(
                       'menu'              => 'primary',
                       'theme_location'    => 'primary',
                       'depth'             => 4,
                       'container'         => '',
                       'container_class'   => '',
                       'menu_class'        => 'ul-h nav-menu nav-menu-left',
                       'menu_id'		    => '',
                       'echo' 				=> true,
                       'fallback_cb'       => 'nt_forester_Wp_Bootstrap_Navwalker::fallback',
                       'walker'            => new nt_forester_Wp_Bootstrap_Navwalker()
                     ));

                  ?>
              </div>

          </div>
      </div>

      <!-- Collapsed Navigation Bar -->
      <nav class="navbar navbar-collaps hidden-1350-mobile">
          <div class="container fluid-container">
              <div class="collapsed-hero-logo">
                  <div class="logo-img">
                      <?php  do_action( 'nt_forester_logo_action'); ?>
                  </div>
              </div>
              <div class="hamburger-menu">
                  <button type="button" class="hamburger-btn clicked">
                      <span class="sr-only"><?php  esc_html_e( 'Toggle navigation','nt-forester' ); ?></span><span class="icon-bar top-bar"></span><span class="icon-bar middle-bar"></span><span class="icon-bar bottom-bar"></span>
                  </button>
              </div>
          </div>
          <!-- Collapsed Dropdown -->
          <div class="collapsed-dropdown">
              <?php

                  wp_nav_menu( array(
                    'menu'              => 'primary',
                    'theme_location'    => 'primary',
                    'depth'             => 4,
                    'container'         => '',
                    'container_class'   => '',
                    'menu_class'        => 'ul-h mobile-menu-inner',
                    'menu_id'		    => '',
                    'echo' 				=> true,
                    'fallback_cb'       => 'nt_forester_Wp_Bootstrap_Navwalker::fallback',
                    'walker'            => new nt_forester_Wp_Bootstrap_Navwalker()
                  ));

               ?>
          </div>
          <!-- -->
      </nav>
      <!-- -->
 </nav>
	    <?php  	endif; ?>

       <?php  if( $nt_forester_menu_layout_type == '2') : ?>
        <!-- TRANSPARENT WHITE NAV BAR -->
        <nav class="navbar sticky transparent-white  nt-theme-menu nav-logo-center">
            <div class="container fluid-container hidden-md hidden-1350">
                <div class="row">
                    <div class="col-md-5">
                       <?php

                           wp_nav_menu( array(
                             'menu'              => 'primary',
                             'theme_location'    => 'primary',
                             'depth'             => 4,
                             'container'         => '',
                             'container_class'   => '',
                             'menu_class'        => 'ul-h nav-menu nav-menu-left',
                             'menu_id'		    => '',
                             'echo' 				=> true,
                             'fallback_cb'       => 'nt_forester_Wp_Bootstrap_Navwalker::fallback',
                             'walker'            => new nt_forester_Wp_Bootstrap_Navwalker()
                           ));

                        ?>
                    </div>
                    <div class="col-md-2">

                        <div class="hero-logo">
                            <div class="logo-img">
                                <?php  do_action( 'nt_forester_logo_action'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                           <?php
                              $nt_forester_footer_social  	= 	ot_get_option( 'nt_forester_footer_social', array() );
                              $nt_forester_footer_soc_tar 	= 	ot_get_option( 'nt_forester_footer_social_target' );
                						if ( ot_get_option( 'nt_forester_footer_social_display' ) != 'off' ) :
                							if ( ! empty( $nt_forester_footer_social ) ) :
                           ?>
                								<ul class="ul-h nav-menu nav-social nav-social-right">
                									<?php foreach( $nt_forester_footer_social as $soc ) {
                										if ( ! empty( $soc ) ) { ?>
                											<li><a href="<?php echo esc_html( $soc['nt_forester_footer_social_link'] ); ?>" <?php if ( $nt_forester_footer_soc_tar != '' ) : ?> target="<?php echo esc_html( $nt_forester_footer_soc_tar ); ?>"<?php endif; ?>>
                                                    <i class="<?php echo esc_attr( $soc['nt_forester_footer_social_text'] ); ?> x3"></i><?php echo esc_attr( $soc['nt_forester_footer_social_title'] ); ?></a></li>
                									<?php
                                              }
                                           }
                                           ?>
                								</ul>
       							<?php
             							endif;
             						endif;
                            ?>
                    </div>
                </div>
            </div>

            <!-- Collapsed Navigation Bar -->
            <nav class="navbar navbar-collaps hidden-1350-mobile">
                <div class="container fluid-container">
                    <div class="collapsed-hero-logo">
                        <div class="logo-img">
                            <?php  do_action( 'nt_forester_logo_action'); ?>
                        </div>
                    </div>
                    <div class="hamburger-menu">
                        <button type="button" class="hamburger-btn clicked">
                            <span class="sr-only"><?php  esc_html_e( 'Toggle navigation','nt-forester' ); ?></span><span class="icon-bar top-bar"></span><span class="icon-bar middle-bar"></span><span class="icon-bar bottom-bar"></span>
                        </button>
                    </div>
                </div>
                <!-- Collapsed Dropdown -->
                <div class="collapsed-dropdown">
                    <?php

                        wp_nav_menu( array(
                          'menu'              => 'primary',
                          'theme_location'    => 'primary',
                          'depth'             => 4,
                          'container'         => '',
                          'container_class'   => '',
                          'menu_class'        => 'ul-h mobile-menu-inner',
                          'menu_id'		    => '',
                          'echo' 				=> true,
                          'fallback_cb'       => 'nt_forester_Wp_Bootstrap_Navwalker::fallback',
                          'walker'            => new nt_forester_Wp_Bootstrap_Navwalker()
                        ));

                     ?>
                </div>
                <!-- -->
            </nav>
            <!-- -->
        </nav>
        <!-- -->
      <?php  	endif; ?>

<?php
	}
endif;

add_action( 'nt_forester_combinemenu_action',  'nt_forester_combinemenu', 10 );


// START WIDGETIZE FOOTER
if ( ! function_exists( 'nt_forester_widgetize' ) ) :
	function nt_forester_widgetize() {

		if ( ot_get_option('nt_forester_widgetize') != 'off') :
			$nt_forester_fw_bg_img	= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_img' ) );
			$nt_forester_fw_bg_img_parallax	= 	esc_attr( ot_get_option( 'nt_forester_fw_bg_img_parallax' ) );
			$nt_forester_parallax_class = ( $nt_forester_fw_bg_img_parallax !='off' ) ? ' parallax' : '';


			if ( is_active_sidebar( 'nt-forester-footer-widgetize' ) ) { ?>
			<div id="footer-widget-area" class="footer-top footer-widgetize<?php echo($nt_forester_parallax_class); ?>">
				<div class="background-image-overlay"></div>
				<?php if ( $nt_forester_fw_bg_img_parallax !='off' ){
					if ( $nt_forester_fw_bg_img !='' ){ ?>
						<div class="background-image">
							<img src="<?php echo esc_url($nt_forester_fw_bg_img); ?>" alt="footer-parallax-bg">
						</div>
				<?php } }?>
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'nt-forester-footer-widgetize' ); ?>
					</div>
				</div>
			</div>
			<?php }
		endif;
	}
endif;

add_action( 'nt_forester_widgetize_action',  'nt_forester_widgetize', 10 );


// Start header top
if ( ! function_exists( 'nt_forester_footer' ) ) :

	function nt_forester_footer() {

		$nt_forester_footer_phone 	   = 	( ot_get_option('nt_forester_footer_phone') );
		$phone_i 	                  = 	( ot_get_option('nt_forester_footer_phone_i') );
		$nt_forester_footer_address 	= 	( ot_get_option('nt_forester_footer_address') );
		$address_i 	                  = 	( ot_get_option('nt_forester_footer_address_i') );
		$nt_forester_footer_mail 	  	= 	( ot_get_option('nt_forester_footer_mail') );
		$mail_i 	  	                  = 	( ot_get_option('nt_forester_footer_mail_i') );

		$nt_forester_footer_social  	= 	ot_get_option( 'nt_forester_footer_social', array() );
		$nt_forester_footer_soc_tar 	= 	ot_get_option( 'nt_forester_footer_social_target' );

		$nt_forester_footer_menu  	= 	ot_get_option( 'nt_forester_footer_menu', array() );
		$nt_forester_footer_menu_tar 	= 	ot_get_option( 'nt_forester_footer_menu_target' );


		if ( ot_get_option( 'nt_forester_footer_display' ) != 'off' ) :
   	if ( ot_get_option( 'nt_forester_footer_type' ) == '1' ) :
      ?>
			<footer id="footer" class="bg-gray nt_forester_footer_type_1 nt_theme_footer">
				<div class="pt60 pb60">
   				<div class="container">
   				   <div class="row">

   					   <div class="text-center m-auto">

            						<?php if ( ot_get_option( 'nt_forester_footer_social_display' ) != 'off' ) :
            							if ( ! empty( $nt_forester_footer_social ) ) : ?>
            								<ul class="social-icons">
            									<?php foreach( $nt_forester_footer_social as $soc ) {
            										if ( ! empty( $soc ) ) { ?>
            											<li><a href="<?php echo esc_html( $soc['nt_forester_footer_social_link'] ); ?>" <?php if ( $nt_forester_footer_soc_tar != '' ) : ?> target="<?php echo esc_html( $nt_forester_footer_soc_tar ); ?>"<?php endif; ?>>
                                                <i class="<?php echo esc_attr( $soc['nt_forester_footer_social_text'] ); ?> x3"></i><?php echo esc_attr( $soc['nt_forester_footer_social_title'] ); ?></a></li>
            									<?php } } ?>
            								</ul>
            							<?php
            							endif; 	endif;

            						// header top social section
            						if ( ot_get_option( 'nt_forester_footer_contact_info_display' ) != 'off' ) :
                                  $phone_i_0 = $phone_i !='' ? $phone_i : 'fa fa-phone' ;
                                  $mail_i_0 = $mail_i !='' ? $mail_i : 'fa fa-envelope-o' ;
                                  $address_i_0 = $address_i !='' ? $address_i : 'fa fa-home' ;
                                 ?>
            							<div class="footer-info">
            								<?php if ( $nt_forester_footer_phone != '' ) : ?> 	<h6><i class="<?php echo esc_attr( $phone_i_0 ); ?> pr10"></i><?php echo esc_html( $nt_forester_footer_phone ); ?></h6> <?php endif; ?>
            								<?php if ( $nt_forester_footer_mail != '' ) : ?> 	<h6><i class="<?php echo esc_attr( $mail_i_0 ); ?> pr10"></i><?php echo esc_html( $nt_forester_footer_mail ); ?></h6> 	<?php endif; ?>
            								<?php if ( $nt_forester_footer_address != '' ) : ?> <h6><i class="<?php echo esc_attr( $address_i_0 ); ?> pr10"></i><?php echo esc_html( $nt_forester_footer_address ); ?></h6> 	<?php endif; ?>
            							</div>
            						<?php
                                 endif;
                              	do_action('nt_forester_copyright_action');
            						?>

		                  </div>

                  </div>
               </div>
    			</div>
    		</footer>
            <?php endif; // footer-1 end ?>

            <?php if ( ot_get_option( 'nt_forester_footer_type' ) == '2' ) :  ?>
               <footer class="footer-black-1 nt_forester_footer_type_2 nt_theme_footer">
                  <div class="container pt100 pb100">
                       <div class="row white">
                           <div class="col-md-9 col-sm-8">

                                 <?php if ( ot_get_option( 'nt_forester_footer_menu_display' ) != 'off' ) :
                                    if ( ! empty( $nt_forester_footer_menu ) ) : ?>

                                     <ul class="ul-h nav-menu width100-sm">
                                        <?php foreach( $nt_forester_footer_menu as $mi ) {
                                           if ( ! empty( $mi ) ) { ?>
                                              <li><a href="<?php echo esc_html( $mi['nt_forester_footer_menu_link'] ); ?>"
                                                 <?php if ( $nt_forester_footer_menu_tar != '' ) : ?> target="<?php echo esc_html( $nt_forester_footer_menu_tar ); ?>"<?php endif; ?> class="btn-scroll">
                                                 <?php echo esc_attr( $mi['nt_forester_footer_menu_title'] ); ?></a></li>
                                        <?php } } ?>
                                     </ul>

                                  <?php endif; endif;
                                   if ( ot_get_option( 'nt_forester_footer_contact_info_display' ) != 'off' ) : ?>
                               <ul class="ul-h footer-contact-info">
                                   <?php if ( $nt_forester_footer_phone != '' ) : ?> <li><p><i class="fa fa-envelope-o pr5"></i><?php echo esc_html( $nt_forester_footer_phone ); ?></p></li>  <?php endif; ?>
                                   <?php if ( $nt_forester_footer_mail != '' ) : ?>  <li><p><i class="fa fa-envelope-o pr5"></i><?php echo esc_html( $nt_forester_footer_mail ); ?></p></li>  <?php endif; ?>
                                   <?php if ( $nt_forester_footer_address != '' ) : ?> <li><p><i class="fa fa-home pr5"></i><?php echo esc_html( $nt_forester_footer_address ); ?></p></li> <?php endif; ?>
                               </ul>
                              <?php endif; ?>

                           </div>
                        <div class="col-md-3 col-sm-4 text-right text-left-md"> <?php do_action('nt_forester_copyright_action'); ?> </div>
                     </div>
	              </div>
               </footer>
         <?php endif; // footer-2 end ?>

         <?php if ( ot_get_option( 'nt_forester_footer_type' ) == '3' ) :  ?>
         <footer class="footer-black-2 nt_forester_footer_type_3 nt_theme_footer">
           <div class="container pt100 pb100">
                <div class="text-center m-auto col-md-6 white">

                     <?php do_action('nt_forester_copyright_action'); ?>

                     <?php if ( ot_get_option( 'nt_forester_footer_social_display' ) != 'off' ) :
                       if ( ! empty( $nt_forester_footer_social ) ) : ?>
                          <ul class="ul-h social-icons mt40">
                             <?php foreach( $nt_forester_footer_social as $soc ) {
                                if ( ! empty( $soc ) ) { ?>
                                   <li><a href="<?php echo esc_html( $soc['nt_forester_footer_social_link'] ); ?>" <?php if ( $nt_forester_footer_soc_tar != '' ) : ?> target="<?php echo esc_html( $nt_forester_footer_soc_tar ); ?>"<?php endif; ?>>
                                      <i class="<?php echo esc_attr( $soc['nt_forester_footer_social_text'] ); ?> "></i></a></li>
                             <?php
                                }
                             }
                             ?>
                          </ul>
                       <?php
                       endif;
                     endif;
                    ?>
                </div>
           </div>
        </footer>
         <?php endif; // footer-2 end ?>


         <?php if ( ot_get_option( 'nt_forester_footer_type' ) == '4' ) :  ?>
            <hr class="separator">
         <footer class="footer-white-2 nt_forester_footer_type_4  nt_theme_footer">
           <div class="container pt100 pb100">
                <div class="text-center m-auto col-md-6 black">

                     <?php do_action('nt_forester_copyright_action'); ?>

                     <?php if ( ot_get_option( 'nt_forester_footer_social_display' ) != 'off' ) :
                       if ( ! empty( $nt_forester_footer_social ) ) : ?>
                          <ul class="ul-h social-icons mt40">
                             <?php foreach( $nt_forester_footer_social as $soc ) {
                                if ( ! empty( $soc ) ) { ?>
                                   <li><a href="<?php echo esc_html( $soc['nt_forester_footer_social_link'] ); ?>" <?php if ( $nt_forester_footer_soc_tar != '' ) : ?> target="<?php echo esc_html( $nt_forester_footer_soc_tar ); ?>"<?php endif; ?>>
                                      <i class="<?php echo esc_attr( $soc['nt_forester_footer_social_text'] ); ?> "></i></a></li>
                             <?php
                                }
                             }
                             ?>
                          </ul>
                       <?php
                       endif;
                     endif;
                    ?>
                </div>
           </div>
        </footer>
         <?php endif; // footer-2 end ?>

<?php
	endif;

	}
endif;

add_action( 'nt_forester_footer_action',  'nt_forester_footer', 10 );


// START FOOTER COPYRIGHT
if ( ! function_exists( 'nt_forester_copyright' ) ) :
	function nt_forester_copyright() {

		if ( ot_get_option('nt_forester_copyright_display') != 'off') :

				$nt_forester_copyright = ot_get_option('nt_forester_copyright');

				$nt_forester_copyright_tag_attribs = array(
					'id' 	=> array(),
					'class' => array(),
					'title' => array(),
					'style' => array(),
				);
				$nt_forester_copyright_allowed_tags = array(
				'div' 	=> $nt_forester_copyright_tag_attribs,
				'h1' 	=> $nt_forester_copyright_tag_attribs,
				'h2' 	=> $nt_forester_copyright_tag_attribs,
				'h3' 	=> $nt_forester_copyright_tag_attribs,
				'mark' 	=> $nt_forester_copyright_tag_attribs,
				'strong'=> $nt_forester_copyright_tag_attribs,
				'br' 	=> $nt_forester_copyright_tag_attribs,
				'p' 	=> $nt_forester_copyright_tag_attribs,
				'span' 	=> $nt_forester_copyright_tag_attribs,
				'i' 	=> $nt_forester_copyright_tag_attribs,
				'hr' 	=> $nt_forester_copyright_tag_attribs,
				'img' 	=> array_merge( $nt_forester_copyright_tag_attribs, array(
						'src' 	 => array(),
						'alt' 	 => array(),
						'width'  => array(),
						'height' => array(),
					) ),
				'a' 	=> array_merge( $nt_forester_copyright_tag_attribs, array(
						'href' 	 => array(),
					) ),
				);

			if ( ot_get_option('nt_forester_copyright') != '' ) :

         ?>
         <?php   if ( ot_get_option( 'nt_forester_footer_type' ) == '1' ) : ?>
				<div class="text-center m-auto footer-copyright">
         <?php
            endif;
            echo wp_kses( $nt_forester_copyright, $nt_forester_copyright_allowed_tags );
            if ( ot_get_option( 'nt_forester_footer_type' ) == '1' ) :
         ?>
				</div>
            <?php
         endif; // nt_forester_footer_type
	      endif; // nt_forester_footer_type
         endif; // nt_forester_copyright

	}

endif;

add_action( 'nt_forester_copyright_action',  'nt_forester_copyright', 10 );


// START SINGLE HEADER STYLE2

if ( ! function_exists( 'nt_forester_blog_hero' ) ) :
	function nt_forester_blog_hero() {

	$blog_h_bg 						= 	esc_attr( ot_get_option( 'nt_forester_blog_headerbg' ) );
	$blog_h_bg_img 					= 	( $blog_h_bg != '' ) ? $blog_h_bg : get_theme_file_uri() . '/images/full_1.jpg';
	$nt_forester_blog_heading_display = 	ot_get_option( 'nt_forester_blog_heading_display' );
	$nt_forester_blog_heading 		= 	ot_get_option( 'nt_forester_blog_heading' );
	$nt_forester_blog_slogan_display 	= 	ot_get_option( 'nt_forester_blog_slogan_display' );
	$nt_forester_blog_slogan 			= 	ot_get_option( 'nt_forester_blog_slogan' );
	$nt_forester_bread_visibility 	= 	ot_get_option( 'nt_forester_bread' );
?>

	<!-- Start Hero Section -->
	<section id="hero" class="page-id-<?php echo get_the_ID(); ?> hero-fullwidth parallax nt-inner-pages-hero">
		<?php if( $blog_h_bg_img != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $blog_h_bg_img ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container white">
				<div class="m-auto">
					<div class="title-service mb15">

						<?php if ( $nt_forester_blog_heading_display != 'off') : ?>
							<?php if ( $nt_forester_blog_heading != '' ) : ?>
								<h1 class="white"><?php echo esc_html( $nt_forester_blog_heading ); ?></h1>
							<?php else: ?>
								<h1 class="white"><?php echo bloginfo('name'); ?></h1>
							<?php endif; ?>
						<?php endif; ?>

						<?php if ( $nt_forester_blog_slogan_display != 'off' ) : ?>
							<?php if ( $nt_forester_blog_slogan != '') : ?>
								<p class="cover-text-sublead"><?php echo esc_html( $nt_forester_blog_slogan ); ?></p>
							<?php else: ?>
								<p class="cover-text-sublead"><?php echo bloginfo('description'); ?></p>
							<?php endif; ?>
						<?php endif; ?>

					</div>

				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->

<?php }

endif;

add_action( 'nt_forester_blog_hero_action',  'nt_forester_blog_hero', 10 );



// START SINGLE HEADER STYLE2

if ( ! function_exists( 'nt_forester_single_header' ) ) :
	function nt_forester_single_header() {

	$nt_forester_bread_display			= 	ot_get_option( 'nt_forester_bread_display' );
	$nt_forester_single_heading_display 	= 	ot_get_option( 'nt_forester_single_heading_display' );
	$nt_forester_singlepageheadbg 		= 	ot_get_option( 'nt_forester_single_headbg' );
	$nt_forester_singlepageheadbg	 		= 	( $nt_forester_singlepageheadbg != '' ) ? $nt_forester_singlepageheadbg : get_theme_file_uri() . '/images/full_1.jpg';

?>

	<!-- Start Hero Section -->
	<section id="hero" class="hero-fullwidth parallax single-post-hero">
		<?php if( $nt_forester_singlepageheadbg != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_singlepageheadbg ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="m-auto">
							<div class="title-service mb15">

								<?php if ( ( $nt_forester_single_heading_display  ) != 'off') : ?>
									<h1 class="white pb60"><?php echo the_title();?></h1>
								<?php endif; ?>

								<?php if( $nt_forester_bread_display != 'off' ) : ?>
									<?php if( function_exists( 'bcn_display' ) ) : ?>
										<p class="breadcrubms"> <?php bcn_display(); ?></p>
									<?php endif; ?>
								<?php endif; ?>

							</div>

						</div>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->

<?php }

endif;

add_action( 'nt_forester_single_header_action',  'nt_forester_single_header', 10 );


// START PORTFOLO SINGLE HEADER STYLE2

if ( ! function_exists( 'nt_forester_single_port_header' ) ) :
	function nt_forester_single_port_header() {

	$nt_forester_bread_display				= 	ot_get_option( 'nt_forester_bread_display' );
	$nt_forester_singleport_heading_display 	= 	ot_get_option( 'nt_forester_singleport_heading_display' );
	$nt_forester_singleport_headbg 			= 	ot_get_option( 'nt_forester_singleport_headbg' );
	$nt_forester_singleport_headbg	 		= ( $nt_forester_singleport_headbg != '' ) ? $nt_forester_singleport_headbg : get_theme_file_uri() . '/images/full_1.jpg';

?>

	<!-- Start Hero Section -->
	<section id="hero" class="hero-fullwidth parallax single-portfolio-hero">
		<?php if( $nt_forester_singleport_headbg != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_singleport_headbg ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="m-auto">
							<div class="title-service mb15">

								<?php if ( ( $nt_forester_singleport_heading_display  ) != 'off') : ?>
									<h1 class="white pb60"><?php echo the_title();?></h1>
								<?php endif; ?>

								<?php if( $nt_forester_bread_display != 'off' ) : ?>
									<?php if( function_exists( 'bcn_display' ) ) : ?>
										<p class="breadcrubms"> <?php bcn_display(); ?></p>
									<?php endif; ?>
								<?php endif; ?>

							</div>

						</div>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->

<?php }

endif;

add_action( 'nt_forester_single_port_header_action',  'nt_forester_single_port_header', 10 );


// START BEFORE DEFAULT POST SINGLE
if ( ! function_exists( 'nt_forester_before_post_single' ) ) :
	function nt_forester_before_post_single() {

	$nt_forester_post_layout = ot_get_option( 'nt_forester_post_layout' );
?>


	<section id="blog" class="single-style-1">
		<div class="container has-margin-bottom">
			<div class="row">
				<div class="col-md-12-off has-margin-bottom-off">

				<!-- right sidebar -->
				<?php if( ( $nt_forester_post_layout ) == 'right-sidebar' || ( $nt_forester_post_layout ) == '') { ?>
				<div class="col-lg-8 col-md-8 col-sm-12 index float-right posts">

				<!-- left sidebar -->
				<?php } elseif( ( $nt_forester_post_layout ) == 'left-sidebar') { ?>
				<?php get_sidebar(); ?>
				<div class="col-lg-8 col-md-8 col-sm-12 index float-left posts">

				<!-- no sidebar -->
				<?php } elseif( ( $nt_forester_post_layout ) == 'full-width') { ?>
				<div class="col-xs-12 full-width-index v">
				<?php }

	}

endif;

add_action( 'nt_forester_before_post_single_action',  'nt_forester_before_post_single', 10 );





// START AFTER DEFAULT POST SINGLE

if ( ! function_exists( 'nt_forester_after_post_single' ) ) :
	function nt_forester_after_post_single() {

	$nt_forester_post_layout = ot_get_option( 'nt_forester_post_layout' ); ?>

				</div><!-- #end sidebar+ content -->

				<!-- right sidebar -->
				<?php if( ( $nt_forester_post_layout ) == 'right-sidebar' || ( $nt_forester_post_layout ) == '') {
					get_sidebar();
				} ?>

				</div>
			</div>
		</div>
	</section>

<?php }

endif;

add_action( 'nt_forester_after_post_single_action',  'nt_forester_after_post_single', 10 );


// Start nt_forester_formats_content

if ( ! function_exists( 'nt_forester_formats_content' ) ) :
	function nt_forester_formats_content() {
		wp_enqueue_style( 'nt-forester-custom-flexslider');
		wp_enqueue_script( 'nt-forester-custom-flexslider');
		wp_enqueue_script( 'fitvids');
		wp_enqueue_script( 'nt-forester-blog-settings');
?>



    <div class="blog-post-text">
      <?php

          if ( is_single() ) :
            the_content();
            else :
            the_excerpt();
          endif ;

          wp_link_pages( array(
            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nt-forester' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
            'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'nt-forester' ) . ' </span>%',
            'separator'   => '<span class="screen-reader-text">, </span>',
          )
       );
      ?>
    </div>
    <div class="blog-post-bottom">
        <div class="row flexAlign">
            <div class="col-md-3">
                <!-- Blog Post Author -->
                <div class="post-author">
                    <?php esc_html_e('Posted by', 'nt-forester'); ?> <h5 class="subheading dark"><?php the_author(); ?></h5>
                </div>
                <!-- -->
            </div>
            <div class="col-md-6 text-center">
                <?php
   					if ( ! is_single() ) : ?>
                     <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-ghost-black btn-scroll mt15"> <?php esc_html_e('View Post', 'nt-forester'); ?></a>
                  <?php else:  ?>

                        <div class="post-share">
                             <ul class="ul-h">
                                <?php if (  is_single() ) : ?>
                       					<div id="share-buttons">
                       						<li><a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-facebook x1"></i></a></li>
                       						<li><a href="http://twitter.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-twitter x1"></i></a></li>
                       						<li><a href="https://plus.google.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-google-plus x1"></i></a></li>
                       						<li><a href="http://www.digg.com/submit?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-digg x1"></i></a></li>
                       						<li><a href="http://reddit.com/submit?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-reddit"></i></a></li>
                       						<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                       						<li><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest"></i></a></li>
                       						<li><a href="http://www.stumbleupon.com/submit?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>
                       					</div>
                       				<?php endif; // is_single() ?>
                             </ul>
                         </div>
               		<?php
   					endif;
   				?>
            </div>
            <div class="col-md-3">
                <!-- Blog Post Date -->
                <div class="post-date">
                    <?php esc_html_e('Posted on', 'nt-forester'); ?> <?php the_time('F j, Y'); ?>
                </div>
                <!-- -->
            </div>
        </div>
    </div>
</div>



	<?php
	}
endif;

add_action( 'nt_forester_formats_content_action',  'nt_forester_formats_content', 10 );


// START BEFORE DEFAULT POST SINGLE
if ( ! function_exists( 'nt_forester_before_post_single_style2' ) ) :
	function nt_forester_before_post_single_style2() {

	$nt_forester_post_layout = ot_get_option( 'nt_forester_post_layout' );
?>


        <section id="blog-post" class="bg-white single-style-2">
            <div class="container pt150 pb100">
                <div class="col-md-9 blog-post-head text-center">
                    <div class="blog-post-category">
                        <h6 class="inline"><?php echo esc_html_e( 'In', 'nt-forester' ); ?></h6> <h4 class="inline black"><?php the_category(', '); ?></h4>
                    </div>
                    <div class="blog-post-title black">
                        <h2><?php the_title(); ?></h2>
                    </div>
                    <div class="blog-post-date">
                        <h6><?php echo esc_html_e( 'Posted on', 'nt-forester' ); ?> <?php echo get_the_date(); ?></h6>
                    </div>
                </div>
                <div class="row">

					<!-- right sidebar -->
					<?php if( ( $nt_forester_post_layout ) == 'right-sidebar' || ( $nt_forester_post_layout ) == '') { ?>
					<div class="col-lg-9 col-md-9 col-sm-12 text-center">

					<!-- left sidebar -->
					<?php } elseif( ( $nt_forester_post_layout ) == 'left-sidebar') { ?>
					<?php get_sidebar(); ?>
					<div class="col-lg-9 col-md-9 col-sm-12 text-center">

					<!-- no sidebar -->
					<?php } elseif( ( $nt_forester_post_layout ) == 'full-width') { ?>
					<div class="col-md-12 text-center">
					<?php }

	}

endif;

add_action( 'nt_forester_before_post_single_style2_action',  'nt_forester_before_post_single_style2', 10 );



// START AFTER DEFAULT POST SINGLE

if ( ! function_exists( 'nt_forester_after_post_single_style2' ) ) :
	function nt_forester_after_post_single_style2() {

	$nt_forester_post_layout = ot_get_option( 'nt_forester_post_layout' ); ?>

				</div><!-- #end sidebar+ content -->

				<!-- right sidebar -->
				<?php if( ( $nt_forester_post_layout ) == 'right-sidebar' || ( $nt_forester_post_layout ) == '') {
					get_sidebar();
				} ?>

			</div>
		</div>
	</section>

<?php }

endif;

add_action( 'nt_forester_after_post_single_style2_action',  'nt_forester_after_post_single_style2', 10 );



// Start nt_forester_formats_content style2
if ( ! function_exists( 'nt_forester_formats_content_single_style2' ) ) :
	function nt_forester_formats_content_single_style2() {
		wp_enqueue_style( 'nt-forester-custom-flexslider');
		wp_enqueue_script( 'nt-forester-custom-flexslider');
		wp_enqueue_script( 'fitvids');
		wp_enqueue_script( 'nt-forester-blog-settings');

	$nt_forester_thumb 	= get_post_thumbnail_id();
	$nt_forester_postthumb = wp_get_attachment_url( $nt_forester_thumb,'full' );
?>

	<div class="blog-post-image">
		<img src="<?php echo esc_url( $nt_forester_postthumb ); ?>" alt="<?php echo esc_url( get_permalink() ); ?>" class="img-responsive m-auto">
	</div>
	<div class="blog-post-body">
		<?php

		if ( is_single() ) :
		the_content();
		else :
		the_excerpt();
		endif ;

		wp_link_pages( array(
		'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nt-forester' ) . '</span>',
		'after'       => '</div>',
		'link_before' => '<span>',
		'link_after'  => '</span>',
		'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'nt-forester' ) . ' </span>%',
		'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>

	</div>
	<div class="blog-post-desc pb20 border-bottom">
		<!-- Blog Post Tags -->
		<div class="inline-block m10">
			<?php
				if(has_tag()) :

					$tags = get_the_tags(get_the_ID());
					  foreach($tags as $tag){
						echo '<a href="'.get_tag_link($tag->term_id).'" rel="tag" class="blog-post-tag tag-'.$tag->name.'">'.$tag->name.'</a> ';
					}

				endif;
			?>

		</div>
		<!--  -->
		<!-- Blog Post Author -->
		<div class="inline-block m10">
			<h6 class="inline"><?php echo esc_html_e('Posted By','nt-forester'); ?></h6> <h4 class="inline black"><?php echo get_the_author(); ?></h4>
		</div>
		<!--  -->

		<!-- Blog Post Social Icons -->
		<div class="inline-block m10">
			<div class="blog-post-social">
				<ul class="social-icons list-none mb0">

               <?php if (  is_single() ) : ?>
   					<div id="share-buttons">
   						<li><a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-facebook x1"></i></a></li>
   						<li><a href="http://twitter.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-twitter x1"></i></a></li>
   						<li><a href="https://plus.google.com/share?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-google-plus x1"></i></a></li>
   						<li><a href="http://www.digg.com/submit?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-digg x1"></i></a></li>
   						<li><a href="http://reddit.com/submit?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-reddit"></i></a></li>
   						<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
   						<li><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest"></i></a></li>
   						<li><a href="http://www.stumbleupon.com/submit?url=<?php echo esc_url( get_permalink() ); ?>" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>
   					</div>
   				<?php endif; // is_single() ?>

				</ul>
			</div>
		</div>
		<!--  -->
	</div>

	<?php
	}
endif;

add_action( 'nt_forester_formats_content_single_style2_action',  'nt_forester_formats_content_single_style2', 10 );
