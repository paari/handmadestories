<?php

add_filter( 'rwmb_meta_boxes', 'nt_forester_register_meta_boxes' );
function nt_forester_register_meta_boxes( $meta_boxes ) {
$meta_boxes = array();

/* ----------------------------------------------------- 
// FRONTPAGE METABOX MENU SETTINGS

$meta_boxes[] = array(
	'title' 		=> esc_html__( 'Metabox menu', 'nt-forester' ),
	'pages'    		=> array( 'page' ),
	'clone-group' 	=> 'onepage-clone-group',
	'id' 			=> 'pagemetaboxmenu',
	'context' 		=> 'side',
	'priority' 		=> 'low',
	'show'  		=> array(
		'template'  => array( 'custom-page.php' )
	),

	'fields' 		=> array(
		array(
			'name' 		=> esc_html__('Header menu type', 'nt-forester'),
			'desc' 		=> esc_html__('Select header menu type', 'nt-forester'),
			'id'   		=> 'nt_forester_menutype',
			'type' 		=> 'select',
			'options'  	=> array(
				'm' 	=> esc_html__( 'Metabox menu', 'nt-forester' ),
				'p' 	=> esc_html__( 'Default Primary menu', 'nt-forester' ),
			),
			'std'       => 'm'
		),
		array(
			'name'  		=> esc_html__( 'Menu item name', 'nt-forester' ),
			'desc'  		=> esc_html__( 'Format: Any text', 'nt-forester' ),
			'id'    		=> 'nt_forester_section_name',
			'type'  		=> 'text',
			'std'   		=> esc_html__( 'Menu item name', 'nt-forester' ),
			'class' 		=> 'custom-class',
			'clone' 		=> true,
			'sort_clone' 	=> true,
			'clone-group' 	=> 'onepage-clone-group',
		),
		array(
			'name'  		=> esc_html__( 'Menu item Url', 'nt-forester' ),
			'desc'  		=> esc_html__( 'Format: #sectionname or http://yoururl.com', 'nt-forester' ),
			'id'    		=> 'nt_forester_section_url',
			'type'  		=> 'text',
			'std'   		=> '#sectionname',
			'class' 		=> 'custom-class',
			'clone' 		=> true,
			'sort_clone' 	=> true,
			'clone-group' 	=> 'onepage-clone-group',
		),
	)
);

/* ----------------------------------------------------- */

/* ----------------------------------------------------- */
// PAGE HEADER COLOR
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Header Color', 'nt-forester' ),
	'id' 		=> 'pageheadersettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Header Color', 'nt-forester' ),
		),
		// color
		array(
			'name' 		=> esc_html__( 'Header navigation background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_nav_bgcolor',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Header sticky navigation background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_sticky_bg',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Header navigation menu item color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_nav_color',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Header navigation menu item hover color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_nav_hovercolor',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Header navigation border bottom Color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_nav_border_color',
			'type' 		=> 'color',
		),
	)
);

/* ----------------------------------------------------- */
// PAGE HEADER COLOR
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Footer Color', 'nt-forester' ),
	'id' 		=> 'pagefootersettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Footer Color', 'nt-forester' ),
		),
		// color
		array(
			'name' 		=> esc_html__( 'Footer background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_f_bgcolor',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Footer border-top color', 'nt-forester' ),
			'id'   		=> 'nt_forester_f_border_t_c',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Footer text color', 'nt-forester' ),
			'id'   		=> 'nt_forester_f_text_c',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Footer social icon background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_f_social_bg',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Footer social icon text Color', 'nt-forester' ),
			'id'   		=> 'nt_forester_f_social_i_c',
			'type' 		=> 'color',
		),
	)
);

/* ----------------------------------------------------- */
// PAGE HEADING DISPLAY
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Hero Display', 'nt-forester' ),
	'id' 		=> 'pageheadingsettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Hero Display', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable page hero section', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_hero_display',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
	)
);

/* ----------------------------------------------------- */
// PAGE HEADING STYLE
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Hero Style', 'nt-forester' ),
	'id' 		=> 'pageheadingstylesettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Hero Style', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Background image', 'nt-forester' ),
			'id'        => 'nt_forester_page_bg_image',
			'type'      => 'image_advanced',
			'max_file_uploads' 	=> 1,
		),
		// color
		array(
			'name' 		=> esc_html__( 'Background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_bg_color',
			'type' 		=> 'color',
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id',
		),
		array(
			'name' 		=> esc_html__( 'Hero height', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_header_height',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
		array(
			'name' 		=> esc_html__( 'Padding top', 'nt-forester' ),
			'id'   		=> 'nt_forester_header_p_top',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
		array(
			'name' 		=> esc_html__( 'Padding bottom', 'nt-forester' ),
			'id'   		=> 'nt_forester_header_p_bottom',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
	)
);

/* ----------------------------------------------------- */
// PAGE HEADING/TITLTE
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Title', 'nt-forester' ),
	'id' 		=> 'pagetitlesettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Title Options', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable Page Title', 'nt-forester' ),
			'id'   		=> 'nt_forester_disable_title',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Use Big Title', 'nt-forester' ),
			'id'   		=> 'nt_forester_use_bigtitle',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name'		=> esc_html__( 'Alternate Page Title', 'nt-forester' ),
			'id'		=> 'nt_forester_alt_title',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),
		array(
			'name' 		=> esc_html__( 'Page Title Color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_title_color',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Page Title margin-bottom', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_title_mb',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
	)
);

/* ----------------------------------------------------- */
// PAGE SUBTITLE
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Subtitle', 'nt-forester' ),
	'id' 		=> 'pagesubtitlesettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Subtitle Options', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable Page Subtitle', 'nt-forester' ),
			'id'  		=> 'nt_forester_disable_subtitle',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Page Subtitle / Rich Text Editor', 'nt-forester' ),
			'id'   		=> 'nt_forester_subtitle',
			'type' 		=> 'wysiwyg',
			'raw'  		=> false,
			'options' 	=> array(
				'textarea_rows' => 4,
				'teeny'         => true,
				'media_buttons' => false,
			),
		),
		array(
			'name' 		=> esc_html__( 'Page Subtitle color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_subtitle_color',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Page Subtitle max-width', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_subtitle_maxw',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
		array(
			'name' 		=> esc_html__( 'Page Subtitle margin-bottom', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_subtitle_mb',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
	)
);

/* ----------------------------------------------------- */
// PAGE BUTTON DISPLAY
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Breadcrumbs Display', 'nt-forester' ),
	'id' 		=> 'pagebreadsettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Breadcrumbs Display', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable page breadcrumbs section', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_bread_display',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
	)
);


/* ----------------------------------------------------- */
// PAGE BUTTON DISPLAY
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'title' 	=> esc_html__( 'Page Button Display', 'nt-forester' ),
	'id' 		=> 'pagebuttonsettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		// heading
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Button Options', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable page button section', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_herobtn_display',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__('Button link type', 'nt-forester'),
			'desc' 		=> esc_html__('Select hero button link type.Internal link options for smoothScroll', 'nt-forester'),
			'id'   		=> 'nt_forester_page_herobtn_linktype',
			'type' 		=> 'select',
			'options'  	=> array(
				'external' 	=> esc_html__( 'External link', 'nt-forester' ),
				'internal' 	=> esc_html__( 'Internal link', 'nt-forester' ),
			),
			'std'       => 'external'
		),
		array(
			'name'		=> esc_html__( 'Button custom title', 'nt-forester' ),
			'id'		=> 'nt_forester_page_herobtn',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),
		array(
			'name'		=> esc_html__( 'Button custom URL', 'nt-forester' ),
			'id'		=> 'nt_forester_page_herobtn_url',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id_btnstyle',
		),
		array(
			'name' 		=> esc_html__( 'Disable button icon', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_herobtn_icon_display',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name'		=> esc_html__( 'Button custom icon', 'nt-forester' ),
			'desc' 		=> esc_html__('Add your custom icon class name.Icon libraries supported by theme: themify and ionicon.', 'nt-forester'),
			'id'		=> 'nt_forester_page_herobtn_icon',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),
		array(
			'name' 		=> esc_html__('Button icon position', 'nt-forester'),
			'desc' 		=> esc_html__('Select hero button icon position', 'nt-forester'),
			'id'   		=> 'nt_forester_page_herobtn_iconpos',
			'type' 		=> 'select',
			'options'  	=> array(
				'before' 	=> esc_html__( 'Before title', 'nt-forester' ),
				'after' 	=> esc_html__( 'After title', 'nt-forester' ),
			),
			'std'       => 'before'
		),
		array(
			'name' 		=> esc_html__('Button style', 'nt-forester'),
			'desc' 		=> esc_html__('Select hero button style', 'nt-forester'),
			'id'   		=> 'nt_forester_page_herobtn_bg',
			'type' 		=> 'select',
			'options'  	=> array(
				'btn-primary' 		=> esc_html__( 'primary', 'nt-forester' ),
				'btn-ghost-white' 	=> esc_html__( 'Ghost white', 'nt-forester' ),
				'btn-ghost-black' 	=> esc_html__( 'Ghost black', 'nt-forester' ),
				'btn-black' 		=> esc_html__( 'Black', 'nt-forester' ),
				'custom' 			=> esc_html__( 'custom color', 'nt-forester' ),
			),
			'std'       => 'primary'
		),
		array(
			'name' 		=> esc_html__( 'Button background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_herobtn_custombg',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Button hover background color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_herobtn_hoverbg',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Button title color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_herobtn_titlecolor',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__( 'Button hover title color', 'nt-forester' ),
			'id'   		=> 'nt_forester_page_herobtn_titlehover',
			'type' 		=> 'color',
		),
		array(
			'name' 		=> esc_html__('Button size', 'nt-forester'),
			'desc' 		=> esc_html__('Select hero button size', 'nt-forester'),
			'id'   		=> 'nt_forester_page_herobtn_size',
			'type' 		=> 'select',
			'options'  	=> array(
				'btn-sm' 	=> esc_html__( 'Small', 'nt-forester' ),
				'btn-md' 	=> esc_html__( 'Medium', 'nt-forester' ),
				'btn-lg' 	=> esc_html__( 'Large', 'nt-forester' ),
				'btn-xs' 	=> esc_html__( 'Mini', 'nt-forester' ),
			),
			'std'       => 'btn-sm'
		),
	)
);

/* ----------------------------------------------------- */
// PAGE SIDEBAR SETTINGS
/* ----------------------------------------------------- */

$meta_boxes[] = array(
	'title' 	=> esc_html__( 'PAGE SIDEBAR', 'nt-forester' ),
	'id' 		=> 'pagesidebarsettings',
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'hide'   	=> array(
		'template'  => array( 'custom-page.php' )
	),
	'fields' 	=> array(
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Page Sidebar', 'nt-forester' ),
		),
		// page layout
		array(
			'name'     => esc_html__( 'Page sidebar', 'nt-forester' ),
			'id'       => 'nt_forester_pagelayout',
			'type'     => 'select',
			'options'  => array(
				'left-sidebar' 	=> esc_html__( 'left', 'nt-forester' ),
				'right-sidebar' => esc_html__( 'right', 'nt-forester' ),
				'full-width' 	=> esc_html__( 'full', 'nt-forester' ),
			),
			'multiple'    => false,
			'std'         => 'right-sidebar',
			'placeholder' => esc_html__( 'Select an Item', 'nt-forester' ),
		)
	)
);

$nt_forester_ot_register_cpt1 = ot_get_option( 'nt_forester_cpt1' );

$nt_forester_cpt_slug1 = ( $nt_forester_ot_register_cpt1 != '' ) ? strtolower( esc_html( $nt_forester_ot_register_cpt1 ) ) : 'portfolio';
/* ----------------------------------------------------- */
// PORTFOLIO SETTINGS
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' 		=> 'portfoliosettings',
	'title' 	=> esc_html__( 'Portfolio Post General', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug1.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(

		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Portfolio Post General', 'nt-forester' ),
		),
		array(
			'name'     	=> esc_html__( 'Select link type', 'nt-forester' ),
			'id'		=> 'nt_forester_port_linktype',
			'type'		=> 'select',
			'multiple'	=> false,
			'std'		=> 'lightbox',
			'options'	=> array(
				'lightbox' 		=> esc_html__( 'Open in Lightbox', 'nt-forester' ),
				'single-popup' 	=> esc_html__( 'Open in Single Popup', 'nt-forester' ),
				'single' 		=> esc_html__( 'Open in Single Page', 'nt-forester' ),
			)
		),
		array(
			'name' 		=> esc_html__('Select Images', 'nt-forester'),
			'desc' 		=> esc_html__('Select the images from the media library or upload your new ones for popup slider', 'nt-forester'),
			'id'   		=> 'nt_forester_port_gallery_image',
			'type' 		=> 'image_advanced',
		),
		array(
			'name'		=> esc_html__( 'Video url ( for simple in Lightbox )', 'nt-forester' ),
			'desc'  	=> sprintf( esc_html( 'You can add youtube or vimeo video for portfolio item. %s %s and %s %s', 'nt-forester' ), '<b>Youtube URL format:</b>', '<code>https://www.youtube.com/watch?v=ZqnAGgjQ7Rs</code>', '<b>Vimeo URL format:</b>', '<code>https://vimeo.com/203116933</code>' ),
			'id'		=> 'nt_forester_port_vidurl',
			'clone'		=> false,
			'type'		=> 'text',
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id_posttitle',
		),
		array(
			'name' 		=> esc_html__( 'Disable post title ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_post_title',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id_author',
		),
		array(
			'name' 		=> esc_html__( 'Disable author name ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_author_name',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__('Before Author Text', 'nt-forester'),
			'desc' 		=> esc_html__('Enter before author text if you want to use before author name.Please leave blank if you do not use.', 'nt-forester'),
			'id'   		=> 'nt_forester_before_author_text',
			'type' 		=> 'text',
		),
	)
);

/* GENARAL PORTFOLIO SINGLE SETINGS ------------*/
$meta_boxes[] = array(
	'id' 		=> 'portfolioallmetasingle',
	'title' 	=> esc_html__( 'Post Meta Detail', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug1.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Post Meta Detail', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable post all detail meta ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_portfolio_all_meta',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
	)
);

/* GENARAL PORTFOLIO SINGLE SETINGS ------------*/
$meta_boxes[] = array(
	'id' 		=> 'portfoliopopupsingle',
	'title' 	=> esc_html__( 'Post Detail', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug1.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(
		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Post Detail', 'nt-forester' ),
		),

		array(
			'name' 		=> esc_html__( 'Disable post content ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_post_content',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id',
		),
		array(
			'name' 		=> esc_html__( 'Disable client name ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_portfolio_client_meta',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' => esc_html__('Client Name for Project', 'nt-forester'),
			'desc' => esc_html__('Enter Client Name for this post', 'nt-forester'),
			'id'   => 'nt_forester_portfolio_single_client_name',
			'type' => 'text',
		),
		array(
			'name' => esc_html__('Add custom link for Client', 'nt-forester'),
			'desc' => esc_html__('Enter link for Client or leave blank.Default Client link is Author link', 'nt-forester'),
			'id'   => 'nt_forester_portfolio_client_link',
			'type' => 'text',
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id',
		),
		array(
			'name' 		=> esc_html__( 'Disable post date ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_portfolio_date_meta',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Disable post categories ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_portfolio_category_meta',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id',
		),
		array(
			'name' 		=> esc_html__( 'Disable button ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_portfolio_custom_btn',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__('Button Title', 'nt-forester'),
			'desc' 		=> esc_html__('Enter button title.', 'nt-forester'),
			'id'   		=> 'nt_forester_portfolio_custom_btn_title',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('Button Custom Link', 'nt-forester'),
			'desc' 		=> esc_html__('Enter button custom link.', 'nt-forester'),
			'id'   		=> 'nt_forester_portfolio_custom_btn_link',
			'type' 		=> 'text',
		),
		array(
			'name'		=> 'Select target type',
			'id'		=> 'nt_forester_portfolio_custom_btn_target',
			'type'		=> 'select',
			'desc'  	=> 'Select button target type',
			'multiple'	=> false,
			'std'		=> '_blank',
			'options'	=> array(
						'_blank'	=> '_blank',
						'_self'		=> '_self',
						'_parent'	=> '_parent',
						'_top'		=> '_top',
			)
		),
	)
);


/* ----------------------------------------------------- */
// PORTFOLIO SHARE SETTINGS
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' 		=> 'portfoliosharesettings',
	'title' 	=> esc_html__( 'Portfolio Share Options', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug1.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(

		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Portfolio Share Options', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable Post Share?', 'nt-forester' ),
			'id'   		=> 'nt_forester_share',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_id_share',
		),
		array(
			'name' 		=> esc_html__( 'Share on Facebook?', 'nt-forester' ),
			'id'   		=> 'nt_forester_share_face',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Share on Twitter?', 'nt-forester' ),
			'id'   		=> 'nt_forester_share_twitter',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Share on Google plus?', 'nt-forester' ),
			'id'   		=> 'nt_forester_share_gplus',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Share on Pinterest?', 'nt-forester' ),
			'id'   		=> 'nt_forester_share_pinterest',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
	)
);

/* ----------------------------------------------------- */
// PORTFOLIO RELATED SETTINGS
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' 		=> 'portfoliorelated',
	'title' 	=> esc_html__( 'Related Portfolio', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug1.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(

		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Related Portfolio', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__( 'Disable Related Post ?', 'nt-forester' ),
			'id'   		=> 'nt_forester_show_portfolio_related',
			'type' 		=> 'checkbox',
			'std'  		=> 0,
		),
		array(
			'name' 		=> esc_html__( 'Related Post Count', 'nt-forester' ),
			'id'   		=> 'nt_forester_related_post_count',
			'type' 		=> 'number',
			'min'  		=> 0,
			'step' 		=> 1,
		),
		array(
			'name' 		=> esc_html__('Related Section Title', 'nt-forester'),
			'desc' 		=> esc_html__('Enter custom title for related post section.', 'nt-forester'),
			'id'   		=> 'nt_forester_related_title',
			'type' 		=> 'text',
			'std' 		=> esc_html__('Related Post', 'nt-forester'),
		),
	)
);

/* ----------------------------------------------------- */
// PORTFOLIO CUSTOM STYLE SETTINGS
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' 		=> 'changeportpoststyle',
	'title' 	=> esc_html__( 'Post Custom Style', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug1.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(

		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Post Custom Style', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__('Caption active wrap background color', 'nt-forester'),
			'desc' 		=> esc_html__('Change the background color for this post.', 'nt-forester'),
			'id'   		=> 'nt_forester_port_activewrapbg',
			'type' 		=> 'color'
		),
		array(
			'name' 		=> esc_html__( 'Caption active wrap background opacity', 'nt-forester' ),
			'id'   		=> 'nt_forester_port_activewrapbgopacity',
			'type' 		=> 'number',
			'min'  		=> 0,
			'max'  		=> 1,
			'step' 		=> 0.1,
		),
		array(
			'name' 		=> esc_html__('Post category color', 'nt-forester'),
			'desc' 		=> esc_html__('Change the category color for this post.', 'nt-forester'),
			'id'   		=> 'nt_forester_port_catcolor',
			'type' 		=> 'color'
		),
		array(
			'name' 		=> esc_html__('Post title color', 'nt-forester'),
			'desc' 		=> esc_html__('category the title color for this post.', 'nt-forester'),
			'id'   		=> 'nt_forester_port_titlecolor',
			'type' 		=> 'color'
		),
		array(
			'name' 		=> esc_html__('Post icon color', 'nt-forester'),
			'desc' 		=> esc_html__('category the icon color for this post.', 'nt-forester'),
			'id'   		=> 'nt_forester_port_iconcolor',
			'type' 		=> 'color'
		),
	)
);

/* ----------------------------------------------------- */
// TEAM PLUGINS SETTINGS
/* ----------------------------------------------------- */
$nt_forester_ot_register_cpt2 = ot_get_option( 'nt_forester_cpt2' );

$nt_forester_cpt_slug2 = ( $nt_forester_ot_register_cpt2 != '' ) ? strtolower( esc_html( $nt_forester_ot_register_cpt2 ) ) : 'team';

$meta_boxes[] = array(
	'title' 		=> esc_html__( 'Team Shortcode Options', 'nt-forester' ),
	'pages'    		=> array( ''.$nt_forester_cpt_slug2.'' ),
	'clone-group' 	=> 'my-clone-group','clone-group' => 'my-clone-group',
	'id' 			=> 'mm_review',
	'context'     	=> 'normal',
	'priority'    	=> 'high',
	'fields' 		=> array(

		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Team Shortcode Options', 'nt-forester' ),
		),
		array(
			'name' 		=> esc_html__('Team Job', 'nt-forester'),
			'id'		=> 'nt_forester_team_job',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'Developer'
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_7', // Not used, but needed
		),
		// TEAM SOCIAL
		array(
			'name'  	=> esc_html__( 'Social Icon Name', 'nt-forester' ),
			'desc'  	=> esc_html__( 'Format: icon-facebook. Enter social icon name here', 'nt-forester' ),
			'id'    	=> 'nt_forester_social_icon',
			'type'  	=> 'text',
			'std'   	=> 'icon-facebook',
			'class' 	=> 'custom-class',
			'clone' 	=> true,
			'clone-group' => 'my-clone-group','clone-group' => 'my-clone-group',
		),
		array(
			'name'  	=> esc_html__( 'Social Url', 'nt-forester' ),
			'desc'  	=> esc_html__( 'Format: http://facebook.com', 'nt-forester' ),
			'id'    	=> 'nt_forester_social_url',
			'type'  	=> 'text',
			'std'   	=> '#',
			'class' 	=> 'custom-class',
			'clone' 	=> true,
			'clone-group' => 'my-clone-group',
		),
		array(
			'type' 		=> 'divider',
			'id'   		=> 'fake_divider_8', // Not used, but needed
		),
		array(
			'name'     	=> esc_html__( 'Select target type', 'nt-forester' ),
			'id'		=> 'nt_forester_social_target',
			'type'		=> 'select',
			'multiple'	=> false,
			'std'		=> '_blank',
			'options'	=> array(
						'_blank'		=> '_blank',
						'_self'			=> '_self',
						'_parent'		=> '_parent',
						'_top'			=> '_top',
			)
		),
	)
);

$nt_forester_ot_register_cpt3 = ot_get_option( 'nt_forester_cpt3' );

$nt_forester_cpt_slug3 = ( $nt_forester_ot_register_cpt3 != '' ) ? strtolower( esc_html( $nt_forester_ot_register_cpt3 ) ) : 'price';

/* ----------------------------------------------------- */
// PRICE TABLE SETTINGS
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' 		=> 'pricesettings',
	'title' 	=> esc_html__( 'Price Table Options', 'nt-forester' ),
	'pages' 	=> array( ''.$nt_forester_cpt_slug3.'' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array(

		array(
			'type' 		=> 'heading',
			'id'   		=> 'page_design_section',
			'name'		=> esc_html__( 'Price Table Options', 'nt-forester' ),
		),
		array(
			'name'		=> esc_html__( 'Price amount', 'nt-forester' ),
			'id'		=> 'nt_forester_price',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> '$ 15'
		),
		array(
			'name'		=> esc_html__( 'Price period', 'nt-forester' ),
			'id'		=> 'nt_forester_period',
			'clone'		=> false,
			'type'		=> 'text',
			'std'   	=> esc_html__( 'Per Month', 'nt-forester' ),
			'std'		=> 'monthly'
		),
		array(
			'name' 		=> esc_html__('Table Features List', 'nt-forester'),
			'desc'  	=> esc_html__( 'Add features for this pack', 'nt-forester' ),
			'id'    	=> 'nt_forester_features_list',
			'type'  	=> 'text',
			'std'   	=> 'Creative Portfolio',
			'class' 	=> 'custom-class',
			'clone' 	=> true,
			'clone-group' => 'my-clone-group','clone-group' => 'my-clone-group',
        ),
		// Price button opt
		array(
			'name'  	=> esc_html__( 'Price button title', 'nt-forester' ),
			'id'    	=> 'nt_forester_prcbtntitle',
			'type'  	=> 'text',
			'clone'		=> false,
			'std'   	=> esc_html__( 'Purchase Now', 'nt-forester' ),
		),
		array(

			'name'  	=> esc_html__( 'Price button link / URL', 'nt-forester' ),
			'id'    	=> 'nt_forester_prcbtnlink',
			'type'  	=> 'text',
			'clone'		=> false,
			'std'   	=> '#0',

		),
		array(
			'name'     	=> esc_html__( 'Select button target type', 'nt-forester' ),
			'id'		=> 'nt_forester_prcbtntarget',
			'type'		=> 'select',
			'multiple'	=> false,
			'std'		=> '_blank',
			'options'	=> array(
						'_blank'	=> '_blank',
						'_self'		=> '_self',
						'_parent'	=> '_parent',
						'_top'		=> '_top',
			)
		),
		array(
			'name' 		=> esc_html__('Price pack background color', 'nt-forester'),
			'desc' 		=> esc_html__('Choose pack background color.', 'nt-forester'),
			'id'   		=> 'nt_forester_prc_bgcolor',
			'type' 		=> 'color'
		),
	)
);


/*-----------------------------------------------------------------------------------*/
/*  METABOXES FOR BLOG POSTS
/*-----------------------------------------------------------------------------------*/

//	GALLERY POST FORMAT
$meta_boxes[] = array(
	'title'    	=> esc_html__('Gallery Settings', 'nt-forester'),
	'pages'    	=> array('post'),
	'fields' 	=> array(
		array(
			'name' 		=> esc_html__('Select Images', 'nt-forester'),
			'desc' 		=> esc_html__('Select the images from the media library or upload your new ones.', 'nt-forester'),
			'id'   		=> 'nt_forester_gallery_image',
			'type' 		=> 'image_advanced',
		)
	)
);

//	QUOTE POST FORMAT
$meta_boxes[] = array(
	'title'    	=> esc_html__('Quote Settings', 'nt-forester'),
	'pages'    	=> array('post'),
	'fields' 	=> array(
		array(
			'name' 		=> esc_html__('The Quote', 'nt-forester'),
			'desc' 		=> esc_html__('Write your quote in this field.', 'nt-forester'),
			'id'   		=> 'nt_forester_quote_text',
			'type' 		=> 'textarea',
			'rows' 		=> 5
		),
		array(
			'name' 		=> esc_html__('The Author', 'nt-forester'),
			'desc' 		=> esc_html__('Enter the name of the author of this quote.', 'nt-forester'),
			'id'   		=> 'nt_forester_quote_author',
			'type' 		=> 'text'
		),
		array(
			'name' 		=> esc_html__('Background Color', 'nt-forester'),
			'desc' 		=> esc_html__('Choose the background color for this quote.', 'nt-forester'),
			'id'   		=> 'nt_forester_quote_bg',
			'type' 		=> 'color'
		),
		array(
			'name' 		=> esc_html__('Background Opacity', 'nt-forester'),
			'desc' 		=> esc_html__('Choose the opacity of the background color.', 'nt-forester'),
			'id'   		=> 'nt_forester_quote_bg_opacity',
			'type' 		=> 'text',
			'std' 		=> 80
		)
	)
);

//	AUDIO POST FORMAT
$meta_boxes[] = array(
	'title'    	=> esc_html__('Audio Settings', 'nt-forester'),
	'pages'    	=> array('post'),
	'fields' 	=> array(
		array(
			'type' 		=> 'heading',
			'name' 		=> esc_html__( 'These settings enable you to embed audio in your posts. Note that for audio, you must supply both MP3 and OGG files to satisfy all browsers. For poster you can select a featured image.', 'nt-forester' ),
			'id'   		=> 'audio_head'
		),
		array(
			'name' 		=> esc_html__('MP3 File URL', 'nt-forester'),
			'desc' 		=> esc_html__('The URL to the .mp3 audio file.', 'nt-forester'),
			'id'   		=> 'nt_forester_audio_mp3',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('OGA File URL', 'nt-forester'),
			'desc' 		=> esc_html__('The URL to the .oga, .ogg audio file.', 'nt-forester'),
			'id'   		=> 'nt_forester_audio_ogg',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('Divider', 'nt-forester'),
			'desc' 		=> esc_html__('divider.', 'nt-forester'),
			'id'   		=> 'nt_forester_audio_divider',
			'type' 		=> 'divider'
		),
		array(
			'name' 		=> esc_html__('SoundCloud', 'nt-forester'),
			'desc' 		=> esc_html__('Enter the url of the soundcloud audio.', 'nt-forester'),
			'id'   		=> 'nt_forester_audio_sc',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('Color', 'nt-forester'),
			'desc' 		=> esc_html__('Choose the color.', 'nt-forester'),
			'id'   		=> 'nt_forester_audio_sc_color',
			'type' 		=> 'color',
			'std'  		=> '#ff7700'
		)
	)
);

//	VIDEO POST FORMAT
$meta_boxes[] = array(
	'title'    	=> esc_html__('Video Settings', 'nt-forester'),
	'pages'    	=> array('post'),
	'fields' 	=> array(
		array(
			'type' 		=> 'heading',
			'name' 		=> esc_html__( 'These settings enable you to embed videos into your posts. Note that for video, you must supply an M4V file to satisfy both HTML5 and Flash solutions. The optional OGV format is used to increase x-browser support for HTML5 browsers such as Firefox and Opera. For the poster, you can select a featured image.', 'nt-forester' ),
			'id'   		=> 'video_head'
		),
		array(
			'name' 		=> esc_html__('M4V File URL', 'nt-forester'),
			'desc' 		=> esc_html__('The URL to the .m4v video file.', 'nt-forester'),
			'id'   		=> 'nt_forester_video_m4v',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('OGV File URL', 'nt-forester'),
			'desc' 		=> esc_html__('The URL to the .ogv video file.', 'nt-forester'),
			'id'   		=> 'nt_forester_video_ogv',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('WEBM File URL', 'nt-forester'),
			'desc' 		=> esc_html__('The URL to the .webm video file.', 'nt-forester'),
			'id'   		=> 'nt_forester_video_webm',
			'type' 		=> 'text',
		),
		array(
			'name' 		=> esc_html__('Embeded Code', 'nt-forester'),
			'desc' 		=> esc_html__('Select the preview image for this video.', 'nt-forester'),
			'id'   		=> 'nt_forester_video_embed',
			'type' 		=> 'textarea',
			'rows' 		=> 8
		)
	)
);

	//end
	return $meta_boxes;
}

?>
