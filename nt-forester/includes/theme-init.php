<?php
/**
 *
 * @package WordPress
 * @subpackage nt_forester
 * @since nt_forester 1.0
 *
**/

/*************************************************
## Google Font
*************************************************/

if ( ! function_exists( 'nt_forester_fonts_url' ) ) :
function nt_forester_fonts_url() {
	$fonts_url = '';

	$montserrat 	= 	_x( 'on', 'Montserrat font: on or off', 	'nt-forester' );
	$poppins 		= 	_x( 'on', 'Poppins font: on or off', 		'nt-forester' );
	$nunito 		= 	_x( 'on', 'Nunito font: on or off', 		'nt-forester' );

	if ( 'off' !== $montserrat || 'off' !== $poppins || 'off' !== $nunito ) {
		$font_families = array();

		if ( 'off' !== $montserrat )
			$font_families[] = 'Montserrat:500,600,700,800';

		if ( 'off' !== $poppins )
			$font_families[] = 'Poppins:300,600';

		if ( 'off' !== $nunito )
			$font_families[] = 'Nunito:200,300,400';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}
endif;

/*************************************************
## Styles and Scripts
*************************************************/


function nt_forester_scripts() {

	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

	wp_enqueue_style( 'bootstrap', 						get_theme_file_uri() . '/css/bootstrap.min.css',false, '1.0');

	wp_register_style( 'nt-forester-plugins', 			get_theme_file_uri() . '/css/plugins.css',false, '1.0');
	wp_register_style( 'nt-forester-custom-theme-style', 	get_theme_file_uri() . '/css/style.css',false, '1.0');
	wp_register_style( 'nt-forester-primary-color', 		get_theme_file_uri() . '/css/colors/primary.css',false, '1.0');
	wp_register_style( 'ion-icon', 						get_theme_file_uri() . '/css/themify-stylesheet.css',false, '1.0');
	wp_register_style( 'themify', 						get_theme_file_uri() . '/css/ionicon-stylesheet.css',false, '1.0');
	wp_register_style( 'nt-forester-update-two', 			get_theme_file_uri() . '/css/update-two.css',false, '1.0');

	if( basename( get_page_template() ) === 'custom-page.php' ){
		// visual composer css for homepage
		wp_enqueue_style( 'nt-forester-plugins', 			get_theme_file_uri() . '/css/plugins.css',false, '1.0');
		wp_enqueue_style( 'nt-forester-custom-theme-style', get_theme_file_uri() . '/css/style.css',false, '1.0');
		wp_enqueue_style( 'nt-forester-primary-color', 	get_theme_file_uri() . '/css/colors/primary.css',false, '1.0');
		wp_enqueue_style( 'ion-icon', 					get_theme_file_uri() . '/css/themify-stylesheet.css',false, '1.0');
		wp_enqueue_style( 'themify', 					get_theme_file_uri() . '/css/ionicon-stylesheet.css',false, '1.0');
	}else{
		wp_enqueue_style( 'nt-forester-navbar', 			get_theme_file_uri() . '/css/blog-extra.css',false, '1.0');
	}

	wp_enqueue_style( 'font-awesome', 					get_theme_file_uri() . '/css/fontawesome.min.css',false, '1.0');
	wp_enqueue_style( 'IonIcons', 					get_theme_file_uri() . '/fonts/IonIcons/stylesheet.css',false, '1.0');
	wp_enqueue_style( 'ElegantIcons', 					get_theme_file_uri() . '/fonts/ElegantIcons/style.css',false, '1.0');
	wp_enqueue_style( 'PeaceSans', 					get_theme_file_uri() . '/fonts/PeaceSans/stylesheet.css',false, '1.0');
	wp_enqueue_style( 'IconsMind', 					get_theme_file_uri() . '/fonts/IconsMind/stylesheet.css',false, '1.0');

	// visual composer css for homepage
	wp_enqueue_style( 'nt-forester-vc', 					get_theme_file_uri() . '/css/framework-visual-composer.css',false, '1.0');
	// flexslider css file for blog post
	wp_enqueue_style( 'nt-forester-custom-flexslider', 	get_theme_file_uri() . '/js/flexslider/flexslider.css',false, '1.0');
	// wordpress css file for blog post
	wp_enqueue_style( 'nt-forester-wordpress',  			get_theme_file_uri() . '/css/framework-wordpress.css',false, '1.0');
	// update css file
	wp_enqueue_style( 'nt-forester-update',  				get_theme_file_uri() . '/css/framework-update.css',false, '1.0');
	// woocommerce
	if ( class_exists( 'woocommerce' ) ) {
	wp_enqueue_style( 'nt-forester-woocommerce',  		get_theme_file_uri() . '/css/framework-woocommerce.css',false, '1.0');
	wp_enqueue_style( 'nt-forester-woocommerce-update',  	get_theme_file_uri() . '/css/woo-update.css',false, '1.0');
	}
	if ( ot_get_option( 'nt_forester_pre_type' ) !='default' ) {
	// Loader archieve
	wp_enqueue_style( 'nt-forester-css-spin-loader',  	get_theme_file_uri() . '/css/framework-css-spin-loader.css',false, '1.0');
	}
	// theme default google webfont loader
	wp_enqueue_style( 'nt-forester-fonts-load',  			nt_forester_fonts_url(),array(), '1.0.0' );

	// custom css
	wp_enqueue_style('nt-forester-custom-style',  	  get_theme_file_uri() . '/css/framework-custom-style.css',false, '1.0');

	// default
	wp_enqueue_style( 'nt-forester-style',				  get_stylesheet_uri() );


	// default scripts for theme
	wp_enqueue_script('bootstrap', 				         get_theme_file_uri() .  '/js/bootstrap.min.js',array('jquery'), '1.0', true);
	wp_enqueue_script('parallaxen', 				         get_theme_file_uri() .  '/js/parallaxen.js',array('jquery'), '1.0', true);
	wp_enqueue_script('waypoints', 				         get_theme_file_uri() .  '/js/waypoints.js',array('jquery'), '1.0', true);
	wp_enqueue_script('scrollreveal', 				      get_theme_file_uri() .  '/js/scrollreveal.js',array('jquery'), '1.0', true);
	wp_enqueue_script('easing', 				            get_theme_file_uri() .  '/js/easing.js',array('jquery'), '1.0', true);
	wp_enqueue_script('yt-player', 				         get_theme_file_uri() .  '/js/yt-player.js',array('jquery'), '1.0', true);
	wp_enqueue_script('counterup', 				         get_theme_file_uri() .  '/js/counterup.js',array('jquery'), '1.0', true);
	wp_enqueue_script('owl-carousel', 				      get_theme_file_uri() .  '/js/owl-carousel.js',array('jquery'), '1.0', true);
	wp_enqueue_script('cube-portfolio', 				   get_theme_file_uri() .  '/js/cube-portfolio.js',array('jquery'), '1.0', true);
	wp_register_script('nt-forester-portfolio-short', 	get_theme_file_uri() .  '/js/portfolio-short.js',array('jquery'), '1.0', true);
	wp_register_script('nt-forester-blog-short', 		get_theme_file_uri() .  '/js/blog-short.js',array('jquery'), '1.0', true);
	wp_enqueue_script('nt-forester-custom-scripts', 	get_theme_file_uri() .  '/js/scripts.js',array('jquery'), '1.0', true);
   if ( ot_get_option( 'nt_forester_smoothscroll' ) =='on' ) {
   wp_enqueue_script('smoothscroll', 		            get_theme_file_uri() .  '/js/smoothscroll.js',array('jquery'), '1.0', true);
   }
	// WP default scripts for theme
	wp_enqueue_script('nt-forester-custom-flexslider', 	get_theme_file_uri() .  '/js/framework-jquery.flexslider.js',array('jquery'), '1.0', true);
	wp_enqueue_script('fitvids', 	 					     get_theme_file_uri() .  '/js/fitvids.js',array('jquery'), '1.0', true);
	wp_enqueue_script('nt-forester-blog-settings', 		get_theme_file_uri() .  '/js/framework-blog-settings.js',array('jquery'), '1.0', true);
wp_enqueue_script('comment-reply');
	wp_enqueue_script( 'modernizr', 					    get_theme_file_uri()  .  '/js/modernizr.min.js',array('jquery'), '2.7.1', false );
	wp_script_add_data('modernizr', 'conditional', 'lt IE 9' );

   wp_enqueue_script( 'respond', 						   get_theme_file_uri()  .  '/js/respond.min.js',array('jquery'), '1.4.2', false );
	wp_script_add_data('respond', 'conditional', 'lt IE 9' );

}

add_action( 'wp_enqueue_scripts', 'nt_forester_scripts' );


/*************************************************
## Admin style and scripts
*************************************************/

function nt_forester_admin_style() {

	// Update CSS within in Admin
	wp_enqueue_style( 'nt-forester-custom-admin', 		get_theme_file_uri().'/css/framework-admin.css');
	wp_enqueue_script('nt-forester-custom-admin',			get_theme_file_uri() . '/js/framework-custom.admin.js');

}
add_action('admin_enqueue_scripts', 'nt_forester_admin_style');


/*************************************************
## Theme option & Metaboxes & shortcodes
*************************************************/

	// theme homepage visual composer shortcodes settings
	if(function_exists('vc_set_as_theme')) {
		require_once get_parent_theme_file_path() . '/includes/visual-shortcodes.php';
	}

	// Metabox plugin check
	if ( ! function_exists( 'rwmb_meta' ) ) {
		function rwmb_meta( $key, $args = '', $post_id = null ) {
			return false;
		}
	}
	// Theme post and page meta plugin for customization and more features
	require_once get_parent_theme_file_path() . '/includes/page-metaboxes.php';

	// Theme css setting file
	require_once get_parent_theme_file_path() . '/includes/custom-style.php';

	// Theme navigation and pagination options
	require_once get_parent_theme_file_path() . '/includes/template-tags.php';

	// Theme parts
	require_once get_parent_theme_file_path() . '/includes/template-parts.php';

	// TGM plugin activation
	require_once get_parent_theme_file_path() . '/includes/tgm.php';

	// image resizer
	require_once get_parent_theme_file_path() . '/includes/aq_resizer.php';

	// Option tree controllers
	if ( ! class_exists( 'OT_Loader' )){

		function ot_get_option() {
			return false;
		}

	}

	// add filter for  options panel loader
	add_filter( 'ot_show_pages', 		'__return_false' );
	add_filter( 'ot_show_new_layout', 	'__return_false' );

	// Theme options admin panel setings file
	require_once get_parent_theme_file_path() . '/includes/theme-options.php';

	// Theme customize css setting file
	require_once get_parent_theme_file_path() . '/includes/custom-style.php';



/*************************************************
## Theme Setup
*************************************************/

if ( ! isset( $content_width ) ) $content_width = 960;

function nt_forester_theme_setup() {

	/*
	* This theme styles the visual editor to resemble the theme style,
	* specifically font, colors, icons, and column width.
	*/
	add_editor_style ( 'custom-editor-style.css' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	* Enable support for Post Thumbnails on posts and pages.
	*
	* See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	*/
	add_theme_support( 'post-thumbnails' );

   // woocommerce
	if ( class_exists( 'woocommerce' ) ) {
		add_theme_support( 'woocommerce' );
      add_theme_support( 'wc-product-gallery-zoom' );
      add_theme_support( 'wc-product-gallery-lightbox' );
      add_theme_support( 'wc-product-gallery-slider' );
	}
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'html5', array( 'search-form' ) );

	/*
	* Enable support for Post Formats.
	*
	* See: https://codex.wordpress.org/Post_Formats
	*/
	add_theme_support( 'post-formats', array('gallery', 'quote', 'video', 'audio'));

	// add_post_type_support( 'portfolio', 'post-formats' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'nt-forester', get_parent_theme_file_path() . '/languages' );

	register_nav_menus( array(
		'primary' 			=> 	esc_html__( 'Primary Menu', 'nt-forester' ),
	) );

}
add_action( 'after_setup_theme', 'nt_forester_theme_setup' );


remove_filter( 'the_excerpt', 'wpautop' );


/*************************************************
## HTML5 SEARCH FORM
*************************************************/

function nt_forester_custom_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . esc_url( home_url( '/' ) ) . '" >
    <div>
		<input type="text" value="' . get_search_query() . '" placeholder="'. esc_attr__( 'Search', 'nt-forester' ) .'" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search','nt-forester' ) .'" />
    </div>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'nt_forester_custom_search_form' );

 function nt_forester_body_theme_class_names($classes) {

  $theme_name =  'ninetheme-theme-name-' . wp_get_theme();
  $theme_version =  'theme-version-' . wp_get_theme()->get( 'Version' );

  $classes[] =  $theme_name;
  $classes[] =  $theme_version;

  return $classes;
 }
add_filter('body_class','nt_forester_body_theme_class_names');


/*************************************************
## Widget columns
*************************************************/

function nt_forester_widgets_init() {
	register_sidebar( array(
		'name' 			=> esc_html__( 'Blog Sidebar', 'nt-forester' ),
		'id' 			=> 'sidebar-1',
		'description'   => esc_html__( 'These are widgets for the Blog page.','nt-forester' ),
		'before_widget' => '<div class="widget  %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>'
	) );
	register_sidebar( array(
		'name' 			=> esc_html__( 'Woo Single Sidebar', 'nt-forester' ),
		'id' 			=> 'shop-single-page-sidebar',
		'description'   => esc_html__( 'These are widgets for the Blog page.','nt-forester' ),
		'before_widget' => '<div class="widget  %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>'
	) );
	register_sidebar( array(
		'name' 			=> esc_html__( 'Woo Shop Page Sidebar', 'nt-forester' ),
		'id' 			=> 'shop-page-sidebar',
		'description'   => esc_html__( 'These are widgets for the Blog page.','nt-forester' ),
		'before_widget' => '<div class="widget  %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>'
	) );
	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer widgetize area', 'nt-forester' ),
		'id' 			=> 'nt-forester-footer-widgetize',
		'description'   => esc_html__( 'Theme footer widgetize area','nt-forester' ),
		'before_widget' => '<div class="col-md-3"><div class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3 class="widget-head">',
		'after_title'   => '</h3>'
	) );
	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer', 'nt-forester' ),
		'id' 			=> 'nt-forester-footer',
		'description'   => esc_html__( 'Theme footer default area','nt-forester' ),
		'before_widget' => '<div class="col-md-3"><div class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3 class="widget-head">',
		'after_title'   => '</h3>'
	) );
	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer widget area 1', 'nt-forester' ),
		'id' 			=> 'nt_forester_footer_widget1',
		'description'   => esc_html__( 'Theme footer widget area first column.','nt-forester' ),
		'before_widget' => '<div class="col-md-3"><div class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3 class="widget-head">',
		'after_title'   => '</h3>'
	) );
	register_sidebar( array(
		'name' 			=> esc_html__( 'Footer widget area 2', 'nt-forester' ),
		'id' 			=> 'nt_forester_footer_widget2',
		'description'   => esc_html__( 'Theme footer widget area second column.','nt-forester' ),
		'before_widget' => '<div class="col-md-3"><div class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3 class="widget-head">',
		'after_title'   => '</h3>'
	) );

}
add_action( 'widgets_init', 'nt_forester_widgets_init' );

/*************************************************
## Include the TGM_Plugin_Activation class.
*************************************************/

function nt_forester_register_required_plugins() {

    $plugins = array(
		array(
            'name'         => esc_html__('Visual CSS Style Editor', "nt-forester"),
            'slug'         => 'yellow-pencil-visual-theme-customizer',
        ),
		array(
            'name'         	=> esc_html__('Breadcrumb NavXT', "nt-forester"),
            'slug'         	=> 'breadcrumb-navxt',
        ),
        array(
            'name'         	=> esc_html__('Contact Form 7', "nt-forester"),
            'slug'         	=> 'contact-form-7',
        ),
        array(
            'name'         	=> esc_html__('Meta Box', "nt-forester"),
            'slug'         	=> 'meta-box',
			'required'     	=> true,
        ),
		array(
            'name'         	=> esc_html__('Option Tree', "nt-forester"),
            'slug'         	=> 'option-tree',
			'required'     	=> true,
        ),
		array(
            'name'         	=> esc_html__('Custom Post Type UI', "nt-forester"),
            'slug'         	=> 'custom-post-type-ui',
			'required'     	=> true,
        ),
		array(
            'name'         	=> esc_html__('Metabox Show/Hide', "nt-forester"),
            'slug'         	=> 'meta-box-show-hide',
			'source'       	=> get_parent_theme_file_path() . '/plugins/meta-box-show-hide.zip',
            'required'     	=> true,
        ),
		array(
            'name'         	=> esc_html__('Metabox Tabs', "nt-forester"),
            'slug'         	=> 'meta-box-tabs',
			'source'       	=> get_parent_theme_file_path() . '/plugins/meta-box-tabs.zip',
            'required'     	=> true,
        ),
		array(
			'name'		   	=> esc_html__('Envato Auto Update Theme', "nt-forester"),
			'slug'		   	=> 'envato-market-update-theme',
			'source'       	=> get_parent_theme_file_path() . '/plugins/envato-market-update-theme.zip',
			'required'	   	=> false,
		),
		array(
			'name'         	=> esc_html__('Visual Composer', "nt-forester"),
			'slug'         	=> 'visual_composer',
			'source'        => get_parent_theme_file_path() . '/plugins/visual_composer.zip',
			'required'     	=> true,
		),
      array(
            'name'         => esc_html__('OneClick Demodata Installer', "nt-forester"),
            'slug'         => 'easy_installer',
            'source'       => get_template_directory() . '/plugins/easy_installer.zip',
            'required'     => false,
      ),
		array(
			'name'         	=> esc_html__('Revolution Slider', "nt-forester"),
			'slug'         	=> 'revolution_slider',
			'source'        => get_parent_theme_file_path() . '/plugins/revolution_slider.zip',
			'required'     	=> false,
		),
		array(
            'name'         => esc_html__('NT Forester Shortcodes', "nt-forester"),
            'slug'         => 'nt-forester-shortcodes',
            'source'       => get_parent_theme_file_path() . '/plugins/nt-forester-shortcodes.zip',
            'required'     => true,
            'version'      => '1.1.1',
        ),
    );

	$config = array(
		'id'           	   => 'tgmpa',
		'default_path' 	   => '',
		'menu'         	   => 'tgmpa-install-plugins',
		'has_notices'  	   => true,
		'dismissable'  	   => true,
		'dismiss_msg'  	   => '',
		'is_automatic' 	   => true,
		'message'     	   => '',
	);

	tgmpa( $plugins, $config );
}

add_action( 'tgmpa_register', 'nt_forester_register_required_plugins' );


/*************************************************
## Register Menu
*************************************************/

class nt_forester_Wp_Bootstrap_Navwalker extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul role=\"menu\" class=\"dropdown-menu\">\n";
	}

	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		/**
		 * Dividers, Headers or Disabled
		 */
		if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider item-has-children">';
		} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider item-has-children">';
		} else if ( strcasecmp( $item->attr_title, 'dropdown-header item-has-children') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="dropdown-header item-has-children">' . esc_attr( $item->title );
		} else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
			$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
		} else {

			$class_names = $value = '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;

			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );


			if($args->has_children && $depth === 0) {
				$class_names .= ' sub item-has-children';
			}elseif($args->has_children && $depth > 0) {
				$class_names .= ' dropdown-submenu';
			}
			if ( in_array( 'current-menu-item', $classes ) )
				$class_names .= ' active';

			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names .'>';

			$atts = array();
			$atts['title']  = ! empty( $item->title )	? $item->title	: '';
			$atts['target'] = ! empty( $item->target )	? $item->target	: '';
			$atts['rel']    = ! empty( $item->xfn )		? $item->xfn	: '';

			// If item has_children add atts to a.
			if ( $args->has_children && $depth === 0 ) {
				$atts['href']   		= $item->url;
				$atts['data-toggle']	= 'dropdown';
				$atts['class']			= 'dropdown-toggle';
			} else {
				$atts['href'] = ! empty( $item->url ) ? $item->url : '';
				$atts['data-scroll']	= ' ';
			}

			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}

			$item_output = $args->before;

			/*
			 * Glyphicons
			 **/
			if ( ! empty( $item->attr_title ) )
				$item_output .= '<a'. $attributes .'><span class=" ' . esc_attr( $item->attr_title ) . '"></span>&nbsp;';
			else
				$item_output .= '<a '. $attributes .'>';

			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret fa fa-angle-down no-border"></span></a>' : '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}

	/**
	 * Traverse elements to create list from elements.
	 **/
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;

        $id_field = $this->db_fields['id'];

        // Display this element.
        if ( is_object( $args[0] ) )
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

	/**
	 * Menu Fallback
	 **/
	public static function fallback( $args ) {
		if ( current_user_can( 'manage_options' ) ) {

			extract( $args );

			$fb_output = null;

			if ( $container ) {
				$fb_output = '<' . $container;

				if ( $container_id )
					$fb_output .= ' id="' . $container_id . '"';

				if ( $container_class )
					$fb_output .= ' class="' . $container_class . '"';

				$fb_output .= '>';
			}

			$fb_output .= '<ul';

			if ( $menu_id )
				$fb_output .= ' id="' . $menu_id . '"';

			if ( $menu_class )
				$fb_output .= ' class="' . $menu_class . '"';

			$fb_output .= '>';
			$fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">' . esc_html__('Add a menu','nt-forester') .'</a></li>';
			$fb_output .= '</ul>';

			if ( $container )
				$fb_output .= '</' . $container . '>';

			echo ($fb_output);
		}
	}
}
