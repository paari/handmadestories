<?php

	get_header();
	get_template_part('index-header');

	$nt_forester_archive_hero_display 	= 	ot_get_option( 'nt_forester_archive_hero_display' );
	$nt_forester_archive_heading_display 	= 	ot_get_option( 'nt_forester_archive_heading_display' );
	$nt_forester_archive_heading 			= 	ot_get_option( 'nt_forester_archive_heading' );
	$nt_forester_archive_slogan_display 	= 	ot_get_option( 'nt_forester_archive_slogan_display' );
	$nt_forester_archive_slogan 			= 	ot_get_option( 'nt_forester_archive_slogan' );
	$nt_forester_bread_display  			= 	ot_get_option( 'nt_forester_bread_display' );
	$nt_forester_archive_layout 			= 	ot_get_option( 'nt_forester_archive_layout' );
	$nt_forester_archivepageheadbg 		= 	ot_get_option( 'nt_forester_archive_headbg' );
	$nt_forester_archivepageheadbg		= 	( $nt_forester_archivepageheadbg != '' ) ? $nt_forester_archivepageheadbg : get_theme_file_uri() . '/images/full_1.jpg';

	wp_enqueue_style( 'nt-forester-custom-theme-style' );
	wp_enqueue_style( 'nt-forester-primary-color' );
	wp_enqueue_style( 'themify' );
	wp_enqueue_style( 'nt-forester-update-two' );

if ( $nt_forester_archive_hero_display != 'off' ) :
?>

	<!-- Start Hero Section -->
	<section id="hero" class="hero-fullwidth parallax">
		<?php if( $nt_forester_archivepageheadbg != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_archivepageheadbg ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container white">
				<div class="m-auto">
					<div class="title-service mb15">

						<?php if( $nt_forester_archive_heading_display != 'off' ) : // PAGE TITLE
							if( $nt_forester_archive_heading != '' ) : ?>
								<h1 class="white uppercase lead-heading"><?php echo esc_html( $nt_forester_archive_heading ); ?></h1>
							<?php else : ?>
								<h1 class="white uppercase lead-heading"><?php the_archive_title(); ?></h1>
							<?php endif;
						endif;

						if( $nt_forester_archive_slogan_display != 'off' ) : // SUBTITLE
							if( $nt_forester_archive_slogan != '' ) : ?>
								<p class="heading-title cover-text-sublead"><?php echo esc_html( $nt_forester_archive_slogan ); ?></p>
							<?php else : ?>
								<p class="heading-title cover-text-sublead"><?php echo esc_html_e( 'Welcome to Our Archive','nt-forester' );?></p>
							<?php endif;
						endif;

						if( $nt_forester_bread_display != 'off' ) : //BREADCRUMB
							if( function_exists( 'bcn_display' ) ) : ?>
								<p class="breadcrubms"><?php bcn_display(); ?></p>
							<?php endif;
						endif; ?>

					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->
<?php endif; ?>

	<section id="blog-post" class="bg-white blog-modern-sidebar-off">
		<div class="container pt150 pb100">
			<?php
				if ( $nt_forester_archive_hero_display == 'off' ) :

					if ( $nt_forester_archive_slogan_display != 'off' ) :
						if ( $nt_forester_archive_slogan != '') : ?>
							<p class="cover-text-sublead text-center heading-title"><?php echo esc_html( $nt_forester_archive_slogan ); ?></p>
						<?php else: ?>
							<p class="cover-text-sublead text-center heading-title"><?php echo esc_html_e( 'Welcome to Our Archive','nt-forester' );?></p>
						<?php endif;
					endif;

					if ( $nt_forester_archive_heading_display != 'off') :
						if ( $nt_forester_archive_heading != '' ) : ?>
							<h1 class="black text-center pb60"><?php echo esc_html( $nt_forester_archive_heading ); ?></h1>
						<?php else: ?>
							<h1 class="black text-center pb60"><?php the_archive_title(); ?></h1>
					<?php
						endif;
					endif;
				endif;
			?>

			<div class="row">

				<!-- right sidebar -->
				<?php if( ( $nt_forester_archive_layout ) == 'right-sidebar' || ( $nt_forester_archive_layout ) == '') { ?>
				<div class="col-lg-9 col-md-9 col-sm-12 text-center">

				<!-- left sidebar -->
				<?php } elseif( ( $nt_forester_archive_layout ) == 'left-sidebar') { ?>
				<?php get_sidebar(); ?>
				<div class="col-lg-9 col-md-9 col-sm-12 text-center">

				<!-- no sidebar -->
				<?php } elseif( ( $nt_forester_archive_layout ) == 'full-width') { ?>
				<div class="col-md-12 text-center">
				<?php }

					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							get_template_part( 'post-format/content', get_post_format() );
						endwhile;
					else :
						get_template_part( 'content', 'none' );
					endif;


					the_posts_pagination( array(
					'prev_text'          => esc_html__( 'Next', 'nt-forester' ),
					'next_text'          => esc_html__( 'Prev', 'nt-forester' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
					) );
				?>
				</div><!-- #end sidebar+ content -->

				<!-- right sidebar -->
				<?php if( ( $nt_forester_archive_layout ) == 'right-sidebar' || ( $nt_forester_archive_layout ) == '') {
					get_sidebar();
				} ?>

			</div>
		</div>
	</section>

<?php get_footer(); ?>
