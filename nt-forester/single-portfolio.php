<?php

	$nt_forester_port_linktype = get_post_meta( get_queried_object_id(), 'nt_forester_port_linktype', true );

	if ( $nt_forester_port_linktype == 'single-popup' ):

		while ( have_posts() ) : the_post();

			get_template_part( 'post-format/portfolio/content-cbp-popup', get_post_format() );

		endwhile; // end of the loop.

	else :

		get_header();
		get_template_part('index-header');

		wp_enqueue_style( 'nt-forester-plugins' );
		wp_enqueue_style( 'nt-forester-custom-theme-style' );
		wp_enqueue_style( 'nt-forester-primary-color' );
		wp_enqueue_style( 'themify' );
		wp_enqueue_style( 'nt-forester-update-two' );

		do_action('nt_forester_single_port_header_action');

		while ( have_posts() ) : the_post();

			the_content();

		endwhile; // end of the loop.

	// do_action('nt_forester_backtop_action');
	get_footer();

	endif;

?>
