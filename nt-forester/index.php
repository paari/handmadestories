<?php

	get_header();
	get_template_part('index-header');

	$nt_forester_blog_hero_display 	= 	 ot_get_option( 'nt_forester_blog_hero_display' );
	$nt_forester_blog_style 			= 	 ot_get_option( 'nt_forester_blog_style' );
	$nt_forester_blog_masonrycolumn 	= 	 ot_get_option( 'nt_forester_blog_masonrycolumn' );
	$nt_forester_blog_masonry_column 	= 	( $nt_forester_blog_masonrycolumn != '' ) ? $nt_forester_blog_masonrycolumn : 2;
	$nt_forester_pagelayout 			= 	ot_get_option( 'nt_forester_bloglayout' );
	$nt_forester_blog_heading_display = 	ot_get_option( 'nt_forester_blog_heading_display' );
	$nt_forester_blog_heading 		= 	ot_get_option( 'nt_forester_blog_heading' );
	$nt_forester_blog_masonry_filter_display 		= 	ot_get_option( 'nt_forester_blog_masonry_filter_display' );

	$nt_forester_blog_slogan_display 	= 	ot_get_option( 'nt_forester_blog_slogan_display' );
	$nt_forester_blog_slogan 			= 	ot_get_option( 'nt_forester_blog_slogan' );
	$nt_forester_bread_visibility 	= 	ot_get_option( 'nt_forester_bread' );

	if ( $nt_forester_blog_hero_display != 'off' ) :

		do_action( 'nt_forester_blog_hero_action' );

	endif;

	if ( $nt_forester_blog_style == 'masonry' || $nt_forester_blog_style == 'masonry-sidebar' ) :

		wp_enqueue_style( 'nt-forester-plugins' );
		wp_enqueue_style( 'nt-forester-custom-theme-style' );
		wp_enqueue_style( 'nt-forester-primary-color' );
		wp_enqueue_style( 'ion-icon');
		wp_enqueue_style( 'themify' );
		wp_enqueue_style( 'nt-forester-update-two' );

	endif;


	if ( $nt_forester_blog_style == 'masonry' ) :

	$nt_forester_masonry_layoutmode 				= 	ot_get_option( 'nt_forester_masonry_layoutmode' );
	$nt_forester_masonry_gridadjustment 			= 	ot_get_option( 'nt_forester_masonry_gridadjustment' );
	$nt_forester_masonry_animationfilter 			= 	ot_get_option( 'nt_forester_masonry_animationfilter' );
	$nt_forester_masonry_itemhorizontalgap		= 	ot_get_option( 'nt_forester_masonry_itemhorizontalgap' );
	$nt_forester_masonry_itemverticalgap 			= 	ot_get_option( 'nt_forester_masonry_itemverticalgap' );
	$nt_forester_blog_masonry_showcounter 		= 	ot_get_option( 'nt_forester_blog_masonry_showcounter' );
	$nt_forester_blog_masonry_container_style		= 	ot_get_option( 'nt_forester_blog_masonry_container_style' );
	$nt_forester_blog_masonry_slidernavdisplay	= 	ot_get_option( 'nt_forester_blog_masonry_slider_navdisplay' );

	$nt_forester_masonrylayoutmode 		= ( $nt_forester_masonry_layoutmode == 'custom' ) ? 'grid' : $nt_forester_masonry_layoutmode;
	$nt_forester_masonrygridadjustment 	= ( $nt_forester_masonry_gridadjustment != '' ) ? $nt_forester_masonry_gridadjustment : 'responsive';
	$nt_forester_masonry_animationfilter 	= ( $nt_forester_masonry_animationfilter != '' ) ? $nt_forester_masonry_animationfilter : 'sequentially';
	$nt_forester_masonryitemhorizontalgap = ( $nt_forester_masonry_itemhorizontalgap != '' ) ? $nt_forester_masonry_itemhorizontalgap : 30;
	$nt_forester_masonryitemverticalgap 	= ( $nt_forester_masonry_itemverticalgap != '' ) ? $nt_forester_masonry_itemverticalgap : 30;

	wp_enqueue_script('nt-forester-blog-short');
	wp_localize_script('nt-forester-blog-short', 'prefix',
	array(
		'layoutmode' 		=> $nt_forester_masonrylayoutmode,//'grid', 'mosaic' or 'slider'

		'gridadjustment' 	=> $nt_forester_masonrygridadjustment,	//'default''alignCenter''responsive'

		'animationfilter' 	=> $nt_forester_masonry_animationfilter,
		/* 'fadeOut''quicksand''bounceLeft''bounceTop''bounceBottom''moveLeft''slideLeft''fadeOutTop''sequentially''skew''slideDelay''3d' Flip'rotateSides''flipOutDelay''flipOut''unfold''foldLeft''scaleDown''scaleSides''frontRow''flipBottom''rotateRoom'  */

		'itemhorizontalgap' => $nt_forester_masonryitemhorizontalgap,
		'itemverticalgap' 	=> $nt_forester_masonryitemverticalgap,

	));
	$nt_forester_blog_masonry_container_style = ( $nt_forester_blog_masonry_container_style == 'boxed' ) ? 'container' : 'fullwidth-style';

	$nt_forester_slidernavdisplay  = ( $nt_forester_blog_masonry_slidernavdisplay != 'off' ) ? ' mt-60 nav-show' : ' nav-hide';
	$nt_forester_sliderclass = ( $nt_forester_masonrylayoutmode == 'slider' ) ? $nt_forester_slidernavdisplay : '';
?>

        <section id="blog" class="bg-white masonry-post">

            <div class="<?php echo esc_html( $nt_forester_blog_masonry_container_style ); ?> pt150 pb130">
			<?php

				if ( $nt_forester_blog_hero_display == 'off' ) :

					if ( $nt_forester_blog_slogan_display != 'off' ) :
						if ( $nt_forester_blog_slogan != '') : ?>
							<p class="cover-text-sublead text-center heading-title"><?php echo esc_html( $nt_forester_blog_slogan ); ?></p>
						<?php else: ?>
							<p class="cover-text-sublead text-center heading-title"><?php echo bloginfo('description'); ?></p>
						<?php endif;
					endif;

					if ( $nt_forester_blog_heading_display != 'off') :
						if ( $nt_forester_blog_heading != '' ) : ?>
							<h1 class="black text-center pb60"><?php echo esc_html( $nt_forester_blog_heading ); ?></h1>
						<?php else: ?>
							<h1 class="black text-center pb60"><?php echo esc_html_e( 'Blog', 'nt-forester' ); ?></h1>
					<?php
						endif;
					endif;
				endif;


				if ( $nt_forester_masonry_layoutmode != 'slider' ) :
					if ( $nt_forester_blog_masonry_filter_display != 'off' ) : ?>
						<div class="posts-filter cbp-l-filters-button text-center">

							<?php
								$nt_forester_blog_showfilter = esc_attr( ot_get_option( 'nt_forester_blog_showfilter' ) );

								$terms = get_categories();
								$count = count($terms);

								if ( $nt_forester_blog_showfilter != 'hide' ){

								if ( $nt_forester_blog_masonry_showcounter != 'off' ){ $filtercounter = '<div class="cbp-filter-counter"></div>';}else{$filtercounter = '';}
									if ( $count > 0 ){ ?>

										<div class="posts-filter cbp-l-filters-button text-center">
											<div data-filter="*" class="cbp-filter-active cbp-filter-item"><?php echo esc_html_e( 'All', 'nt-forester' ); ?><?php echo ( $filtercounter); ?></div>

									<?php
										foreach ( $terms as $term ) {
											$termname = strtolower($term->name);
											$termslug = strtolower($term->slug);
											$term_slug = str_replace(' ', '-', $termslug);
									?>
											<div data-filter=".<?php echo esc_html( $term_slug ); ?>" class="cbp-filter-item"><?php echo esc_html( $term->name ); ?><?php echo ( $filtercounter); ?></div>
									<?php } ?>
										</div>
								<?php }
								}
							?>

						</div>
					<?php endif;
				endif;?>

                <div id="blogtwo-boxed-col-<?php echo esc_html( $nt_forester_blog_masonry_column ); ?>" class="cbp<?php echo esc_html( $nt_forester_sliderclass ); ?>">

					<?php

						if ( have_posts() ) :
							while ( have_posts() ) : the_post();
								get_template_part( 'post-format/masonry/content', get_post_format() );
							endwhile;
						else :
							get_template_part( 'content', 'none' );
						endif;
					?>

                </div>
					<?php

						the_posts_pagination( array(
   					'prev_text'          => esc_html__( 'Next', 'nt-forester' ),
   					'next_text'          => esc_html__( 'Prev', 'nt-forester' ),
						) );
					?>
            </div>
        </section>

<?php elseif ( $nt_forester_blog_style == 'masonry-sidebar' ) : ?>

        <section id="blog-post" class="bg-white blog-modern-sidebar">
            <div class="container pt150 pb100">
				<?php
					if ( $nt_forester_blog_hero_display == 'off' ) :

						if ( $nt_forester_blog_slogan_display != 'off' ) :
							if ( $nt_forester_blog_slogan != '') : ?>
								<p class="cover-text-sublead text-center heading-title"><?php echo esc_html( $nt_forester_blog_slogan ); ?></p>
							<?php else: ?>
								<p class="cover-text-sublead text-center heading-title"><?php echo bloginfo('description'); ?></p>
							<?php endif;
						endif;

						if ( $nt_forester_blog_heading_display != 'off') :
							if ( $nt_forester_blog_heading != '' ) : ?>
								<h1 class="black text-center pb60"><?php echo esc_html( $nt_forester_blog_heading ); ?></h1>
							<?php else: ?>
								<h1 class="black text-center pb60"><?php echo esc_html_e( 'Blog', 'nt-forester' ); ?></h1>
						<?php
							endif;
						endif;
					endif;
				?>

                <div class="row">

					<!-- right sidebar -->
					<?php if( ( $nt_forester_pagelayout ) == 'right-sidebar' || ( $nt_forester_pagelayout ) == '') { ?>
					<div class="col-lg-9 col-md-9 col-sm-12 text-center">

					<!-- left sidebar -->
					<?php } elseif( ( $nt_forester_pagelayout ) == 'left-sidebar') { ?>
					<?php get_sidebar(); ?>
					<div class="col-lg-9 col-md-9 col-sm-12 text-center">

					<!-- no sidebar -->
					<?php } elseif( ( $nt_forester_pagelayout ) == 'full-width') { ?>
					<div class="col-md-12 text-center">
					<?php }

						if ( have_posts() ) :
							while ( have_posts() ) : the_post();
								get_template_part( 'post-format/masonry/content', get_post_format() );
							endwhile;
						else :
							get_template_part( 'content', 'none' );
						endif;


						the_posts_pagination( array(
   					'prev_text'          => esc_html__( 'Next', 'nt-forester' ),
   					'next_text'          => esc_html__( 'Prev', 'nt-forester' ),
						) );
					?>
				</div><!-- #end sidebar+ content -->

				<!-- right sidebar -->
				<?php if( ( $nt_forester_pagelayout ) == 'right-sidebar' || ( $nt_forester_pagelayout ) == '') {
					get_sidebar();
				} ?>

			</div>
		</div>
	</section>

<?php else : ?>

	<section id="blog" class="blog-classic">
		<div class="container has-margin-bottom">
			<div class="row">

				<?php if( ( $nt_forester_pagelayout ) =='right-sidebar' || ($nt_forester_pagelayout ) =='' ){ ?>
				<div class="col-lg-9  col-md-9 col-sm-12 index float-right posts">
				<?php } elseif(( $nt_forester_pagelayout ) == 'left-sidebar') {
					get_sidebar(); ?>
				<div class="col-lg-9  col-md-9 col-sm-12 index float-left posts">
				<?php } elseif(( $nt_forester_pagelayout ) == 'full-width') { ?>
				<div class="col-xs-12 full-width-index v">
				<?php }

					if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						 get_template_part( 'post-format/content', get_post_format() );
					endwhile;

					 the_posts_pagination( array(
							'prev_text'          => esc_html__( 'Previous page', 'nt-forester' ),
							'next_text'          => esc_html__( 'Next page', 'nt-forester' ),
							'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
						) );

					 else :

					 get_template_part( 'content', 'none' );

					 endif;
				?>

				</div>

				<?php

					if( ( $nt_forester_pagelayout ) =='right-sidebar' || ($nt_forester_pagelayout ) =='' ){
						 get_sidebar();
					}
				?>

			</div>
		</div>
	</section>

<?php endif;

get_footer(); ?>
