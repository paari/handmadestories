<?php

	$nt_forester_bread_display			= 	ot_get_option( 'nt_forester_bread_display' );
	$nt_forester_single_heading_display 	= 	ot_get_option( 'nt_forester_single_heading_display' );
	$nt_forester_singlepageheadbg 		= 	ot_get_option( 'nt_forester_single_headbg' );
	$nt_forester_singlepageheadbg	 		= 	( $nt_forester_singlepageheadbg != '' ) ? $nt_forester_singlepageheadbg : get_theme_file_uri() . '/images/full_1.jpg';

	$nt_forester_post_layout = ot_get_option( 'nt_forester_post_layout' );

?>

	<!-- Start Hero Section -->
	<section id="hero" class="hero-fullwidth parallax">
		<?php if( $nt_forester_singlepageheadbg != '' ) : ?>
			<div class="background-image overlay">
				<img src="<?php echo esc_url( $nt_forester_singlepageheadbg ); ?>" alt="<?php echo the_title(); ?>">
			</div>
		<?php endif; ?>

		<div class="hero-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="m-auto">
							<div class="title-service mb15">

								<?php if ( ( $nt_forester_single_heading_display  ) != 'off') : ?>
									<h1 class="white pb60"><?php the_title(); ?></h1>
								<?php endif; ?>

								<?php if( $nt_forester_bread_display != 'off' ) : ?>
									<?php if( function_exists( 'bcn_display' ) ) : ?>
										<p class="breadcrubms"> <?php bcn_display(); ?></p>
									<?php endif; ?>
								<?php endif; ?>

							</div>

						</div>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.hero-content -->
	</section>
	<!-- End Hero Section -->


	<section id="blog" class="page-internal-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12-off has-margin-bottom-off">

				<?php if( ot_get_option( 'woosingle' ) == 'right-sidebar' || ot_get_option( 'woosingle' ) == '') { ?>
					<div class="col-lg-9  col-md-9 col-sm-9 index float-right posts">
				<?php } elseif( ot_get_option( 'woosingle' ) == 'left-sidebar') { ?>

					<div id="widget-area" class="widget-area col-lg-3 col-md-3 col-sm-3 woo-sidebar">
                  <?php  
                     if (  is_active_sidebar( 'shop-single-page-sidebar' )  ) :
   					      dynamic_sidebar( 'shop-single-page-sidebar' );
                     endif;
                  ?>
					</div>
					<div class="col-lg-9  col-md-9 col-sm-9 index float-left posts">

				<?php } elseif( ot_get_option( 'woosingle' ) == 'full-width') { ?>
					<div class="col-xs-12 full-width-index v">
				<?php } ?>

					<?php woocommerce_content(); ?>

			   </div><!-- #end sidebar+ content -->

				<?php if( ot_get_option( 'woosingle' ) == 'right-sidebar' || ot_get_option( 'woosingle' ) == '') { ?>
					<div id="widget-area" class="widget-area col-lg-3 col-md-3 col-sm-3 woo-sidebar">
                  <?php
                     if (  is_active_sidebar( 'shop-single-page-sidebar' )  ) :
   					      dynamic_sidebar( 'shop-single-page-sidebar' );
                     endif;
                  ?>
					</div>
				<?php } ?>

				</div>
			</div>
		</div>
	</section>

	<?php get_footer(); ?>
